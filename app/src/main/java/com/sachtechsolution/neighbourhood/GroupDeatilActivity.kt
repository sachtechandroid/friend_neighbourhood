package com.sachtechsolution.neighbourhood

import android.os.Bundle
import android.os.Handler
import com.sachtechsolution.neighbourhood.basepackage.base.BaseActivity
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.gone
import com.sachtechsolution.neighbourhood.extension.openActivity
import com.sachtechsolution.neighbourhood.extension.showToast
import com.sachtechsolution.neighbourhood.extension.visible
import com.sachtechsolution.neighbourhood.model.Group
import com.sachtechsolution.neighbourhood.model.User
import com.sachtechsolution.neighbourhood.ui.groups.GroupActivity
import com.sachtechsolution.neighbourhood.ui.groups.GroupDetailFragment
import com.sachtechsolution.neighbourhood.ui.groups.GroupInfoFragment
import com.sachtechsolution.neighbourhood.ui.home.adapters.MemberListAdapter
import kotlinx.android.synthetic.main.fragment_group_info.*

class GroupDeatilActivity : BaseActivity() {

    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }

    val group by lazy { intent.getParcelableExtra("group") as Group }

    lateinit private var mAdapter2: MemberListAdapter


    override fun fragmentContainer(): Int {
        return R.id.container
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_group_deatil)

        navigator.addFragment(GroupInfoFragment::class.java,true)
    }


    private fun saveGroup() {
        group.members = mAdapter2.userIdList
        if (!group.members.contains(fireStoreHelper.currentUserUid()))
            group.members.add(fireStoreHelper.currentUserUid())
        showProgress()
        fireStoreHelper.updateGroup(group = group, gid = group.gid).addOnCompleteListener {
            if (it.isSuccessful) {
                mAdapter2.userIdList.forEach {
                    fireStoreHelper.getUser(it).addOnCompleteListener {
                        val user = it.result.toObject(User::class.java)!!
                        if (user.groups.contains(group.gid)) {
                            user!!.groups.remove(group.gid)
                            fireStoreHelper.setUser(user.uid, user)
                        }
                    }
                }
                Handler().postDelayed({
                    hideProgress()
                    showToast("Group updated")
                    finish()
                    openActivity<GroupActivity>()
                }, 2000)
            } else {
                onError()
            }
        }.addOnFailureListener { onError() }
    }

    private fun exitGroup() {
        showProgress()
        val user = pref.getUserFromPref()
        user!!.groups.remove(group.gid)
        if (group.members.contains(user.uid))
            group.members.remove(user.uid)
        fireStoreHelper.setUser(fireStoreHelper.currentUserUid(), user).addOnCompleteListener {
            if (it.isSuccessful) {
                group.members.remove(fireStoreHelper.currentUserUid())
                fireStoreHelper.updateGroup(group = group, gid = group.gid)
                        .addOnCompleteListener {
                            pref.setUserToPref(user)
                            finish()
                            hideProgress()
                        }
            } else {
                onError()
            }
        }.addOnFailureListener {
            onError()
        }
    }

    private fun onError() {
        hideProgress()
        showToast("try again")
    }

    private fun getAllMembers() {
        group.members.forEach {
            fireStoreHelper.getUser(it).addOnCompleteListener {
                if (it.isSuccessful) {
                    val user = it.result.toObject(User::class.java)
                    user!!.uid = it.result.id
                    mAdapter2.addUser(user = user!!)
                }
            }
        }
    }

    private fun getUser() {
        showProgress()
        fireStoreHelper.getUser(group.createdBy).addOnCompleteListener {
            if (it.isSuccessful) {
                val user = it.result.toObject(User::class.java)
                extra.text = "This group is created on ${group.createdOn} by ${user!!.firstName} ${user!!.lastName}"
                hideProgress()
            } else
                hideProgress()
        }.addOnFailureListener { hideProgress() }
    }
}

