package com.sachtechsolution.neighbourhood.app

import android.app.Application
import android.support.multidex.MultiDex

class App : Application() {


    override fun onCreate() {
        super.onCreate()
        MultiDex.install(this)
        application = this
    }

    companion object {
        lateinit var application: App
        fun getApp() = application
    }
}
