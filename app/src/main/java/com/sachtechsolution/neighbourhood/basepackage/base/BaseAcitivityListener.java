package com.sachtechsolution.neighbourhood.basepackage.base;

import com.sachtechsolution.neighbourhood.basepackage.controller.Navigator;


/**
 * * Created by Gurtek Singh on other/27/2018.
 * Sachtech Solution
 * gurtek@protonmail.ch
 */

public interface BaseAcitivityListener {

     Navigator getNavigator();

}
