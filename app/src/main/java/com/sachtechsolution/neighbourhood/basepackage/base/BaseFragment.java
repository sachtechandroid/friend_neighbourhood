package com.sachtechsolution.neighbourhood.basepackage.base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.sachtechsolution.neighbourhood.HomePage;
import com.sachtechsolution.neighbourhood.intracter.HomeActivityIntracter;
import com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings.profile.FillUpProfileActivity;

import gurtek.mrgurtekbasejava.base.ModelPrefrence;

public abstract class BaseFragment extends Fragment {
   public ModelPrefrence pref;
   public HomeActivityIntracter intracterListner;

    protected static BaseAcitivityListener baseAcitivityListener;
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof BaseAcitivityListener) {
            baseAcitivityListener = (BaseAcitivityListener) context;
            pref = new ModelPrefrence(context);
            try {
                intracterListner = (HomePage) context;

            } catch (Exception e) {
            }

        }
    }



    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(viewToCreate(), container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pref = new ModelPrefrence(getActivity());

    }

    @LayoutRes
    protected abstract int viewToCreate();

    public void showProgress() {
        ProgressDialogFragment.showProgress(getActivity().getSupportFragmentManager());
    }

    public void showToast(String s){
        Toast.makeText(getActivity(), ""+s, Toast.LENGTH_SHORT).show();
    }
    public void showProgress(String s) {
        ProgressDialogFragment.showProgress(getActivity().getSupportFragmentManager(), s);
    }

    public void hideProgress() {
        ProgressDialogFragment.hideProgress();
    }

}
