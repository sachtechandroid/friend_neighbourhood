package com.sachtechsolution.neighbourhood.basepackage.controller;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment;


/**
 * * Created by Gurtek Singh on other/27/2018.
 * Sachtech Solution
 * gurtek@protonmail.ch
 */

public class Navigator {

    private AppCompatActivity activity;
    private int container;
    private final Transition addTransition;
    private final Transition replaceTransition;

    public Navigator(AppCompatActivity activity, @IdRes int container,
                     Transition addTransition,
                     Transition replaceTransition) {
        this.activity = activity;
        this.container = container;
        this.addTransition = addTransition;
        this.replaceTransition = replaceTransition;
    }

    /**
     * @param classToOpen class which you want to open
     *                    if want to add more data in class
     *                    {@link #openActivity(Class, Bundle)}
     */
    public void openActivity(Class<? extends AppCompatActivity> classToOpen) {
        openActivity(classToOpen, null);
    }

    /**
     * @param classToOpen class which you want to open
     * @param extras      put extra data which you want in other activity
     */
    public void openActivity(Class<? extends AppCompatActivity> classToOpen, Bundle extras) {
        Intent intent = new Intent(activity, classToOpen);
        if (extras != null) intent.putExtras(extras);
        activity.startActivity(intent);
    }


    private void performTransition(Class<? extends BaseFragment> fragment, boolean isaddtoStack, Transition usertransition, Bundle extras) throws IllegalAccessException, InstantiationException {

        FragmentManager manager = activity.getSupportFragmentManager();
        String className = fragment.getClass().getSimpleName();
        Fragment baseFragment = manager.findFragmentByTag(className);

        if (baseFragment == null) {
            FragmentTransaction transaction = manager.beginTransaction();
            baseFragment = fragment.newInstance();
            usertransition.performTransiton(transaction, container, baseFragment);
            if(isaddtoStack)
            transaction.addToBackStack(className);

            transaction.commitAllowingStateLoss();
        } else {
            manager.popBackStackImmediate(className, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }

        if (extras != null) baseFragment.setArguments(extras);

    }

    public void addFragment(Class<? extends BaseFragment> fragment,boolean isAddtoStack) {
        try {
            performTransition(fragment,isAddtoStack, addTransition, null);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
    }

    public void addFragment(Class<? extends BaseFragment> fragment,boolean isAddtoStack, Bundle extras) {
        try {
            performTransition(fragment, isAddtoStack, addTransition, extras);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
    }


    public void replaceFragment(Class<? extends BaseFragment> fragment,boolean isAddtoStack) {
        try {
            performTransition(fragment, isAddtoStack, replaceTransition, null);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
    }

    public void replaceFragment(Class<? extends BaseFragment> fragment,boolean isAddtoStack, Bundle extras) {
        try {
            performTransition(fragment, isAddtoStack, replaceTransition, extras);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
    }


}
