package gurtek.mrgurtekbasejava.base

import android.content.Context
import android.content.SharedPreferences
import com.google.gson.Gson
import com.sachtechsolution.neighbourhood.model.ArchiveList
import com.sachtechsolution.neighbourhood.model.User

class ModelPrefrence(context: Context) {
    private var preferences: SharedPreferences = context.applicationContext.getSharedPreferences("MyPref", 0)
    private var editor: SharedPreferences.Editor

    init {
        editor = preferences.edit()

    }


    fun getUserFromPref(): User? {
        return Gson().fromJson(preferences.getString(USER, ""), User::class.java)
    }

    fun setUserToPref(user: User) {
        editor.putString(USER, Gson().toJson(user))
        editor.apply()
        editor.commit()
    }

    fun getArchvie() = Gson().fromJson(preferences.getString(LIST, ""), ArchiveList::class.java)

    fun setArchive(list: ArchiveList) {
        editor.putString(LIST, Gson().toJson(list))
        editor.apply()
        editor.commit()
    }

    fun cleaUser() {
        editor.clear()
        editor.apply()
        editor.commit()
    }

    companion object {
        var USER = "user"
        var LIST = "list"
    }

    fun setSetting(setting: String, value: Boolean) {
        editor.putBoolean(setting, value)
        editor.apply()
        editor.commit()

    }

    fun getSettings(setting: String): Boolean {
        return preferences.getBoolean(setting, true)
    }

    fun setFollow(setting: String, value: String) {
        editor.putString(setting, value)
        editor.apply()
        editor.commit()

    }

    fun getFollow(setting: String): String? {
        return preferences.getString(setting, "")
    }
}