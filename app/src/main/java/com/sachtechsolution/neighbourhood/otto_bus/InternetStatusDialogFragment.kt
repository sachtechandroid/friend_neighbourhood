package com.sachtechsolution.neighbourhood.otto_bus

import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import com.example.manish.internetstatus.NetworkUtil
import com.example.manish.internetstatus.OttoBus
import com.google.common.eventbus.Subscribe
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import kotlinx.android.synthetic.main.view_internet_status.*


open class InternetStatusDialogFragment: DialogFragment(), View.OnClickListener {

    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }

    override fun onClick(p0: View?) {
        when(p0!!.id) {

            R.id.txtEnable -> {
                startActivity(Intent(Settings.ACTION_WIRELESS_SETTINGS))
                dismiss() }

            R.id.txtOk-> {
                if (NetworkUtil.getConnectivityStatusString(context!!).equals("Not connected to Internet")) {
                    dismiss()
                    InternetStatusDialogFragment.newInstance().show(fragmentManager, "option_fragment")
                }
                else {
                    dismiss()
                }

            }
        }
    }

    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
    }


    @Subscribe
    fun getMessage(internetStatus:String) {

        if (internetStatus.equals("Not connected to Internet")){
            }
        else {
            dismiss()

        }

    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)
        return inflater.inflate(R.layout.view_internet_status, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        OttoBus.bus.register(this)

        //listner
        txtEnable.setOnClickListener(this)
        txtOk.setOnClickListener(this)
    }

    companion object {

        fun newInstance(): InternetStatusDialogFragment {
            return InternetStatusDialogFragment()

        }
    }

}