package com.example.manish.internetstatus

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent


class NetworkChangeReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {

        val status = NetworkUtil.getConnectivityStatusString(context)

        OttoBus.bus.post(status)

    }

}