package com.sachtechsolution.neighbourhood.extension

import android.app.Activity
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.LocationManager
import android.net.ConnectivityManager
import android.net.Uri
import android.os.Build
import android.os.Looper
import android.os.Handler
import android.provider.Settings
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.text.TextUtils
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.sachtechsolution.neighbourhood.app.App
import com.sachtechsolution.neighbourhood.basepackage.base.NetworkUtils
import java.util.*


fun Context.showToast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_LONG).show()
}

fun String.isSecure(): Boolean {
    return this.length > 5
}

fun Context.getLocationMode(): Int {
    return Settings.Secure.getInt(this.contentResolver, Settings.Secure.LOCATION_MODE)

}
fun Context.openLink(url: String) {
    val i = Intent(Intent.ACTION_VIEW)
    i.data = Uri.parse(url)
    startActivity(i)
}

inline fun <reified T> Context.openActivity() {
    startActivity(Intent(this, T::class.java))
}

inline fun <reified T> Context.openActivity(key: String, extra: String) {
    var intent = Intent(this, T::class.java)
    intent.putExtra(key, extra)
    startActivity(intent)
}


fun Context.isLocationEnabled(context: Context): Boolean {
    var locationMode = 0
    val locationProviders: String

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
        try {
            locationMode = Settings.Secure.getInt(context.contentResolver, Settings.Secure.LOCATION_MODE)

        } catch (e: Settings.SettingNotFoundException) {
            e.printStackTrace()
            return false
        }

        return locationMode != Settings.Secure.LOCATION_MODE_OFF

    } else {
        locationProviders = Settings.Secure.getString(context.contentResolver, Settings.Secure.LOCATION_PROVIDERS_ALLOWED)
        return !TextUtils.isEmpty(locationProviders)
    }


}


fun isPermissionsGranted(context: Context, p0: Array<String>): Boolean {
    p0.forEach {
        if (ContextCompat.checkSelfPermission(context, it) == PackageManager.PERMISSION_DENIED)
            return false
    }
    return true
}

fun askForPermissions(activity: Activity, array: Array<String>) {
    ActivityCompat.requestPermissions(activity, array, 0)
}

fun isNetConnected(): Boolean {
    val cm = App.getApp().getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val activeNetwork = cm.activeNetworkInfo
    return activeNetwork != null //&& activeNetwork.isConnectedOrConnecting
}

fun Context.showConfirmAlert(message: String?, positiveText: String?
                             , negativetext: String?
                             , onConfirmed: () -> Unit = {}
                             , onCancel: () -> Unit = { }) {

    if (message.isNullOrEmpty()) return

    val builder = AlertDialog.Builder(this)

    builder.setMessage(message)
            .setCancelable(false)
            .setPositiveButton(positiveText) { dialog, id ->
                //do things

                onConfirmed.invoke()
                dialog.dismiss()
            }
            .setNegativeButton(negativetext) { dialog, id ->
                //do things

                onCancel.invoke()
                dialog.dismiss()
            }

    val alert = builder.create()
    alert.show()
    alert.getButton(Dialog.BUTTON_NEGATIVE).isAllCaps = false
    alert.getButton(Dialog.BUTTON_POSITIVE).isAllCaps = false


}


fun View.visible() {
    this.visibility = View.VISIBLE
}

fun View.invisible() {
    this.visibility = View.INVISIBLE
}

fun View.gone() {
    this.visibility = View.GONE
}

fun Context.networkAvaileble(): Boolean {
    return NetworkUtils.isNetworkAvailable(this)
}

fun Context.calculateNoOfColumns(): Int {
    val displayMetrics = resources.displayMetrics
    val dpWidth = displayMetrics.widthPixels / displayMetrics.density
    return (dpWidth / 90).toInt()

}

fun Context.hideKeyboard(activity: Activity) {
    val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    //Find the currently focused view, so we can grab the correct window token from it.
    var view = activity.currentFocus
    //If no view currently has focus, create a new one, just so we can grab a window token from it
    if (view == null) {
        view = View(activity)
    }
    imm.hideSoftInputFromWindow(view.windowToken, 0)
}


fun Context.share(text: String) {
    val sendIntent: Intent = Intent().apply {
        action = Intent.ACTION_SEND
        putExtra(Intent.EXTRA_TEXT, text)
        type = "text/plain"
    }
    startActivity(sendIntent)
}

fun getDateDifference(startDate: Long): String {
    var different = Calendar.getInstance().time.time - startDate

    val secondsInMilli: Long = 1000
    val minutesInMilli = secondsInMilli * 60
    val hoursInMilli = minutesInMilli * 60
    val daysInMilli = hoursInMilli * 24
    val monthInMilli = hoursInMilli * 24 * 30
    val yearInMilli = hoursInMilli * 24 * 30 * 12

    val years = different / yearInMilli
    different %= yearInMilli

    val months = different / monthInMilli
    different %= monthInMilli

    val days = different / daysInMilli
    different %= daysInMilli

    val hours = different / hoursInMilli
    different %= hoursInMilli

    val minutes = different / minutesInMilli

    if (years > 1)
        return "$years years ago"
    else if (years == 1.toLong())
        return "a year ago"
    else if (months > 1)
        return "$months months ago"
    else if (months == 1.toLong())
        return "a month ago"
    else if (days > 1)
        return "$days days ago"
    else if (days == 1.toLong())
        return "a day ago"
    else if (hours > 1)
        return "$hours hours ago"
    else if (hours == 1.toLong())
        return "a hour ago"
    else if (minutes > 1)
        return "$minutes minutes ago"
    else
        return "few seconds ago"
}

fun isPostHasTime(startDate: Long): Boolean {
    var different = Calendar.getInstance().time.time - startDate
    val secondsInMilli: Long = 1000
    val minutesInMilli = secondsInMilli * 60
    val hoursInMilli = minutesInMilli * 60
    val daysInMilli = hoursInMilli * 24
    val monthInMilli = hoursInMilli * 24 * 30
    val yearInMilli = hoursInMilli * 24 * 30 * 12

    different %= yearInMilli
    val months = different / monthInMilli

    return months <= 0
}

fun hideKeyboard(activity: Activity) {
    val imm = activity.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    var view = activity.currentFocus
    if (view == null) {
        view = View(activity)
    }
    imm.hideSoftInputFromWindow(view.windowToken, 0)
}

fun openKeyboard(activity: Activity, view: View) {
    val inputMethodManager = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.toggleSoftInputFromWindow(
            view.applicationWindowToken,
            InputMethodManager.SHOW_FORCED, 0)
}

fun checkLoc(context: Context): Boolean {
    //if(ActivityCompat.checkSelfPermission(this,android.Manifest.permission.ACCESS_FINE_LOCATION)!= PackageManager.PERMISSION_GRANTED)
    val locationManager = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
    return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)
}


inline fun <T> T.workerMainThread(crossinline body: () -> Unit) {
    val handler = Handler(Looper.getMainLooper())
    handler.post { body.invoke() }
}