package com.sachtechsolution.neighbourhood.model.places_bussiness

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResultsItem(
	val reference: String? = null,
	val types: List<String?>? = null,
	val scope: String? = null,
	val icon: String? = null,
	val name: String? = null,
	val rating: Double? = null,
	val geometry: Geometry? = null,
	val vicinity: String? = null,
	val id: String? = null,
	val photos: List<PhotosItem?>? = null,
	val plusCode: PlusCode? = null,
	val placeId: String? = null,
	val openingHours: OpeningHours? = null
): Parcelable
