package com.sachtechsolution.neighbourhood.model

import java.util.*

data class RecoComment(
        var rid: String = "",
        var uid: String = "",
        var recommendation: String = "",
        var date: Date = Date()
)