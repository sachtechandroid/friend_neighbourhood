package com.sachtechsolution.neighbourhood.model.location_from_zip

data class Viewport(
        val southwest: Southwest? = null,
        val northeast: Northeast? = null
)
