package com.sachtechsolution.neighbourhood.model.places_bussiness

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Geometry(
	val viewport: Viewport? = null,
	val location: Location? = null
): Parcelable
