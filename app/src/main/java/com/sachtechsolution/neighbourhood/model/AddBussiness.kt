package com.sachtechsolution.neighbourhood.model

data class AddBussiness(
                  var name:String="",
                  var icon:String="https://maps.gstatic.com/mapfiles/place_api/icons/generic_business-71.png",
                  var latlng:String="",
                  var address:String="",
                  var type:String="",
                        var rid:String= ""

)