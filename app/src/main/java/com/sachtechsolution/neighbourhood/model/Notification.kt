package com.sachtechsolution.neighbourhood.model

/* whoGuri 11/9/18 */
data class Notification(var type: String = "",
                        var pid:String="",
                        var text:String="",
                        val date: Long = System.currentTimeMillis(),
                        val suid:String="",
                        val read:Boolean=true
                        ) {
    companion object {
        val REPLY = "reply"
        val POLL = "poll"
        val MESSAGE="message"
        val ALERT="alert"
        val THANKS="thanks"
    }
}