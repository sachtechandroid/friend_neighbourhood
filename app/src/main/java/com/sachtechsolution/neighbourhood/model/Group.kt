package com.sachtechsolution.neighbourhood.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/* whoGuri 10/10/18 */
@Parcelize
data class Group(
        var gid: String = "",
        val name: String = "",
        val desc: String = "",
        val image: String = "",
        val membersCount: Int = 0,
        val createdOn: String = "",
        val createdBy: String = "",
        val zipCode: String = "",
        var members: ArrayList<String> = ArrayList()
) : Parcelable