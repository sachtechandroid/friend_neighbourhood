package com.sachtechsolution.neighbourhood.model

/* whoGuri 4/10/18 */
data class ArchiveList(var idList: ArrayList<String>,
                       var timestampList: ArrayList<String>
)