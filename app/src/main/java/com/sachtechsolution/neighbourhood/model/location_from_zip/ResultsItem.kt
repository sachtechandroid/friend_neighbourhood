package com.sachtechsolution.neighbourhood.model.location_from_zip

data class ResultsItem(
        val formattedAddress: String? = null,
        val types: List<String?>? = null,
        val geometry: Geometry? = null,
        val addressComponents: List<AddressComponentsItem?>? = null,
        val placeId: String? = null
)
