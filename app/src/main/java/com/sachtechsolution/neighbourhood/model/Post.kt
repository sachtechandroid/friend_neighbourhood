package com.sachtechsolution.neighbourhood.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.*
import kotlin.collections.ArrayList

/* whoGuri 24/8/18 */
@Parcelize
data class Post(
        val lat: Double=0.0,
        val long: Double=0.0,
        var pid: String = "",
        val uid: String = "",
        val date: Long=Calendar.getInstance().time.time ,
        val subject: String = "",
        val message: String = "",
        val imagesLinks: String = "",
        var repliesCount: Int = 0,
        var thanksCount: Int = 0,
        val type: String = "",
        val subType:String="",
        val extra: String = "",
        val sale: Sale? =null,
        val poll: Poll?=null,
        val event:Event?=null,
        val privacy: ArrayList<String> =ArrayList(),
        val location: String="",
        val zipCode:String="",
        val pdfLink:String="",
        val society: String=""
):Parcelable {
     object postType {
        val FOR_SALE= "forSale"
        val GENERAL= "general"
        val INTERESTS= "interests"
        val CRIME_SAFETY = "crimeSafety"
        val LOST_FOUND = "lostFond"
        val RECOMMENDATION = "recommendation"
        val POLL= "poll"
        val ALERT= "alert"
        val EVENT= "event"
        val ADS= "ads"
        val CAMPAIGN="campaign"
        val REAL_ESTATE="real_estate"
        val GROUP="group"
        val MESSAGE="MESSAGE"
    }

    @Parcelize
    data class Sale(val title:String="",
                    val price: Int=0,
                    val originalPrice: Int=0,
                    val detail:String="",
                    val type:String="",
                    val street:String="",
                    val discount:Int=0
                    ):Parcelable
    @Parcelize
    data class Bookmark(val post_id:String=""
                    ):Parcelable

    @Parcelize
    data class RealEstate(var title:String="",
                          var message:String="",
                          var images:String="",
                          var latlng:String="",
                          var address:String="",
                          var zipCode:String="",
                          var uid:String=""):Parcelable
    @Parcelize
    data class Event(val location:String="",
                     val startDate:String="",
                     val endDate:String="",
                     val expiry:Date=Date()
                     ):Parcelable

}