package com.sachtechsolution.neighbourhood.model.location_from_zip

data class Northeast(
	val lng: Double? = null,
	val lat: Double? = null
)
