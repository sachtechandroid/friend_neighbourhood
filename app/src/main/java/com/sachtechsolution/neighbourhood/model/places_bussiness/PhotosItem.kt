package com.sachtechsolution.neighbourhood.model.places_bussiness

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PhotosItem(
	val photoReference: String? = null,
	val width: Int? = null,
	val htmlAttributions: List<String?>? = null,
	val height: Int? = null
): Parcelable
