package com.sachtechsolution.neighbourhood.model.location_from_zip

data class LocationFromZip(
        val results: List<ResultsItem?>? = null,
        val status: String? = null
)
