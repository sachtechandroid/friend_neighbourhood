package com.sachtechsolution.neighbourhood.model

import java.util.*

/* whoGuri 31/8/18 */
data class Reply(
        var rid:String="",
        var uid:String="",
        var reply:String="",
        var date:Long=System.currentTimeMillis()
)

