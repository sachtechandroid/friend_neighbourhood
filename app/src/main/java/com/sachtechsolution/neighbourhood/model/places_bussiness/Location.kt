package com.sachtechsolution.neighbourhood.model.places_bussiness

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Location(
	val lng: Double? = null,
	val lat: Double? = null
): Parcelable
