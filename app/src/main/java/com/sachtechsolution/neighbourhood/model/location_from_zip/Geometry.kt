package com.sachtechsolution.neighbourhood.model.location_from_zip

data class Geometry(
        val viewport: Viewport? = null,
        val bounds: Bounds? = null,
        val location: Location? = null,
        val locationType: String? = null
)
