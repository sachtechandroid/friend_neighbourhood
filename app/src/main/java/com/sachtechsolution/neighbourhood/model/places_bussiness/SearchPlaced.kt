package com.sachtechsolution.neighbourhood.model.places_bussiness

data class SearchPlaced(
	val nextPageToken: String? = null,
	val htmlAttributions: List<Any?>? = null,
	val results: List<ResultsItem?>? = null,
	val status: String? = null
)
