package com.sachtechsolution.neighbourhood.model.places_bussiness

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Viewport(
	val southwest: Southwest? = null,
	val northeast: Northeast? = null
): Parcelable
