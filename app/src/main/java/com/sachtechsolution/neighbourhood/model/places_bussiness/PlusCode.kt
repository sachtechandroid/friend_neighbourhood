package com.sachtechsolution.neighbourhood.model.places_bussiness

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PlusCode(
	val compoundCode: String? = null,
	val globalCode: String? = null
): Parcelable
