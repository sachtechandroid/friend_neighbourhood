package com.sachtechsolution.neighbourhood.model

import android.os.Parcelable
import com.google.firebase.iid.FirebaseInstanceId
import kotlinx.android.parcel.Parcelize

/* whoGuri 24/8/18 */
@Parcelize
data class User(var uid: String = "",
                val email: String = "",
                var firstName: String = "",
                var lastName: String = "",
                val gender: String = "",
                val imageLink: String = "",
                var lat: Double = 0.0,
                var lng: Double = 0.0,
                var mainLocality: String = "",
                var address: String = "",
                var zipCode: String = "",
                val token: String = FirebaseInstanceId.getInstance().token!!,
                var street: String = "",
                val groups: ArrayList<String> = ArrayList(),
                var society: String = ""
) : Parcelable


@Parcelize
class EmergencyContacts(var email1: String = "", var email2: String = "", var email3: String = "") : Parcelable

@Parcelize
class ContactInformation(var email: String = "", var homePhone: String = "", var mobilePhone: String = "") : Parcelable

@Parcelize
class Family(var significant: Significant? = Significant(),
             var child: Child? = Child(),
             var pet: Pet? = Pet()) : Parcelable {

    @Parcelize
    class Pet(
            var name: String = "",
            var imageLink: String = "",
            var category: String = "",
            var profile: String = "") : Parcelable

    @Parcelize
    class Significant(var name: String = "", var imageLink: String = "") : Parcelable

    @Parcelize
    class Child(var name: String = "", var imageLink: String = "", var age: String = "") : Parcelable
}

@Parcelize
class Biography(var movedYear: String = "",
                var growUp: String = "",
                var aboutYou: String = "",
                var aboutDo: AboutDo? = AboutDo(),
                var background: String = "") : Parcelable {

    @Parcelize
    class AboutDo(var jobTitle: String = "", var company: String = "", var city: String = "") : Parcelable

    class Interest(var interest: String = "")

    class Skills(var skills: String = "")

    class AboutLoveNei(var aboutLoveNei: String = "")


}

class AllFamily(var name: String = "", var image: String = "", var age: String = "", var category: String = "", var id: String = "")