package com.sachtechsolution.neighbourhood.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

import kotlinx.android.parcel.Parcelize;

/* whoGuri 4/9/18 */
@Parcelize
public class Poll implements Parcelable {
    public String question="";
    public String description="";
    public int no=0;
    public ArrayList<String> choices=new ArrayList();

    public Poll() {
    }

    public Poll(String question, String description, int no, ArrayList<String> choices) {
        this.question = question;
        this.description = description;
        this.no = no;
        this.choices = choices;
    }

    protected Poll(Parcel in) {
        question = in.readString();
        description = in.readString();
        no = in.readInt();
        choices = in.createStringArrayList();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(question);
        dest.writeString(description);
        dest.writeInt(no);
        dest.writeStringList(choices);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Poll> CREATOR = new Creator<Poll>() {
        @Override
        public Poll createFromParcel(Parcel in) {
            return new Poll(in);
        }

        @Override
        public Poll[] newArray(int size) {
            return new Poll[size];
        }
    };
}