package com.sachtechsolution.neighbourhood.model.location_from_zip

data class AddressComponentsItem(
	val types: List<String?>? = null,
	val shortName: String? = null,
	val longName: String? = null
)
