package com.sachtechsolution.neighbourhood.chat

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.app.ActivityCompat
import android.support.v4.app.ActivityOptionsCompat
import android.support.v4.view.ViewCompat
import android.support.v7.app.ActionBar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.ListView
import com.google.firebase.database.*
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseActivity
import com.sachtechsolution.neighbourhood.chat.adapter.ChatDetailsListAdapter
import com.sachtechsolution.neighbourhood.chat.data.ParseFirebaseData
import com.sachtechsolution.neighbourhood.chat.data.SettingsAPI
import com.sachtechsolution.neighbourhood.chat.data.Tools
import com.sachtechsolution.neighbourhood.chat.model.ChatMessage
import com.sachtechsolution.neighbourhood.chat.model.Friend
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.showToast
import com.sachtechsolution.neighbourhood.intracter.Api_Hitter
import com.sachtechsolution.neighbourhood.model.ArchiveList
import com.sachtechsolution.neighbourhood.model.User
import kotlinx.android.synthetic.main.activity_chat_details.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList

class ActivityChatDetails : BaseActivity() {

    override fun fragmentContainer(): Int {
        return 0
    }

    private var btn_send: Button? = null
    private var et_content: EditText? = null
    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }
    private var listview: ListView? = null
    private var actionBar: ActionBar? = null
    private var friend: Friend? = null
    private val items = ArrayList<ChatMessage>()
    private var parent_view: View? = null
    internal var pfbd: ParseFirebaseData? = null
    internal var set: SettingsAPI? = null

    internal var chatNode: String? = null
    internal var chatNode_1: String? = null
    internal var chatNode_2: String? = null
    internal var ref: DatabaseReference? = null

    private val contentWatcher = object : TextWatcher {
        override fun afterTextChanged(etd: Editable) {
            btn_send!!.isEnabled = etd.toString().trim { it <= ' ' }.isNotEmpty()
            //draft.setContent(etd.toString());
        }

        override fun beforeTextChanged(arg0: CharSequence, arg1: Int, arg2: Int, arg3: Int) {}

        override fun onTextChanged(arg0: CharSequence, arg1: Int, arg2: Int, arg3: Int) {}
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat_details)
        parent_view = findViewById(android.R.id.content)
        et_content = findViewById(R.id.text_content)
        pfbd = ParseFirebaseData(this)
        set = SettingsAPI(this)

        // animation transition
        ViewCompat.setTransitionName(parent_view!!, KEY_FRIEND)

        // initialize conversation data
        val intent = intent
        friend = intent.extras!!.getSerializable(KEY_FRIEND) as Friend
        initBar()
        chatTitle.text = friend!!.name

        iniComponen()
        chatNode_1 = set!!.readSetting("myid") + "-" + friend!!.id
        chatNode_2 = friend!!.id + "-" + set!!.readSetting("myid")

        ref = FirebaseDatabase.getInstance().getReference(MESSAGE_CHILD)
        ref!!.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                if (dataSnapshot.hasChild(chatNode_1!!)) {
                    chatNode = chatNode_1
                } else if (dataSnapshot.hasChild(chatNode_2!!)) {
                    chatNode = chatNode_2
                } else {
                    chatNode = chatNode_1
                }
                val totalData = dataSnapshot.child(chatNode.toString()).toString()
                items.clear()
                items.addAll(pfbd!!.getMessageListForUser(totalData))
                mAdapter = ChatDetailsListAdapter(this@ActivityChatDetails, items)
                listview!!.adapter = mAdapter
                listview!!.setSelectionFromTop(mAdapter.count, 0)
                listview!!.requestFocus()
                registerForContextMenu(listview)
                bindView()
            }

            override fun onCancelled(databaseError: DatabaseError) {
                Snackbar.make(window.decorView, "Could not connect", Snackbar.LENGTH_LONG).show()
            }
        })

        // for system bar in lollipop
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Tools.systemBarLolipop(this)
        }
    }

    private var isArchived: Boolean = false

    private fun initBar() {
        if (pref.getArchvie()?.idList?.contains(friend!!.id) ?: false) {
            isArchived = true
            chatOptions.setImageResource(R.drawable.ic_unarchive)
        } else {
            isArchived = false
            chatOptions.setImageResource(R.drawable.ic_archive)
        }

        chatBack.setOnClickListener {
            finish()
        }
        chatOptions.setOnClickListener {
            if (isArchived) {
                isArchived = false
                val list1 = pref.getArchvie()?.idList
                val list2 = pref.getArchvie()?.timestampList
                val index = list1!!.indexOf(friend!!.id)
                list1.removeAt(index)
                list2!!.removeAt(index)
                pref.setArchive(ArchiveList(list1, list2))
                showToast("Chat removed from Archive")
                chatOptions.setImageResource(R.drawable.ic_archive)
            } else {
                isArchived = true
                val listId = pref.getArchvie()?.idList ?: ArrayList()
                val listTimestamp = pref.getArchvie()?.timestampList ?: ArrayList()
                listId.add(friend!!.id)
                listTimestamp.add(mAdapter.lastMessage())
                pref.setArchive(ArchiveList(listId, listTimestamp))
                showToast("Chat added to Archive")
                chatOptions.setImageResource(R.drawable.ic_unarchive)
            }
        }
    }

    fun initToolbar() {
        val toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)
        actionBar = supportActionBar
        actionBar!!.setDisplayHomeAsUpEnabled(true)
        actionBar!!.setHomeButtonEnabled(true)
        actionBar!!.title = friend!!.name
        toolbar.setNavigationOnClickListener {
            finish()
        }
    }

    fun bindView() {
        try {
            mAdapter.notifyDataSetChanged()
            listview!!.setSelectionFromTop(mAdapter.count, 0)
        } catch (e: Exception) {

        }

    }

    fun iniComponen() {
        listview = findViewById(R.id.listview)
        btn_send = findViewById(R.id.btn_send)
        et_content = findViewById(R.id.text_content)

        btn_send!!.setOnClickListener {
            //      ChatMessage im=new ChatMessage(et_content.getText().toString(), String.valueOf(System.currentTimeMillis()),friend.getId(),friend.getName(),friend.getPhoto());
            val hm = HashMap<String, Any>()
            hm.put("text", et_content!!.text.toString())
            hm.put("timestamp", System.currentTimeMillis().toString())
            hm.put("receiverid", friend!!.id)
            hm.put("receivername", friend!!.name)
            hm.put("receiverphoto", friend!!.photo)
            hm.put("senderid", set!!.readSetting("myid"))
            hm.put("sendername", set!!.readSetting("myname"))
            hm.put("senderphoto", set!!.readSetting("mydp"))

            ref!!.child(chatNode.toString()).push().setValue(hm)
            et_content!!.setText("")
            hideKeyboard()
            createNotification(friend!!.id)
        }

        et_content!!.addTextChangedListener(contentWatcher)
        if (et_content!!.length() == 0) {
            btn_send!!.isEnabled = false
        }
        hideKeyboard()
    }

    private fun createNotification(receiverId: String) {
        var cUser = pref.getUserFromPref()
        fireStoreHelper.getUser(receiverId).addOnCompleteListener {
            if (it.isSuccessful) {
                if (it.result.exists()) {
                    val pUser = it.result.toObject(User::class.java)

                    val callback = Api_Hitter.ApiInterface("http://stsmentor.com/").sendNotification(cUser!!.firstName + " " + cUser!!.lastName + " sent you a message", pUser!!.token)
                    callback.enqueue(object : Callback<ResponseBody> {
                        override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                            showToast("got error")
                        }

                        override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                            if (response.errorBody() != null) {
                                showToast("Message Not sent")
                            } else if (response.body() != null) {
                                // showToast("Notification sent")
                            }
                        }

                    })
                }
            }
            hideProgress()
        }.addOnFailureListener { hideProgress() }
    }


    private fun hideKeyboard() {
        val view = this.currentFocus
        if (view != null) {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
    }

    companion object {
        var KEY_FRIEND = "FRIEND"

        // give preparation animation activity transition
        fun navigate(activity: AppCompatActivity, transitionImage: View, obj: Friend) {
            val intent = Intent(activity, ActivityChatDetails::class.java)
            intent.putExtra(KEY_FRIEND, obj)
            val options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, transitionImage, KEY_FRIEND)
            ActivityCompat.startActivity(activity, intent, options.toBundle())
        }

        lateinit var mAdapter: ChatDetailsListAdapter

        val MESSAGE_CHILD = "messages"
    }
}
