package com.sachtechsolution.neighbourhood.chat.fragment

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.design.widget.TabLayout
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.ValueEventListener
import com.sachtechsolution.neighbourhood.HomePage
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.chat.ActivityChatDetails
import com.sachtechsolution.neighbourhood.chat.ActivitySelectFriend
import com.sachtechsolution.neighbourhood.chat.adapter.ChatsListAdapter
import com.sachtechsolution.neighbourhood.chat.data.ParseFirebaseData
import com.sachtechsolution.neighbourhood.chat.data.SettingsAPI
import com.sachtechsolution.neighbourhood.chat.model.ChatMessage
import com.sachtechsolution.neighbourhood.chat.widget.DividerItemDecoration
import com.sachtechsolution.neighbourhood.extension.gone
import com.sachtechsolution.neighbourhood.extension.openActivity
import com.sachtechsolution.neighbourhood.extension.visible
import com.sachtechsolution.neighbourhood.model.ArchiveList
import kotlinx.android.synthetic.main.fragment_chat.*

class ChatsFragment : BaseFragment() {

    var recyclerView: RecyclerView? = null

    private var mLayoutManager: LinearLayoutManager? = null
    var mAdapter: ChatsListAdapter? = null
    private var progressBar: ProgressBar? = null
    internal var view: View? = null

    internal var pfbd: ParseFirebaseData? = null
    internal var set: SettingsAPI? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        chat_text.visible()
        add.setOnClickListener {
            context!!.openActivity<ActivitySelectFriend>()
        }
        tabSetup()
    }

    private var chatList: MutableList<ChatMessage>?=null
    lateinit private var archiveList: MutableList<ChatMessage>
    lateinit private var inboxList: MutableList<ChatMessage>

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        view = inflater.inflate(R.layout.fragment_chat, container, false)
        pfbd = ParseFirebaseData(context)
        set = SettingsAPI(context)
        // activate fragment menu
        setHasOptionsMenu(true)

        recyclerView = view!!.findViewById<View>(R.id.recyclerView) as RecyclerView
        progressBar = view!!.findViewById<View>(R.id.progressBar) as ProgressBar

        // use a linear layout manager
        mLayoutManager = LinearLayoutManager(activity!!)
        recyclerView!!.layoutManager = mLayoutManager
        recyclerView!!.setHasFixedSize(true)
        recyclerView!!.addItemDecoration(DividerItemDecoration(activity!!, DividerItemDecoration.VERTICAL_LIST))
        archiveList = ArrayList()
        inboxList = ArrayList()
        val ref = FirebaseDatabase.getInstance().getReference(MESSAGE_CHILD)
        ref.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                var totalData = ""
                if (dataSnapshot.value != null)
                    totalData = dataSnapshot.value!!.toString()

                mAdapter = ChatsListAdapter(context, inboxList)
                recyclerView!!.adapter = mAdapter
                chatList = pfbd!!.getLastMessageList(totalData)
                filterChat(true)

                mAdapter!!.setOnItemClickListener { v, obj, position ->
                    if (obj.receiver.id == set!!.readSetting("myid"))
                        ActivityChatDetails.navigate((activity as HomePage?)!!, v.findViewById(R.id.lyt_parent), obj.sender)
                    else if (obj.sender.id == set!!.readSetting("myid"))
                        ActivityChatDetails.navigate((activity as HomePage?)!!, v.findViewById(R.id.lyt_parent), obj.receiver)
                }

                bindView()
            }

            override fun onCancelled(databaseError: DatabaseError) {
                if (getView() != null)
                    Snackbar.make(getView()!!, "Could not connect", Snackbar.LENGTH_LONG).show()
            }
        })

        return view
    }

    private fun filterChat(b: Boolean) {
        inboxList.clear()
        archiveList.clear()
        val size = pref.getArchvie()?.idList?.size ?: 0
        if (size > 0) {
            chatList?.forEach {
                if (pref.getArchvie()?.idList?.contains(it.sender.id)!! || pref.getArchvie()?.idList?.contains(it.receiver.id)!!) {
                    var index = pref.getArchvie()?.idList?.indexOf(it.sender.id)
                    if (index == -1)
                        index = pref.getArchvie()?.idList?.indexOf(it.receiver.id)
                    if (pref.getArchvie()?.timestampList!![index!!].equals(it.text))
                        archiveList.add(it)
                    else{
                        inboxList.add(it)
                        val list1=pref.getArchvie()?.idList
                        val list2=pref.getArchvie()?.timestampList
                        list1!!.removeAt(index)
                        list2!!.removeAt(index)
                        pref.setArchive(ArchiveList(list1,list2))
                    }
                } else
                    inboxList.add(it)
            }
            if (b)
                mAdapter?.changeList(inboxList)
            else
                mAdapter?.changeList(archiveList)
        } else if (size == 0 && b)
            mAdapter?.changeList(chatList)
        else if (size == 0 && !b)
            mAdapter?.changeList(archiveList)
    }

    private fun tabSetup() {
        chatTab.addTab(chatTab.newTab().setText("INBOX"), true)
        chatTab.addTab(chatTab.newTab().setText("ARCHIVE"))

        chatTab.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(p0: TabLayout.Tab?) {
            }

            override fun onTabUnselected(p0: TabLayout.Tab?) {
            }

            override fun onTabSelected(p0: TabLayout.Tab?) {
                when (p0!!.position) {
                    0 -> {
                        filterChat(true)
                    }
                    1 -> {
                        filterChat(false)
                    }
                }
            }

        })
    }

    override fun viewToCreate(): Int {
        return 0
    }

    fun bindView() {
        try {
            mAdapter!!.notifyDataSetChanged()
            progressBar!!.visibility = View.GONE
            chat_text.gone()
        } catch (e: Exception) {
        }

    }

    companion object {

        const val MESSAGE_CHILD = "messages"
    }

    override fun onResume() {
        super.onResume()
        filterChat(true)
        chatTab.getTabAt(0)?.select()
    }
}
