package com.sachtechsolution.neighbourhood.chat.model;

import java.io.Serializable;

public class Friend implements Serializable {
	private String id;
	private String name;
	private String photo;
	private String society;
	public String getSubLocality() {
		return society;
	}



	public Friend(String id, String name, String photo,String society) {
		this.id = id;
		this.name = name;
		this.photo = photo;
		this.society = society;
	}

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getPhoto() {
		return photo;
	}

}
