package com.sachtechsolution.neighbourhood.constants

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
class PetDirectory(
        val petName:String="",
        val petType:String="",
        val petImage: String ="",
        val profile:String="",
        val userName:String="",
        val date: Date = Calendar.getInstance().time,
        val userPic:String="",
        val street:String="",
        val petColor:String="",
        val petbreed:String="",
        val petSize:String=""
):Parcelable