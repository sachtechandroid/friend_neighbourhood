package com.sachtechsolution.neighbourhood.constants

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class InvitationCode(
        val code:String="",
        val userUid: String=""
        ): Parcelable
