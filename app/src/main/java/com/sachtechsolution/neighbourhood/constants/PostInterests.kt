package com.sachtechsolution.neighbourhood.constants

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PostInterests(val type:String="",
                         val uid:String="",
                         val id:String=""
): Parcelable