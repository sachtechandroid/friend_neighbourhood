package com.sachtechsolution.neighbourhood.constants

object ObjBiography
{
    var moveYear=""
    var growUp=""
    var jobTitle=""
    var company=""
    var city=""
    var background=""
    var aboutYou=""

    fun clearBiography(){
         moveYear=""
         growUp=""
         jobTitle=""
         company=""
         city=""
         background=""
         aboutYou=""
    }
}