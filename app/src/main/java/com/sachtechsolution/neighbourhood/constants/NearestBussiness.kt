package com.sachtechsolution.neighbourhood.constants

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class NearestBussiness(val name: String = "",
                            val icon: String = "",
                            val id: String = "",
                            val type: String = "",
                            val recCount: String = "",
                            val vicinity: String = "",
                            val rating: String = "3.4",
                            val latLng: String = ""
) : Parcelable