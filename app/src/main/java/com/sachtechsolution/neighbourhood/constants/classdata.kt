package com.sachtechsolution.neighbourhood.constants

object classdata{

    var title:String=""
    var price:Int=0
    var originalPrice:Int=0
    var detail:String=""
    var category:String=""
    var image: String = ""

    fun clear(){
        price =0
        title =""
        originalPrice =0
        detail =""
        category =""
        image =""
    }

}