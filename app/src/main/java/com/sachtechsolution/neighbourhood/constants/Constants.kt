package com.sachtechsolution.neighbourhood.constants

import android.net.Uri
import com.sachtechsolution.neighbourhood.model.Group
import com.sachtechsolution.neighbourhood.model.Post

/* whoGuri 25/8/18 */
object Constants{
    var PDF_URI: Uri? =null
    var IMAGE_LIST= ArrayList<Uri?>()
    var LAT:Double=0.0
    var LONG:Double=0.0
    var email:String=""
    var IS_THANKED_LIST=ArrayList<Boolean>()
    var IS_POLLED_LIST=ArrayList<Boolean>()
    var POSITION=-1
    var DELETED_POSITION=-1
    var ADDRESS=""
    var STREET=""
    var SOCIETY=""
    var ZIP_CODE=""
    var ADDRESSES=ArrayList<String>()
    var SOCIETIES=ArrayList<String>()
    var estate=Post.RealEstate()
    var MAIN_LOCALITY=""
    var NEXT:String=""
    var POST:Post?=null
    var REDIUS=0.00000901*4000
    var BACK=true
    var group:Group?= null
}