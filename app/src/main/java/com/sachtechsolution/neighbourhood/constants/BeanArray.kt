package com.sachtechsolution.neighbourhood.constants

import android.os.Parcelable
import com.sachtechsolution.neighbourhood.model.places_bussiness.ResultsItem

import kotlinx.android.parcel.Parcelize

@Parcelize
data class BeanArray(
        val image:Int=0,
        val name:String="",
        val type:String="",
        var list: ArrayList<ResultsItem>
): Parcelable