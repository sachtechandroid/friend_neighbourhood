package com.sachtechsolution.neighbourhood.constants

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class InterestsPost(val type:String="",
                         val uid:String="",
                         val subject:String="",
                         val message:String="",
                         val userName:String="",
                         val imageLink:String=""
): Parcelable