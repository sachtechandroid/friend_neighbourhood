package com.sachtechsolution.neighbourhood.intracter

import com.sachtechsolution.neighbourhood.intracter.Api_Hitter.retrofit
import com.sachtechsolution.neighbourhood.model.location_from_zip.LocationFromZip
import com.sachtechsolution.neighbourhood.model.places_bussiness.SearchPlaced

import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.PUT
import retrofit2.http.Query

/**
 * Created by Balwinder Badgal on 12/14/2017.
 */
object Api_Hitter {

    var retrofit: Retrofit? = null

    fun ApiInterface(baseUrl: String): Api_Hitter.AApiInterface {

        val okHttpClient = OkHttpClient().newBuilder()
                .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .build()

        retrofit = Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build()


        return retrofit!!.create(AApiInterface::class.java)

    }

    interface AApiInterface {

        @GET("SMTP/sendmail.php?key=manish")
        fun sendInvitation(@Query("name") name: String,@Query("mail") mail: String,@Query("code") code: String): Call<ResponseBody>

        @GET("json?sensor=true&key=AIzaSyAkyIYbu78HM4YUune6MzNobDZZG7cZrKQ")
        fun getBussiness(@Query("types") type: String, @Query("location") location:String,@Query("radius") radius:String): Call<SearchPlaced>

        @GET("place/nearbysearch/json?radius=8000&sensor=true&key=AIzaSyAkyIYbu78HM4YUune6MzNobDZZG7cZrKQ")
        fun searchBussiness(@Query("name") name: String, @Query("location") location: String): Call<SearchPlaced>

        @PUT("neighbourhood/fcm.php?key=manish")
        fun sendNotification(@Query("fcm_title") fcm_title: String, @Query("fcm_token") fcm_token: String): Call<ResponseBody>

        @PUT("neighbourhood/fcm.php?key=manish")
        fun sendNotificationToAll(@Query("fcm_title") fcm_title: String, @Query("fcm_token") fcm_token: String, @Query("service_type") service_type: String): Call<ResponseBody>

    }
}
