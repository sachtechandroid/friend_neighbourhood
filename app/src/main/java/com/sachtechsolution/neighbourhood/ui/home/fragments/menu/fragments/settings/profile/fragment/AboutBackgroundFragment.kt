package com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings.profile.fragment

import android.os.Bundle
import android.view.View
import com.example.manish.internetstatus.OttoBus
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.constants.ObjBiography
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.showToast
import com.sachtechsolution.neighbourhood.model.Biography
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings.profile.FillUpProfileActivity
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.frament_background.*

class AboutBackgroundFragment:BaseFragment() {

    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }

    override fun viewToCreate(): Int {
        return R.layout.frament_background
    }
    override fun onResume() {
        super.onResume()
        OttoBus.bus.register(this)
        FillUpProfileActivity.mcount!!.text="4 of 4"
    }


    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
    }


    @Subscribe
    fun getMessage(internetStatus:String) {

        if (internetStatus.equals("Not connected to Internet")){
            InternetStatusDialogFragment.newInstance().show(fragmentManager, "option_fragment")
        }else {
            showToast("internet enabled ")

        }

    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnNext.setOnClickListener {
            if (etNote.text.isEmpty())
                activity!!.showToast("enter note")
            else
            {
                updateBiography(etNote.text.toString())



            }
        }
    }

    private fun updateBiography(background: String) {
        showProgress()
        var biography= Biography(movedYear = ObjBiography.moveYear,growUp = ObjBiography.growUp,aboutYou = ObjBiography.aboutYou,
                aboutDo = Biography.AboutDo(ObjBiography.jobTitle,ObjBiography.company,ObjBiography.city)
        ,background =background)

        fireStoreHelper.updateBiography(biography,pref.getUserFromPref()!!.uid).addOnCompleteListener {
            if (it.isSuccessful) {
                activity!!.showToast("done")
                activity!!.finish()
                ObjBiography.clearBiography()
            } else {
                showToast("error")
                hideProgress()
            }
            hideProgress()
        }.addOnFailureListener {
            showToast("failed")
            hideProgress()
        }
    }
    }
