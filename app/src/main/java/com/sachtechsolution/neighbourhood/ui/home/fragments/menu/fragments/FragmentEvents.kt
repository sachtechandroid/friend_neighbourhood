package com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments

import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.BaseTransientBottomBar
import android.util.Log
import android.view.MenuItem
import android.view.View
import com.example.manish.internetstatus.OttoBus
import com.google.firebase.firestore.DocumentSnapshot
import com.kennyc.bottomsheet.BottomSheet
import com.kennyc.bottomsheet.BottomSheetListener
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.constants.Constants
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.gone
import com.sachtechsolution.neighbourhood.extension.isPostHasTime
import com.sachtechsolution.neighbourhood.extension.showToast
import com.sachtechsolution.neighbourhood.model.Post
import com.sachtechsolution.neighbourhood.model.User
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.sachtechsolution.neighbourhood.ui.home.adapters.HomePagePostAdapter
import com.sachtechsolution.neighbourhood.ui.home.fab.fragment.event.FragmentEvent
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_events.*

class FragmentEvents:BaseFragment(),HomePagePostAdapter.OpenBottomSheet, BottomSheetListener {
    override fun OnOpenBottomSheet(pid: String, position: Int, type: String, post: Post, uid: String) {
        postId = pid
        postPosition = position
        BottomSheet.Builder(activity)
                .setSheet(R.menu.list_sheet)
                .setListener(this)
                .`object`("Some object")
                .show()
    }

    var postId = ""
    var postPosition = 0

    override fun viewToCreate(): Int {
        return R.layout.fragment_events
    }


    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
    }


    @Subscribe
    fun getMessage(internetStatus:String) {

        if (internetStatus.equals("Not connected to Internet")){
            InternetStatusDialogFragment.newInstance().show(fragmentManager, "option_fragment")
        }else {
            showToast("internet enabled ")

        }

    }


    lateinit var adapter: HomePagePostAdapter
    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }
    val zipCode by lazy { pref.getUserFromPref()?.mainLocality ?: "" }
    val user by lazy { pref.getUserFromPref()!!}

    lateinit var postLis: MutableList<DocumentSnapshot>
    var postCount = -1

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter = HomePagePostAdapter(context!!, this, baseAcitivityListener,pref)
        events_rv.adapter=adapter
        event_text.text="Loading events"
        Constants.IS_THANKED_LIST.clear()
        Constants.IS_POLLED_LIST.clear()
        fireStoreHelper.getPostsByType(type = Post.postType.EVENT,zipCode =  user.zipCode,
                society = user.society).addOnCompleteListener {
            if (it.isSuccessful) {
                postLis = it.result.documents
                getPost()
                if (postLis.size==0)
                    event_text.text="No event found"
            } else {
                event_text.text="No event found"
                showToast("something went wrong")
            }
        }.addOnFailureListener {
            Log.e("", "")
        }
        neighbors_back_iv.setOnClickListener { activity!!.onBackPressed() }
        new_event.setOnClickListener {
            Constants.NEXT="event"
            baseAcitivityListener.navigator.replaceFragment(FragmentEvent::class.java, true)
        }
    }
    private fun getPost() {
        postCount++
        if (postCount<postLis.size) {
            var post = postLis[postCount].toObject(Post::class.java)
            post!!.pid = postLis[postCount].id
            if (isPostHasTime(post.date)&&event_text!=null)
                getPostUser(post)
        }
    }

    private fun getPostUser(post: Post) {
        fireStoreHelper.getUser(post!!.uid).addOnCompleteListener {
            if (it.isSuccessful) {
                if (it.result.exists()) {
                    val user = it.result.toObject(User::class.java)
                    fireStoreHelper.getThanks(post.pid).addOnCompleteListener {
                        if (it.isSuccessful) {
                            event_text?.gone()
                            var isThanked = false
                            if (it.result.exists())
                                isThanked = true
                                adapter!!.addItem(post, user!!, isThanked, false)
                                getPost()
                        } else {
                            getPost()
                        }
                    }
                }
            }
        }.addOnFailureListener { getPost() }
    }

    fun deletePost() {
        showProgress()
        fireStoreHelper.deletePost(postId).addOnCompleteListener {
            if (it.isSuccessful) {
                onRemoveBookmark(postId)
            } else {
                showToast("error")
                hideProgress()
            }


        }.addOnFailureListener {
            showToast("failed")
            hideProgress()
        }
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menuPrivateMessage -> {

                context!!.showToast("clicked")
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onSheetShown(bottomSheet: BottomSheet, `object`: Any?) {
        Log.v(ContentValues.TAG, "onSheetShown with Object " + `object`!!)
    }

    override fun onSheetItemSelected(bottomSheet: BottomSheet, item: MenuItem, `object`: Any?) {

        if (item.title.equals("Turn on Notification"))
            showToast(item.title.toString())
        else if (item.title.equals("Add Bookmark")) {
            createBookmark()
        } else if (item.title.equals("Unsubscribe From News Page"))
            showToast(item.title.toString())
        else if (item.title.equals("Hide Post"))
            deletePost()
    }

    private fun createBookmark() {

        fireStoreHelper.createBookmark(Post.Bookmark(postId)).addOnCompleteListener {
            if (it.isSuccessful) {
                activity!!.showToast("Added to Bookmark")
            } else
                activity!!.showToast("error to bookmark")
        }.addOnFailureListener {
            activity!!.showToast("error to bookmark")
        }
    }

    override fun onSheetDismissed(bottomSheet: BottomSheet, `object`: Any?, @BaseTransientBottomBar.BaseCallback.DismissEvent dismissEvent: Int) {
        Log.v(ContentValues.TAG, "onSheetDismissed $dismissEvent")

        when (dismissEvent) {

            BottomSheetListener.DISMISS_EVENT_BUTTON_POSITIVE -> context!!.showToast("Positive Button Clicked")

            BottomSheetListener.DISMISS_EVENT_BUTTON_NEGATIVE -> context!!.showToast("Negative Button Clicked")
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
    }

    fun onRemoveBookmark(pid: String) {
        fireStoreHelper.removeBookmark(pid).addOnCompleteListener {
            if (it.isSuccessful) {
                showToast("Post deleted")
                adapter!!.deletePost(postPosition)
                adapter!!.notifyDataSetChanged()
                hideProgress()
            } else {
                hideProgress()
                context!!.showToast("unable to remove bookmark")
            }
        }.addOnFailureListener {
            hideProgress()
            context!!.showToast("unable to remove bookmark")
        }.addOnCanceledListener {
            hideProgress()
            context!!.showToast("unable to remove bookmark")
        }
    }

    override fun onResume() {
        super.onResume()

        OttoBus.bus.register(this)
        //getAllPost()
        if (Constants.POSITION != -1) {
            if (Constants.POST != null)
                adapter?.postsList?.set(Constants.POSITION, Constants.POST)
            adapter!!.notifyItemChanged(Constants.POSITION)
            Constants.POSITION = -1
            Constants.POST = null
        }
        if(Constants.DELETED_POSITION!=-1){
            val position=Constants.DELETED_POSITION
            adapter!!.deletePost(position)
            Constants.DELETED_POSITION=-1
        }
        if (intracterListner!=null)
        {
            intracterListner!!.updateSelectedPos(0)
        }
    }
}