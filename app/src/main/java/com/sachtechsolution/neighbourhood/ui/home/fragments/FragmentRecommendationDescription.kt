package com.sachtechsolution.neighbourhood.ui.home.fragments

import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.view.View
import com.bumptech.glide.Glide
import com.example.manish.internetstatus.OttoBus
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.constants.NearestBussiness
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.getLocationMode
import com.sachtechsolution.neighbourhood.extension.showToast
import com.sachtechsolution.neighbourhood.model.RecoComment
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.sachtechsolution.neighbourhood.ui.home.activities.FragmentAddRecommendation
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_recommendation_description.*

class FragmentRecommendationDescription:BaseFragment() {

    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }
    lateinit var adapter: CommentAdapter

    override fun viewToCreate(): Int {
        return R.layout.fragment_recommendation_description
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        adapter = CommentAdapter(activity!!, baseAcitivityListener,pref)
        commentRecyclerView.adapter = adapter


        val data= arguments!!.getParcelable<NearestBussiness>(FragmentAddRecommendation.DATA)

        if (data!==null) {
            getRecommendations(data.id!!)
            txtName.text=data.name
            etAddress.text=data.vicinity
            txtRating.text=" Rating- ${data.rating}"
            Glide.with(context!!).load(data.icon).into(category_icon)
        }
        if (arguments!=null)


        post_description_back.setOnClickListener{
            activity!!.onBackPressed()
        }
        btnPost.setOnClickListener { createComment(data) }
    }



    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
    }


    @Subscribe
    fun getMessage(internetStatus:String) {

        if (internetStatus.equals("Not connected to Internet")){
            InternetStatusDialogFragment.newInstance().show(fragmentManager, "option_fragment")
        }else {
            showToast("internet enabled ")

        }

    }

    override fun onResume() {
        super.onResume()
        OttoBus.bus.register(this)
    }

    private fun getRecommendations(id: String) {
        fireStoreHelper.getRecommendation(id).addOnCompleteListener {
            if (it.isSuccessful) {
                it.result.documents.forEach {
                    var comment = it.toObject(RecoComment::class.java)
                    fireStoreHelper.getUser(comment!!.uid).addOnCompleteListener {
                        if (comment.rid.equals(id))
                        if (it.isSuccessful) {
                            if (it.result.exists()) {
                                val user = it.result.toObject(com.sachtechsolution.neighbourhood.model.User::class.java)
                                adapter.addItem(comment, user!!)
                            }
                        } else {
                            activity!!.showToast("")
                        }
                    }
                }
            } else
                activity!!.showToast("")
        }.addOnFailureListener { activity!!.showToast("") }

    }


        private fun createComment(data: NearestBussiness?) {

            if (!etComment.text.isNullOrEmpty()) {
                showProgress()
                fireStoreHelper.createRecommendaton(data!!.id!!, RecoComment(rid= data.id!!,recommendation = etComment.text.toString(),
                        uid = fireStoreHelper.currentUserUid())).addOnCompleteListener {
                    if (it.isSuccessful) {
                        etComment.setText("")
                        etComment.clearFocus()
                        adapter.clear()
                        getRecommendations(data.id)
                        hideProgress()
                    }
                        else{
                        activity!!.showToast("error")
                        hideProgress()
                    }

                }.addOnFailureListener {
                    activity!!.showToast("error")
                    hideProgress()
                }
            }
        }
}