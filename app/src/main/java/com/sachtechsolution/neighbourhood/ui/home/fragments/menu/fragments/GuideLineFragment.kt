package com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments

import android.os.Bundle
import android.view.View
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_guide_line.*

class GuideLineFragment : BaseFragment() {
    override fun viewToCreate(): Int {
        return R.layout.fragment_guide_line
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        guide_back_iv.setOnClickListener {
            activity!!.onBackPressed()
        }
    }
}
