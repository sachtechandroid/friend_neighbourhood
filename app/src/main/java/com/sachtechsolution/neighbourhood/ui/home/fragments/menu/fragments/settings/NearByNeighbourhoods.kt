package com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings

import android.annotation.SuppressLint
import android.graphics.Color
import android.location.Location
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.PolygonOptions
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_nearby_neighbourhood.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.model.PolylineOptions
import com.google.android.gms.maps.model.Polyline




class NearByNeighbourhoods : BaseFragment(), OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, View.OnClickListener , MapFragmentAdapter.OpenDone {

    override fun OnEventDone() {

    }

    @SuppressLint("MissingPermission")
    override fun onConnected(p0: Bundle?) {
        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleClient)


        if (mLocation != null) {
            var mLat = 30.7046//mLocation.latitude
            var mLog = 76.7179//mLocation.longitude
            var mLatLong = LatLng(mLat, mLog)
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mLatLong, 12f))

            val polyline1 = mMap.addPolyline(PolylineOptions()
                    .clickable(true)
                    .add(
                            LatLng(-35.016, 143.321),
                            LatLng(-34.747, 145.592),
                            LatLng(-34.364, 147.891),
                            LatLng(-33.501, 150.217),
                            LatLng(-32.306, 149.248),
                            LatLng(-32.491, 147.309)))
            polyline1.tag = "A"

            // Position the map's camera near Alice Springs in the center of Australia,
            // and set the zoom factor so most of Australia shows on the screen.
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(-23.684, 133.903), 4f))

        }

    }

    override fun onConnectionSuspended(p0: Int) {
    }

    @SuppressLint("MissingPermission")
    override fun onMapReady(p0: GoogleMap?) {
        mMap = p0!!
        mMap.uiSettings.isZoomControlsEnabled = true
        mMap.isMyLocationEnabled = true
        // Set listeners for click events.

    }

    override fun viewToCreate(): Int {
        return R.layout.fragment_nearby_neighbourhood
    }

    private lateinit var mMap: GoogleMap
    private lateinit var mGoogleClient: GoogleApiClient
    lateinit var mLocation: Location
    lateinit var uMarker: Marker
    private var myMap: SupportMapFragment? = null

    private var markerView: View? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        myMap = childFragmentManager.findFragmentById(R.id.location_fragment) as SupportMapFragment
        myMap?.getMapAsync(this)
        mGoogleClient = GoogleApiClient.Builder(activity!!)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .build()
        mGoogleClient.connect()
        markerView = LayoutInflater.from(activity).inflate(R.layout.marker_layout, null)

        //adapter set
        var adapter= MapFragmentAdapter(activity!!,this)
        eventRecyclerView.adapter=adapter

          //listner
        btnBack.setOnClickListener(this)

    }

    override fun onClick(p0: View?) {
        when(p0!!.id)
        {
            R.id.btnBack->activity!!.onBackPressed()
        }
    }
}
