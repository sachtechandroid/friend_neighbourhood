package com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings.profile

import android.graphics.Color
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.example.manish.internetstatus.OttoBus
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.extension.showToast
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.squareup.otto.Subscribe
import fisk.chipcloud.ChipCloud
import fisk.chipcloud.ChipCloudConfig
import kotlinx.android.synthetic.main.activity_more_chips_activty.*

class MoreChipsActivty : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_more_chips_activty)
        getOther()

        //txtCancel.setOnClickListener { onBackPressed() }

    }

    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
    }


    @Subscribe
    fun getMessage(internetStatus:String) {

        if (internetStatus.equals("Not connected to Internet")){
            InternetStatusDialogFragment.newInstance().show(supportFragmentManager, "option_fragment")
        }else {
            showToast("internet enabled ")

        }

    }

    override fun onResume() {
        super.onResume()
        OttoBus.bus.register(this)
    }

    private fun getOther() {
        val config = ChipCloudConfig()
                .selectMode(ChipCloud.SelectMode.multi)
                .checkedChipColor(resources.getColor(R.color.optional))
                .checkedTextColor(Color.parseColor("#ffffff"))
                .uncheckedChipColor(Color.parseColor("#e0e0e0"))
                .uncheckedTextColor(resources.getColor(R.color.optional))

        val chipCloud = ChipCloud(this, cvMoreChips, config)
        val demoArray = resources.getStringArray(R.array.array_more_chips)
        chipCloud.addChips(demoArray)

        chipCloud.setListener { index, checked, userClick ->
            if (userClick) {
                intent.putExtra("data",chipCloud.getLabel(index))
                setResult(intent.getIntExtra("code",4),intent)
                finish()
            }

        }
    }


}
