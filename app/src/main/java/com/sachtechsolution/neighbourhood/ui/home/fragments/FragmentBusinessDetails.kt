package com.sachtechsolution.neighbourhood.ui.home.fragments

import android.os.Bundle
import android.view.View
import com.example.manish.internetstatus.OttoBus
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment

import com.sachtechsolution.neighbourhood.ui.home.adapters.CommentsAdapter
import com.sachtechsolution.neighbourhood.ui.home.adapters.MembersAdapter
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_business_details.*

class FragmentBusinessDetails:BaseFragment() {
    var mAdapter:CommentsAdapter?=null
    var membersAdapter:MembersAdapter?=null
    override fun viewToCreate(): Int {
        return R.layout.fragment_business_details
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mAdapter= CommentsAdapter(this.activity!!, baseAcitivityListener)
        comments_rv.adapter=mAdapter
        membersAdapter=MembersAdapter(this.activity!!)
        members_rv.adapter=membersAdapter
        onClick()
    }


    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
    }


    @Subscribe
    fun getMessage(internetStatus:String) {

        if (internetStatus.equals("Not connected to Internet")){
            InternetStatusDialogFragment.newInstance().show(fragmentManager, "option_fragment")
        }else {
            showToast("internet enabled ")

        }

    }

    override fun onResume() {
        super.onResume()
        OttoBus.bus.register(this)
    }

    private fun onClick() {
        recommend.setOnClickListener {
            baseAcitivityListener.navigator.replaceFragment(FragmentAddComment::class.java,true)
        }
        add_comment.setOnClickListener {
            baseAcitivityListener.navigator.replaceFragment(FragmentAddComment::class.java,true)
        }
        top_heading.setOnClickListener {
            baseAcitivityListener.navigator.replaceFragment(FragmentNeighbourhood::class.java,true)
        }
        business_details_back.setOnClickListener {
            baseAcitivityListener.navigator.replaceFragment(FragmentRecommendations::class.java,true)
        }

    }
}