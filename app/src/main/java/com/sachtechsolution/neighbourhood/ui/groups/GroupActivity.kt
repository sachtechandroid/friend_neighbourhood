package com.sachtechsolution.neighbourhood.ui.groups

import android.os.Bundle
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseActivity

class GroupActivity : BaseActivity() {

    override fun fragmentContainer(): Int {
        return R.id.groupContainer
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_group)
        navigator.replaceFragment(GroupListFragment::class.java,true)
    }
}
