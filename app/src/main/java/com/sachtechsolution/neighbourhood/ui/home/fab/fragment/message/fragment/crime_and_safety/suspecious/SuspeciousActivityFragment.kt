package com.sachtechsolution.neighbourhood.ui.home.fab.fragment.message.fragment.crime_and_safety.suspecious

import android.os.Bundle
import android.view.View
import com.example.manish.internetstatus.OttoBus
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.sachtechsolution.neighbourhood.ui.home.fab.fragment.message.fragment.crime_and_safety.a_crime.AddCrimeFragment
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_crime.*

class SuspeciousActivityFragment:BaseFragment(), View.OnClickListener {

    override fun viewToCreate(): Int {
        return R.layout.fragment_suspecious_activity
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //listner
        btnBack.setOnClickListener(this)
        btnGotIt.setOnClickListener(this)
    }


    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
    }


    @Subscribe
    fun getMessage(internetStatus:String) {

        if (internetStatus.equals("Not connected to Internet")){
            InternetStatusDialogFragment.newInstance().show(fragmentManager, "option_fragment")
        }else {
            showToast("internet enabled ")

        }

    }

    override fun onResume() {
        super.onResume()
        OttoBus.bus.register(this)
    }

    override fun onClick(p0: View?) {
        when(p0!!.id)
        {
            R.id.btnBack->activity!!.onBackPressed()
            R.id.btnGotIt-> baseAcitivityListener.navigator.replaceFragment(AddSuspeciousFragment::class.java,true)
        }
    }
}