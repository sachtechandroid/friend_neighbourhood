package com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments

import android.os.Bundle
import android.util.Log
import android.view.View
import com.google.firebase.firestore.DocumentSnapshot
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.gone
import com.sachtechsolution.neighbourhood.extension.visible
import com.sachtechsolution.neighbourhood.model.Post
import com.sachtechsolution.neighbourhood.model.User
import com.sachtechsolution.neighbourhood.ui.home.adapters.HomePagePostAdapter
import kotlinx.android.synthetic.main.fragment_view_interests.*

class FragmentViewInterests : BaseFragment(), HomePagePostAdapter.OpenBottomSheet {

    override fun OnOpenBottomSheet(pid: String, position: Int, type: String, post: Post, uid: String) {

    }

    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }
    val adapter by lazy { HomePagePostAdapter(context!!, this, baseAcitivityListener, pref)}


    override fun viewToCreate(): Int {
        return R.layout.fragment_view_interests
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val type=arguments!!.getString("type")
        getPosts(type)
        recyclerView.adapter=adapter

        icBack.setOnClickListener { activity!!.onBackPressed() }
        add_post.setOnClickListener {
            var bundle=Bundle()
            bundle.putString("type",type)
            baseAcitivityListener.navigator.replaceFragment(FragmentAaddInterestsPost::class.java,true,bundle) }
    }

    lateinit private var postLis: MutableList<DocumentSnapshot>

    private fun getPosts(type: String?) {
        showProgress()
        txtEmpty.visible()
        txtEmpty.text = "Loading posts"
        fireStoreHelper.getInterestsPosts(type =type!!, zipCode = pref.getUserFromPref()!!.zipCode).addOnCompleteListener {
            if (it.isSuccessful) {
                hideProgress()
                hideProgress()
                postLis = it.result.documents
                if (postLis.isEmpty())
                    txtEmpty.text = "No post yet. To create post click on +."
                postCount = -1
                getPost()
            } else {
                //showToast("something went wrong")
            }
        }.addOnFailureListener {
            Log.e("", "")
        }
    }

    private var postCount = -1

    private fun getPost() {
        postCount++
        if (postLis.size > postCount) {
            var post = postLis[postCount].toObject(Post::class.java)
            post!!.pid = postLis[postCount].id
            /*if (isPostHasTime(post.date.time) && home_welcome_text != null)*/
            getPostUser(post)
        }
    }


    private fun getPostUser(post: Post) {
        fireStoreHelper.getUser(post!!.uid).addOnCompleteListener {
            if (it.isSuccessful) {
                if (it.result.exists()) {
                    val user = it.result.toObject(User::class.java)
                    fireStoreHelper.getThanks(post.pid).addOnCompleteListener {
                        if (it.isSuccessful) {
                            txtEmpty?.gone()
                            var isThanked = false
                            if (it.result.exists())
                                isThanked = true
                            if (post.type.equals(Post.postType.POLL)) {
                                fireStoreHelper.getIsPollAnswered(post.pid).addOnCompleteListener {
                                    if (it.isSuccessful) {
                                        var isPolled = false
                                        if (it.result.documents.size > 0)
                                            isPolled = true
                                        adapter!!.addItem(post, user!!, isThanked, isPolled)
                                        getPost()
                                    } else {
                                        adapter!!.addItem(post, user!!, isThanked, false)
                                        getPost()
                                    }
                                }.addOnFailureListener {
                                    getPost()
                                    showToast("")
                                }.addOnCanceledListener {
                                    showToast("")
                                    getPost()
                                }
                            } else {
                                getPost()
                                adapter!!.addItem(post, user!!, isThanked, false)
                            }
                            txtEmpty?.gone()
                        } else {
                            getPost()
                        }
                    }
                }
            }
        }.addOnFailureListener { getPost() }
    }
}