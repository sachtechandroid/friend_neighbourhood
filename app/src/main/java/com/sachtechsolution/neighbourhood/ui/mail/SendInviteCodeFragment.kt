package com.sachtechsolution.neighbourhood.ui.mail.data

import android.os.Bundle
import android.util.Patterns
import android.view.View
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.constants.InvitationCode
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.showToast
import com.sachtechsolution.neighbourhood.intracter.Api_Hitter
import kotlinx.android.synthetic.main.view_invitecode.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*


class SendInviteCodeFragment : BaseFragment(), View.OnClickListener {


    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }

    override fun viewToCreate(): Int {
        return R.layout.view_invitecode
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //listner
        ivClose.setOnClickListener(this)
        txtSendMail.setOnClickListener(this)
    }


    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.ivClose ->
                activity!!.onBackPressed()

            R.id.txtSendMail -> {
                val email = eMail.text.toString()

                if (Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                    //sendEmailRelay(email)
                    val min = 2045
                    val max = 9044
                    val inviteCode = Random().nextInt(max - min + 1) + min
                    //validateUid(inviteCode.toString(), email)
                    showProgress()
                    validateCode(inviteCode = inviteCode.toString(), email = email)

                } else
                    context!!.showToast("Enter valid email address")

            }
        }
    }


    private fun validateCode(inviteCode: String, email: String) {

        fireStoreHelper.isInvitationCodePresent(inviteCode).addOnCompleteListener {
            if (it.isSuccessful) {
                if (it.result.exists()) {
                    hideProgress()
                    validateCode(inviteCode, email)
                    //showToast("Click again to invite friend")
                } else {
                    storeCodeToFirestore(inviteCode, email)
                }

            }
        }
    }

    private fun storeCodeToFirestore(inviteCode: String, email: String) {
        val user = pref!!.getUserFromPref()
        val uid = pref.getUserFromPref()!!.uid
        val code = InvitationCode(code = inviteCode, userUid = uid)
        fireStoreHelper.storeInvitationCode(code, inviteCode).addOnCompleteListener {
            if (it.isSuccessful) {
                //sendEmailRelay(inviteCode,email)
                hitAPiFOrSendMAil(inviteCode, email, user!!.firstName + " " + user!!.lastName)
            } else {
                hideProgress()
                showToast("Send again")
            }
        }
    }

    private fun hitAPiFOrSendMAil(inviteCode: String, email: String, name: String) {

        val callback = Api_Hitter.ApiInterface("http://stsmentor.com/").sendInvitation(name, email, inviteCode)
        callback.enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                hideProgress()
                context!!.showToast("unable to invite")
            }

            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                if (response.errorBody() != null) {
                    hideProgress()

                    context!!.showToast("Invitation Not sent")
                } else if (response.body() != null) {
                    eMail.setText("")
                    hideProgress()
                    context!!.showToast("Invitation sent")
                }
            }

        })
    }
}