package com.sachtechsolution.neighbourhood.ui.extra.fragments

import android.os.Bundle
import android.support.v4.view.ViewPager
import android.view.View
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.ui.extra.ImageViewPagerAdapter
import kotlinx.android.synthetic.main.fragment_image_view.*

class ImageViewFragment() : BaseFragment() {

    override fun viewToCreate(): Int {
        return R.layout.fragment_image_view
    }

    val images by lazy { arguments?.getString("images") ?: "" }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val size= images.split("#").size-1
        image_view_pager.adapter = ImageViewPagerAdapter(activity!!.supportFragmentManager, images)
        image_view_pager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {

            override fun onPageScrollStateChanged(p0: Int) {
            }

            override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {
                image_count.text = ""+(p0+1)+" of $size"
            }

            override fun onPageSelected(p0: Int) {
            }
        })
    }
}
