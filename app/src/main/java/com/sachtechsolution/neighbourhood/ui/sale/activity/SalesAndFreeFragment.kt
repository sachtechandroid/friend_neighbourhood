package com.sachtechsolution.neighbourhood.ui.sale.activity

import android.os.Bundle
import android.view.View
import com.example.manish.internetstatus.OttoBus
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.DocumentSnapshot
import com.google.firebase.firestore.QuerySnapshot
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.constants.Constants
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.gone
import com.sachtechsolution.neighbourhood.extension.visible
import com.sachtechsolution.neighbourhood.model.Post
import com.sachtechsolution.neighbourhood.model.User
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.sachtechsolution.neighbourhood.ui.home.fab.fragment.message.fragment.sale_and_free.fragment.ForSaleAndFreeFragment
import com.sachtechsolution.neighbourhood.ui.postDetails.SalePostDetailActivity
import com.sachtechsolution.neighbourhood.ui.sale.adapter.SaleTopMenuAdapter
import com.sachtechsolution.neighbourhood.ui.sale.adapter.SalesAndFreeImageAdapter
import com.sachtechsolution.neighbourhood.ui.sale.adapter.SalesOptionMenuAdapter
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_sale_and_free.*


class SalesAndFreeFragment : BaseFragment(), FragmentSaleShowingDialog.Showing, FragmentSaleSortDialog.Sorting, SalesAndFreeImageAdapter.OpenDetailActivity, SaleTopMenuAdapter.Type, SalesOptionMenuAdapter.Sorting {

    private val saleTopMenuAdapter by lazy { SaleTopMenuAdapter(this,context) }
    private val salesAndFreeImageAdapter by lazy { SalesAndFreeImageAdapter(activity!!, this) }
    private val salesOptionMenuAdapter by lazy { SalesOptionMenuAdapter(this) }
    private var saleItems: MutableList<DocumentSnapshot>? = null
    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }
    val uid by lazy { fireStoreHelper.currentUserUid() }
    var listner: Task<QuerySnapshot>? = null
    var user: User? = null
    var firstFilter = ""
    val currentUser by lazy { pref.getUserFromPref() }
    var secondFilter = ""
    var post = ArrayList<Post>()

    override fun onShowing(text: String) {

        if (text.equals("Street")){
            updateAdapter(filterByStreet(post))}
        else if (text.equals("Society")){
            updateAdapter(filterbySociety(post))}
        else{
            updateAdapter(post)}


    }

    private fun filterbySociety(list: ArrayList<Post>): ArrayList<Post> {
        val filterList = arrayListOf<Post>()
        list.forEach {
            if (it.society.equals(currentUser!!.society))
                filterList.add(it)
        }
        return filterList
        }

    private fun filterByStreet(list: ArrayList<Post>): ArrayList<Post> {

        val filterList = arrayListOf<Post>()
        list.forEach {
            if (it.sale!!.street.equals(currentUser!!.street))
                filterList.add(it)
        }
        return filterList
        }


    override fun onSort(text: String) {
      //   showToast(text)
        if (salesAndFreeImageAdapter.list.size > 0) {
            if (text.equals("Relevance"))
                updateAdapter(filterListRelevance(salesAndFreeImageAdapter.list))
            else if (text.equals("Newest First"))
                updateAdapter(filterList(salesAndFreeImageAdapter.list))
            else
                updateAdapter(filterListPrice(salesAndFreeImageAdapter.list))
        }
    }

    private fun filterListRelevance(list: ArrayList<Post>): ArrayList<Post> {
        val filterList = arrayListOf<Post>()
        filterList.addAll(list)
        return filterList.apply { sortBy { it -> it.sale!!.type } }
    }

    private fun filterListPrice(list: ArrayList<Post>): ArrayList<Post> {
        val filterList = arrayListOf<Post>()
        filterList.addAll(list)
        return filterList.apply { sortBy { it -> it.sale!!.price } }
    }

    private fun filterListDiscount(list: ArrayList<Post>): ArrayList<Post> {
        val filterList = arrayListOf<Post>()
        list.forEach {
            if (it.sale!!.discount != 0)
                filterList.add(it)
        }
        return filterList
    }

    private fun filterList(list: ArrayList<Post>): ArrayList<Post> {
        val filterList = arrayListOf<Post>()
        filterList.addAll(list)
        return filterList.apply { sortByDescending { bean -> bean.date } }
    }

    private fun updateAdapter(list: ArrayList<Post>) {
        salesAndFreeImageAdapter.clearAll()
        salesAndFreeImageAdapter.update(list)
        salesAndFreeImageAdapter.notifyDataSetChanged()


    }

    override fun onClickSorting(sorting: String) {
        if (sorting.equals(SalesOptionMenuAdapter.SHOWING)) {
            FragmentSaleShowingDialog.newInstance(this).show(fragmentManager, "option_fragment")

        } else if (sorting.equals(SalesOptionMenuAdapter.DISCOUNT)) {
            updateAdapter(filterListDiscount(post))
        } else if (sorting.equals(SalesOptionMenuAdapter.SORT)) {

            FragmentSaleSortDialog.newInstance(this).show(fragmentManager, "option_fragment")
        } else if (sorting.equals(SalesOptionMenuAdapter.FREE)) {
            secondFilter = sorting
            updateAdapter(filterListFree(post))
        }
    }

    private fun filterListFree(list: ArrayList<Post>): ArrayList<Post> {

        val filterList = arrayListOf<Post>()
        list.forEach {
            if (it.sale!!.price == 0)
                filterList.add(it)
        }
        return filterList
    }


    override fun SaleType(s: String) {
        if (firstFilter.equals(s))
            firstFilter = ""
        else
            firstFilter = s
        showItems()
    }


    override fun onOpenActivity(post: Post, user: User, position: Int) {
        baseAcitivityListener.navigator.openActivity(SalePostDetailActivity::class.java, Bundle().apply {
            putParcelable("post", post)
            putParcelable("user", user)
            putInt("position", position)
        })
    }


    override fun viewToCreate(): Int {
        return R.layout.fragment_sale_and_free
    }


    @Subscribe
    fun getMessage(internetStatus: String) {

        if (internetStatus.equals("Not connected to Internet")) {
            InternetStatusDialogFragment.newInstance().show(fragmentManager, "option_fragment")
        } else {
            showToast("internet enabled ")

        }

    }


    override fun onResume() {
        super.onResume()
        OttoBus.bus.register(this)
        if (Constants.DELETED_POSITION != -1) {
            val position = Constants.DELETED_POSITION
            salesAndFreeImageAdapter!!.removeItem(position)
            Constants.DELETED_POSITION = -1
        }
/*        salesAndFreeImageAdapter.clearAll()
        salesAndFreeImageAdapter.notifyDataSetChanged()
        getPosts("Your Items")*/
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpRecyclerView()
        onClick()
        sale_text.visible()
        showProgress()

        fireStoreHelper.getSalePosts(pref.getUserFromPref()!!.zipCode).addOnCompleteListener {
            if (it.isSuccessful) {
                saleItems = it.result.documents
                if (saleItems!!.size == 0)
                    sale_text.text = "No item found"
                showItems()
                hideProgress()
            } else {
                hideProgress()
                showToast("something went wrong")
            }
        }.addOnFailureListener {
            hideProgress()
            showToast("something went wrong")
        }
    }

    private fun showItems() {
        post.clear()
        salesAndFreeImageAdapter.clearAll()
        saleItems!!.forEach {
            var post = it.toObject(Post::class.java)
            post?.pid = it.id
            firstFilter(post)
        }
    }

    private fun firstFilter(post: Post?) {
        if (firstFilter.isNullOrEmpty())
            secondFilter(post)
        else if (firstFilter.equals("Your Items")) {
            if (post!!.uid.equals(uid))
                showItem(post)
        } else {
            if (post!!.sale!!.type.equals(firstFilter))
                secondFilter(post)
        }
    }

    private fun secondFilter(post: Post?) {
        if (secondFilter.isNullOrEmpty())
            showItem(post)
        else if (secondFilter.equals("Free")) {
            if (post!!.sale!!.price == 0)
                showItem(post)
        } /*else if (secondFilter.equals("Broader local area")) {
            if (post!!.privacy.contains(pref.getUserFromPref()!!.society))
                showItem(post)
        }*/
    }

    private fun showItem(post: Post?) {
        sale_text?.gone()
        this.post.add(post!!)
        salesAndFreeImageAdapter.addItem(post!!)
        salesAndFreeImageAdapter.notifyDataSetChanged()

    }

    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
        listner = null
    }

    private fun onClick() {
        btnBack.setOnClickListener {
            activity!!.onBackPressed()
        }
        sale_free_fab.setOnClickListener {
            Constants.NEXT = "sale"
            baseAcitivityListener.navigator.replaceFragment(ForSaleAndFreeFragment::class.java, true)
        }
    }

    private fun setUpRecyclerView() {
        top_menu_recycler.adapter = saleTopMenuAdapter
        image_recycler.adapter = salesAndFreeImageAdapter
        top_menu_option_recycler.adapter = salesOptionMenuAdapter
    }
}
