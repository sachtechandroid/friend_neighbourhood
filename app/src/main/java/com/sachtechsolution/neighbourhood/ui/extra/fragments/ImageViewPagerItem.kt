package com.sachtechsolution.neighbourhood.ui.extra.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import com.bumptech.glide.Glide
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_image_view_pager_item.*

@SuppressLint("ValidFragment")
class ImageViewPagerItem(val s: String) : BaseFragment() {


    override fun viewToCreate(): Int {
        return R.layout.fragment_image_view_pager_item
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Glide.with(activity!!).load(s).into(pager_item_image)
    }

}
