package com.sachtechsolution.neighbourhood.ui.sale.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.RelativeLayout
import com.sachtechsolution.neighbourhood.R
import kotlinx.android.synthetic.main.sale_top_menu_item.view.*

class SaleTopMenuAdapter(var lister: Type,val context: Context?) : RecyclerView.Adapter<SaleTopMenuAdapter.ViewHolder>() {
    val catogriesList= arrayListOf<String>("Your Items","Appliances","Baby & Kids",
            "Bicycles","Clothing & Accessories","Electronics","Furniture","Garage Sales"
    ,"Garden","Home Decor","Housing","Motoring","Other","Pet Supplies","Sports & Outdoors",
            "Tickets","Tools","Toys & Games")


    private val topMenuImages = arrayListOf<Int>(R.drawable.your_items, R.drawable.baby_kids,
            R.drawable.furniture, R.drawable.home_decor, R.drawable.appliances,
            R.drawable.automotive, R.drawable.bicycles, R.drawable.clothing_accessories,
            R.drawable.electronics, R.drawable.garage_sales, R.drawable.garden,
            R.drawable.housing, R.drawable.other, R.drawable.pet_supplies,
            R.drawable.sports_outdoors, R.drawable.tickets, R.drawable.tools,
            R.drawable.toys_games)

    private var lastSelectedPosition = -1
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val v: View = LayoutInflater.from(p0.context).inflate(R.layout.sale_top_menu_item, p0, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return catogriesList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.itemView.menu_title.text = catogriesList[position]
        holder.itemView.menu_image.setImageResource(topMenuImages[position])
        if (lastSelectedPosition == position) {
            holder.itemView.view.setBackgroundColor(context!!.resources.getColor(R.color.colorPrimary))
            holder.itemView.menu_title.setTextColor(context!!.resources.getColor(R.color.white))
        } else {
            holder.itemView.view.setBackgroundColor(context!!.resources.getColor(R.color.white))
            holder.itemView.menu_title.setTextColor(context!!.resources.getColor(R.color.black))
        }
    }

    interface Type{
        fun SaleType(s: String)
    }
    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var view: RelativeLayout = itemView.findViewById(R.id.view)

        init {

            view.setOnClickListener {
                lastSelectedPosition = adapterPosition
                notifyDataSetChanged()

                lister.SaleType(catogriesList[lastSelectedPosition])

            }
        }
    }

}