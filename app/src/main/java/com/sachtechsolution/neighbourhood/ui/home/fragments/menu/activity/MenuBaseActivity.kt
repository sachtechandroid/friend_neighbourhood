package com.sachtechsolution.neighbourhood.ui.home.fragments.menu.activity

import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseActivity
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.constants.Constants
import com.sachtechsolution.neighbourhood.extension.getLocationMode
import com.sachtechsolution.neighbourhood.extension.isLocationEnabled
import com.sachtechsolution.neighbourhood.extension.showToast
import com.sachtechsolution.neighbourhood.model.Post
import com.sachtechsolution.neighbourhood.ui.RiverstoneFragmenthome.fragments.menu.fragments.GeneralFragment
import com.sachtechsolution.neighbourhood.ui.bookmark.BookmarkFragment
import com.sachtechsolution.neighbourhood.ui.home.fab.fragment.message.fragment.CreatePostFragment
import com.sachtechsolution.neighbourhood.ui.home.fragments.FragmentRecommendations
import com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.*
import com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings.profile.fragment.FragmentRealEstate
import com.sachtechsolution.neighbourhood.ui.local_agencies.LocalAgenciesFragment
import com.sachtechsolution.neighbourhood.ui.lost_found.LostFoundFragment
import com.sachtechsolution.neighbourhood.ui.mail.data.SendInviteCodeFragment
import com.sachtechsolution.neighbourhood.ui.pet_directory.fragment.PetDirectoryFragment
import com.sachtechsolution.neighbourhood.ui.sale.activity.SalesAndFreeFragment

class MenuBaseActivity : BaseActivity() {

    override fun fragmentContainer(): Int {
        return R.id.container
    }
    var fragment: String?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)
        if (intent.getBundleExtra(CREATEPOST)==null) {

            openFragment()
        }
        else
        {
            if (isLocationEnabled(this)&&getLocationMode() == 3) {
                navigator.replaceFragment(CreatePostFragment::class.java, true,
                        Bundle().apply {
                            putString("id", intent.getBundleExtra(CREATEPOST)["id"].toString())
                            putString("gid", intent.getBundleExtra(CREATEPOST)["gid"].toString())
                        }) }
            else
            {
                showToast("Enable Location with high accuracy option")
                startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
            }

        }


    }

    private fun openFragment() {

        fragment = intent.getStringExtra(FRAGMENT)
        when (fragment) {
            FORSALE_AND_FREE -> navigator.replaceFragment(SalesAndFreeFragment::class.java, true)
            LOST_AND_FOUND -> navigator.replaceFragment(LostFoundFragment::class.java, true)
            CRIME_AND_SAFETY -> navigator.replaceFragment(CrimeSafetyFragment::class.java, true)
            GENERAL -> navigator.replaceFragment(GeneralFragment::class.java, true)
            DOCUMENTS -> navigator.replaceFragment(DocumentFragment::class.java, true)
            PROFILE -> navigator.replaceFragment(ProfileFragment::class.java, true)
            NEIGHBOURS -> {
                navigator.openActivity(NeighborsFragment::class.java)
                finish()
            }
            SETTINGS -> navigator.replaceFragment(SettingsFragment::class.java, true)
            SOCIETY -> navigator.replaceFragment(SocietyFragment::class.java, true)
            BOOKMARK -> navigator.replaceFragment(BookmarkFragment::class.java, true)
            PET_DIRECTORY -> navigator.replaceFragment(PetDirectoryFragment::class.java, true)

            //CREATEPOST -> navigator.replaceFragment(GroupListFragment::class.java, true)
            //RECOMMENDATIONS -> navigator.replaceFragment(RecommendationsFragment::class.java, true)
            REAL_ESTATE -> navigator.replaceFragment(FragmentRealEstate::class.java, true)
            EVENT -> navigator.replaceFragment(FragmentEvents::class.java, true)
            GUIDE -> navigator.replaceFragment(GuideLineFragment::class.java, true)
            ALLINTEREST -> navigator.replaceFragment(FragmentAllInterests::class.java, true)
            INVITATION -> navigator.replaceFragment(SendInviteCodeFragment::class.java, true)
            RECOMMENDATIONS -> navigator.replaceFragment(FragmentRecommendations::class.java, true)
            LOCAL_AGENCY -> navigator.replaceFragment(LocalAgenciesFragment::class.java, true)
            CREATEPOST -> navigator.replaceFragment(LocalAgenciesFragment::class.java, true)

        }
    }


    companion object {
        var FRAGMENT = "fragment"
        var LOCAL_AGENCY = "agency"
        var INVITATION = "invitation"
        var FORSALE_AND_FREE = "forSaleAndFree"
        var RECOMMENDATIONS = "recommendations"
        var REAL_ESTATE = "realEstate"
        var EVENT = "event"
        var CRIME_AND_SAFETY = "cimeAndSafety"
        var ALLINTEREST = "menu_Interest"
        var LOST_AND_FOUND = "lostAndFound"
        var DOCUMENTS = "documents"
        var GENERAL = "general"
        var SETTINGS = "settings"
        var BOOKMARK = "bookmark"
        var HELP = "help"
        var NEIGHBOURS = "neighbour"
        var PROFILE = "profile"
        var PET_DIRECTORY = "pet_directory"
        var SOCIETY = "society"
        var CREATEPOST = "createPost"
        var GUIDE = "guideline"
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        // check if the request code is same as what is passed  here it is 2
        if (requestCode == 2) {
            val message = data!!.getCharSequenceArrayListExtra("recommendation")
        }
    }
}
