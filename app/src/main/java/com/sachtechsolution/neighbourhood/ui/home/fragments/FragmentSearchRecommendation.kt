package com.sachtechsolution.neighbourhood.ui.home.fragments

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import com.example.manish.internetstatus.OttoBus
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.constants.NearestBussiness
import com.sachtechsolution.neighbourhood.intracter.Api_Hitter
import com.sachtechsolution.neighbourhood.model.places_bussiness.ResultsItem
import com.sachtechsolution.neighbourhood.model.places_bussiness.SearchPlaced
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.sachtechsolution.neighbourhood.ui.home.activities.FragmentAddRecommendation
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_search_recommendations.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FragmentSearchRecommendation:BaseFragment(),SearchBussinessAdapter.OnClick {

    override fun onBussinessClick(mbundle: String, resultsItem: ResultsItem?) {
        val nbean = NearestBussiness(icon = resultsItem!!.icon!!,id= resultsItem.id!!,vicinity = resultsItem.vicinity!!,name = resultsItem.name!!, recCount = resultsItem.rating.toString())
        var bundle=Bundle()
        bundle.putString("id","type")
        bundle.putParcelable(FragmentAddRecommendation.DATA,nbean)
        baseAcitivityListener.navigator.replaceFragment(FragmentRecommendationDescription::class.java,true,bundle)
    }

    var bussinessCallBack: Call<SearchPlaced>?=null


    override fun viewToCreate(): Int {
        return R.layout.fragment_search_recommendations
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        search_back.setOnClickListener { activity!!.onBackPressed() }
        search.addTextChangedListener(object :TextWatcher{
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                if (bussinessCallBack!=null)
                    bussinessCallBack!!.cancel()
                    searchBussiniess(p0.toString())
            }

        })
    }


    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
    }


    @Subscribe
    fun getMessage(internetStatus:String) {

        if (internetStatus.equals("Not connected to Internet")){
            InternetStatusDialogFragment.newInstance().show(fragmentManager, "option_fragment")
        }else {
            showToast("internet enabled ")

        }

    }

    override fun onResume() {
        super.onResume()
        OttoBus.bus.register(this)
    }

    fun searchBussiniess(search: String) {
        bussinessCallBack = Api_Hitter.ApiInterface("https://maps.googleapis.com/maps/api/").searchBussiness(search,pref.getUserFromPref()!!.lat.toString()+","+pref.getUserFromPref()!!.lng.toString())
        bussinessCallBack!!.enqueue(object : Callback<SearchPlaced> {
            override fun onResponse(call: Call<SearchPlaced>, response: Response<SearchPlaced>) {
                if (response.body()!!.results!!.isNotEmpty()) {
                    response.body()!!.results
                    var data = response.body()!!.results
                    if (data!!.isNotEmpty()&&businesses_rv!=null) {
                        var bussinessesAdapter = SearchBussinessAdapter(activity!!, this@FragmentSearchRecommendation, data,type = "id")
                        businesses_rv.adapter = bussinessesAdapter
                    }
                }
            }

            override fun onFailure(call: Call<SearchPlaced>, t: Throwable) {

            }
        })
    }
}