package com.sachtechsolution.neighbourhood.ui.home.fragments

import android.os.Bundle
import android.view.View
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseActivity
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import kotlinx.android.synthetic.main.activity_comment_description.*

class ActivityCommentDescription:BaseActivity(){
    override fun fragmentContainer(): Int {
        return R.id.comment_description_container
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_comment_description)
        recommend_back.setOnClickListener {
            navigator.replaceFragment(FragmentCategory::class.java,true)
        }

    }
}