package com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings.profile

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.TextView
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.R.id.count
import com.sachtechsolution.neighbourhood.basepackage.base.BaseActivity
import com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings.profile.fragment.AboutYourselfFragment
import kotlinx.android.synthetic.main.activity_fill_up_profiel.*

class FillUpProfileActivity : BaseActivity(){

    override fun fragmentContainer(): Int {
        return R.id.cointainer
    }

    companion object {

           var mcount:TextView?=null

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fill_up_profiel)
        mcount=findViewById(R.id.count)
        navigator.replaceFragment(AboutYourselfFragment::class.java,true)

    }
}
