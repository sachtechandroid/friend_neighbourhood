package com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings

import android.os.Bundle
import android.view.View
import com.example.manish.internetstatus.OttoBus
import com.google.firebase.auth.FirebaseAuth
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.model.User
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.ProfileFragment
import com.sachtechsolution.neighbourhood.ui.login.fragments.EditAddressFragment
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_edit_profile.*

class EditProfileFragment:BaseFragment(), View.OnClickListener {
    var user: User?=null
    val firestore by lazy { FireStoreHelper.getInstance() }

    override fun viewToCreate(): Int {
        return R.layout.fragment_edit_profile
    }


    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
    }


    @Subscribe
    fun getMessage(internetStatus:String) {

        if (internetStatus.equals("Not connected to Internet")){
            InternetStatusDialogFragment.newInstance().show(fragmentManager, "option_fragment")
        }else {
            showToast("internet enabled ")
        }

    }

    override fun onResume() {
        super.onResume()
        OttoBus.bus.register(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        user = pref.getUserFromPref()
        etFirstName.setText(user!!.firstName)
        etLastName.setText(user!!.lastName)
        etEmail.setText(user!!.email)
        etAddress.setText(user!!.address)

        //listner
        cancelLayout.setOnClickListener(this)
        nextLayout.setOnClickListener(this)
        txtProfile.setOnClickListener(this)
        etAddress.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {

        when(p0!!.id)
        {
            R.id.etAddress->{
                baseAcitivityListener.navigator.replaceFragment(EditAddressFragment::class.java,true)
            }
            R.id.cancelLayout->activity!!.onBackPressed()

            R.id.nextLayout-> updateUser()

            //R.id.txtProfile-> baseAcitivityListener.navigator.replaceFragment(ProfileFragment::class.java,true)
        }
    }

    fun updateUser(){

        val firstName= etFirstName.text.toString()
        val lastName= etLastName.text.toString()
        val adddress=etAddress.text.toString()
        user=pref.getUserFromPref()
        user!!.address=adddress
        user!!.firstName=firstName
        user!!.lastName=lastName


            showProgress()
            firestore.setUser(user = user!!,uid = user!!.uid).addOnCompleteListener {
                if (it.isSuccessful){
                    showToast("updated")
                    hideProgress()
                    getUser()
                }
                else{
                    showToast("error")
                    hideProgress()
                }
            }.addOnFailureListener {
                showToast("fail")
                hideProgress()
            }

    }
    fun getUser()
    {
        firestore.getUser(firestore.currentUserUid()).addOnCompleteListener {
            if (it.isSuccessful)
            {
                if(it.result.exists()){
                    val user = it.result.toObject(User::class.java)
                    pref.setUserToPref(user!!)
                }
            }
        }

    }

}