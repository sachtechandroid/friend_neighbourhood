package com.sachtechsolution.neighbourhood.ui.login.fragments

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.content.Intent
import android.location.Location
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import com.example.manish.internetstatus.OttoBus
import com.facebook.AccessToken
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.Status
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.places.ui.PlaceSelectionListener
import com.google.android.gms.location.places.ui.SupportPlaceAutocompleteFragment
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.LocationBFragment
import com.sachtechsolution.neighbourhood.extension.getLocationMode
import com.sachtechsolution.neighbourhood.extension.isNetConnected
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings.MapFragmentAdapter
import com.sachtechsolution.neighbourhood.ui.login.SamplePresenter
import com.squareup.otto.Subscribe
import com.yayandroid.locationmanager.configuration.Configurations
import com.yayandroid.locationmanager.configuration.LocationConfiguration
import com.yayandroid.locationmanager.constants.FailType
import com.yayandroid.locationmanager.constants.ProcessType
import kotlinx.android.synthetic.main.fragment_sign_up_address.*

class SignUpAddressFragment : LocationBFragment(), AddressConfirmationDialogFragment.AddressConfirm, SamplePresenter.SampleView, OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, View.OnClickListener, MapFragmentAdapter.OpenDone {

    override fun onEdit() {
        baseAcitivityListener.navigator.replaceFragment(EditAddressFragment::class.java, true)
    }

    override fun onConfirm(s: String) {

        baseAcitivityListener.navigator.replaceFragment(SignUpCredentialFragment::class.java, true)
    }

    private var mMap: GoogleMap? = null
    private lateinit var mGoogleClient: GoogleApiClient
    lateinit var mLocation: Location
    var selectedLatLong: LatLng? = null
    private var myMap: SupportMapFragment? = null
    var zipCode = ""
    private var progressDialog: ProgressDialog? = null
    var fragment: SupportPlaceAutocompleteFragment? = null
    private var samplePresenter: SamplePresenter? = null
    private var markerView: View? = null
    var token: AccessToken?=null
    var locationRequest: LocationRequest?=null

    @SuppressLint("MissingPermission")
    override fun onMapReady(p0: GoogleMap?) {
        mMap = p0!!
        mMap!!.uiSettings.isZoomControlsEnabled = true
        mMap!!.uiSettings.isMyLocationButtonEnabled = true
        mMap!!.isMyLocationEnabled = true

    }


    @Subscribe
    fun getMessage(internetStatus: String) {

        if (internetStatus.equals("Not connected to Internet")) {
            InternetStatusDialogFragment.newInstance().show(fragmentManager, "option_fragment")
        } else {
            showToast("internet enabled ")
        }

    }


    @SuppressLint("MissingPermission")
    override fun onConnected(p0: Bundle?) {

        locationRequest = LocationRequest.create()
                .setInterval(1000)
                .setFastestInterval(500)
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)


        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleClient)

        if (mLocation != null) {
            var mLat = mLocation.latitude
            var mLog = mLocation.longitude
            var mLatLong = LatLng(mLat, mLog)
            selectedLatLong=mLatLong
            upDateMarker(mLatLong)
        }
    }

    override fun onConnectionSuspended(p0: Int) {
    }

    override fun OnEventDone() {
    }

    override fun viewToCreate(): Int {
        return R.layout.fragment_sign_up_address
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

         if (arguments!==null) {
           token = arguments!!.get("token") as AccessToken
       }

        fragment = childFragmentManager?.findFragmentById(R.id.place_autocomplete_fragment) as SupportPlaceAutocompleteFragment
        fragment!!.setBoundsBias(LatLngBounds(
                LatLng(-33.880490, 151.184363),
                LatLng(-33.858754, 151.229596)))

        fragment!!.setOnPlaceSelectedListener(object : PlaceSelectionListener {


            override fun onPlaceSelected(place: com.google.android.gms.location.places.Place) {
                //Toast.makeText(context, "SearchZip is : $place", Toast.LENGTH_SHORT).show()
                selectedLatLong = place.latLng
                upDateMarker(place.latLng)
            }

            override fun onError(status: Status) {

            }
        })

        //listners
        signup_address_find_neighbor_tv.setOnClickListener(this)
        signup_address_back_iv.setOnClickListener(this)
        enterManually.setOnClickListener(this)
    }


    override fun onClick(p0: View?) {
        if (isNetConnected()) {
            when (p0!!.id) {
                R.id.signup_address_find_neighbor_tv -> {
                    if (selectedLatLong == null)
                        showToast("Enter Location")
                    else {
                        //AddressConfirmationDialogFragment.newInstance(selectedLatLong,this,false).show(fragmentManager, "option_fragment")
                        if (token!==null)
                        {
                            baseAcitivityListener.navigator.replaceFragment(
                                    EditAddressFragment::class.java, true,
                                    Bundle().apply {
                                        putParcelable("latLong", selectedLatLong)
                                        putParcelable("token", token)
                                    })
                        }else{
                            baseAcitivityListener.navigator.replaceFragment(
                                    EditAddressFragment::class.java, true,
                                    Bundle().apply {
                                        putParcelable("latLong", selectedLatLong)
                                    })
                        }
                    }
                }
                R.id.signup_address_back_iv -> fragmentManager?.popBackStackImmediate()
                R.id.enterManually -> {
                    if (token!==null)
                    baseAcitivityListener.navigator.replaceFragment(
                            EditAddressFragment::class.java, true,Bundle().apply {
                        putParcelable("token", token)
                    })
                    else
                        baseAcitivityListener.navigator.replaceFragment(
                                EditAddressFragment::class.java, true)
                }
            }
        } else
            showToast("Enable internet")
    }

    var marker: Marker? = null
    private fun upDateMarker(pafKietLocation: LatLng) {
        if (marker == null)
            marker = mMap?.addMarker(MarkerOptions().position(pafKietLocation)
                    .title("Your Location").draggable(true))
        else
            marker?.position = pafKietLocation

        mMap?.setMinZoomPreference(14.0f)
        mMap?.setMaxZoomPreference(75.0f)
        mMap?.moveCamera(CameraUpdateFactory.newLatLng(pafKietLocation))

        mMap?.setOnMapClickListener { latLng ->

            selectedLatLong = latLng
            marker?.position = latLng

            // context!!.showToast(latLng.toString())
        }

        mMap?.setOnMarkerDragListener(object : GoogleMap.OnMarkerDragListener {
            override fun onMarkerDragStart(arg0: Marker) {}

            override fun onMarkerDragEnd(arg0: Marker) {
                Log.d("System out", "onMarkerDragEnd...")
                selectedLatLong = arg0.position
                mMap?.animateCamera(CameraUpdateFactory.newLatLng(arg0.position))
                //context!!.showToast(arg0.position.toString())
            }

            override fun onMarkerDrag(arg0: Marker) {}
        })
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        samplePresenter = SamplePresenter(this)
        getLocation()
    }

    override fun getLocationConfiguration(): LocationConfiguration {
        return Configurations.defaultConfiguration("Gimme the permission!", "Would you mind to turn GPS on?")
    }

    override fun onLocationChanged(location: Location) {
        samplePresenter!!.onLocationChanged(location)
    }

    override fun onLocationFailed(@FailType failType: Int) {
        samplePresenter!!.onLocationFailed(failType)
    }

    override fun onProcessTypeChanged(@ProcessType processType: Int) {
        samplePresenter!!.onProcessTypeChanged(processType)
    }

    override fun onResume() {
        super.onResume()
        OttoBus.bus.register(this)

        if (locationManager.isWaitingForLocation && !locationManager.isAnyDialogShowing) {
            displayProgress()
        }
    }

    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
        dismissProgress()
        fragment = null
    }

    private fun displayProgress() {
        if (progressDialog == null) {
            progressDialog = ProgressDialog(context)
            progressDialog!!.window!!.addFlags(Window.FEATURE_NO_TITLE)
            progressDialog!!.setMessage("Getting location...")
        }

        if (!progressDialog!!.isShowing) {
            progressDialog!!.show()
        }
    }

    override fun getText(): String {
        return "vfmlvbm"
    }

    override fun setText(text: String) {
        myMap = childFragmentManager.findFragmentById(R.id.map_fragment) as SupportMapFragment
        myMap?.getMapAsync(this)
        mGoogleClient = GoogleApiClient.Builder(activity!!)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .build()
        mGoogleClient.connect()
        markerView = LayoutInflater.from(activity).inflate(R.layout.marker_layout, null)

    }

    override fun updateProgress(text: String) {
        if (progressDialog != null && progressDialog!!.isShowing) {
            progressDialog!!.setMessage(text)
        }
    }

    override fun dismissProgress() {
        if (progressDialog != null && progressDialog!!.isShowing) {
            progressDialog!!.dismiss()
        }
    }


}
