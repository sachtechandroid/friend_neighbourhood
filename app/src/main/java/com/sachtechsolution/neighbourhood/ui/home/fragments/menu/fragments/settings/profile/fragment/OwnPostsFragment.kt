package com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings.profile.fragment

import android.os.Bundle
import android.util.Log
import android.view.View
import com.example.manish.internetstatus.OttoBus
import com.google.firebase.firestore.DocumentSnapshot
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.constants.Constants
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.gone
import com.sachtechsolution.neighbourhood.extension.isPostHasTime
import com.sachtechsolution.neighbourhood.model.Post
import com.sachtechsolution.neighbourhood.model.User
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.sachtechsolution.neighbourhood.ui.home.adapters.HomePagePostAdapter
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_count_post_likes.*


class OwnPostsFragment : BaseFragment() {

    override fun viewToCreate(): Int {
        return R.layout.fragment_count_post_likes
    }

    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
    }


    @Subscribe
    fun getMessage(internetStatus:String) {

        if (internetStatus.equals("Not connected to Internet")){
            InternetStatusDialogFragment.newInstance().show(fragmentManager, "option_fragment")
        }else {
            showToast("internet enabled ")

        }

    }



    var adapter: HomePagePostAdapter? = null
    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }

    lateinit var postLis: MutableList<DocumentSnapshot>
    var postCount = -1


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ivBack.setOnClickListener { activity!!.onBackPressed() }
        adapter = HomePagePostAdapter(context!!, null, baseAcitivityListener, pref)
        rvCountPosts.adapter = adapter
        Constants.IS_THANKED_LIST.clear()
        Constants.IS_POLLED_LIST.clear()

        fireStoreHelper.getOwnPosts(pref.getUserFromPref()!!.uid).addOnCompleteListener {
            if (it.isSuccessful) {
                postLis = it.result.documents
                if (postLis.size == 0)
                    ownPostsMessage.text = "No post found"
                getPost()
            }
        }.addOnFailureListener {
            showToast(it.message!!)
        }
    }

    private fun getPost() {
        postCount++
        if (postLis.size > postCount) {
            var post = postLis[postCount].toObject(Post::class.java)
            post!!.pid = postLis[postCount].id
            if (!post.type.equals(Post.postType.FOR_SALE)) {
                if (isPostHasTime(post.date) && ownPostsMessage != null)
                    getPostUser(post)
            } else
                getPost()
        } else {
            if (adapter?.postsList?.size ?: 0 == 0)
               ownPostsMessage.text = "No post found"
        }
    }

    private fun getPostUser(post: Post) {
        fireStoreHelper.getUser(post!!.uid).addOnCompleteListener {
            if (it.isSuccessful) {
                if (it.result.exists()) {
                    val user = it.result.toObject(User::class.java)
                    fireStoreHelper.getThanks(post.pid).addOnCompleteListener {
                        if (it.isSuccessful) {
                            var isThanked = false
                            if (it.result.exists())
                                isThanked = true
                            if (post.type.equals(Post.postType.POLL)) {
                                fireStoreHelper.getIsPollAnswered(post.pid).addOnCompleteListener {
                                    if (it.isSuccessful) {
                                        var isPolled = false
                                        if (it.result.documents.size > 0)
                                            isPolled = true
                                        adapter!!.addItem(post, user!!, isThanked, isPolled)
                                        getPost()
                                    } else {
                                       adapter!!.addItem(post, user!!, isThanked, false)
                                        getPost()
                                    }
                                }.addOnFailureListener {
                                    getPost()
                                    showToast("")
                                }.addOnCanceledListener {
                                    showToast("")
                                    getPost()
                                }
                            } else {
                                getPost()
                               adapter!!.addItem(post, user!!, isThanked, false)
                            }
                            ownPostsMessage?.gone()
                        } else {
                            getPost()
                        }
                    }
                }
            }
        }.addOnFailureListener { getPost() }
    }

    override fun onResume() {
        super.onResume()
        OttoBus.bus.register(this)
        //getAllPost()
        if (Constants.POSITION != -1) {
            if (Constants.POST != null)
                adapter?.postsList?.set(Constants.POSITION, Constants.POST)
            adapter!!.notifyItemChanged(Constants.POSITION)
            Constants.POSITION = -1
            Constants.POST = null
        }
        if (Constants.DELETED_POSITION != -1) {
            val position = Constants.DELETED_POSITION
            adapter!!.deletePost(position)
            Constants.DELETED_POSITION = -1
        }
        intracterListner?.updateSelectedPos(0)
    }
}