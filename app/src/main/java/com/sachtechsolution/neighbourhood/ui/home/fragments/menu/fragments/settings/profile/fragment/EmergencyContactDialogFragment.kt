package com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings.profile.fragment

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import com.example.manish.internetstatus.OttoBus
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.ProgressDialogFragment
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.showToast
import com.sachtechsolution.neighbourhood.model.EmergencyContacts
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.squareup.otto.Subscribe
import gurtek.mrgurtekbasejava.base.ModelPrefrence
import kotlinx.android.synthetic.main.view_emergency_contacts.*


open class EmergencyContactDialogFragment : DialogFragment(), View.OnClickListener {

    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.ivClose ->
                dismiss()
            R.id.txtNext -> {
                val email = etEmailOne.text.toString().trim()
                val email2 = etEmailTwo.text.toString().trim()
                val email3 = etEmailThree.text.toString().trim()
                if (!Patterns.EMAIL_ADDRESS.matcher(email).matches() || !Patterns.EMAIL_ADDRESS.matcher(email2).matches()
                        || !Patterns.EMAIL_ADDRESS.matcher(email3).matches())
                    context!!.showToast("enter valid email")
                else {
                    uploadData(EmergencyContacts(email1 = email
                            , email2 = email2, email3 = email3))
                }
            }
        }
    }


    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
    }


    @Subscribe
    fun getMessage(internetStatus: String) {

        if (internetStatus.equals("Not connected to Internet")) {
            InternetStatusDialogFragment.newInstance().show(fragmentManager, "option_fragment")
        } else {
            context!!.showToast("internet enabled ")

        }

    }

    override fun onResume() {
        super.onResume()
        OttoBus.bus.register(this)
    }


    private fun uploadData(emergencyContacts: EmergencyContacts) {

        ProgressDialogFragment.showProgress(fragmentManager)
        fireStoreHelper.updateEmergencyContacts(emergencyContacts, pref!!.getUserFromPref()!!.uid).addOnCompleteListener {
            if (it.isSuccessful) {
                context!!.showToast("Updated")
                dismiss()
            } else {
                context!!.showToast("error")
                ProgressDialogFragment.hideProgress()
            }
            ProgressDialogFragment.hideProgress()
        }.addOnFailureListener {
            context!!.showToast("error")
            ProgressDialogFragment.hideProgress()
        }

    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)
        return inflater.inflate(R.layout.view_emergency_contacts, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        //listner
        ivClose.setOnClickListener(this)
        txtNext.setOnClickListener(this)
    }

    companion object {
        var pref: ModelPrefrence? = null
        fun newInstance(pref: ModelPrefrence): EmergencyContactDialogFragment {
            this.pref = pref
            return EmergencyContactDialogFragment()

        }
    }

}