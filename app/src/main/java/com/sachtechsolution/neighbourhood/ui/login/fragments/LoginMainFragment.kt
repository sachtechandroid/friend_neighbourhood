package com.sachtechsolution.neighbourhood.ui.login.fragments

import android.Manifest
import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.view.View
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.constants.Constants
import com.sachtechsolution.neighbourhood.extension.askForPermissions
import com.sachtechsolution.neighbourhood.extension.getLocationMode
import com.sachtechsolution.neighbourhood.extension.isPermissionsGranted
import kotlinx.android.synthetic.main.fragment_login_main.*

/* whoGuri garden/appliances/home_decor */
class LoginMainFragment : BaseFragment() {

    override fun viewToCreate(): Int {
        return R.layout.fragment_login_main
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        login_main_sign_in_tv.setOnClickListener {
            baseAcitivityListener.navigator.replaceFragment(LoginFragment::class.java, true)
        }
        login_main_find_neighbor_tv.setOnClickListener {
            if (isPermissionsGranted(context!!, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION)))
                if(context!!.getLocationMode() != 3)
                {
                    showToast("please select high accuracy option")
                    startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
                    // do stuff
                }else
                    baseAcitivityListener.navigator.replaceFragment(SignUpAddressFragment::class.java, true)
            else
                askForPermissions(activity!!, arrayOf(android.Manifest.permission.ACCESS_FINE_LOCATION))
        }
        login_main_have_invite_code_tv.setOnClickListener {
            baseAcitivityListener.navigator.replaceFragment(InviteCodeFragment::class.java, true)
        }

    }
}