package com.sachtechsolution.neighbourhood.ui.home.fragments

import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.view.View
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.*
import com.sachtechsolution.neighbourhood.ui.MainActivity
import com.sachtechsolution.neighbourhood.ui.groups.GroupActivity
import com.sachtechsolution.neighbourhood.ui.home.fragments.menu.activity.MenuBaseActivity
import com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.FragmentEvents
import com.sachtechsolution.neighbourhood.ui.local_agencies.LocalAgenciesFragment
import com.sachtechsolution.neighbourhood.ui.mail.data.SendInviteCodeFragment
import com.sachtechsolution.neighbourhood.ui.pet_directory.fragment.PetDirectoryFragment
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_menu.*


class FragmentMenu : BaseFragment(), View.OnClickListener {


    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }

    override fun viewToCreate(): Int {
        return R.layout.fragment_menu
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        menu_society.text = pref.getUserFromPref()?.society
        //listner
        menu_profile.setOnClickListener(this)
        menu_neighbors.setOnClickListener(this)
        menu_sale.setOnClickListener(this)
        menu_lost.setOnClickListener(this)
        menu_crime.setOnClickListener(this)
        menu_general.setOnClickListener(this)
        menu_help.setOnClickListener(this)
        menu_settings.setOnClickListener(this)
        menu_real_estate.setOnClickListener(this)
        menu_events.setOnClickListener(this)
        menu_recommendation.setOnClickListener(this)
        menu_bookmarks.setOnClickListener(this)
        menu_society.setOnClickListener(this)
        txtCategory.setOnClickListener(this)
        txtPeople.setOnClickListener(this)
        menuInvite.setOnClickListener(this)
        menu_Documents.setOnClickListener(this)
        menu_guideline.setOnClickListener(this)
        menu_pet_directory.setOnClickListener(this)
        menu_local_agency.setOnClickListener(this)
        menu_group.setOnClickListener(this)
        menu_Interest.setOnClickListener(this)
    }


    override fun onClick(view: View?) {

        when (view!!.id) {

            R.id.menu_profile ->
                if (context!!.getLocationMode() != 3) {
                    showToast("please select high accuracy option")
                    startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
                } // do stuff
                else{
                    activity!!.openActivity<MenuBaseActivity>(MenuBaseActivity.FRAGMENT, MenuBaseActivity.PROFILE)
                }
            R.id.menu_guideline->
                activity!!.openActivity<MenuBaseActivity>(MenuBaseActivity.FRAGMENT, MenuBaseActivity.GUIDE)

            R.id.menu_general ->
                activity!!.openActivity<MenuBaseActivity>(MenuBaseActivity.FRAGMENT, MenuBaseActivity.GENERAL)

            R.id.menu_neighbors ->
                activity!!.openActivity<MenuBaseActivity>(MenuBaseActivity.FRAGMENT, MenuBaseActivity.NEIGHBOURS)

            R.id.menu_sale ->
                activity!!.openActivity<MenuBaseActivity>(MenuBaseActivity.FRAGMENT, MenuBaseActivity.FORSALE_AND_FREE)

            R.id.menu_lost ->
                activity!!.openActivity<MenuBaseActivity>(MenuBaseActivity.FRAGMENT, MenuBaseActivity.LOST_AND_FOUND)

            R.id.menu_crime ->
                activity!!.openActivity<MenuBaseActivity>(MenuBaseActivity.FRAGMENT, MenuBaseActivity.CRIME_AND_SAFETY)

            R.id.menu_Interest ->
                activity!!.openActivity<MenuBaseActivity>(MenuBaseActivity.FRAGMENT, MenuBaseActivity.ALLINTEREST)

            R.id.txtCategory -> {
            }

            R.id.txtPeople -> {
            }

            R.id.menuInvite -> {
                activity!!.openActivity<MenuBaseActivity>(MenuBaseActivity.FRAGMENT, MenuBaseActivity.INVITATION)
                //baseAcitivityListener.navigator.replaceFragment(SendInviteCodeFragment::class.java, true)
            }

            R.id.menu_help -> context!!.openLink("http://www.google.com")

            R.id.menu_settings ->
                activity!!.openActivity<MenuBaseActivity>(MenuBaseActivity.FRAGMENT, MenuBaseActivity.SETTINGS)

            R.id.menu_bookmarks ->
                activity!!.openActivity<MenuBaseActivity>(MenuBaseActivity.FRAGMENT, MenuBaseActivity.BOOKMARK)


            R.id.menu_real_estate ->
                activity!!.openActivity<MenuBaseActivity>(MenuBaseActivity.FRAGMENT, MenuBaseActivity.REAL_ESTATE)

            R.id.menu_recommendation -> {
                activity!!.openActivity<MenuBaseActivity>(MenuBaseActivity.FRAGMENT, MenuBaseActivity.RECOMMENDATIONS)

            }

            R.id.menu_events ->
                activity!!.openActivity<MenuBaseActivity>(MenuBaseActivity.FRAGMENT, MenuBaseActivity.EVENT)

            R.id.menu_society ->
                activity!!.openActivity<MenuBaseActivity>(MenuBaseActivity.FRAGMENT, MenuBaseActivity.SOCIETY)

            R.id.menu_pet_directory ->
                activity!!.openActivity<MenuBaseActivity>(MenuBaseActivity.FRAGMENT, MenuBaseActivity.PET_DIRECTORY)

            R.id.menu_local_agency ->
                activity!!.openActivity<MenuBaseActivity>(MenuBaseActivity.FRAGMENT, MenuBaseActivity.LOCAL_AGENCY)
                //baseAcitivityListener.navigator.replaceFragment(LocalAgenciesFragment::class.java, true)

            R.id.menu_Documents->
                activity!!.openActivity<MenuBaseActivity>(MenuBaseActivity.FRAGMENT, MenuBaseActivity.DOCUMENTS)

            R.id.menu_group-> {
                context!!.openActivity<GroupActivity>()
                //activity!!.finish()
            }
        }
    }


    override fun onResume() {
        super.onResume()
        if (intracterListner!==null)
        intracterListner!!.updateSelectedPos(4)

        var user= pref.getUserFromPref()
        txtUserName.text=user!!.firstName+" "+user.lastName
        txtUserAddress.text=user!!.society
        if (user!!.imageLink.isNotEmpty()) {
            progressBar.visible()
            Picasso.with(context)
                    .load(user.imageLink)
                    .into(userDp, object : com.squareup.picasso.Callback {
                        override fun onError() {
                          progressBar.visible()
                        }

                        override fun onSuccess() {
                            if(progressBar!==null)
                             progressBar.gone()
                        }

                    })
            //  Glide.with(context).load(user.imageLink).into(holder.itemView.post_profile)
        }

        }
}