package com.sachtechsolution.neighbourhood.ui

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.sachtechsolution.neighbourhood.GroupDeatilActivity
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseActivity
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.getDateDifference
import com.sachtechsolution.neighbourhood.extension.gone
import com.sachtechsolution.neighbourhood.extension.visible
import com.sachtechsolution.neighbourhood.model.Group
import com.sachtechsolution.neighbourhood.model.Post
import com.sachtechsolution.neighbourhood.model.User
import com.sachtechsolution.neighbourhood.ui.groups.GroupInfoFragment
import com.sachtechsolution.neighbourhood.ui.home.fragments.menu.activity.MenuBaseActivity
import jp.wasabeef.glide.transformations.BlurTransformation
import kotlinx.android.synthetic.main.activity_main2.*
import kotlinx.android.synthetic.main.item_row_chat.view.*

class MainActivity : BaseActivity(), View.OnClickListener {

    private var btnSend: Button? = null
    private var edtMessage: EditText? = null
    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }
    private var rvMessage: RecyclerView? = null
    val user by lazy { pref.getUserFromPref() }
    private var mFirebaseDatabase: FirebaseDatabase? = null
    private var mDatabaseReference: DatabaseReference? = null
    val group by lazy { intent!!.getParcelableExtra("gName") as Group }
    private var adapter: FirebaseRecyclerAdapter<Post, ChatViewHolder>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)


        btnSend = findViewById(R.id.btn_send)
        btnSend!!.setOnClickListener(this)
        groupName.text = group.name
        edtMessage = findViewById(R.id.edt_message)
        rvMessage = findViewById(R.id.rv_chat)
        rvMessage!!.setHasFixedSize(true)
        rvMessage!!.layoutManager = LinearLayoutManager(this)

        mFirebaseDatabase = FirebaseDatabase.getInstance()
        mDatabaseReference = mFirebaseDatabase!!.reference

        createGroupPost.setOnClickListener {
            var bundle = Bundle()
            bundle.putString("id", Post.postType.GROUP)
            bundle.putString("gid", group.name)
            var intent = Intent(this, MenuBaseActivity::class.java)
            intent.putExtra(MenuBaseActivity.CREATEPOST, bundle)
            startActivity(intent)


            /* navigator.replaceFragment(CreatePostFragment::class.java, true,
                     Bundle().apply {
                         putString("id", Post.postType.GROUP)
                         putString("gid", group.gid)
                     })*/
        }

        groupInfo.setOnClickListener {
            var intent=Intent(this, GroupDeatilActivity::class.java)
            intent.putExtra("group",group)
            startActivity(intent)
        }
        icBack.setOnClickListener { onBackPressed() }

        adapter = object : FirebaseRecyclerAdapter<Post, ChatViewHolder>(
                Post::class.java,
                R.layout.item_row_chat,
                ChatViewHolder::class.java,
                mDatabaseReference!!.child("group").child(group.name)
        ) {
            override fun populateViewHolder(viewHolder: ChatViewHolder, model: Post, position: Int) {

                fireStoreHelper.getUser(model.uid).addOnCompleteListener {
                    if (it.isSuccessful) {
                        val user = it.result.toObject(User::class.java)

                        if (user!=null)
                        {
                            if (user!!.imageLink.isNotEmpty())
                                Glide.with(viewHolder.itemView.context).load(user.imageLink).into(viewHolder.itemView.post_user_image)


                            viewHolder.itemView.post_time.text = getDateDifference(model.date)
                            viewHolder.tvMessage.text = model.message
                            viewHolder.tvEmail.text = user.firstName + " " + user.lastName

                            val images = model?.imagesLinks?.split("#")
                            var size = images?.size!! - 1
                            if (size == 0) {
                                viewHolder.itemView.post_image_ll_1.gone()
                                viewHolder.itemView.post_image_rl_0.gone()


                            } else if (size == 1) {
                                viewHolder.itemView.post_image_ll_1.gone()
                                viewHolder.itemView.post_image_rl_0.visible()
                                Glide.with(this@MainActivity).load(images[0]).into(viewHolder.itemView.post_image_0_front)
                                Glide.with(this@MainActivity).load(images[0])
                                        .apply(RequestOptions.bitmapTransform(BlurTransformation(25, 3)))
                                        .into(viewHolder.itemView.post_image_0_back)

                            } else if (size!! > 1) {
                                viewHolder.itemView.post_image_ll_1.visible()
                                viewHolder.itemView.post_image_rl_0.gone()
                                Glide.with(this@MainActivity).load(images[0]).into(viewHolder.itemView.post_image_1_front)
                                Glide.with(this@MainActivity).load(images[0])
                                        .apply(RequestOptions.bitmapTransform(BlurTransformation(25, 3)))
                                        .into(viewHolder.itemView.post_image_1_back)
                                Glide.with(this@MainActivity).load(images[1]).into(viewHolder.itemView.post_image_2_front)
                                Glide.with(this@MainActivity).load(images[1])
                                        .apply(RequestOptions.bitmapTransform(BlurTransformation(25, 3)))
                                        .into(viewHolder.itemView.post_image_2_back)

                                if (size > 2) {
                                    viewHolder.itemView.post_more_images.visible()
                                    viewHolder.itemView.post_more_images.text = "+" + (size - 2)
                                } else
                                    viewHolder.itemView.post_more_images.gone()

                            }


                        }
                    }
                }

            }
        }
        rvMessage!!.adapter = adapter
    }

    override fun fragmentContainer(): Int {
        return 0
    }

    override fun onClick(v: View) {
        if (v.id == R.id.btn_send) {
            val message = edtMessage!!.text.toString().trim { it <= ' ' }
            if (!TextUtils.isEmpty(message)) {

                val post = Post(message = message, uid = user!!.uid,
                        type = Post.postType.MESSAGE)
                /* val param = HashMap<String, Any>()
                 param["sender"] = pref.getUserFromPref()!!.email
                 param["message"] = message*/

                mDatabaseReference!!.child("group").child(group.name)
                        .push()
                        .setValue(post)
                        .addOnCompleteListener { task ->
                            edtMessage!!.setText("")
                            if (task.isSuccessful) {
                                Log.d("SendMessage", "Sukses")
                            } else {
                                Log.d("SendMessage", "failed ")
                            }
                        }
            }
        }
    }

    class ChatViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        internal var tvEmail: TextView = itemView.findViewById(R.id.post_user_name)
        internal var tvMessage: TextView = itemView.findViewById(R.id.tv_message)

    }

    override fun onBackPressed() {
        super.onBackPressed()
        adapter = null
        finish()
    }
}

