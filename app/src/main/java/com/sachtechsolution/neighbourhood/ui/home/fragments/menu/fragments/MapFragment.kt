package com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments

import android.annotation.SuppressLint
import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.location.Location
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ProgressBar
import android.widget.TextView
import com.example.manish.internetstatus.OttoBus
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.places.ui.SupportPlaceAutocompleteFragment
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.LocationBFragment
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.gone
import com.sachtechsolution.neighbourhood.extension.visible
import com.sachtechsolution.neighbourhood.model.User
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings.MapFragmentAdapter
import com.sachtechsolution.neighbourhood.ui.login.SamplePresenter
import com.squareup.otto.Subscribe
import com.squareup.picasso.Picasso
import com.yayandroid.locationmanager.configuration.Configurations
import com.yayandroid.locationmanager.configuration.LocationConfiguration
import com.yayandroid.locationmanager.constants.FailType
import com.yayandroid.locationmanager.constants.ProcessType
import de.hdodenhof.circleimageview.CircleImageView

class MapFragment : LocationBFragment(), View.OnClickListener, SamplePresenter.SampleView, OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, MapFragmentAdapter.OpenDone {

    private var progressDialog: ProgressDialog? = null
    var fragment: SupportPlaceAutocompleteFragment? = null
    private var samplePresenter: SamplePresenter? = null

    override fun getLocationConfiguration(): LocationConfiguration {
        return Configurations.defaultConfiguration("Gimme the permission!", "Would you mind to turn GPS on?")
    }

    override fun onLocationChanged(location: Location) {
        samplePresenter!!.onLocationChanged(location)
    }

    override fun onLocationFailed(@FailType failType: Int) {
        samplePresenter!!.onLocationFailed(failType)
    }

    override fun onProcessTypeChanged(@ProcessType processType: Int) {
        samplePresenter!!.onProcessTypeChanged(processType)
    }

    override fun onResume() {
        super.onResume()
        OttoBus.bus.register(this)

        if (locationManager.isWaitingForLocation && !locationManager.isAnyDialogShowing) {
            displayProgress()
        }
    }

    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
        dismissProgress()
        fragment = null
    }


    @Subscribe
    fun getMessage(internetStatus: String) {

        if (internetStatus.equals("Not connected to Internet")) {
            InternetStatusDialogFragment.newInstance().show(fragmentManager, "option_fragment")
        } else {
            showToast("internet enabled ")

        }

    }


    private fun displayProgress() {
        if (progressDialog == null) {
            progressDialog = ProgressDialog(context)
            progressDialog!!.window!!.addFlags(Window.FEATURE_NO_TITLE)
            progressDialog!!.setMessage("Getting location...")
        }

        if (!progressDialog!!.isShowing) {
            progressDialog!!.show()
        }
    }

    override fun getText(): String {
        return "vfmlvbm"
    }

    override fun setText(text: String) {
        myMap = childFragmentManager.findFragmentById(R.id.map_fragment) as SupportMapFragment
        myMap?.getMapAsync(this)
        mGoogleClient = GoogleApiClient.Builder(activity!!)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .build()
        mGoogleClient.connect()
        markerView = LayoutInflater.from(activity).inflate(R.layout.marker_layout, null)


    }

    override fun updateProgress(text: String) {
        if (progressDialog != null && progressDialog!!.isShowing) {
            progressDialog!!.setMessage(text)
        }
    }

    override fun dismissProgress() {
        if (progressDialog != null && progressDialog!!.isShowing) {
            progressDialog!!.dismiss()
        }
    }

    private var mMap: GoogleMap?=null
    private lateinit var mGoogleClient: GoogleApiClient
    lateinit var mLocation: Location
    private var myMap: SupportMapFragment? = null
    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }
    private var markerView: View? = null

    @SuppressLint("MissingPermission")
    override fun onMapReady(p0: GoogleMap?) {
        mMap = p0!!
        mMap?.uiSettings?.isZoomControlsEnabled = true
        mMap?.isMyLocationEnabled = false
        getUserSociety()
    }

    @SuppressLint("MissingPermission")
    override fun onConnected(p0: Bundle?) {
        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleClient)
        if (mLocation != null) {
            getUserSociety()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        samplePresenter = SamplePresenter(this)
        getLocation()
    }

    override fun onConnectionSuspended(p0: Int) {
    }

    override fun onClick(p0: View?) {
    }

    override fun OnEventDone() {
    }

    override fun viewToCreate(): Int {
        return R.layout.fragment_map
    }


    val currentUser by lazy { pref.getUserFromPref() }
    val currentUserId by lazy { fireStoreHelper.currentUserUid() }

    fun getUserSociety() {
        fireStoreHelper.getNeighborsBySociety(currentUser!!.society).addOnCompleteListener {
            if (it.isSuccessful && mMap!=null) {
                if (it.result.documents.size > 0) {
                    val latlng = pref.getUserFromPref()
                    mMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(latlng!!.lat, latlng.lng), 11F))
                    mMap?.clear()
                }
                it.result.documents.forEach {
                    val user = it.toObject(User::class.java)!!
                    if (!user.uid.equals(currentUserId))
                        addMarker(user)
                }

            } else {
                showToast("something went wrong")
            }

        }.addOnFailureListener {
            hideProgress()
            showToast("something went wrong")
        }
    }

    fun getUserStreet() {
        fireStoreHelper.getNeighborsByStreet(currentUser!!.street).addOnCompleteListener {
            if (it.isSuccessful) {
                if (it.result.documents.size > 1) {
                    val latlng = pref.getUserFromPref()
                    mMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(latlng!!.lat, latlng.lng), 11F))
                    mMap?.clear()
                }
                it.result.documents.forEach {
                    val user = it.toObject(User::class.java)!!
                    if (!user.uid.equals(currentUserId))
                        addMarker(user)
                }

            } else {
                showToast("something went wrong")
            }

        }.addOnFailureListener {
            hideProgress()
            showToast("something went wrong")
        }
    }

    fun getUserZip() {
        fireStoreHelper.getAllNeighbors(currentUser!!.zipCode).addOnCompleteListener {
            if (it.isSuccessful) {
                if (it.result.documents.size > 1) {
                    val latlng = pref.getUserFromPref()
                    mMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(latlng!!.lat, latlng.lng), 11F))
                    mMap?.clear()
                }
                it.result.documents.forEach {
                    val user = it.toObject(User::class.java)!!
                    if (!user.uid.equals(currentUserId))
                        addMarker(user)
                }

            } else {
                showToast("something went wrong")
            }

        }.addOnFailureListener {
            hideProgress()
            showToast("something went wrong")
        }
    }


    private fun addMarker(user: User) {
        //var latlng = user.latlng.split(",")

        if (user.imageLink.isNotEmpty()) {
            mMap?.addMarker(MarkerOptions()
                    .position(LatLng(user.lat, user.lng))
                    .icon(BitmapDescriptorFactory.fromBitmap(
                            createCustomMarker(activity!!, user.imageLink, user.firstName + " " + user.lastName))))?.title = user.society;
            mMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(user.lat, user.lng), 15F))

        } else {
            mMap?.addMarker(MarkerOptions()
                    .position(LatLng(user.lat, user.lng))
                    .icon(BitmapDescriptorFactory.fromBitmap(
                            createCustomMarker(activity!!, user.imageLink, user.firstName + " " + user.lastName))))?.title = user.society;
            mMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(user.lat, user.lng), 15F))
        }

    }

    fun createCustomMarker(context: Context, image: String, _name: String): Bitmap {

        val marker = (context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(R.layout.custom_marker_layout, null)


        val progressBar = marker.findViewById(R.id.progressBar) as ProgressBar
        val markerImage = marker.findViewById(R.id.user_dp) as CircleImageView

        if (image.isNotEmpty()) {

            Picasso.with(context)
                    .load(image)
                    .into(markerImage, object : com.squareup.picasso.Callback {
                        override fun onError() {
                            progressBar.visible()
                        }

                        override fun onSuccess() {
                            progressBar.gone()
                        }

                    })
        } else
            markerImage.setImageResource(R.drawable.dummy_user)

        val txt_name = marker.findViewById(R.id.name) as TextView
        txt_name.text = _name

        val displayMetrics = DisplayMetrics()
        (context as Activity).windowManager.defaultDisplay.getMetrics(displayMetrics)
        marker.layoutParams = ViewGroup.LayoutParams(52, ViewGroup.LayoutParams.WRAP_CONTENT)
        marker.measure(displayMetrics.widthPixels, displayMetrics.heightPixels)
        marker.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels)
        marker.buildDrawingCache()
        val bitmap = Bitmap.createBitmap(marker.measuredWidth, marker.measuredHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        marker.draw(canvas)

        return bitmap
    }
}