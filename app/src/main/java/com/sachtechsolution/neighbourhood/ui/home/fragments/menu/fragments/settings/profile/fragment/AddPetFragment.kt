package com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings.profile.fragment

import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.v4.content.ContextCompat
import android.support.v4.content.FileProvider
import android.view.View
import com.bumptech.glide.Glide
import com.example.manish.internetstatus.OttoBus
import com.google.android.flexbox.FlexboxLayout
import com.google.firebase.storage.StorageReference
import com.sachtechsolution.neighbourhood.BuildConfig
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.gone
import com.sachtechsolution.neighbourhood.extension.showToast
import com.sachtechsolution.neighbourhood.extension.visible
import com.sachtechsolution.neighbourhood.model.Family
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.sachtechsolution.neighbourhood.ui.home.fab.fragment.message.dialogs.SelectOptionDialogFragment
import com.squareup.otto.Subscribe
import fisk.chipcloud.ChipCloud
import fisk.chipcloud.ChipCloudConfig
import kotlinx.android.synthetic.main.fragment_add_pet.*
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


class AddPetFragment : BaseFragment(), View.OnClickListener, SelectOptionDialogFragment.onOptionSelect {

    val fireStoreHelper: FireStoreHelper by lazy { FireStoreHelper.getInstance() }
    override fun forGallery() {
        var intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(intent, GALLERY)
    }

    var photoURI: Uri? = null
    override fun forCamera() {
        try {
            dispatchTakePictureIntent();
        } catch (e: IOException) {
        }
    }

    @Throws(IOException::class)
    private fun dispatchTakePictureIntent() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(context!!.packageManager) != null) {
            // Create the File where the photo should go
            var photoFile: File? = null
            try {
                photoFile = createImageFile()
            } catch (ex: IOException) {
                // Error occurred while creating the File
                return
            }

            // Continue only if the File was successfully created
            if (photoFile != null) {

                photoURI = FileProvider.getUriForFile(context!!,
                        BuildConfig.APPLICATION_ID + ".provider",
                        createImageFile())
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                startActivityForResult(takePictureIntent, TAKE_PICTURE)
            }
        }
    }

    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "JPEG_" + timeStamp + "_"
        val storageDir = File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM), "Camera")
        val image = File.createTempFile(
                imageFileName, /* prefix */
                ".jpg", /* suffix */
                storageDir      /* directory */
        )
        return image
    }


    lateinit var mRef: StorageReference
    var config: ChipCloudConfig? = null
    var dog = false
    var cat = false
    var more = false
    var selectedName = ""
    var selectedType = "Dog"
    var profile = ""
    private var TAKE_PICTURE = 12
    private var GALLERY = 13
    private var selectedImage: Uri? = null

    override fun viewToCreate(): Int {
        return R.layout.fragment_add_pet
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        //listner
        getChiosWithAvtar()
        txtCancel.setOnClickListener(this)
        txtAdd.setOnClickListener(this)
        ivCamera.setOnClickListener(this)
    }


    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
    }


    @Subscribe
    fun getMessage(internetStatus: String) {

        if (internetStatus.equals("Not connected to Internet")) {
            InternetStatusDialogFragment.newInstance().show(fragmentManager, "option_fragment")
        } else {
            showToast("internet enabled ")

        }

    }

    override fun onResume() {
        super.onResume()
        OttoBus.bus.register(this)
    }

    private fun getChiosWithAvtar() {

        val drawableConfig = ChipCloudConfig()
                .selectMode(ChipCloud.SelectMode.single)
                .checkedChipColor(resources.getColor(R.color.optional))
                .checkedTextColor(Color.parseColor("#ffffff"))
                .uncheckedChipColor(Color.parseColor("#e0e0e0"))
                .uncheckedTextColor(resources.getColor(R.color.optional))

        val chip = ChipCloud(activity, showSelector, drawableConfig)
        chip.addChip("Dog", ContextCompat.getDrawable(activity!!, R.drawable.dogside))
        chip.addChip("Cat", ContextCompat.getDrawable(activity!!, R.drawable.cat))
        chip.addChip("More")
        chip.setChecked(0)
        val demoArray = resources.getStringArray(R.array.dog_array)
        getChips(demoArray, cvDog, dog)
        dog = true
        chip.setListener { index, checked, userClick ->
            if (userClick && checked) {
                selectedType = chip.getLabel(index)
                if (chip.getLabel(index).equals("Dog")) {
                    val demoArray = resources.getStringArray(R.array.dog_array)
                    getChips(demoArray, cvDog, dog)
                    petIcon.setImageResource(R.drawable.dogside)
                    dog = true
                    cvDog.visible()
                    cvCat.gone()
                    cvMore.gone()
                } else if (chip.getLabel(index).equals("Cat")) {
                    val demoArray = resources.getStringArray(R.array.array_cat)
                    getChips(demoArray, cvCat, cat)
                    cat = true
                    petIcon.setImageResource(R.drawable.cat)
                    cvCat.visible()
                    cvDog.gone()
                    cvMore.gone()
                } else if (chip.getLabel(index).equals("More")) {
                    val demoArray = resources.getStringArray(R.array.array_more)
                    getChips(demoArray, cvMore, more)
                    more = true
                    petIcon.setImageResource(R.drawable.more)
                    cvMore.visible()
                    cvCat.gone()
                    cvDog.gone()
                }

            }

        }
    }

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.txtCancel -> activity!!.onBackPressed()
            R.id.txtAdd -> uploadImage(selectedImage)
            R.id.ivCamera -> SelectOptionDialogFragment.newInstance(this)
                    .show(fragmentManager, "option_fragment")

        }
    }

    private fun uploadData(image: String) {

        var data = Family.Pet(name = selectedName, imageLink = image, category = selectedType, profile = profile)
        fireStoreHelper.updatePet(data, "pet", pref.getUserFromPref()!!.uid).addOnCompleteListener {

            if (it.isSuccessful) {
                hideProgress()
                showToast("Updated")
                activity!!.onBackPressed()


            } else {
                showToast("error")
                hideProgress()
            }

            hideProgress()

        }.addOnFailureListener {
            showToast("error")
            hideProgress()
        }


    }

    private fun getChips(demoArray: Array<String>, cvPet: FlexboxLayout, isadded: Boolean) {

        config = ChipCloudConfig()
                .selectMode(ChipCloud.SelectMode.single)
                .checkedChipColor(resources.getColor(R.color.optional))
                .checkedTextColor(Color.parseColor("#ffffff"))
                .uncheckedChipColor(Color.parseColor("#e0e0e0"))
                .uncheckedTextColor(resources.getColor(R.color.optional))

        val chipCloud = ChipCloud(activity, cvPet, config)

        if (!isadded)
            chipCloud.addChips(demoArray)

        chipCloud.setListener { index, checked, userClick ->
            if (userClick && checked) {
                profile = chipCloud.getLabel(index)

            }

        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            TAKE_PICTURE -> if (resultCode == Activity.RESULT_OK) {
                setImage(photoURI)
            }

            GALLERY -> {
                if (data != null) {
                    val selectedImage = data!!.data
                    this.selectedImage = selectedImage
                    setImage(selectedImage)
                }

            }
        }
    }

    fun setImage(uri: Uri?) {
        Glide.with(activity!!).load(uri).into(petIcon)
    }

    private fun uploadImage(selectedImage: Uri?) {
        selectedName = etName.text.toString().trim()
        if (selectedName.isEmpty())
            activity!!.showToast("enter name")
        if (selectedType.isEmpty())
            activity!!.showToast("select pet type")
        if (selectedImage == null)
            activity!!.showToast("select image")
        else {
            showProgress()
            var time = java.sql.Timestamp(Date().time)
            var timestamp = time.toString().replace(":", "").replace("-", "").replace(" ", "").replace(".", "")
            mRef = fireStoreHelper.getStorageRef().child("Family").child(fireStoreHelper.currentUserUid() + timestamp)
            mRef.putFile(selectedImage!!).addOnCompleteListener {
                if (it.isSuccessful) {
                    it.result
                    mRef.downloadUrl.addOnSuccessListener { p0 -> uploadData(p0.toString()) }.addOnFailureListener {
                        hideProgress()
                    }
                }
            }.addOnFailureListener {
                hideProgress()
            }
        }
    }
}
