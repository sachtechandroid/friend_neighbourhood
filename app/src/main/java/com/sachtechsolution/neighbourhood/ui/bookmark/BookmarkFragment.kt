package com.sachtechsolution.neighbourhood.ui.bookmark

import android.os.Bundle
import android.view.View
import com.example.manish.internetstatus.OttoBus
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.constants.Constants
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.gone
import com.sachtechsolution.neighbourhood.extension.showToast
import com.sachtechsolution.neighbourhood.extension.visible
import com.sachtechsolution.neighbourhood.model.Post
import com.sachtechsolution.neighbourhood.model.User
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.sachtechsolution.neighbourhood.ui.bookmark.adapter.BookmarkAdapter
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_bookmark.*

class BookmarkFragment : BaseFragment(), BookmarkAdapter.removeBookmark {

    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }
    private val adapter by lazy { BookmarkAdapter(activity!!, baseAcitivityListener, this) }

    override fun viewToCreate(): Int {
        return R.layout.fragment_bookmark
    }

    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
    }


    @Subscribe
    fun getMessage(internetStatus:String) {

        if (internetStatus.equals("Not connec[uted to Internet")){
            InternetStatusDialogFragment.newInstance().show(fragmentManager, "option_fragment")
        }
        else {
            showToast("internet enabled ")
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        super.onViewCreated(view, savedInstanceState)
        recyclerView_bookmark.adapter = adapter
        Constants.DELETED_POSITION=-1
        Constants.IS_POLLED_LIST.clear()
        Constants.IS_THANKED_LIST.clear()
        getAllBookmark()
        icBack.setOnClickListener { activity!!.onBackPressed() }
        bookmark_message.visible()
    }

    private fun getAllBookmark() {

        fireStoreHelper.getBookmark().addOnCompleteListener {
            if (it.isSuccessful) {
                if (it.result.documents.size == 0)
                    bookmark_message.text = "Open post from Home, Lost & Found, General and Lost & Found screen and add that to bookmarks."
                it.result.documents.forEach {

                    var bookmark = it.toObject(Post.Bookmark::class.java)

                    fireStoreHelper.getPostsById(bookmark!!.post_id).addOnCompleteListener {
                        if (it.isSuccessful) {
                            val post = it.result.toObject(Post::class.java)!!
                            post?.pid = it.result.id

                            fireStoreHelper.getUser(post!!.uid).addOnCompleteListener {
                                if (it.isSuccessful) {
                                    if (it.result.exists()) {
                                        val user = it.result.toObject(User::class.java)

                                        fireStoreHelper.getThanks(post.pid).addOnCompleteListener {
                                            if (it.isSuccessful) {
                                                var isThanked = false
                                                if (it.result.exists())
                                                    isThanked = true
                                                if (post.type.equals(Post.postType.POLL)) {

                                                    fireStoreHelper.getIsPollAnswered(post.pid).addOnCompleteListener {
                                                        if (it.isSuccessful) {
                                                            var isPolled = false
                                                            if (it.result.documents.size > 0)
                                                                isPolled = true
                                                            adapter!!.addItem(post, user!!, isThanked, isPolled)
                                                        }
                                                        else {
                                                            adapter!!.addItem(post, user!!, isThanked, false)
                                                        }
                                                    }.addOnFailureListener {
                                                        showToast("")
                                                    }.addOnCanceledListener {
                                                        showToast("")
                                                    }
                                                }
                                                else {
                                                    adapter!!.addItem(post, user!!, isThanked, false)
                                                }
                                            }
                                        }
                                    }
                                    bookmark_message.gone()
                                }
                            }
                        }
                        else {
                            showToast("something went wrong")
                        }
                    }
                }
            }
            else
                showToast("something went wrong")

        }.addOnFailureListener {
            showToast("")

        }.addOnCanceledListener {
            showToast("")

        }

    }

    override fun onRemoveBookmark(pid: String, position: Int) {

        fireStoreHelper.removeBookmark(pid).addOnCompleteListener {
            if (it.isSuccessful) {

                context!!.showToast("bookmark removed")
                adapter.removePost(position)
                adapter.notifyItemRemoved(position)
            }
            else
                context!!.showToast("unable to remove bookmark")

        }.addOnFailureListener {
            context!!.showToast("unable to remove bookmark")
        }.addOnCanceledListener {
            context!!.showToast("unable to remove bookmark")
        }
    }


    override fun onResume() {
        super.onResume()

        OttoBus.bus.register(this)
        //getAllPost()
        if (Constants.POSITION != -1) {
            if (Constants.POST != null)
                adapter?.postsList?.set(Constants.POSITION, Constants.POST)
            adapter!!.notifyItemChanged(Constants.POSITION)
            Constants.POSITION = -1
            Constants.POST = null
        }
        if(Constants.DELETED_POSITION!=-1){
            val position= Constants.DELETED_POSITION
            adapter!!.deletePost(position)
            Constants.DELETED_POSITION=-1
        }
        //intracterListner!!.updateSelectedPos(0)
    }
}