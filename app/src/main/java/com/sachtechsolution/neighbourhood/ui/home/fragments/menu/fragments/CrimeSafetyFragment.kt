package com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import com.example.manish.internetstatus.OttoBus
import com.google.firebase.firestore.DocumentSnapshot
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.constants.Constants
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.gone
import com.sachtechsolution.neighbourhood.extension.isPostHasTime
import com.sachtechsolution.neighbourhood.model.Post
import com.sachtechsolution.neighbourhood.model.User
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.sachtechsolution.neighbourhood.ui.home.adapters.HomePagePostAdapter
import com.sachtechsolution.neighbourhood.ui.home.fab.fragment.message.fragment.crime_and_safety.a_crime.AddCrimeFragment
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_crime_safety.*

class CrimeSafetyFragment : BaseFragment(), HomePagePostAdapter.OpenBottomSheet {

    override fun viewToCreate(): Int {
        return R.layout.fragment_crime_safety
    }


    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
    }


    @Subscribe
    fun getMessage(internetStatus:String) {

        if (internetStatus.equals("Not connected to Internet")){
            InternetStatusDialogFragment.newInstance().show(fragmentManager, "option_fragment")
        }else {
            showToast("internet enabled ")

        }

    }

    override fun OnOpenBottomSheet(pid: String, position: Int, type: String, post: Post, uid: String) {
    }

    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }
    val user by lazy { pref.getUserFromPref()!! }
    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter = HomePagePostAdapter(activity!!, this, baseAcitivityListener, pref)
        recyclerView_crime_safety.adapter = adapter
        Constants.IS_THANKED_LIST.clear()
        Constants.IS_POLLED_LIST.clear()
        fireStoreHelper.getPostsByType(type = Post.postType.CRIME_SAFETY,zipCode =  user.zipCode,
                society = user.society).addOnCompleteListener {
            if (it.isSuccessful) {
                postLis = it.result.documents
                getPost()
                if (postLis.size==0)
                    crim_text.text = "No crime found"
            } else {
                showToast("something went wrong")
            }
        }.addOnFailureListener { showToast(it.message) }
        new_crime.setOnClickListener {
            Constants.NEXT = "crime"
            baseAcitivityListener.navigator.replaceFragment(AddCrimeFragment::class.java, true)
        }
        btnBack.setOnClickListener { activity!!.onBackPressed() }
    }

    private var postCount = -1
    lateinit var postLis: MutableList<DocumentSnapshot>
    lateinit var adapter: HomePagePostAdapter
    private fun getPost() {
        postCount++
        if (postLis.size > postCount) {
            var post = postLis[postCount].toObject(Post::class.java)
            post!!.pid = postLis[postCount].id
            if (isPostHasTime(post.date))
                getPostUser(post)
        }
    }

    private fun getPostUser(post: Post) {
        fireStoreHelper.getUser(post!!.uid).addOnCompleteListener {
            if (it.isSuccessful) {
                if (it.result.exists()) {
                    val user = it.result.toObject(User::class.java)
                    fireStoreHelper.getThanks(post.pid).addOnCompleteListener {
                        if (it.isSuccessful) {
                            var isThanked = false
                            if (it.result.exists())
                                isThanked = true
                            getPost()
                            crim_text?.gone()
                            adapter!!.addItem(post, user!!, isThanked, false)
                        } else {
                            getPost()
                            adapter!!.addItem(post, user!!, false, false)
                            crim_text?.gone()
                        }

                    }
                }
            }
        }.addOnFailureListener { getPost() }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        Constants.IS_THANKED_LIST.clear()
        Constants.IS_POLLED_LIST.clear()
    }
    override fun onResume() {
        super.onResume()

        OttoBus.bus.register(this)
        //getAllPost()
        if (Constants.POSITION != -1) {
            if (Constants.POST != null)
                adapter?.postsList?.set(Constants.POSITION, Constants.POST)
            adapter!!.notifyItemChanged(Constants.POSITION)
            Constants.POSITION = -1
            Constants.POST = null
        }
        if(Constants.DELETED_POSITION!=-1){
            val position=Constants.DELETED_POSITION
            adapter!!.deletePost(position)
            Constants.DELETED_POSITION=-1
        }
        //intracterListner!!.updateSelectedPos(0)
    }
}
