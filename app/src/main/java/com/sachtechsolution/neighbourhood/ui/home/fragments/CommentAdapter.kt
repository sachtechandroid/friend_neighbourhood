package com.sachtechsolution.neighbourhood.ui.home.fragments

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseAcitivityListener
import com.sachtechsolution.neighbourhood.extension.gone
import com.sachtechsolution.neighbourhood.extension.openActivity
import com.sachtechsolution.neighbourhood.extension.visible
import com.sachtechsolution.neighbourhood.model.RecoComment
import com.sachtechsolution.neighbourhood.model.User
import com.sachtechsolution.neighbourhood.ui.home.AnotherUserProfileActivity
import com.sachtechsolution.neighbourhood.ui.home.fragments.menu.activity.MenuBaseActivity
import com.squareup.picasso.Picasso
import gurtek.mrgurtekbasejava.base.ModelPrefrence
import kotlinx.android.synthetic.main.view_comments.view.*
import java.text.SimpleDateFormat
import java.util.*

class CommentAdapter(var context: Context, val baseAcitivityListener: BaseAcitivityListener, val pref: ModelPrefrence): RecyclerView.Adapter<CommentAdapter.ViewHolder>() {
    var reply =ArrayList<RecoComment>()
    var user=ArrayList<User>()


    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val v= LayoutInflater.from(p0.context).inflate(R.layout.view_comments,p0,false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return reply.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var reply=reply[position]
        var user=user[position]

        holder.itemView.user_name.text=user.firstName+" "+user.lastName
        //holder.itemView.time=recommendation.date.time
        holder.itemView.store_explanation.text=reply.recommendation
        holder.itemView.time.text=getDate(reply.date,"EEE, MMM d")
        if (user.imageLink.isNotEmpty()) {
            holder.itemView.progressBar.visible()
            Picasso.with(context)
                    .load(user.imageLink)
                    .into(holder.itemView.post_profile, object : com.squareup.picasso.Callback {
                        override fun onError() {
                            holder.itemView.progressBar.visible()
                        }

                        override fun onSuccess() {
                            holder.itemView.progressBar.gone()
                        }

                    })
          //  Glide.with(context).load(user.imageLink).into(holder.itemView.post_profile)
        }
        /* holder.itemView.setOnClickListener {

             baseAcitivityListener.navigator.replaceFragment(FragmentRecommendationDescription::class.java,true)
         }
         holder.itemView.user_name.setOnClickListener { openCustomActivity(user.uid) }
         holder.itemView.post_profile.setOnClickListener { openCustomActivity(user.uid) }
     */
    }
    fun openCustomActivity(uid: String) {
        if (uid.equals(pref.getUserFromPref()!!.uid))
            context!!.openActivity<MenuBaseActivity>(MenuBaseActivity.FRAGMENT, MenuBaseActivity.PROFILE)
        else
            context!!.openActivity<AnotherUserProfileActivity>("uid",uid)



    }
    fun addItem(reply: RecoComment, user: User) {

        this.reply.add(reply)
        this.user.add(user)
        notifyDataSetChanged()
    }

    fun clear() {

        reply.clear()
        user.clear()
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView)

    fun getDate(date: Date, format:String): String? {
        val formatter = SimpleDateFormat(format)
        return formatter.format(date)
    }
}