package com.sachtechsolution.neighbourhood.ui.home.fab.fragment.message.fragment.crime_and_safety.something_else

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.v4.content.FileProvider
import android.support.v7.widget.GridLayoutManager
import android.view.View
import com.example.manish.internetstatus.OttoBus
import com.sachtechsolution.neighbourhood.BuildConfig
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.constants.Constants
import com.sachtechsolution.neighbourhood.extension.showToast
import com.sachtechsolution.neighbourhood.model.Post
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.sachtechsolution.neighbourhood.ui.home.fab.fragment.message.adapter.SingleImageSelecterAapter
import com.sachtechsolution.neighbourhood.ui.home.fab.fragment.message.dialogs.ImageActionDialogFragment
import com.sachtechsolution.neighbourhood.ui.home.fab.fragment.message.dialogs.SelectOptionDialogFragment
import com.sachtechsolution.neighbourhood.ui.home.fab.fragment.message.dialogs.ViewImage
import com.sachtechsolution.neighbourhood.ui.home.fab.fragment.message.fragment.crime_and_safety.CreateCrimePostFragment
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_something_else.*
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class SomethingElsaFragment : BaseFragment(), SingleImageSelecterAapter.SelectImage, SelectOptionDialogFragment.onOptionSelect, ImageActionDialogFragment.onSelectAction, View.OnClickListener {

    var photoURI: Uri? = null
    private var TAKE_PICTURE = 12
    private var GALLERY = 13
    private var imageUri: Uri? = null
    var adapter: SingleImageSelecterAapter? = null

    override fun viewPhoto(uri: Uri?) {
        ViewImage.newInstance(uri)
                .show(fragmentManager, "option_fragment")
    }

    override fun viewToCreate(): Int {
        return R.layout.fragment_something_else
    }

    override fun removePhoto(position: Int) {
        adapter!!.removeImage(position)
        adapter!!.notifyItemRemoved(position)
    }


    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
    }


    @Subscribe
    fun getMessage(internetStatus:String) {

        if (internetStatus.equals("Not connected to Internet")){
            InternetStatusDialogFragment.newInstance().show(fragmentManager, "option_fragment")
        }else {
            showToast("internet enabled ")

        }

    }

    override fun onResume() {
        super.onResume()
        OttoBus.bus.register(this)
    }



    override fun forGallery() {
        var intent = Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(intent, GALLERY)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter = SingleImageSelecterAapter(context!!, this)
        rvSomethingElse.layoutManager = GridLayoutManager(context, 4)
        rvSomethingElse.adapter = adapter
        //listner
        btnBack.setOnClickListener(this)
        crime_else_next.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.btnBack -> activity!!.onBackPressed()
            R.id.crime_else_next -> {

                val subject = crime_else_subject_et.text.toString()
                val message = crime_else_message_et.text.toString()

                if (subject.isNullOrEmpty())
                    context!!.showToast("Enter Subject")

                else if (message.isNullOrEmpty())
                    context!!.showToast("Enter message")

                else {
                    Constants.IMAGE_LIST= adapter?.list!!
                    baseAcitivityListener.navigator.replaceFragment(CreateCrimePostFragment::class.java,
                            true, Bundle().apply {
                        putString("subject", subject.capitalize())
                        putString("message", message.capitalize())
                    })
                }
            }
        }
    }


    override fun OnImageSelected(position: Int, uri: Uri?) {
        ImageActionDialogFragment.newInstance(this, position, uri)
                .show(fragmentManager, "option_fragment")
    }


    override fun OnImageChoose() {
        if (adapter!!.list.size == 9)
            showToast("you can't select more images")
        else
            SelectOptionDialogFragment.newInstance(this)
                    .show(fragmentManager, "option_fragment")
    }


    override fun forCamera() {
        try {
            dispatchTakePictureIntent();
        } catch (e: IOException) {
        }
    }

    @Throws(IOException::class)
    private fun dispatchTakePictureIntent() {

        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        // Ensure that there's a camera activity to handle the intent

        if (takePictureIntent.resolveActivity(context!!.packageManager) != null) {
            // Create the File where the photo should go
            var photoFile: File? = null
            try {
                photoFile = createImageFile()
            } catch (ex: IOException) {
                // Error occurred while creating the File
                return
            }

            // Continue only if the File was successfully created
            if (photoFile != null) {

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                    // only for gingerbread and newer versions
                    photoURI = FileProvider.getUriForFile(context!!,
                            BuildConfig.APPLICATION_ID + ".provider",
                            createImageFile())
                }else
                {
                    photoURI = Uri.fromFile(createImageFile())
                }

                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                startActivityForResult(takePictureIntent, TAKE_PICTURE)
            }
        }
    }

    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name

        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "JPEG_" + timeStamp + "_"
        val storageDir = File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM), "Camera")

        val image = File.createTempFile(
                imageFileName, /* prefix */
                ".jpg", /* suffix */
                storageDir      /* directory */)
        return image
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {

            TAKE_PICTURE -> if (resultCode == Activity.RESULT_OK) {
                val selectedImage = imageUri
                adapter!!.addImage(selectedImage)
                adapter!!.notifyDataSetChanged()
            }

            GALLERY -> {
                if (data != null) {
                    val selectedImage = data!!.data
                    adapter!!.addImage(selectedImage)
                    adapter!!.notifyDataSetChanged()
                }

            }
        }
    }


}