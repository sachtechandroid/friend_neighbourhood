package com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings.profile.fragment

import android.content.Context
import android.support.v4.view.PagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.Toast
import com.bumptech.glide.Glide
import com.sachtechsolution.neighbourhood.R


class MyCustomPagerAdapter(internal var context: Context, internal var images: List<String>) : PagerAdapter() {

    override fun getCount(): Int {
        return images.size-1
    }

    internal var layoutInflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater


    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object` as LinearLayout
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val itemView = layoutInflater.inflate(R.layout.item, container, false)

        val imageView = itemView.findViewById(R.id.imageView) as ImageView
        Glide.with(context).load(images[position]).into(imageView)

        container.addView(itemView)

        /*//listening to image click
        imageView.setOnClickListener { Toast.makeText(context, "you clicked image " + (position + 1), Toast.LENGTH_LONG).show() }
*/
        return itemView
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as LinearLayout)
    }
}