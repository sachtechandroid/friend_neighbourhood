package com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings.profile.fragment

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Intent
import android.location.Geocoder
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.v4.content.FileProvider
import android.support.v7.widget.GridLayoutManager
import android.util.Log
import android.view.*
import com.example.manish.internetstatus.OttoBus
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.Status
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.places.ui.PlaceSelectionListener
import com.google.android.gms.location.places.ui.SupportPlaceAutocompleteFragment
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.storage.StorageReference
import com.sachtechsolution.neighbourhood.BuildConfig
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.constants.Constants
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.isLocationEnabled
import com.sachtechsolution.neighbourhood.model.Post
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.sachtechsolution.neighbourhood.ui.home.fab.fragment.message.adapter.SingleImageSelecterAapter
import com.sachtechsolution.neighbourhood.ui.home.fab.fragment.message.dialogs.ImageActionDialogFragment
import com.sachtechsolution.neighbourhood.ui.home.fab.fragment.message.dialogs.SelectOptionDialogFragment
import com.sachtechsolution.neighbourhood.ui.home.fab.fragment.message.dialogs.ViewImage
import com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings.MapFragmentAdapter
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_add_estate.*
import kotlinx.android.synthetic.main.view_confirm.*
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


class FragmentEditEstate : BaseFragment(), SingleImageSelecterAapter.SelectImage, SelectOptionDialogFragment.onOptionSelect, ImageActionDialogFragment.onSelectAction, OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, View.OnClickListener, MapFragmentAdapter.OpenDone {
    private lateinit var mMap: GoogleMap
    private lateinit var mGoogleClient: GoogleApiClient
    lateinit var mLocation: Location
    private var size = 0
    var message: String? = null
    var title: String? = null
    var fragment: SupportPlaceAutocompleteFragment? = null
    private var count = 0
    private var myMap: SupportMapFragment? = null
    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }
    lateinit var mRef: StorageReference
    var mCurrentPhotoPath = ""
    var selectedLatLong: LatLng? = null
    var marker: Marker? = null
    val user by lazy { pref.getUserFromPref() }
    private var markerView: View? = null

    override fun viewPhoto(uri: Uri?) {
        ViewImage.newInstance(uri)
                .show(fragmentManager, "option_fragment")
    }

    override fun removePhoto(position: Int) {
        adapter!!.removeImage(position)
        adapter!!.notifyItemRemoved(position)
    }

    private var REQUEST_TAKE_PHOTO = 18
    private var GALLERY = 13
    var adapter: SingleImageSelecterAapter? = null


    override fun forGallery() {
        var intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(intent, GALLERY)
    }

    var photoURI: Uri?=null
    override fun forCamera() {
        try {
            dispatchTakePictureIntent();
        } catch ( e:IOException) {
        }
    }

    @Throws(IOException::class)
    private fun dispatchTakePictureIntent() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(context!!.packageManager) != null) {
            // Create the File where the photo should go
            var photoFile: File? = null
            try {
                photoFile = createImageFile()
            } catch (ex: IOException) {
                // Error occurred while creating the File
                return
            }

            // Continue only if the File was successfully created
            if (photoFile != null) {

                photoURI = FileProvider.getUriForFile(context!!,
                        BuildConfig.APPLICATION_ID + ".provider",
                        createImageFile())
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                startActivityForResult(takePictureIntent,REQUEST_TAKE_PHOTO)
            }
        }
    }
    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "JPEG_" + timeStamp + "_"
        val storageDir = File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM), "Camera")
        val image = File.createTempFile(
                imageFileName, /* prefix */
                ".jpg", /* suffix */
                storageDir      /* directory */
        )
        return image
    }



    override fun OnImageSelected(position: Int, uri: Uri?) {
        ImageActionDialogFragment.newInstance(this, position, uri)
                .show(fragmentManager, "option_fragment")
    }


    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
    }


    @Subscribe
    fun getMessage(internetStatus:String) {

        if (internetStatus.equals("Not connected to Internet")){
            InternetStatusDialogFragment.newInstance().show(fragmentManager, "option_fragment")
        }else {
            showToast("internet enabled ")

        }

    }

    override fun onResume() {
        super.onResume()
        OttoBus.bus.register(this)
    }

    override fun OnImageChoose() {
        if (adapter!!.list.size == 9)
            showToast("you can't select more images")
        else
            SelectOptionDialogFragment.newInstance(this)
                    .show(fragmentManager, "option_fragment")
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            REQUEST_TAKE_PHOTO -> if (resultCode == Activity.RESULT_OK) {
                adapter!!.addImage(photoURI)
                adapter!!.notifyDataSetChanged()
            }

            GALLERY -> {
                if (data != null) {
                    val selectedImage = data!!.data
                    adapter!!.addImage(selectedImage)
                    adapter!!.notifyDataSetChanged()
                }

            }
        }
    }

    @SuppressLint("MissingPermission")
    override fun onMapReady(p0: GoogleMap?) {
        mMap = p0!!
        mMap.uiSettings.isZoomControlsEnabled = true
        mMap.isMyLocationEnabled = true

    }

    @SuppressLint("MissingPermission")
    override fun onConnected(p0: Bundle?) {
        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleClient)


        if (mLocation != null) {
            //var latLng = pref.getUserFromPref()!!.latlng.split(",")
            upDateMarker(LatLng(pref.getUserFromPref()!!.lat,pref.getUserFromPref()!!.lng))
        }
    }

    override fun onConnectionSuspended(p0: Int) {

    }

    override fun onClick(p0: View?) {

    }

    override fun OnEventDone() {

    }

    override fun viewToCreate(): Int {
        return R.layout.fragment_add_estate
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setRecyclerView()
        if (activity!!.isLocationEnabled(activity!!)) {
            myMap = childFragmentManager.findFragmentById(R.id.map_fragment) as SupportMapFragment
            myMap?.getMapAsync(this)
            mGoogleClient = GoogleApiClient.Builder(activity!!)
                    .addConnectionCallbacks(this)
                    .addApi(LocationServices.API)
                    .build()
            mGoogleClient.connect()
            markerView = LayoutInflater.from(activity).inflate(R.layout.marker_layout, null)

        } else {
            showToast("enable location")
        }

        fragment = childFragmentManager?.findFragmentById(R.id.place_autocomplete_fragment) as SupportPlaceAutocompleteFragment
        fragment!!.setBoundsBias(LatLngBounds(
                LatLng(-33.880490, 151.184363),
                LatLng(-33.858754, 151.229596)))

        fragment!!.setOnPlaceSelectedListener(object : PlaceSelectionListener {


            override fun onPlaceSelected(place: com.google.android.gms.location.places.Place) {
                //Toast.makeText(context, "SearchZip is : $place", Toast.LENGTH_SHORT).show()
                selectedLatLong = place.latLng
                upDateMarker(place.latLng)
            }

            override fun onError(status: Status) {

            }
        })

        ivBack.setOnClickListener {
            activity!!.onBackPressed()
        }
        ivDone.setOnClickListener {
            createEstate()
        }

    }

    private fun createEstate() {
        title = etPrice.text.toString()
        message = etMessage.text.toString()
        size = adapter!!.list.size

        if (title.isNullOrEmpty())
            showToast("enter title")
        else if (message.isNullOrEmpty())
            showToast("enter message")

        else {
            showProgress()
            val locationList = Geocoder(activity)
                    .getFromLocation(selectedLatLong!!.latitude, selectedLatLong!!.longitude, 1)
            if (locationList.size > 0) {
                val location = locationList[0]
                var address = location.getAddressLine(0).toString()
                getAddress(title!!,message,address)
            } else {
                showToast("Cant get your location.. try again")
                hideProgress()
            }
        }

    }

    private fun getAddress(title: String, message: String?, address: String) {
        hideProgress()
        confirmDialog(title!!, message!!, address)
    }

    private fun confirmDialog(title: String, message: String, address: String) {

        var dialog = Dialog(context)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.view_confirm)
        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)
        dialog.txtPrice.text = title
        dialog.txtMessage.text = message
        dialog.etAddress.setText(address)
        dialog.icClose.setOnClickListener { dialog.dismiss() }
        dialog.txtConfirm.setOnClickListener {
            uploadImage(dialog.etAddress.text.toString())
            dialog.dismiss()
            showProgress()
        }

        var window = dialog.window
        window.setGravity(Gravity.CENTER)
        window.setLayout(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT);


        dialog.show()


    }

    private fun setRecyclerView() {

        adapter = SingleImageSelecterAapter(context!!, this)
        rvGeneralPost.layoutManager = GridLayoutManager(context!!, 6)
        rvGeneralPost.adapter = adapter
    }

    private fun uploadImage(address: String) {
        var time = java.sql.Timestamp(Date().time)
        var timestamp = time.toString().replace(":", "").replace("-", "").replace(" ", "").replace(".", "")
        if (size == 0)
            alsoCreateRealEstatePost(address)
        else {
            mRef = fireStoreHelper.getStorageRef().child("estateImages").child(fireStoreHelper.currentUserUid() + timestamp)
            adapter!!.list!![count]?.let {
                mRef.putFile(it).addOnCompleteListener {
                    if (it.isSuccessful) {
                        it.result
                        mRef!!.downloadUrl.addOnSuccessListener { p0 ->
                            count += 1
                            mCurrentPhotoPath = mCurrentPhotoPath + p0.toString() + "#"
                            if (size == count)
                                alsoCreateRealEstatePost(address)
                            else
                                uploadImage(address)
                        }.addOnFailureListener {
                            hideProgress()
                        }
                    }
                }.addOnFailureListener {
                    hideProgress()
                }
            }
        }
    }


    private fun createEstate(address: String,pid:String) {

        val estate = Post.RealEstate(title = title!!, message = message!!,
                uid = fireStoreHelper.currentUserUid(),
                address = address,zipCode = user!!.zipCode,
                latlng = selectedLatLong!!.latitude.toString() + "," + selectedLatLong!!.longitude.toString(),
                images = mCurrentPhotoPath)

        fireStoreHelper.createEstate(estate,pid).addOnCompleteListener {
            if (it.isSuccessful) {
                hideProgress()
                showToast("Posted")
                activity!!.onBackPressed()
                hideProgress()
            } else {
                hideProgress()
                showToast("error")
            }
        }.addOnFailureListener {
            showToast("failed")
            hideProgress()
        }.addOnCanceledListener {
            showToast("cancled")
            hideProgress()
        }
    }

    private fun alsoCreateRealEstatePost(address: String) {

        var post=Post(subject = title!!, message = message!!,
                uid = user!!.uid,
                location = address,privacy = Constants.ADDRESSES,
                type = Post.postType.REAL_ESTATE,
                lat = selectedLatLong!!.latitude,long = selectedLatLong!!.longitude,
                imagesLinks = mCurrentPhotoPath,zipCode = user!!.zipCode)
        fireStoreHelper.createPost(post).addOnCompleteListener {
            if(it.isSuccessful)
            {
                createEstate(address,it.result.id)

            }else{
                hideProgress()
                showToast("unable to post")
            }
        }.addOnFailureListener {
            hideProgress()
            showToast("unable to post")
        }
    }

    private fun upDateMarker(pafKietLocation: LatLng) {

        if (marker==null)
        marker = mMap.addMarker(MarkerOptions().position(pafKietLocation)
                .title("Your Location").draggable(true))

        mMap.setMinZoomPreference(12.0f)
        mMap.setMaxZoomPreference(72.0f)
        mMap.moveCamera(CameraUpdateFactory.newLatLng(pafKietLocation))
        selectedLatLong = pafKietLocation

        mMap.setOnMapClickListener { latLng ->

            selectedLatLong = latLng
            marker!!.position = latLng
            //context!!.showToast(latLng.toString())
        }

        mMap.setOnMarkerDragListener(object : GoogleMap.OnMarkerDragListener {
            override fun onMarkerDragStart(arg0: Marker) {}

            override fun onMarkerDragEnd(arg0: Marker) {
                Log.d("System out", "onMarkerDragEnd...")
                selectedLatLong = arg0.position
                mMap.animateCamera(CameraUpdateFactory.newLatLng(arg0.position))
               // context!!.showToast(arg0.position.toString())
            }

            override fun onMarkerDrag(arg0: Marker) {}
        })
    }

}