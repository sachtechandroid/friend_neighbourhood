package com.sachtechsolution.neighbourhood.ui.home.adapters

import android.os.Bundle
import android.support.v4.app.FragmentActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseAcitivityListener
import com.sachtechsolution.neighbourhood.extension.getDateDifference
import com.sachtechsolution.neighbourhood.model.Notification
import com.sachtechsolution.neighbourhood.model.Post
import com.sachtechsolution.neighbourhood.model.User
import com.sachtechsolution.neighbourhood.ui.postDetails.PostDetailActivity
import kotlinx.android.synthetic.main.notification_item.view.*

/* whoGuri home_decor/appliances/home_decor */
class NotificationAdapter(val activity: FragmentActivity?, val baseAcitivityListener: BaseAcitivityListener) : RecyclerView.Adapter<NotificationAdapter.MyViewHolder>() {


    val notifications = ArrayList<Notification>()
    val posts = ArrayList<Post>()
    val users = ArrayList<User>()


    fun addNotification(notification: Notification, user: User?, post: Post) {
        notifications.add(notification)
        users.add(user!!)
        posts.add(post)
        notifyItemInserted(notifications.size - 1)
    }

    fun removeItem(position: Int){
        notifications.removeAt(position)
        users.removeAt(position)
        posts.removeAt(position)
        notifyItemRemoved(position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView: View = LayoutInflater.from(parent.context)
                .inflate(R.layout.notification_item, parent, false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.itemView.apply {
            if (!users[position].imageLink.isNullOrEmpty())
                Glide.with(activity!!).load(users[position].imageLink).into(notification_image)
            else
                notification_user_name.text=users[position].firstName.subSequence(0,1)
                notification_text.text=users[position].firstName+" "+users[position].lastName+" "+notifications[position].text
                notification_time.text= getDateDifference(notifications[position].date)
                notification_user_name.text = users[position].firstName.subSequence(0, 1)
                notification_text.text = users[position].firstName + notifications[position].text
                notification_time.text = getDateDifference(notifications[position].date)
        }

        holder.itemView.setOnClickListener {
            baseAcitivityListener.navigator.openActivity(PostDetailActivity::class.java, Bundle().apply {
                putParcelable("post", posts[position])
                putParcelable("user", users[position])
            })
        }
    }

    override fun getItemCount(): Int {
        return notifications.size
    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view)

}