package com.sachtechsolution.neighbourhood.ui.login.fragments

import android.os.Bundle
import android.util.Patterns
import android.view.View
import com.example.manish.internetstatus.OttoBus
import com.google.firebase.auth.FirebaseAuth
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_forget_password.*


class ForgetPasswordFragment : BaseFragment() {
    override fun viewToCreate(): Int {
        return R.layout.fragment_forget_password
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        OttoBus.bus.register(this)

        forget_pass_back_iv.setOnClickListener {
            fragmentManager?.popBackStackImmediate()
        }
        forget_pass_send_mail_tv.setOnClickListener {
            val email=forget_pass_email_tiet.text.toString().trim()
            if(Patterns.EMAIL_ADDRESS.matcher(email).matches()){
                sendForgotMessage(email)
            }
            else{
                showToast("Enter valid email")
            }
        }
    }
    fun sendForgotMessage(emailAddress:String)
    {
        showProgress()
        val auth = FirebaseAuth.getInstance()

        auth.sendPasswordResetEmail(emailAddress)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        showToast("Email sent to your account")
                        baseAcitivityListener.navigator.replaceFragment(LoginMainFragment::class.java,true)
                        hideProgress()
                    }else {
                        showToast(task.exception!!.message)
                        hideProgress()
                    }
                }.addOnFailureListener {
                    hideProgress()}
                .addOnCanceledListener {
                    hideProgress()}
    }

    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
    }


    @Subscribe
    fun getMessage(internetStatus:String) {

        if (internetStatus.equals("Not connected to Internet")){
            InternetStatusDialogFragment.newInstance().show(fragmentManager, "option_fragment")
        }else {
            showToast("internet enabled ")

        }

    }
}