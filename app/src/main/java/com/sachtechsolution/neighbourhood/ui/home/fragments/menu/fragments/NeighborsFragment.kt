package com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments

import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.example.manish.internetstatus.OttoBus
import com.google.firebase.firestore.DocumentSnapshot
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseActivity
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.getLocationMode
import com.sachtechsolution.neighbourhood.extension.showToast
import com.sachtechsolution.neighbourhood.ui.home.adapters.MemberListAdapter
import com.sachtechsolution.neighbourhood.ui.home.fragments.ListFRagment
import com.sachtechsolution.neighbourhood.ui.login.fragments.SignUpAddressFragment
import kotlinx.android.synthetic.main.fragment_neighbors.*


class NeighborsFragment : BaseActivity() {
    override fun fragmentContainer(): Int {
        return R.id.fragment_container
    }

    var state = 0
    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }
    val currentUser by lazy { pref.getUserFromPref() }
    var mAdapter: MemberListAdapter? = null
    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
    }


    override fun onResume() {
        super.onResume()
        OttoBus.bus.register(this)
    }

    fun getList() = list
    lateinit private var fList: ListFRagment

    //lateinit private var mList: MapFragment

    private var mapF: MapFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_neighbors)

        tab_layout.addTab(tab_layout.newTab().setText("List"))
        tab_layout.addTab(tab_layout.newTab().setText("Map"))
        fList = ListFRagment()
        replaceFragment(fList)
        val user = pref.getUserFromPref()!!
        var array: Array<String>
        if (user.society.equals(user.street))
            array = arrayOf(user.street, user.zipCode)
        else
            array = arrayOf(user.society, user.street, user.zipCode)

        val dataAdapter = ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, array)
        friendFilter.adapter = dataAdapter
        friendFilter.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                when (position) {
                    0 -> {
                        if (array.size == 2) {
                            if (state == 0)
                                getNeighborsByStreet()
                            else
                                mapF?.getUserStreet()
                        } else {
                            if (state == 0)
                                getNeighborsBySociety()
                            else
                                mapF?.getUserSociety()
                        }
                    }
                    1 -> {
                        if (array.size != 2) {
                            if (state == 0)
                                getNeighborsByStreet()
                            else
                                mapF?.getUserStreet()
                        } else if (array.size != 2) {
                            if (state == 0)
                                getNeighborsByZip()
                            else
                                mapF!!.getUserZip()
                        }
                    }
                    2 -> {
                        if (state == 0)
                            getNeighborsByZip()
                        else
                            mapF!!.getUserZip()
                    }
                }
            }
        }
        tab_layout.setOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(p0: TabLayout.Tab?) {
            }

            override fun onTabUnselected(p0: TabLayout.Tab?) {
            }

            override fun onTabSelected(p0: TabLayout.Tab?) {

                if (p0!!.position == 0) {
                    state = 0
                    replaceFragment(fList)
                } else if (p0.position == 1) {
                    if(getLocationMode() != 3) {
                        showToast("please select high accuracy option")
                        startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
                    } // do stuff
                  else{
                    state = 1
                    mapF = MapFragment()
                    replaceFragment(mapF!!)
                    friendFilter.setSelection(0)
                }
                    //mList.getUserSociety()
                }

            }

        })
        neighbors_back_iv.setOnClickListener {
            onBackPressed()
        }

    }

    private var list: MutableList<DocumentSnapshot>? = null

    private fun getNeighborsByZip() {
        // showProgress()
        fireStoreHelper.getAllNeighbors(currentUser!!.zipCode).addOnCompleteListener {
            if (it.isSuccessful) {
                list = it.result.documents
                if (state == 0)
                    fList.setList(list!!)
                hideProgress()
            } else {
                hideProgress()
                showToast("something went wrong")
            }
        }.addOnFailureListener {
            hideProgress()
            showToast("something went wrong")
        }
    }

    private fun getNeighborsByStreet() {
        //showProgress()
        fireStoreHelper.getNeighborsByStreet(currentUser!!.street).addOnCompleteListener {
            if (it.isSuccessful) {
                list = it.result.documents
                if (state == 0)
                    fList.setList(list!!)
                hideProgress()
            } else {
                hideProgress()
                showToast("something went wrong")
            }
        }.addOnFailureListener {
            hideProgress()
            showToast("something went wrong")
        }
    }

    private fun getNeighborsBySociety() {
        //showProgress()
        fireStoreHelper.getNeighborsBySociety(currentUser!!.society).addOnCompleteListener {
            if (it.isSuccessful) {
                list = it.result.documents
                if (state == 0)
                    fList.setList(list!!)

                hideProgress()
            } else {
                hideProgress()
                showToast("something went wrong")
            }
        }.addOnFailureListener {
            hideProgress()
            showToast("something went wrong")
        }
    }

    fun replaceFragment(fragment: Fragment) {
        var fragmentManager = supportFragmentManager
        var transaction = fragmentManager.beginTransaction()
        transaction.replace(R.id.fragment_container, fragment)
        transaction.commit()
    }
}
