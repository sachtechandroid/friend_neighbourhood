package com.sachtechsolution.neighbourhood.ui.home.fragments

import android.os.Bundle
import android.view.View
import com.example.manish.internetstatus.OttoBus
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.sachtechsolution.neighbourhood.ui.home.adapters.AllCategoriesAdapter
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_all_categories.*
import kotlin.collections.ArrayList


class FragmentAllCategories:BaseFragment(), AllCategoriesAdapter.onClickChoice {

    override fun onChoiceSelected(choice: String, image: Int, name: String) {
        var bundle=Bundle()
        bundle.putString(FragmentCategory.CATEGORY, choice)
        bundle.putInt("image",image)
        bundle.putString("name",name)
        baseAcitivityListener.navigator.replaceFragment(FragmentCategory::class.java,true,bundle)

    }

    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
    }


    @Subscribe
    fun getMessage(internetStatus:String) {

        if (internetStatus.equals("Not connected to Internet")){
            InternetStatusDialogFragment.newInstance().show(fragmentManager, "option_fragment")
        }else {
            showToast("internet enabled ")

        }

    }



    var images=ArrayList<Int>()
    override fun viewToCreate(): Int {
        return R.layout.fragment_all_categories
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        images.add(0, R.drawable.atm);
        images.add(1, R.drawable.bakery);
        images.add(2, R.drawable.cash);
        images.add(3, R.drawable.cycling);
        images.add(4, R.drawable.book);
        images.add(5, R.drawable.cafe);
        images.add(6, R.drawable.dentists_icon);
        images.add(7, R.drawable.electrician);
        images.add(8, R.drawable.gym);
        images.add(9, R.drawable.conditioner);
        images.add(10, R.drawable.hospital);
        images.add(11, R.drawable.jewelery);
        images.add(12, R.drawable.pet);
        images.add(13, R.drawable.plumbering);
        images.add(14,R.drawable.police);
        images.add(15,R.drawable.postoffice);
        images.add(16,R.drawable.agent);
        images.add(17,R.drawable.restraunt_icon);
        images.add(18,R.drawable.school);
        images.add(19,R.drawable.shoes);
        images.add(20,R.drawable.mall);
        images.add(21,R.drawable.leaves);
        images.add(22,R.drawable.travel);

    }

    override fun onResume() {
        super.onResume()
        OttoBus.bus.register(this)
        var placename = resources.getStringArray(R.array.places)
        var placetype= resources.getStringArray(R.array.place_type)
        var  allCategories= AllCategoriesAdapter(context!!,images,placename, placetype = placetype,listner = this)
        all_categories_rv.adapter=allCategories
    }
}