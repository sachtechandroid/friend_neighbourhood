package com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings.profile

import android.os.Bundle
import android.util.Patterns
import android.view.View
import com.example.manish.internetstatus.OttoBus
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.showToast
import com.sachtechsolution.neighbourhood.model.ContactInformation
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_contactnformation.*

class ContactnformationFragment : BaseFragment() {
   val fireStoreHelper by lazy { FireStoreHelper.getInstance() }

    override fun viewToCreate(): Int {
        return R.layout.fragment_contactnformation
    }

    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
    }


    @Subscribe
    fun getMessage(internetStatus:String) {

        if (internetStatus.equals("Not connected to Internet")){
            InternetStatusDialogFragment.newInstance().show(fragmentManager, "option_fragment")
        }else {
            showToast("internet enabled ")
        }

    }

    override fun onResume() {
        super.onResume()
        OttoBus.bus.register(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        txtDone.setOnClickListener {
            var email=etEmail.text.toString().trim()
            var homePhone=etHomePhone.text.toString().trim()
            var mobilePhone=etMobilePhone.text.toString().trim()

            if (!Patterns.EMAIL_ADDRESS.matcher(email).matches())
                context!!.showToast("enter valid email")
            else if (homePhone.length<8)
                context!!.showToast("enter valid home phone")
            else if (mobilePhone.length<8)
                context!!.showToast("enter valid mobile phone")
            else
                uploadData(ContactInformation(email,homePhone,mobilePhone))
        }
    }

    private fun uploadData(contacts: ContactInformation) {

        showProgress()
        fireStoreHelper.updateContactsInformation(contacts,pref.getUserFromPref()!!.uid).addOnCompleteListener {
            if (it.isSuccessful) {
                context!!.showToast("Updated")
                activity!!.onBackPressed()
                hideProgress()
            } else {
                context!!.showToast("error")
               hideProgress()
            }
        }.addOnFailureListener {
            context!!.showToast("error")
            hideProgress()
        }

    }
}
