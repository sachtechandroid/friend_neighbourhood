package com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments

import android.os.Bundle
import android.view.View
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.constants.PostInterests
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.ui.home.adapters.FollowInterestsAdapter
import kotlinx.android.synthetic.main.fragment_follow_interests.*

class FragmentFollowInterests : BaseFragment(), View.OnClickListener {

    var list=ArrayList<PostInterests>()
    var index=0
    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }
    var adapter: FollowInterestsAdapter?=null
    val uid by lazy { pref.getUserFromPref()!!.uid }



    override fun viewToCreate(): Int {
        return R.layout.fragment_follow_interests
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //set listner
        icBack.setOnClickListener(this)
        create_interests.setOnClickListener(this)

        setupAdapter()
    }

    private fun setupAdapter() {
        //showProgress()
        adapter = FollowInterestsAdapter(this,pref,resources.getStringArray(R.array.all_interest_array))
        recyclerView.adapter=adapter
        //getAllInterests()
    }

    override fun onClick(view: View?) {
        when(view!!.id)
        {
            R.id.icBack->activity!!.onBackPressed()
            /*R.id.create_interests->

                baseAcitivityListener.navigator.replaceFragment(FragmentAddInterests::class.java,true)
        */}
    }

    /*private fun getAllInterests() {
        val interest = resources.getStringArray(R.array.all_interest_array)
        if (index<interest.size) {
            getData(interest[index])
        }
        else{
            adapter!!.addItem(list)
            adapter!!.notifyDataSetChanged()
            hideProgress()
        }
    }

    private fun getData(type: String) {
        fireStoreHelper.checkfollow(type, uid).addOnCompleteListener {
            if (it.isSuccessful) {
                if (it.result.size() > 0) {
                    var interests=PostInterests(type=type,uid = uid,id = it.result.documents[0].id)
                    list.add(interests)
                    index += 1
                    getAllInterests()
                } else {
                    var interests=PostInterests(type=type,uid = uid)
                    list.add(interests)
                    index += 1
                    getAllInterests()
                }

            } else {
                var interests=PostInterests(type=type,uid = uid)
                list.add(interests)
                index += 1
                getAllInterests()
            }

        }

    }
*/
}