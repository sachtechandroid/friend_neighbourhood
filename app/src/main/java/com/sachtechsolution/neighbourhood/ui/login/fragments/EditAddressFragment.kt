package com.sachtechsolution.neighbourhood.ui.login.fragments

import android.location.Geocoder
import android.os.Bundle
import android.view.View
import com.facebook.AccessToken
import com.google.android.gms.maps.model.LatLng
import com.sachtechsolution.neighbourhood.HomePage
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.constants.Constants
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.model.User
import kotlinx.android.synthetic.main.fragment_edit_address.*


class EditAddressFragment : BaseFragment() {

    val firestore by lazy { FireStoreHelper.getInstance() }
    var token: AccessToken? = null
    override fun viewToCreate(): Int {
        return R.layout.fragment_edit_address
    }

    var address = ""//etAddress.text.toString()
    var society = ""
    var mainLocality = ""
    var zipCode = ""
    var apt = ""
    var street = ""
    var lat = 0.0
    var long = 0.0

    val latLong by lazy { arguments?.getParcelable<LatLng>("latLong") }
    val user by lazy { pref.getUserFromPref() }
    var invitationCode: String = ""

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (user != null) {
            val apt = user!!.address.split(",")[0]
            etAptNo.setText(apt)
            etStreetAddress.setText(user!!.street)
            etSocitey.setText(user!!.society)
            etMainLocality.setText(user!!.mainLocality)
            etZipCode.setText(user!!.zipCode)
        }

        if (arguments?.getString("invitationCode") ?: null != null) {
            //showProgress()
            invitationCode = arguments!!.getString("invitationCode")
            firestore.getUser(arguments!!.getString("uid")).addOnCompleteListener {
                if (it.isSuccessful) {
                    val user = it.result.toObject(User::class.java)
                    //var saddress = user!!.address.split(",")
                    lat = user!!.lat
                    long = user!!.lng
                    address = user.address
                    zipCode = user.zipCode
                    society = user.society
                    mainLocality = user.mainLocality
                    street = user.street
                    //apt = saddress[0].trim()

                    //etAptNo.setText(apt)
                    etStreetAddress.setText(street)
                    etSocitey.setText(society)
                    etMainLocality.setText(mainLocality)
                    etZipCode.setText(zipCode)
                    hideProgress()
                }
            }
        } else {
            if (arguments?.get("token") ?: null !== null) {
                token = arguments!!.get("token") as AccessToken
            }

            try {
                val locationList = Geocoder(context)
                        .getFromLocation(latLong!!.latitude, latLong!!.longitude, 3)
                if (locationList.size > 0 && locationList[0].hasLatitude()) {
                    val location = locationList[0]
                    lat = location!!.latitude
                    long = location!!.longitude
                    address = location.getAddressLine(0).toString()
                    zipCode = location.postalCode ?: ""

                    val addressArray = locationList[0].getAddressLine(0).split(",")
                    if (addressArray.size > 4) {
                        society = addressArray[1].trim().capitalize()
                        apt = addressArray[0].trim().capitalize()
                        street = addressArray[2].trim().capitalize()
                    }
                    if (location.locality != null) {
                        mainLocality = location.locality.trim().capitalize()
                    } else if (location.subAdminArea != null) {
                        mainLocality = location.subAdminArea.trim().capitalize()
                    }

                    if (location.subLocality != null) {
                        street = location.subLocality.trim().capitalize()
                    } else if (location.featureName != null) {
                        street = location.featureName.trim().capitalize()
                    }

                    if (street.equals(mainLocality))
                        street = ""

                    if (society.equals(street) || society.equals(mainLocality))
                        society = ""
                    if (street.isNullOrEmpty() || society.isNotEmpty()) {
                        street = society
                        society = ""
                    }
                    if (location.subThoroughfare != null)
                        apt = location.subThoroughfare
                    if (location.thoroughfare != null)
                        street = location.thoroughfare
                    //txtSocitey.text=Constants.SOCIETY
                }
                hideProgress()
                etAptNo.setText(apt)
                etStreetAddress.setText(street)
                if (!society.equals(street))
                    etSocitey.setText(society)
                etMainLocality.setText(mainLocality)
                etZipCode.setText(zipCode)
            } catch (e: Exception) {
                hideProgress()
            }
        }

        //etAddress.setText(Constants.ADDRESS)

/*
        etAptNo.setText("")
        etStreetAddress.setText("")
        etSocitey.setText(Constants.SOCIETY)
        etMainLocality.setText(Constants.MAIN_LOCALITY)
        etSubLocality.setText(Constants.SUB_LOCALITY)
        etZipCode.setText(zipCode)
*/
        ivClose.setOnClickListener {
            activity!!.onBackPressed()
        }
        etContinue.setOnClickListener {
            apt = etAptNo.text.toString().trim()
            street = etStreetAddress.text.toString().trim()
            society = etSocitey.text.toString().trim()
            mainLocality = etMainLocality.text.toString().trim()
            zipCode = etZipCode.text.toString().trim()
            address = ""//etAddress.text.toString()

            street = street.replace("-", " ")
            society = society.replace("-", " ")
            mainLocality = mainLocality.replace("-", " ")

            street = street.replace(",", " ")
            society = society.replace(",", " ")
            mainLocality = mainLocality.replace(",", " ")

            street = street.capitalize()
            society = society.capitalize()
            mainLocality = mainLocality.capitalize()

            if (apt.isNullOrEmpty()) {
                showToast("Enter your apartment or flat number")
            } else if (street.isNullOrEmpty()) {
                showToast("Enter your street address")
            } else if (mainLocality.isNullOrEmpty()) {
                showToast("Enter your city")
            } else if (zipCode.isNullOrEmpty() || zipCode.length < 4) {
                showToast("Enter your zip code")
            } else {
                //showProgress()

                try {
                    val list = Geocoder(activity).getFromLocationName("$society $street $mainLocality", 1)
                    if (list.size > 0) {
                        Constants.MAIN_LOCALITY = mainLocality
                        if (society.isNullOrEmpty())
                            Constants.SOCIETY = street
                        else
                            Constants.SOCIETY = society
                        Constants.STREET = street
                        Constants.ZIP_CODE = zipCode
                        if (street.equals(society))
                            Constants.ADDRESS = "$apt, $street, $mainLocality ${Constants.ZIP_CODE}"
                            else
                        Constants.ADDRESS = "$apt, $society, $street, $mainLocality ${Constants.ZIP_CODE}"
                        Constants.LAT = list[0].latitude
                        Constants.LONG = list[0].longitude

                        if (user == null && arguments?.get("token") ?: null == null && arguments?.get("invitationCode") ?: null == null) {
                            hideProgress()
                            baseAcitivityListener.navigator.replaceFragment(SignUpCredentialFragment::class.java, true)

                        } else if (invitationCode.isNotEmpty()) {
                            hideProgress()
                            var bundle = Bundle()
                            bundle.putString("invitationCode", invitationCode)
                            baseAcitivityListener.navigator.replaceFragment(SignUpCredentialFragment::class.java, true, bundle)
                        } else if (token !== null) {
                            hideProgress()
                            var bundle = Bundle()
                            bundle.putParcelable("token", token)
                            baseAcitivityListener.navigator.replaceFragment(SignUpInfoFragment::class.java, true, bundle)
                        } else {

                            user!!.mainLocality = Constants.MAIN_LOCALITY
                            user!!.street = Constants.STREET
                            user!!.society = Constants.SOCIETY
                            user!!.zipCode = Constants.ZIP_CODE
                            user!!.address = Constants.ADDRESS
                            user!!.lat = Constants.LAT
                            user!!.lng = Constants.LONG
                            showProgress()
                            firestore.setUser(user!!.uid, user!!).addOnCompleteListener {
                                if (it.isSuccessful) {
                                    hideProgress()
                                    pref.setUserToPref(user!!)
                                    firestore.getAddresses(user!!.zipCode + "," + user!!.mainLocality).addOnCompleteListener {
                                        if (it.isSuccessful) {
                                            Constants.ADDRESSES.clear()
                                            Constants.SOCIETIES.clear()
                                            it.result.documents.forEach {
                                                val list3 = it.data?.get("society") as ArrayList<String>
                                                if (it.id.equals(user?.street))
                                                    Constants.SOCIETIES = list3
                                                list3.forEach {
                                                    Constants.ADDRESSES.add(it)
                                                }
                                            }
                                            if (!Constants.SOCIETIES.contains(user!!.society))
                                                Constants.SOCIETIES.add(user!!.society)
                                            val map = HashMap<String, ArrayList<String>>()
                                            map.put("society", Constants.SOCIETIES)
                                            firestore.addAddress(zip = user!!.zipCode + "," + user!!.mainLocality, street = user!!.street,
                                                    map = map).addOnCompleteListener {
                                            }
                                            val map2 = HashMap<String, String>()
                                            map2.put("soci", "yo")
                                            firestore.addAddressdummy(user!!.zipCode + "," + user!!.mainLocality, map2)
                                            hideProgress()
                                            baseAcitivityListener.navigator.openActivity(HomePage::class.java)
                                            //activity!!.onBackPressed()
                                        } else {
                                            hideProgress()
                                            showToast("Something went wrong")
                                        }
                                    }.addOnFailureListener {
                                        hideProgress()
                                        showToast("Something went wrong")
                                    }

                                } else {
                                    hideProgress()
                                    showToast("Something went wrong")
                                }
                            }
                        }
                    } else {
                        showToast("Wrong Address. Enter correct Address")
                    }
                } catch (e: Exception) {
                    hideProgress()
                    showToast("Wrong Address. Enter correct Address")
                }
            }
        }
    }
}
