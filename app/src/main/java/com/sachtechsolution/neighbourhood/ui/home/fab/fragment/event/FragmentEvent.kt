package com.sachtechsolution.neighbourhood.ui.home.fab.fragment.event

import android.os.Bundle
import android.view.View
import com.example.manish.internetstatus.OttoBus
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.constants.Constants
import com.sachtechsolution.neighbourhood.extension.gone
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.sachtechsolution.neighbourhood.ui.home.fab.adapter.EventAdapter
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.custom_toolbar.*
import kotlinx.android.synthetic.main.fragment_event.*

class FragmentEvent : BaseFragment(), EventAdapter.OpenDone {

    private var subAddress: String = ""
    //lateinit var adapter: ChooseNeighborsAdapter

    override fun OnEventDone() {

    }

    override fun viewToCreate(): Int {
        return R.layout.fragment_event
    }


    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
    }


    @Subscribe
    fun getMessage(internetStatus: String) {

        if (internetStatus.equals("Not connected to Internet")) {
            InternetStatusDialogFragment.newInstance().show(fragmentManager, "option_fragment")
        } else {
            showToast("internet enabled ")

        }

    }

    override fun onResume() {
        super.onResume()
        OttoBus.bus.register(this)
    }


    lateinit var list: ArrayList<String>

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val user= pref.getUserFromPref()!!

        list= ArrayList()
        list.add(user.society)
        sociteyRB.text = user.society
        subLocalityRB.text = user.street
        zipCodeRB.text = user.mainLocality+" + Nearby"

        if (user.society.equals(user.street)){
            sociteyRB.gone()
            subLocalityRB.isChecked=true
        }
        sociteyRB.setOnCheckedChangeListener { buttonView, isChecked ->

            if (isChecked) {
                list.clear()
                list.add(user.society)
            }
        }

        subLocalityRB.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                list.clear()
                list= Constants.SOCIETIES
            }
        }
        zipCodeRB.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                list.clear()
                list=Constants.ADDRESSES
            }
        }


/*
        adapter = ChooseNeighborsAdapter(subAddress)
        addresses_list.adapter = adapter
        private_area.text = subAddress
        private_area.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                addresses_list.gone()
                adapter.addressList.clear()
                adapter.checkedList.clear()
            }
        }
        protected_area.setOnCheckedChangeListener { buttonView, isChecked ->
            if(isChecked){
                addresses_list.visible()
            }
        }
*/
        cancelLayout.setOnClickListener { activity!!.onBackPressed() }
        nextLayout.setOnClickListener {
/*            if (adapter!!.addressList.size == 0)
                adapter!!.addressList.add(subAddress)*/
            baseAcitivityListener.navigator.replaceFragment(FragmentAddNewEvent::class.java,
                    true, Bundle().apply { putStringArrayList("list", list) })
        }

    }
}