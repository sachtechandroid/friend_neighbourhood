package com.sachtechsolution.neighbourhood.ui.postDetails

import android.os.Bundle
import android.support.v4.view.ViewPager
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import android.view.View
import com.bumptech.glide.Glide
import com.example.manish.internetstatus.OttoBus
import com.kennyc.bottomsheet.BottomSheet
import com.kennyc.bottomsheet.BottomSheetListener
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseActivity
import com.sachtechsolution.neighbourhood.chat.ActivityChatDetails
import com.sachtechsolution.neighbourhood.chat.model.Friend
import com.sachtechsolution.neighbourhood.constants.Constants
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.*
import com.sachtechsolution.neighbourhood.model.Post
import com.sachtechsolution.neighbourhood.model.User
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.sachtechsolution.neighbourhood.ui.extra.ImageViewPagerAdapter
import com.sachtechsolution.neighbourhood.ui.home.AnotherUserProfileActivity
import com.sachtechsolution.neighbourhood.ui.home.fragments.menu.activity.MenuBaseActivity
import com.sachtechsolution.neighbourhood.ui.sale.adapter.SalesAndFreeImageAdapter
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.activity_sale_post_detail.*

class SalePostDetailActivity : BaseActivity(), View.OnClickListener, SalesAndFreeImageAdapter.OpenDetailActivity, BottomSheetListener {

    override fun onSheetItemSelected(p0: BottomSheet, item: MenuItem?, p2: Any?) {

        if (item!!.title.equals("Share"))
            share("testing for share")
        else if (item!!.title.equals("Report item"))
            showToast("Report item here")
        else if (item!!.title.equals("Hide item from feed"))
            deletePost(post.pid)
    }


    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
    }


    @Subscribe
    fun getMessage(internetStatus: String) {

        if (internetStatus.equals("Not connected to Internet")) {
            InternetStatusDialogFragment.newInstance().show(supportFragmentManager, "option_fragment")
        } else {
            showToast("internet enabled ")

        }

    }

    override fun onResume() {
        super.onResume()
        OttoBus.bus.register(this)
    }

    override fun onSheetDismissed(p0: BottomSheet, p1: Any?, p2: Int) {
    }

    override fun onSheetShown(p0: BottomSheet, p1: Any?) {
    }

    override fun fragmentContainer(): Int {
        return 0
    }

    override fun onOpenActivity(post: Post, user: User, position: Int) {
        navigator.openActivity(SalePostDetailActivity::class.java, Bundle().apply {
            putParcelable("post", post)
            putParcelable("user", user)
            putInt("position", position)
            finish()
        })
    }

    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }
    val post by lazy { intent.getParcelableExtra<Post>("post") }
    val user by lazy { intent.getParcelableExtra<User>("user") }
    val position by lazy { intent.getIntExtra("position", -1) }
    var salesAndFreeImageAdapter: SalesAndFreeImageAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sale_post_detail)
        setImagePager()
        if (post.uid.equals(pref.getUserFromPref()!!.uid)) {
            txtUname.gone()
            sale_delete.visible()
            sale_report.gone()
        } else {
            sale_report.visible()
            sale_delete.gone()
            txtUname.visible()
        }
        //listner
        btnBack.setOnClickListener(this)
        sale_report.setOnClickListener(this)
        sale_delete.setOnClickListener(this)

        setData()
        getPosts()
        txtName.setOnClickListener { openCustomActivity(post.uid) }
        post_detail_user_image.setOnClickListener { openCustomActivity(post.uid) }
        post_detail_user_name_latter.setOnClickListener { openCustomActivity(post.uid) }
    }

    fun OpenBottomSheet() {
        BottomSheet.Builder(this)
                .setSheet(R.menu.sale_sheet)
                .setListener(this)
                .`object`("Some object")
                .show()
    }

    fun openCustomActivity(uid: String) {
        if (uid.equals(pref.getUserFromPref()!!.uid))
            openActivity<MenuBaseActivity>(MenuBaseActivity.FRAGMENT, MenuBaseActivity.PROFILE)
        else
            openActivity<AnotherUserProfileActivity>("uid", uid)

    }

    private fun setData() {
        salesAndFreeImageAdapter = SalesAndFreeImageAdapter(this!!, this)
        txtTime.text = getDateDifference(post.date)
        txtUname.text = "Message " + user!!.firstName
        txtName.text = user!!.firstName + "" + user.lastName
        post_detail_user_name_latter.text = user!!.firstName.subSequence(0, 1)
        txtDetail.text = post.sale!!.detail
        if (post.sale!!.price.toString().equals("0"))
            txtPrice.text = "free"
        else
            txtPrice.text = "Price " + post.sale!!.price.toString()

        image_recycler.adapter = salesAndFreeImageAdapter
        Glide.with(this).load(user.imageLink).into(post_detail_user_image)
        txtUname.setOnClickListener {
            var friend = Friend(user!!.uid, user!!.firstName + " " + user!!.lastName, user!!.imageLink, user!!.street)
            ActivityChatDetails.navigate((this as AppCompatActivity?)!!, txtUname, friend)

        }
    }

    val cUser by lazy { pref!!.getUserFromPref()!! }
    private fun getPosts() {

        fireStoreHelper.getPostsByType(Post.postType.FOR_SALE, zipCode = cUser.zipCode,
                society = cUser.society).addOnCompleteListener {
            if (it.isSuccessful) {
                it.result.documents.forEach {
                    val post = it.toObject(Post::class.java)
                    var next = false
                    post!!.privacy.forEach {
                        if (it.equals(pref.getUserFromPref()!!.society)) {
                            next = true
                        }
                    }
                    if (next) {

                                    salesAndFreeImageAdapter!!.addItem(post)
                                    salesAndFreeImageAdapter!!.notifyDataSetChanged()
                    }
                }

            }
        }.addOnFailureListener {
            showToast(it.message!!)
        }
    }

    private fun setImagePager() {
        val size = post.imagesLinks.split("#").size - 1
        viewpager.adapter = ImageViewPagerAdapter(supportFragmentManager, post.imagesLinks)
        viewpager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(p0: Int) {
            }

            override fun onPageScrolled(p0: Int, p1: Float, p2: Int) {
            }

            override fun onPageSelected(p0: Int) {
            }
        })
    }

    override fun onClick(p0: View?) {

        when (p0!!.id) {
            R.id.btnBack -> onBackPressed()
            R.id.sale_delete -> deletePost(post.pid)
            R.id.sale_report -> reportPost()
        }
    }

    fun reportPost() {
        fireStoreHelper.createReport(uid = post.uid, pid = post.pid, post = post).addOnCompleteListener {
            if (it.isSuccessful) {
                showToast("Reported Successfully")
            } else {
                showToast("Not Reported")
            }
        }.addOnFailureListener {
            showToast(it.message!!) }

    }

    fun deletePost(id: String) {
        showProgress()
        fireStoreHelper.deletePost(id).addOnCompleteListener {
            if (it.isSuccessful) {
                fireStoreHelper.deleteNotification(id).addOnCompleteListener {
                    next()
                }
            } else {
                //showToast("error")
                hideProgress()
            }
            hideProgress()
        }.addOnFailureListener {
            showToast("failed")
            hideProgress()
        }
    }

    private fun next() {
        Constants.DELETED_POSITION = position
        showToast("Post deleted")
        hideProgress()
        finish()
    }
}
