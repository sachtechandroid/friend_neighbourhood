package com.sachtechsolution.neighbourhood.ui.home.fab.fragment.poll

import android.os.Bundle
import android.view.View
import com.sachtechsolution.neighbourhood.HomePage
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.intracter.Api_Hitter
import com.sachtechsolution.neighbourhood.model.Poll
import com.sachtechsolution.neighbourhood.model.Post
import com.sachtechsolution.neighbourhood.model.User
import com.sachtechsolution.neighbourhood.ui.home.fab.adapter.AddChoiceAdapter
import kotlinx.android.synthetic.main.fragment_add_poll_choices.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class AddPollChoicesFragment : BaseFragment() {

    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }
    val question by lazy { arguments!!.getString("question") }
    val desc by lazy { arguments!!.getString("description") }
    val addressList by lazy { arguments!!.getStringArrayList("list") }
    lateinit var adapter: AddChoiceAdapter



    override fun viewToCreate(): Int {
        return R.layout.fragment_add_poll_choices
    }

     override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

             adapter = AddChoiceAdapter()
             add_poll_send2.setOnClickListener {

            val choices = ArrayList<String>()
            val choice1 = choice_edit_1.text.toString()
            val choice2 = choice_edit_2.text.toString()
            if (!choice1.isNullOrEmpty())
                choices.add(choice1)
            if (!choice2.isNullOrEmpty())
                choices.add(choice2)

                 adapter.arrayList.forEach {
                if (!it.isNullOrEmpty())
                    choices.add(it)
            }
            if (choices.size<2){
                showToast("Add atleast two choices")
            }else{
                //val array = arrayOfNulls<String>(choices.size)
                //choices.toArray(array)
                showProgress()
                val poll= Poll(question,desc,choices.size,choices)
                val post=Post(uid = fireStoreHelper.currentUserUid(),type = Post.postType.POLL,society = pref.getUserFromPref()!!.society,
                        zipCode= pref.getUserFromPref()!!.zipCode,poll = poll,privacy = addressList)

                fireStoreHelper.createPost(post).addOnCompleteListener {
                    if (it.isSuccessful) {

                        hideProgress()
                        showToast("Posted")
                        baseAcitivityListener.navigator.openActivity(HomePage::class.java)
                        activity!!.finish()
                        /*fireStoreHelper.getAllNeighbors(pref.getUserFromPref()!!.zipCode).addOnCompleteListener {
                            if (it.isSuccessful) {
                                var token = ""
                                it.result.documents.forEach {
                                    if (!it.id.equals(pref.getUserFromPref()!!.uid)&& addressList.contains(it.toObject(User::class.java)!!.society))
                                        token = token + "," + it.toObject(User::class.java)!!.token
                                }
                                if (!token.isNullOrEmpty()) {
                                    token = token.substring(1)
                                    val title = "${pref.getUserFromPref()!!.firstName} ${pref.getUserFromPref()!!.lastName} add a poll in neighborhood"

                                    val callback = Api_Hitter.ApiInterface("http://stsmentor.com/").sendNotificationToAll(title, token, "zxc")
                                    callback.enqueue(object : Callback<ResponseBody> {
                                        override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                                            next()
                                        }

                                        override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                                            next()
                                        }
                                    })
                                } else {
                                    next()
                                }
                            }
                        }.addOnFailureListener {
                            next()
                        }
*/
                    } else {
                        showToast("unable to Post")
                        hideProgress()
                    }
                }.addOnFailureListener {
                    showToast("unable to Post")
                    hideProgress()
                }.addOnCanceledListener{
                    showToast("unable to Post")
                    hideProgress()
                }

            }
        }
        recycler_poll.adapter = adapter
    }

    private fun next() {
        hideProgress()
        showToast("Posted")
        baseAcitivityListener.navigator.openActivity(HomePage::class.java)
        activity!!.finish()
    }
}
