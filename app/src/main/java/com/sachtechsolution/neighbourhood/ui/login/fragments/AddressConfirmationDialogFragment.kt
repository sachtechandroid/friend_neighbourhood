package com.sachtechsolution.neighbourhood.ui.login.fragments

import android.location.Geocoder
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.app.FragmentManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.Toast
import com.example.manish.internetstatus.OttoBus
import com.google.android.gms.maps.model.LatLng
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.constants.Constants
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.showToast
import com.sachtechsolution.neighbourhood.extension.visible
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.view_address_confirm.*


open class AddressConfirmationDialogFragment : DialogFragment(), View.OnClickListener {

    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.ivClose ->
                dismiss()
            R.id.txtContinue -> {
                dismiss()
                listner!!.onConfirm("")
            }
            R.id.txtPickOwnAddress -> {
                dismiss()
                listner!!.onConfirm("pickAddress")
            }
            R.id.txtEdit-> {
                dismiss()
                listner!!.onEdit()
            }

        }
    }


    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
    }


    @Subscribe
    fun getMessage(internetStatus: String) {

        if (internetStatus.equals("Not connected to Internet")) {
            InternetStatusDialogFragment.newInstance().show(fragmentManager, "option_fragment")
        } else {
            context!!.showToast("internet enabled ")

        }

    }

    override fun onResume() {
        super.onResume()
        OttoBus.bus.register(this)
    }

    private fun updateData() {
        try {
            val locationList = Geocoder(context)
                    .getFromLocation(latlang!!.latitude, latlang!!.longitude, 3)
            if (locationList.size > 0) {
                val location = locationList[0]
                Constants.LAT = latlang!!.latitude
                Constants.LONG = latlang!!.longitude
                Constants.ADDRESS = location.getAddressLine(0).toString()
                Constants.ZIP_CODE = location.postalCode

                val addressArray = locationList[0].getAddressLine(0).split(",")
                if (addressArray.size> 4)
                    Constants.SOCIETY = addressArray[1]

                etAddress.text = location.getAddressLine(0).toString()
                if (location.locality != null) {
                    txtStreetAddress.text = location.locality
                    Constants.MAIN_LOCALITY = location.locality
                } else if (location.subAdminArea != null) {
                    txtStreetAddress.text = location.subAdminArea
                    Constants.MAIN_LOCALITY = location.subAdminArea
                }

                if (location.subLocality != null) {
                    txtSubLocality.text = location.subLocality
                    Constants.STREET = location.subLocality
                } else if (location.featureName != null) {
                    txtSubLocality.text = location.featureName
                    Constants.STREET= location.featureName
                }
                if (location.thoroughfare!= null) {
                    txtSubLocality.text = location.thoroughfare
                    Constants.STREET = location.thoroughfare
                }

                if (Constants.MAIN_LOCALITY.isNullOrEmpty() || Constants.STREET.isNullOrEmpty()) {
                    Toast.makeText(context, "Try again", Toast.LENGTH_SHORT).show()
                    this.dismiss()
                }

                if (Constants.SOCIETY.equals(Constants.STREET)||Constants.SOCIETY.equals(Constants.MAIN_LOCALITY))
                    Constants.SOCIETY=""
                txtSocitey.text=Constants.SOCIETY
            }
        } catch (e: Exception) {
            Toast.makeText(context, "Try again", Toast.LENGTH_SHORT).show()
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)
        return inflater.inflate(R.layout.view_address_confirm, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        updateData()
        if (isInvitation)
        {
            txtPickOwnAddress.visible()
        }

        //listner
        ivClose.setOnClickListener(this)
        txtContinue.setOnClickListener(this)
        txtPickOwnAddress.setOnClickListener(this)
        txtEdit.setOnClickListener(this)
    }

    companion object {
        var latlang: LatLng? = null
        var listner: AddressConfirm? = null
        var fragmentManager: FragmentManager? = null
        var isInvitation=false
        fun newInstance(selectedLatLong: LatLng?, listner: AddressConfirm, isInvitation: Boolean): AddressConfirmationDialogFragment {
            latlang = selectedLatLong
            this.listner = listner
            this.isInvitation= isInvitation
            return AddressConfirmationDialogFragment()
        }
    }

    interface AddressConfirm {
        fun onConfirm(s: String)
        fun onEdit()
    }
}