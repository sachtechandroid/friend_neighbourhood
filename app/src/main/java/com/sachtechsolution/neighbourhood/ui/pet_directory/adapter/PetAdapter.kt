package com.sachtechsolution.neighbourhood.ui.pet_directory.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.constants.PetDirectory
import kotlinx.android.synthetic.main.item_pet.view.*

class PetAdapter(val context:Context,val click: ItemClickListener):RecyclerView.Adapter<PetAdapter.ViewHolder>() {
    var petlist=ArrayList<PetDirectory>()


    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val v:View=LayoutInflater.from(p0.context).inflate(R.layout.item_pet,p0,false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return petlist.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Glide.with(context).load(petlist[position].petImage).into(holder.itemView.pet_image)

        if (petlist[position].userPic.isNotEmpty())
        Glide.with(context).load(petlist[position].userPic).into(holder.itemView.user_image)
        holder.itemView.user_name.text=petlist[position].userName
        holder.itemView.pet_name.text=petlist[position].petName
        holder.itemView.setOnClickListener {
            click.clickListner(petlist[position])
        }
    }

    fun update(pet: PetDirectory) {
        petlist.add(pet)
        notifyDataSetChanged()
    }

    fun updateAll(petList: ArrayList<PetDirectory>) {
        this.petlist.addAll(petList)
        notifyDataSetChanged()
    }

    fun clearAll() {

        petlist.clear()
        notifyDataSetChanged()
    }

    class ViewHolder(v:View):RecyclerView.ViewHolder(v) {

    }
    interface ItemClickListener{
        fun clickListner(pos: PetDirectory)
    }
}