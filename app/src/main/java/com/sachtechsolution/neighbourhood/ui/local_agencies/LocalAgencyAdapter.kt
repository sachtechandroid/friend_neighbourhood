package com.sachtechsolution.neighbourhood.ui.local_agencies

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import com.bumptech.glide.Glide
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.extension.gone
import com.sachtechsolution.neighbourhood.model.places_bussiness.ResultsItem
import kotlinx.android.synthetic.main.item_businesses.view.*

class LocalAgencyAdapter(val context: Context, val pBar: ProgressBar) : RecyclerView.Adapter<LocalAgencyAdapter.ViewHolder>() {
    var list = ArrayList<ResultsItem?>()

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val v: View = LayoutInflater.from(p0.context).inflate(R.layout.item_businesses, p0, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.business_name.text = list[position]!!.name
        holder.itemView.counting.text = position.plus(1).toString() + "."
        Glide.with(context).load(list[position]!!.icon).into(holder.itemView.ivCategory)
        holder.itemView.txtRecommendationCount.text = list[position]!!.vicinity
        if (position>2)
            pBar.gone()


    }

    fun update(list: List<ResultsItem?>?) {
        this.list.addAll(list!!)
        notifyDataSetChanged()
    }

    class ViewHolder(v: View) : RecyclerView.ViewHolder(v) {

    }

}