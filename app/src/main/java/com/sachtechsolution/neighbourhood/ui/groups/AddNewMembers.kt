package com.sachtechsolution.neighbourhood.ui.groups

import android.os.Bundle
import android.os.Handler
import android.util.Patterns
import android.view.View
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.model.Group
import com.sachtechsolution.neighbourhood.model.User
import com.sachtechsolution.neighbourhood.ui.home.adapters.MemberListAdapter
import kotlinx.android.synthetic.main.fragment_add_members.*

/* whoGuri 13/10/18 */
class AddNewMembers : BaseFragment() {
    override fun viewToCreate(): Int {
        return R.layout.fragment_add_members
    }

    val fireBaseHelper by lazy { FireStoreHelper.getInstance() }
    val group by lazy { arguments!!.getParcelable<Group>("group") }

    private var mAdapter: MemberListAdapter? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListener()
        mAdapter = MemberListAdapter(this!!.activity!!, true)
        neighbors_list.adapter = mAdapter
        getNeighborsByZip()
    }

    val uid by lazy { fireBaseHelper.currentUserUid() }
    private fun setListener() {
        createGroupBack.setOnClickListener { activity!!.onBackPressed() }
        create_group.setOnClickListener {
            if (mAdapter!!.selectedList.size == 0) {
                activity!!.onBackPressed()
            } else {
                showProgress()
                group.members.addAll(mAdapter!!.selectedList)
                val fcuid=fireBaseHelper.currentUserUid()
                if (!group.members.contains(fcuid))
                    group.members.add(fcuid)
                //mAdapter!!.selectedList
                fireBaseHelper.updateGroup(gid = group.gid, group = group).addOnCompleteListener {
                    if (it.isSuccessful) {
                        mAdapter!!.selectedList.forEach {
                            val id = it
                            fireBaseHelper.getUser(id).addOnCompleteListener {
                                if (it.isSuccessful) {
                                    val user = it.result.toObject(User::class.java)
                                    if (!user!!.groups.contains(group.gid)) {
                                        user!!.groups.add(group.gid)
                                        fireBaseHelper.setUser(id, user)
                                    }
                                }
                            }
                        }
                        Handler().postDelayed({
                            hideProgress()
                            showToast("Group updated")
                            activity!!.finish()
                            baseAcitivityListener.navigator.openActivity(GroupActivity::class.java)
                        }, 2000)
                    } else
                        showError()
                }.addOnFailureListener { showError() }
            }
        }
        addMember.setOnClickListener {
            val email = groupEmail.text.toString()
            if (!Patterns.EMAIL_ADDRESS.matcher(email).matches())
                showToast("Enter valid email")
            else {
                showProgress()
                fireBaseHelper.getUserByEmail(email).addOnCompleteListener {
                    if (it.isSuccessful) {
                        if (it.result?.documents?.size ?: 0 != 0) {
                            val user = it.result.documents[0].toObject(User::class.java)
                            if (!group.members.contains(user!!.uid))
                                group.members.add(user!!.uid)
                            fireBaseHelper.updateGroup(gid = group.gid, group = group).addOnCompleteListener {
                                if (it.isSuccessful) {
                                    if (!user!!.groups.contains(group.gid)) {
                                        user!!.groups.add(group.gid)
                                        fireBaseHelper.setUser(user.uid, user).addOnCompleteListener {
                                            if (it.isSuccessful) {
                                                hideProgress()
                                                showToast("New member Added")
                                                activity!!.finish()
                                                baseAcitivityListener.navigator.openActivity(GroupActivity::class.java)
                                            }
                                        }.addOnFailureListener { showError() }
                                    } else {
                                        hideProgress()
                                        showToast("All Ready a member")
                                        activity!!.finish()
                                        baseAcitivityListener.navigator.openActivity(GroupActivity::class.java)
                                    }
                                } else
                                    showError()
                            }.addOnFailureListener { showError() }

                        } else {
                            hideProgress()
                            showToast("No user found with this Email")
                        }
                        //if (it.exresult.toObjects())
                    } else
                        showError()
                }.addOnFailureListener { showError() }
            }
        }
    }

    private fun showError() {
        hideProgress()
        showToast("Try again")
    }

    private fun getNeighborsByZip() {
        showProgress()
        fireBaseHelper.getAllNeighbors(pref.getUserFromPref()!!.zipCode).addOnCompleteListener {
            if (it.isSuccessful) {
                mAdapter?.add2(it.result.documents, group.members)
                hideProgress()
            } else {
                hideProgress()
                showToast("something went wrong")
            }
        }.addOnFailureListener {
            hideProgress()
            showToast("something went wrong")
        }
    }


}