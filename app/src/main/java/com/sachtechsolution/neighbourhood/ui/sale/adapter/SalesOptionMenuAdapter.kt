package com.sachtechsolution.neighbourhood.ui.sale.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sachtechsolution.neighbourhood.R
import kotlinx.android.synthetic.main.sales_menu_options_item.view.*

class SalesOptionMenuAdapter(var listner:Sorting): RecyclerView.Adapter<SalesOptionMenuAdapter.ViewHolder>() {

    val optionMenuItems= arrayListOf("Free","Discount","Showing","Sort")
    val images= arrayListOf(R.drawable.ic_free,R.drawable.discount,R.drawable.ic_area,R.drawable.ic_sort)

companion object {
    val DISCOUNT="Discount"
    val SHOWING="Showing"
    val SORT="Sort"
    val FREE="Free"
}

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
      val v:View=LayoutInflater.from(p0.context).inflate(R.layout.sales_menu_options_item,p0,false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return 4
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        p0.itemView.option_name.text = optionMenuItems[p1]
        p0.itemView.icon.setImageResource(images[p1])

        p0.itemView.setOnClickListener {
            listner.onClickSorting(optionMenuItems[p1])
        }
    }

    interface Sorting
    {
        fun onClickSorting(sorting: String)
    }
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }
}