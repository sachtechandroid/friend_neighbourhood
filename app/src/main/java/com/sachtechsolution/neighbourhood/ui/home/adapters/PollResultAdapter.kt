package com.sachtechsolution.neighbourhood.ui.home.adapters

/* whoGuri 4/9/18 */
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import com.sachtechsolution.neighbourhood.R
import kotlinx.android.synthetic.main.poll_result_item.view.*

class PollResultAdapter(val context: Context, val choices: ArrayList<String>, val results: ArrayList<Int>, val totalPolls: Int) : RecyclerView.Adapter<PollResultAdapter.ViewHolder>() {


    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val itemView: View = LayoutInflater.from(context)
                .inflate(R.layout.poll_result_item, p0, false)
        return ViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return choices.size
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {

        p0.itemView.apply {
            choice_text.text = choices[p1]
            var perecent = 0

            if (totalPolls != 0)
                perecent = (results[p1] * 100) / totalPolls

            choice_percent.text = "$perecent %"
            poll_result_view.layoutParams = LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.MATCH_PARENT, perecent.toFloat())

        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}