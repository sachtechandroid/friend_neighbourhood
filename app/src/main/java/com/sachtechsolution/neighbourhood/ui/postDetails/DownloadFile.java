package com.sachtechsolution.neighbourhood.ui.postDetails;

/* whoGuri 21/9/18 */

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.sachtechsolution.neighbourhood.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by dell on 4/27/2018.
 */

public class DownloadFile {
    Context context;
    String postUrl;
    ArrayList<String> key;// =new ArrayList();
    ArrayList<String> images;// =new ArrayList();
    ArrayList<Uri> uris;// =new ArrayList();
    String text = "";
    ImageView sharebtn;
    int size = 0;
    int count = 0;

    public DownloadFile(Context context, String postUrl, String key, String text, ImageView sharebtn) {
        this.context = context;
        this.key = new ArrayList();
        this.images = new ArrayList();
        this.uris = new ArrayList();
        String images[] = postUrl.split("#");
        size = 1;//images.length;
        for (int i = 0; i < size; i++) {
            this.images.add(images[i]);
            this.key.add(key + i+".jpg");
        }
        this.text = text;
        this.postUrl = postUrl;
        this.sharebtn = sharebtn;
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((Activity) context, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 10);
        } else {
            Toast.makeText(context, "wait while downloading file", Toast.LENGTH_SHORT).show();
            new DownloadTask().execute();
        }

    }

    private class DownloadTask extends AsyncTask<Void, Void, Void> {
        File storage = null;
        File base = null;
        File outputFile;
        // File finalStorage=null;

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                URL url = new URL(postUrl);
                HttpURLConnection c = (HttpURLConnection) url.openConnection();
                c.setRequestMethod("GET");
                c.connect();
                base = new File(Environment.getExternalStorageDirectory() + File.separator + ".neighborhood");
                if (!base.exists()) {
                    base.mkdir();
                }
                storage = new File(Environment.getExternalStorageDirectory() + File.separator + ".neighborhood" + File.separator + ".temp");
                if (!storage.exists()) {
                    storage.mkdir();
                }
                outputFile = new File(storage,key.get(count));
                if (!outputFile.exists()) {
                    outputFile.createNewFile();
                }
                FileOutputStream fos = new FileOutputStream(outputFile);
                InputStream is = c.getInputStream();
                byte[] buffer = new byte[1024];
                int len1 = 0;
                while ((len1 = is.read(buffer)) != -1) {
                    fos.write(buffer, 0, len1);
                }
                fos.close();
                is.close();
            } catch (Exception e) {
                e.printStackTrace();
                outputFile = null;
                Log.e("@@@@@@", "Download Error Exception " + e.getMessage());
            }

            return null;
        }

        @SuppressLint("ResourceAsColor")
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            File from = new File(Environment.getExternalStorageDirectory() + File.separator + ".neighborhood" + File.separator + ".temp" + File.separator + key.get(count));
            File to = new File(Environment.getExternalStorageDirectory() + File.separator + ".neighborhood" + File.separator + key.get(count));
            from.renameTo(to);
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                Uri picUri = FileProvider.getUriForFile(
                        context, context.getApplicationContext()
                                .getPackageName() + ".provider", to);
                uris.add(picUri);
            } else {
                uris.add(Uri.fromFile(to));
            }
            //uris.add(Uri.parse(Environment.getExternalStorageDirectory() + File.separator + ".neighborhood" + File.separator + key.get(count)));
            count++;
            if(count==size){
            Snackbar snackbar = Snackbar.make(sharebtn, "Download is Finished",
                    Snackbar.LENGTH_LONG);
            snackbar.setActionTextColor(context.getResources().getColor(R.color.white));
            snackbar.setAction("Share", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_SEND);
                    //intent.putExtra(Intent.EXTRA_TEXT,text);
                    intent.setType("image/*");
                    intent.putExtra(Intent.EXTRA_STREAM,uris.get(0));
                    context.startActivity(Intent.createChooser(intent, "share"));
                }
            });
            snackbar.show();}
            else {
                this.execute();
            }
        }
    }
}
