package com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings

import android.os.Bundle
import android.view.View
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings.adapters.MobileNotificationAdapter
import kotlinx.android.synthetic.main.fragment_mobile_notifications.*

class MobileNotification:BaseFragment(), MobileNotificationAdapter.AlterSettings {


    override fun onAlter(type: String, value: Boolean) {
        pref.setSetting(type,value)
    }

    private val mobileNotificationAdapter by lazy { MobileNotificationAdapter(pref,this) }

    override fun viewToCreate(): Int {
        return R.layout.fragment_mobile_notifications
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mobile_recycler.adapter=mobileNotificationAdapter
        btnBack.setOnClickListener {
            activity!!.onBackPressed()
        }
    }
}