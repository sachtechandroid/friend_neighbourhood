package com.sachtechsolution.neighbourhood.ui.home.adapters

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.constants.PostInterests
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.gone
import com.sachtechsolution.neighbourhood.extension.visible
import com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.FragmentFollowInterests
import gurtek.mrgurtekbasejava.base.ModelPrefrence
import kotlinx.android.synthetic.main.item_follow_interests.view.*


class FollowInterestsAdapter(val context: FragmentFollowInterests, val pref: ModelPrefrence, val list: Array<String>) : RecyclerView.Adapter<FollowInterestsAdapter.ViewHolder>() {

    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {

        val v = LayoutInflater.from(p0.context).inflate(R.layout.item_follow_interests, p0, false)
        var viewHolder = ViewHolder(v)

        return viewHolder
    }

    override fun getItemCount(): Int {
        return list.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var list = list[position]


        if (list.contains("head")) {
                holder.itemView.txtName.text = list.substring(4)
                holder.itemView.txtFolow.gone()
        } else {
            holder.itemView.txtName.text = list
            if (pref.getFollow(list)!!.isNotEmpty())
            holder.itemView.txtFolow.text="unfollow"
            else
                holder.itemView.txtFolow.text="follow"
            holder.itemView.txtFolow.visible()
        }
        holder.itemView.txtFolow.setOnClickListener {
                if (holder.itemView.txtFolow.text.equals("follow"))
                {
                    follow(list,holder.itemView.txtFolow)
                }
                else
                {
                    unfollow(pref.getFollow(list)!!,list, holder.itemView.txtFolow)

                }

        }


    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }
    fun follow(type: String, txtFolow: TextView)
    {
        context.showProgress()
        val interests=PostInterests(type = type,uid =pref.getUserFromPref()!!.uid)
        fireStoreHelper.followInterests(interests).addOnCompleteListener {
            if (it.isSuccessful) {
                txtFolow.text = "unfollow"
                pref.setFollow(type, it.result.id)
                context.hideProgress()

            }
            }.addOnFailureListener {
            context!!.showToast(it.message!!)
                context.hideProgress()
            }

    }
    fun unfollow(id: String, type: String, txtFolow: TextView)
    {
        if (id.isNotEmpty()){
            context.showProgress()
        fireStoreHelper.unfollowInterests(id).addOnCompleteListener {
            if (it.isSuccessful)
            {
                txtFolow.text = "follow"
                pref.setFollow(type,"")
                context.hideProgress()
            }
            else
                context.hideProgress()
        }.addOnFailureListener {
            context!!.showToast(it.message!!)
            context.hideProgress()
        }
        }

    }

}