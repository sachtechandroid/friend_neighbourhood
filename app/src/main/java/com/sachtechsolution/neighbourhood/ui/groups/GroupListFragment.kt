package com.sachtechsolution.neighbourhood.ui.groups

import android.os.Bundle
import android.view.View
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.gone
import com.sachtechsolution.neighbourhood.extension.visible
import com.sachtechsolution.neighbourhood.model.Group
import com.sachtechsolution.neighbourhood.ui.groups.adapter.GroupAdapter
import kotlinx.android.synthetic.main.fragment_group_list.*

class GroupListFragment:BaseFragment() {
    val groupAdapter by lazy { GroupAdapter(context!!) }

    override fun viewToCreate(): Int {
        return R.layout.fragment_group_list
    }
    val fireBaseHelper by lazy { FireStoreHelper.getInstance() }
    lateinit var groups :ArrayList<String>
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //email_recycler.adapter=groupAdapter
        //other_email_recycler.adapter=groupAdapter
        pref.getUserFromPref()?.groups?:ArrayList()
        recyclerView_groups.adapter=groupAdapter
        setListeners()
    }

    override fun onResume() {
        super.onResume()
        groups=pref.getUserFromPref()?.groups?:ArrayList()
        if (groups.size == 0) {
            gruops_text.text = "No group found. Create your own group and add neighbors to it."
            gruops_text.visible()
        }
        else {
            groupAdapter.groupsList.clear()
            gruops_text.gone()
            getGroups()
        }
    }

    private fun getGroups() {
        groups.forEach {
            fireBaseHelper.getGroup(it).addOnFailureListener {  }.addOnCompleteListener {
                if (it.isSuccessful){
                    val group=it.result.toObject(Group::class.java)!!
                    group.gid=it.result.id
                    groupAdapter.addGroup(group)
                }
            }
        }
    }

    private fun setListeners() {
        new_group.setOnClickListener {
            baseAcitivityListener.navigator.replaceFragment(CreateGroupFragments::class.java,true)
        }
        group_btnBack.setOnClickListener {
            activity!!.onBackPressed()
        }
    }
}