package com.sachtechsolution.neighbourhood.ui.home.fragments

import android.os.Bundle
import android.view.View
import com.example.manish.internetstatus.OttoBus
import com.google.firebase.firestore.QuerySnapshot
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.constants.NearestBussiness
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.showToast
import com.sachtechsolution.neighbourhood.intracter.Api_Hitter
import com.sachtechsolution.neighbourhood.model.AddBussiness
import com.sachtechsolution.neighbourhood.model.places_bussiness.SearchPlaced
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.sachtechsolution.neighbourhood.ui.home.activities.AddRecommendedActivity
import com.sachtechsolution.neighbourhood.ui.home.activities.FragmentAddRecommendation
import com.sachtechsolution.neighbourhood.ui.home.adapters.BussinessesAdapter
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_category.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.ArrayList
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView





class FragmentCategory:BaseFragment(),BussinessesAdapter.OnClick {

    val radius = "8000"
    val latLng by lazy { ""+pref.getUserFromPref()!!.lat+","+pref.getUserFromPref()!!.lng }
    var count = 0
    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }
    var mAdapter:CommentAdapter?= null
    var bussinessesAdapter:BussinessesAdapter?=null


    override fun onBussinessClick(type: String, resultsItem: NearestBussiness) {
        var bundle=Bundle()
        bundle.putString("id",type)
        bundle.putParcelable(FragmentAddRecommendation.DATA,resultsItem)
        baseAcitivityListener.navigator.replaceFragment(FragmentRecommendationDescription::class.java, true,bundle)
    }


    override fun viewToCreate(): Int {
        return R.layout.fragment_category
    }



    @Subscribe
    fun getMessage(internetStatus:String) {

        if (internetStatus.equals("Not connected to Internet")){
            InternetStatusDialogFragment.newInstance().show(fragmentManager, "option_fragment")
        }else {
            showToast("internet enabled ")

        }

    }

    override fun onResume() {
        super.onResume()
        OttoBus.bus.register(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mAdapter= CommentAdapter(this.activity!!, baseAcitivityListener, pref)
        bussinessesAdapter=BussinessesAdapter(activity!!, this@FragmentCategory,pBar)
        businesses_rv.adapter = bussinessesAdapter

        onClick()

        if (arguments!![CATEGORY] == DENTIST) {
            category_icon.setImageResource(R.drawable.dentists_icon)
            txtName.text=arguments!![CATEGORY].toString()

            getDataForPlaces(DEN, latLng,radius)
            //getComments(DEN)
        }

        else if (arguments!![CATEGORY].equals(RESTORENT)) {
            category_icon.setImageResource(R.drawable.restraunt_icon)
            txtName.text=arguments!![CATEGORY].toString()
            getDataForPlaces(RES, latLng, radius)
            //getComments(RES)
        }

        else if (arguments!![CATEGORY].equals(GYM)) {
            getDataForPlaces(GY, latLng, radius)
            category_icon.setImageResource(R.drawable.gym)
            txtName.text=arguments!![CATEGORY].toString()
          //  getComments(GY)
        }

        else if (arguments!![CATEGORY].equals(MALL)) {
            category_icon.setImageResource(R.drawable.mall)
            txtName.text=arguments!![CATEGORY].toString()
            getDataForPlaces(MAL, latLng, radius)
          //  getComments(MAL)
        }

        else if (arguments!![CATEGORY].equals(HAIR)) {
            category_icon.setImageResource(R.drawable.hair)
            txtName.text=arguments!![CATEGORY].toString()
            getDataForPlaces(HA, latLng, radius)
        //    getComments(HA)
        }

        else if (arguments!![CATEGORY].equals(PLUMBERS)) {
            category_icon.setImageResource(R.drawable.plumbers_icon)
            txtName.text=arguments!![CATEGORY].toString()
            getDataForPlaces(PUL, latLng, radius)
          //  getComments(PUL)
        }
        else if (arguments!![CATEGORY].equals(ATM)) {
            category_icon.setImageResource(R.drawable.atm)
            txtName.text=arguments!![CATEGORY].toString()
            getDataForPlaces(AT, latLng, radius)
         //   getComments(AT)
        }

        else if (arguments!![CATEGORY].equals(HOSPITAL)) {
            category_icon.setImageResource(R.drawable.hospital)
            txtName.text=arguments!![CATEGORY].toString()
            getDataForPlaces(HOS, latLng, radius)
         //   getComments(HOS)
        }

        else if (arguments!![CATEGORY].equals(REAL_ESTATE)) {
            category_icon.setImageResource(R.drawable.real_estate_icon)
            txtName.text=arguments!![CATEGORY].toString()
            getDataForPlaces(REL, latLng, radius)
           // getComments(REL)
        }

        else {
            category_icon.setImageResource(arguments!!["image"] as Int)
            txtName.text=arguments!!["name"].toString()
            getDataForPlaces(arguments!![CATEGORY].toString(), ""+pref.getUserFromPref()!!.lat+pref.getUserFromPref()!!.lng, radius)
            //getComments(arguments!![CATEGORY].toString())
        }
    }


    fun onClick(){

        business_recommend.setOnClickListener {
            baseAcitivityListener.navigator.replaceFragment(AddRecommendedActivity::class.java,true)
        }
            category_back.setOnClickListener {activity!!.onBackPressed()}

    }

    companion object {
        var CATEGORY="category"
        var DENTIST="Dentists"
        var RESTORENT="Restraunts"
        var MALL= "Shopping mall"
        var HAIR="Hair care"
        var HOSPITAL="Hospitals"
        var PLUMBERS="Plumbers"
        var REAL_ESTATE="Real estate agents"
        var DEN="dentist"
        var RES="restaurant"
        var GYM="Gym"
        var GY="gym"
        var MAL="shopping_mall"
        var HA="hair_care"
        var PUL="plumber"
        var HOS="hospital"
        var REL="real_estate_agency"
        var ATM="ATM"
        var AT="atm"
    }

    var bussiness: Call<SearchPlaced>?=null
    private fun getDataForPlaces(type: String, location: String, radius: String) {

        bussiness = Api_Hitter.ApiInterface("https://maps.googleapis.com/maps/api/place/nearbysearch/").getBussiness(type, location,radius)
        bussiness!!.enqueue(object : Callback<SearchPlaced> {
            override fun onResponse(call: Call<SearchPlaced>, response: Response<SearchPlaced>) {

                if (response.body()!!.results!!.isNotEmpty()) {
                    val data = response.body()
                    if (data != null) {
                        if (data.results!!.size in 1..19)
                            getDataForPlaces(type,location,"15000")
                        else
                        getCountData(data, count,type)
                    }
                }else {
                    getDataForPlaces(type,location,radius)
                }
            }

            override fun onFailure(call: Call<SearchPlaced>, t: Throwable) {

            }
        })

    }
    var list= ArrayList<NearestBussiness>()
    fun getCountData(data: SearchPlaced, count: Int,type: String) {
        fireStoreHelper.getRecommendation(data.results!![count]!!.id!!).addOnCompleteListener {
            if (it.isComplete) {
                val nbean = NearestBussiness(id= data.results[count]!!.id!!,vicinity = data.results[count]!!.vicinity!!,name = data!!.results!![count]!!.name!!, recCount = it.result.size().toString(),icon = data.results[count]!!.icon!!)
                list.add(nbean)

                if (count == data.results!!.size - 1){
                    nowGetFirestoreBusiness(type)
                } else {
                    getCountData(data, count + 1,type)
                }

            } else
                context!!.showToast("error to get count")


        }.addOnFailureListener { context!!.showToast("error to get count") }
                .addOnCanceledListener { context!!.showToast("error to get count") }
    }
    private fun nowGetFirestoreBusiness(type: String) {

        fireStoreHelper.getBussiness(type).addOnCompleteListener {
            if (it.isSuccessful)
            {
                if (it.result.documents.size>0)
                {
                    count=0
                    getCountDataAgain(it.result)

                }else{
                    bussinessesAdapter!!.updateAdapter(filterList(list))
                    bussinessesAdapter!!.notifyDataSetChanged()
                }
            }
        }.addOnFailureListener { context!!.showToast("error to get count") }
                .addOnCanceledListener { context!!.showToast("error to get count") }
    }

    private fun getCountDataAgain(result: QuerySnapshot) {

        /*if (count==result.size()-1)
        {
            topYearAdapter = BussinessesAdapter(activity!!, this@FragmentCategory, filterList(list))
            businesses_rv.adapter = topYearAdapter

        }*/

            var business= result.documents[count].toObject(AddBussiness::class.java)
            fireStoreHelper.getRecommendation(business!!.rid).addOnCompleteListener {
                if (it.isComplete) {
                    val nbean = NearestBussiness(id=business.rid,vicinity = business.address,name = business.name, recCount = it.result.size().toString(),icon = business.icon)
                    list.add(nbean)
                    count=count+1
                    getCountDataAgain(result)

                } else
                    context!!.showToast("error to get count")


            }.addOnFailureListener { context!!.showToast("error to get count") }
                    .addOnCanceledListener { context!!.showToast("error to get count") }



    }

    private fun filterList(list: ArrayList<NearestBussiness>): ArrayList<NearestBussiness> {
        val filterList = arrayListOf<NearestBussiness>()
        filterList.addAll(list)
        return filterList.apply { sortByDescending { nearestBussiness -> nearestBussiness.recCount } }
    }

    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
        bussiness!!.cancel()
    }
}
