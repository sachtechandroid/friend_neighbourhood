package com.sachtechsolution.neighbourhood.ui.home.fab.fragment.message.adapter

import android.content.Context
import android.net.Uri
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.sachtechsolution.neighbourhood.R
import kotlinx.android.synthetic.main.view_general_post.view.*


class SingleImageSelecterAapter(activity: Context, var listner: SelectImage) : RecyclerView.Adapter<SingleImageSelecterAapter.MyViewHolder>() {

    var list=ArrayList<Uri?>()
    internal var context: Context = activity


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val itemView: View = LayoutInflater.from(parent.context)
                .inflate(R.layout.view_general_post, parent, false)

        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        if (list!!.size>0&&list!!.size>position){
            Glide.with(context).load(list!![position]).into(holder.itemView.ivGeneralPost)
        }else
            Glide.with(context).load(R.drawable.icon_layer).into(holder.itemView.ivGeneralPost)


        holder.itemView.ivGeneralPost.setOnClickListener {

            if (position==list!!.size)
                listner.OnImageChoose()
            else
                listner.OnImageSelected(position,list!![position])
        }
    }

    override fun getItemCount(): Int {
        return list!!.size+1
    }

    fun addImage(selectedImage: Uri?) {
        list!!.add(selectedImage)
        notifyDataSetChanged()

    }

    fun removeImage(position: Int) {
        list.removeAt(position)
        notifyItemRemoved(position)
        notifyItemRangeChanged(position, list.size)
    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view)

    interface SelectImage{
        fun OnImageSelected(position: Int, uri: Uri?)
        fun OnImageChoose()
    }
}
