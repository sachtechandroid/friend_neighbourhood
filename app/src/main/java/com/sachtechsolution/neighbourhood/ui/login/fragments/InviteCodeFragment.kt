package com.sachtechsolution.neighbourhood.ui.login.fragments

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.provider.Settings
import android.text.SpannableString
import android.text.TextPaint
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.view.View
import com.example.manish.internetstatus.OttoBus
import com.google.android.gms.maps.model.LatLng
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.basepackage.base.LocationBFragment
import com.sachtechsolution.neighbourhood.constants.InvitationCode
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.getLocationMode
import com.sachtechsolution.neighbourhood.model.User
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_invite_code.*

class InviteCodeFragment : BaseFragment(), AddressConfirmationDialogFragment.AddressConfirm {

    override fun onEdit() {

    }

    override fun onConfirm(openFragment: String) {

        if (openFragment.isEmpty())
            baseAcitivityListener.navigator.replaceFragment(SignUpCredentialFragment::class.java,true)
        else
            if(context!!.getLocationMode() != 3)
            {
                showToast("please select high accuracy option")
                startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
                // do stuff
            }else
            baseAcitivityListener.navigator.replaceFragment(SignUpAddressFragment::class.java,true)

    }

    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }

    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
    }


    @Subscribe
    fun getMessage(internetStatus:String) {

        if (internetStatus.equals("Not connected to Internet")){
            InternetStatusDialogFragment.newInstance().show(fragmentManager, "option_fragment")
        }else {
            showToast("internet enabled ")

        }

    }
    override fun viewToCreate(): Int {
        return R.layout.fragment_invite_code
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        OttoBus.bus.register(this)

        invite_code_back_iv.setOnClickListener {
            fragmentManager?.popBackStackImmediate()
        }

        invite_code_accept_invitation_tv.setOnClickListener {
            /*if (invite_code_zip_tiet.text.isNullOrEmpty())
                showToast("enter zip code")*/
            if (invite_code_tiet.text.isNullOrEmpty())
                showToast("enter 4 digit invite code")
            else
                validateInvitationCode(invite_code_tiet.text.toString())
        }

        val string = SpannableString("Already a member? Sign in to enter your verification code")

        string.setSpan(object : ClickableSpan() {
            override fun onClick(widget: View?) {
                baseAcitivityListener.navigator.replaceFragment(LoginFragment::class.java, true)
            }

            override fun updateDrawState(ds: TextPaint?) {
                super.updateDrawState(ds)
                ds?.color = Color.parseColor("#1bb123")
            }
        }, 18, 25, SpannableString.SPAN_INCLUSIVE_INCLUSIVE)
        invite_code_signin_tv.movementMethod = LinkMovementMethod.getInstance()
        invite_code_signin_tv.text = string

    }

    private fun validateInvitationCode(invitationCode: String) {
            showProgress()
            fireStoreHelper.isInvitationCodePresent(invitationCode).addOnCompleteListener {
                if (it.isSuccessful)
                {
                    if (it.result.exists())
                    {
                        val invitationCode=it.result.toObject(InvitationCode::class.java)
                        showDialogForConfirmation(invitationCode)
                    }else {
                        hideProgress()
                        showToast("invalid invitation code")
                    }

                }
            }
        }

    private fun showDialogForConfirmation(invitationCode: InvitationCode?) {
        fireStoreHelper.getUser(invitationCode!!.userUid).addOnCompleteListener {
            if (it.isSuccessful) {
                if (it.result.exists()) {
                    val user = it.result.toObject(User::class.java)
                    val latLng = LatLng(user!!.lat, user.lng)
                    hideProgress()
                    baseAcitivityListener.navigator.replaceFragment(
                            EditAddressFragment::class.java, true,
                            Bundle().apply {
                                putParcelable("latLong", latLng)
                                putString("invitationCode", invitationCode.code)
                                putString("uid", invitationCode.userUid)
                            })
                    //AddressConfirmationDialogFragment.newInstance(latLng, this, true).show(fragmentManager, "option_fragment")
                }

            }
        }
    }

}
