package com.sachtechsolution.neighbourhood.ui.home.fab.fragment.message.fragment

import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.provider.SyncStateContract
import android.view.View
import com.example.manish.internetstatus.OttoBus
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.constants.Constants
import com.sachtechsolution.neighbourhood.extension.getLocationMode
import com.sachtechsolution.neighbourhood.extension.isLocationEnabled
import com.sachtechsolution.neighbourhood.extension.showToast
import com.sachtechsolution.neighbourhood.model.Post
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.sachtechsolution.neighbourhood.ui.home.fab.fragment.message.fragment.crime_and_safety.something_else.SomethingElsaFragment
import com.sachtechsolution.neighbourhood.ui.home.fab.fragment.message.fragment.sale_and_free.fragment.ForSaleAndFreeFragment
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_fabmessage.*

class FragmentFabMessage:BaseFragment(), View.OnClickListener {


    override fun viewToCreate(): Int {
        return R.layout.fragment_fabmessage
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //listner
        txtForSaleAndFree.setOnClickListener(this)
        btnClose.setOnClickListener(this)
        txtGeneral.setOnClickListener(this)
        txtCrimeAndSafety.setOnClickListener(this)
        txtLostandFound.setOnClickListener(this)
        txtRecommendations.setOnClickListener(this)
        txtCampaign.setOnClickListener(this)
    }



    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
    }


    @Subscribe
    fun getMessage(internetStatus:String) {

        if (internetStatus.equals("Not connected to Internet")){
            InternetStatusDialogFragment.newInstance().show(fragmentManager, "option_fragment")
        }else {
            showToast("internet enabled ")

        }

    }

    override fun onResume() {
        super.onResume()
        OttoBus.bus.register(this)
    }


    override fun onClick(view: View?) {
        var id=view!!.id
        Constants.NEXT="home"

        when(id)
        {
            R.id.txtForSaleAndFree-> baseAcitivityListener.navigator.replaceFragment(ForSaleAndFreeFragment::class.java,true)
            R.id.txtGeneral->{
                if (context!!.isLocationEnabled(context!!)&&context!!.getLocationMode()==3)
                {
                    baseAcitivityListener.navigator.replaceFragment(CreatePostFragment::class.java,true,Bundle().apply { putString("id",Post.postType.GENERAL) })
                }
                else
                {
                    context!!.showToast("Enable Location with high accuracy option")
                    startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
                }
            }
            R.id.btnClose->activity!!.onBackPressed()
            R.id.txtCrimeAndSafety->baseAcitivityListener.navigator
                    .replaceFragment(SomethingElsaFragment::class.java,true)
                    //.replaceFragment(CrimeAndSafety::class.java,true)
            R.id.txtLostandFound->
            {
                if (context!!.isLocationEnabled(context!!)&&context!!.getLocationMode() == 3) {
                    baseAcitivityListener.navigator.replaceFragment(CreatePostFragment::class.java,true,Bundle().apply { putString("id",Post.postType.LOST_FOUND) })

                }
                else
                {
                    context!!.showToast("Enable Location with high accuracy option")
                    startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
                }
            }
            R.id.txtRecommendations->baseAcitivityListener.navigator.replaceFragment(CreatePostFragment::class.java,true,Bundle().apply { putString("id",Post.postType.RECOMMENDATION) })
            R.id.txtCampaign->
            {
                if (context!!.isLocationEnabled(context!!)&&context!!.getLocationMode() == 3) {
                    baseAcitivityListener.navigator.replaceFragment(CreatePostFragment::class.java,true,Bundle().apply { putString("id",Post.postType.CAMPAIGN) })

                }
                else
                {
                    context!!.showToast("Enable Location with high accuracy option")
                    startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
                }
            }
        }
    }

}