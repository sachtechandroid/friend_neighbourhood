package com.sachtechsolution.neighbourhood.ui.home.fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.example.manish.internetstatus.OttoBus
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.constants.BeanArray
import com.sachtechsolution.neighbourhood.constants.NearestBussiness
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.intracter.Api_Hitter
import com.sachtechsolution.neighbourhood.model.places_bussiness.ResultsItem
import com.sachtechsolution.neighbourhood.model.places_bussiness.SearchPlaced
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.sachtechsolution.neighbourhood.ui.home.activities.AddRecommendedActivity
import com.sachtechsolution.neighbourhood.ui.home.activities.FragmentAddRecommendation
import com.sachtechsolution.neighbourhood.ui.home.adapters.CategoriesAdapter
import com.sachtechsolution.neighbourhood.ui.home.adapters.PlacesAdapter
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_recommendation.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList

class FragmentRecommendations : BaseFragment(), PlacesAdapter.onClickChoice {

    override fun seeMore(name: String, image: Int, choice: String) {
        var bundle = Bundle()
        bundle.putString(FragmentCategory.CATEGORY, choice)
        bundle.putInt("image", image)
        bundle.putString("name", name)
        baseAcitivityListener.navigator.replaceFragment(FragmentCategory::class.java, true, bundle)
    }

    var mplace: Array<out String>? = null
    var position = 0
    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }
    var mAdapter: CategoriesAdapter? = null
    var placesAdapter: PlacesAdapter? = null
    var images = ArrayList<Int>()
    var places: Array<out String>? = null
    val radius="8000"
    var bussiness: Call<SearchPlaced>?=null

    override fun onChoiceSelected(name: String, image: Int, choice: String, resultsItem: ResultsItem) {

             val nbean = NearestBussiness(icon = resultsItem.icon!!,id= resultsItem.id!!,vicinity = resultsItem.vicinity!!,name = resultsItem.name!!, recCount = resultsItem.rating.toString())
             var bundle=Bundle()
             bundle.putString("id","type")
             bundle.putParcelable(FragmentAddRecommendation.DATA, nbean)
             baseAcitivityListener.navigator.replaceFragment(FragmentRecommendationDescription::class.java, true,bundle)



    }


    override fun onPause() {
        super.onPause()
        bussiness!!.cancel()
        OttoBus.bus.unregister(this)
    }


    @Subscribe
    fun getMessage(internetStatus:String) {

        if (internetStatus.equals("Not connected to Internet")){
            InternetStatusDialogFragment.newInstance().show(fragmentManager, "option_fragment")
        }else {
            showToast("internet enabled ")

        }

    }





    override fun viewToCreate(): Int {
        return R.layout.fragment_recommendation
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        txtyear.text = "Announcing the ${Calendar.getInstance()[Calendar.YEAR]}"
        addDataToimagesList()

        mAdapter = CategoriesAdapter(activity!!, baseAcitivityListener)
        categories_rv.adapter = mAdapter

        places = resources.getStringArray(R.array.places)
        placesAdapter = PlacesAdapter(this.activity!!, this,progressBar)
        places_rv.adapter = placesAdapter


        onClick()

    }

    fun addDataToimagesList() {
        images.add(0, R.drawable.atm)
        images.add(1, R.drawable.bakery)
        images.add(2, R.drawable.cash)
        images.add(3, R.drawable.cycling)
        images.add(4, R.drawable.book)
        images.add(5, R.drawable.cafe)
        images.add(6, R.drawable.dentists_icon)
        images.add(7, R.drawable.electrician)
        images.add(8, R.drawable.gym)
        images.add(9, R.drawable.conditioner)
        images.add(10, R.drawable.hospital)
        images.add(11, R.drawable.jewelery)
        images.add(12, R.drawable.pet)
        images.add(13, R.drawable.plumbering)
        images.add(14, R.drawable.police)
        images.add(15, R.drawable.postoffice)
        images.add(16, R.drawable.agent)
        images.add(17, R.drawable.restraunt_icon)
        images.add(18, R.drawable.school)
        images.add(19, R.drawable.shoes)
        images.add(20, R.drawable.mall)
        images.add(21, R.drawable.leaves)
        images.add(22, R.drawable.travel)
    }

    private fun getPlaces() {

        mplace = resources.getStringArray(R.array.place_type)
        if (position < mplace!!.size)
            getDataForPlaces(mplace!![position], ""+pref.getUserFromPref()!!.lat+","+pref.getUserFromPref()!!.lng,radius)
    }

    private fun getDataForPlaces(type: String, location: String, radius: String) {

        if (bussiness!=null)
            bussiness=null
        bussiness = Api_Hitter.ApiInterface("https://maps.googleapis.com/maps/api/place/nearbysearch/").getBussiness(type, location,radius)
        bussiness!!.enqueue(object : Callback<SearchPlaced> {
            override fun onResponse(call: Call<SearchPlaced>, response: Response<SearchPlaced>) {

                if (response.body()!!.results!!.isNotEmpty()) {
                    val data = response.body()
                    if (data != null) {
                        if (data.results!!.size in 1..4) {
                            getDataForPlaces(type, location, "15000")

                            /*else {
                            var count = 0
                            getCountData(data, count, list)
                        }*/
                        } else {
                            val bean = BeanArray(image = images[position], name = places!![position], list = filterList(data.results as List<ResultsItem>), type = mplace!![position])
                            placesAdapter!!.add(bean)
                            placesAdapter!!.notifyDataSetChanged()
                            position += 1
                            hideProgress()
                            getPlaces()
                        }
                    }
                }
                else
                    getDataForPlaces(type,location,radius)
            }

            override fun onFailure(call: Call<SearchPlaced>, t: Throwable) {
            }
        })


    }

/*
    fun getCountData(data: SearchPlaced, count: Int, list: ArrayList<NearestBussiness>) {
        fireStoreHelper.getRecommendation(data.results!![count]!!.id!!).addOnCompleteListener {
            if (it.isComplete) {
                val nbean = NearestBussiness(icon = data.results[count]!!.icon!!,id= data.results[count]!!.id!!,vicinity = data.results[count]!!.vicinity!!,name = data!!.results!![count]!!.name!!, recCount = it.result.size().toString())
                list.add(nbean)

                if (count == data.results!!.size - 1) {
                    val bean = BeanArray( image = images[position], name = places!![position], list = filterList(list), type = mplace!![position])
                    placesAdapter!!.add(bean)
                    position += 1
                    getPlaces()
                } else {
                    getCountData(data, count + 1, list)
                }
            } else
                context!!.showToast("error to get count")


        }.addOnFailureListener { context!!.showToast("error to get count") }
                .addOnCanceledListener { context!!.showToast("error to get count") }
    }
*/

    private fun filterList(list: List<ResultsItem>): ArrayList<ResultsItem> {
        val filterList = arrayListOf<ResultsItem>()
        filterList.addAll(list)
        return filterList.apply { sortByDescending { nearestBussiness -> nearestBussiness.rating } }
    }

    fun onClick() {
        topOftheYear.setOnClickListener {
            baseAcitivityListener.navigator.replaceFragment(TopOfYearRecommendations::class.java, true) }

        all_categories_text.setOnClickListener {
            baseAcitivityListener.navigator.replaceFragment(FragmentAllCategories::class.java, true) }

        search_card.setOnClickListener {
            baseAcitivityListener.navigator.replaceFragment(FragmentSearchRecommendation::class.java, true) }

        add_more.setOnClickListener {
            baseAcitivityListener.navigator.replaceFragment(AddRecommendedActivity::class.java, true) }
    }

    override fun onResume() {
        super.onResume()
        getPlaces()
        OttoBus.bus.register(this)
        if (intracterListner!=null)
        {
            intracterListner!!.updateSelectedPos(1)
        }
    }
}