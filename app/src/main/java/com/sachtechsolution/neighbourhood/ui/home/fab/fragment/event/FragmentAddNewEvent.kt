package com.sachtechsolution.neighbourhood.ui.home.fab.fragment.event

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.os.Bundle
import android.view.View
import android.widget.TextView
import com.example.manish.internetstatus.OttoBus
import com.google.common.eventbus.Subscribe
import com.sachtechsolution.neighbourhood.HomePage
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.showToast
import com.sachtechsolution.neighbourhood.intracter.Api_Hitter
import com.sachtechsolution.neighbourhood.model.Post
import com.sachtechsolution.neighbourhood.model.User
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment

import kotlinx.android.synthetic.main.custom_toolbar.*
import kotlinx.android.synthetic.main.fragment_addnew_event.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*


class FragmentAddNewEvent : BaseFragment(), View.OnClickListener {

    private var mYear: Int = 0
    private var mMonth: Int = 0
    private var mDay: Int = 0
    private var mHour: Int = 0
    private var mMinute: Int = 0

    private var nYear: Int = -1
    private var nMonth: Int = -1
    private var nDay: Int = -1
    private var nHour: Int = -1
    private var nMinute: Int = -1

    private var eYear: Int = -1
    private var eMonth: Int = -1
    private var eDay: Int = -1
    private var eHour: Int = -1
    private var eMinute: Int = -1

    val user by lazy { pref.getUserFromPref() }
    val addressList by lazy { arguments!!.getStringArrayList("list") }
    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }


    override fun viewToCreate(): Int {
        return R.layout.fragment_addnew_event
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val c = Calendar.getInstance()
        mYear = c.get(Calendar.YEAR)
        mMonth = c.get(Calendar.MONTH)
        mDay = c.get(Calendar.DAY_OF_MONTH)
        nextLayout.setOnClickListener {
            postEvent()
        }
        //Listner
        txtClear.setOnClickListener(this)
        cancelLayout.setOnClickListener(this)
        btnStartDate.setOnClickListener(this)
        btnStartTime.setOnClickListener(this)
        btnEndTime.setOnClickListener(this)
        btnEndDate.setOnClickListener(this)
        btnStartT.setOnClickListener(this)
        btnStartD.setOnClickListener(this)
        btnEndT.setOnClickListener(this)
        btnEndD.setOnClickListener(this)
    }

    lateinit var expiry: Date

    private fun postEvent() {
        val subject = event_subject.text.toString()
        val message = event_message.text.toString()
        val location = event_location.text.toString()

        if (subject.isEmpty() || nYear == -1 || eYear == -1 || nMinute == -1 || eMinute == -1 || message.isNullOrEmpty() || location.isNullOrEmpty())
            activity!!.showToast("Enter all details")
        else {
            showProgress()
            //Date(eYear,eMonth,eDay)
            var eTime=""
            if (eHour == 12)
                eTime = "${eHour} : $eHour PM"
            else if (eHour == 0)
                eTime = "12 : $eMinute AM"
            else if (eHour > 12)
                eTime = "${eHour- 12} : ${eMinute} PM"
            else
                eTime = "$eHour: $eMinute AM"

            var nTime=""
            if (nHour == 12)
                eTime = "${nHour} : $nHour PM"
            else if (nHour == 0)
                nTime = "12 : $nMinute AM"
            else if (nHour > 12)
                nTime = "${nHour- 12} : ${nMinute} PM"
            else
                nTime = "$nHour: $nMinute AM"

            //val endDateString = "$eDay/$eMonth /$eYear $eTime"
            var eemonth=eMonth+1
            var nnmonyh=nMonth+1
            val endDateString = eDay.toString()+"/"+eemonth+"/"+eYear+" "+eTime.toString()
            val startDateString =  nDay.toString()+"/"+nnmonyh+"/"+nYear+" "+nTime.toString()
            //val startDateString = "$nDay/$nMonth/$nYear $nTime"

            val event = Post.Event(location, startDate = startDateString,
                    endDate = endDateString,  expiry = expiry)
            val post = Post(subject = subject, message = message, uid = user!!.uid, privacy = addressList,
                    event = event, type = Post.postType.EVENT, zipCode= user!!.zipCode,society = pref.getUserFromPref()!!.society)

            fireStoreHelper.createPost(post).addOnCompleteListener {
                if (it.isSuccessful) {

                    fireStoreHelper.getAllNeighbors(pref.getUserFromPref()!!.zipCode).addOnCompleteListener {
                        if (it.isSuccessful) {
                            var token = ""
                            it.result.documents.forEach {
                                if (!it.id.equals(pref.getUserFromPref()!!.uid)&& addressList.contains(it.toObject(User::class.java)!!.society))
                                    token = token + "," + it.toObject(User::class.java)!!.token
                            }
                            if (!token.isNullOrEmpty()) {
                                token = token.substring(1)
                                val title = "There is a new event in your neighborhood"
                                val callback = Api_Hitter.ApiInterface("http://stsmentor.com/").sendNotificationToAll(title, token, "zxc")
                                callback.enqueue(object : Callback<ResponseBody> {
                                    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                                        next()
                                    }

                                    override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                                        next()
                                    }
                                })
                            } else {
                                next()
                            }
                        }
                    }.addOnFailureListener {
                        next()
                    }
                }
            }.addOnFailureListener {
                hideProgress()
                showToast("Try again")
            }
        }
    }


    fun next(){
        hideProgress()
        showToast("Posted")
        baseAcitivityListener.navigator.openActivity(HomePage::class.java)
        activity!!.finish()
    }



    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
    }


    @Subscribe
    fun getMessage(internetStatus:String) {

        if (internetStatus.equals("Not connected to Internet")){
            InternetStatusDialogFragment.newInstance().show(fragmentManager, "option_fragment")
        }else {
            showToast("internet enabled ")

        }

    }

    override fun onResume() {
        super.onResume()
        OttoBus.bus.register(this)
    }


    override fun onClick(view: View?) {
        var id = view!!.id
        when (id) {
            R.id.txtClear -> {
                txtEndTime.setText(R.string.end_time)
                txtEndDate.setText(R.string.end_date)
                txtStartDate.setText("Start date")
                txtStartTime.setText("Start time")

                nYear = -1
                nMonth = -1
                nDay = -1
                nHour = -1
                nMinute = -1

                eYear = -1
                eMonth = -1
                eDay = -1
                eHour = -1
                eMinute = -1
            }

            R.id.btnStartDate -> pickUpDate(txtStartDate)
            R.id.btnStartTime -> pickUpTime(txtStartTime)
            R.id.btnEndTime -> pickUpEndTime(txtEndTime)
            R.id.btnEndDate -> pickUpEndDate(txtEndDate)
            R.id.cancelLayout -> activity!!.onBackPressed()
            R.id.btnStartD -> pickUpDate(txtStartDate)
            R.id.btnStartT->pickUpTime(txtStartTime)
            R.id.btnEndT -> pickUpEndTime(txtEndTime)
            R.id.btnEndD -> pickUpEndDate(txtEndDate)

        }
    }


    private fun pickUpTime(txt: TextView?) {
        if (nYear != -1) {

            val timePickerDialog = TimePickerDialog(context,
                    TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                        nHour = hourOfDay
                        nMinute = minute
                        eHour=-1
                        eMinute=-1
                        txtEndTime.setText(R.string.end_time)
                        txtEndDate.setText(R.string.end_date)
                        onTimeSet(hourOfDay, minute, txt)
                    }, mHour, mMinute, false)
            timePickerDialog.show()
        } else showToast("Select event start date first")
    }


    private fun pickUpDate(txtStartDate: TextView) {

        val datePickerDialog = DatePickerDialog(context,
                DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

                    val calendar = Calendar.getInstance()
                    calendar.set(year, monthOfYear, dayOfMonth)
                    val date = calendar.time

                    if (date.after(Date())) {
                        nDay = dayOfMonth
                        nMonth = monthOfYear
                        nYear = year

                        eYear=-1
                        eMonth=-1
                        eDay=-1
                        txtEndTime.setText(R.string.end_time)
                        txtEndDate.setText(R.string.end_date)

                        val cal = GregorianCalendar(year, monthOfYear, dayOfMonth)
                        val sdf = SimpleDateFormat("EEE, dd MMM yyyy")
                        val dateAsString = sdf.format(cal.time) //"08.01.2013"
                        txtStartDate!!.text = dateAsString

                    } else
                        showToast("Selected date should be after current date")
                }, mYear, mMonth, mDay)

        datePickerDialog.show()
    }

    private fun pickUpEndTime(txt: TextView?) {
        if (nYear != -1 && nHour != -1 && eYear != -1) {

            val timePickerDialog = TimePickerDialog(context,
                    TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                        val calendar = Calendar.getInstance()
                        calendar.set(eYear, eMonth, eDay)
                        val date = calendar.time

                        val calendar2 = Calendar.getInstance()
                        calendar2.set(nYear, nMonth, nDay)
                        val date2 = calendar2.time
                        if (date == date2 && hourOfDay <= nHour) {
                            showToast("End event date and time should be after ")
                        } else {
                            eHour = hourOfDay
                            eMinute = minute
                            onTimeSet(hourOfDay, minute, txt)
                        }
                    }, 0, 0, false)
            timePickerDialog.show()
        } else
            showToast("Please select end date first")
    }



    private fun pickUpEndDate(txtStartDate: TextView) {
        if (nYear != -1) {

            val datePickerDialog = DatePickerDialog(context,
                    DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                        val calendar = Calendar.getInstance()
                        calendar.set(year, monthOfYear, dayOfMonth)
                        val date = calendar.time

                        val calendar2 = Calendar.getInstance()
                        calendar2.set(nYear, nMonth, nDay)
                        val date2 = calendar2.time

                        if (!date.before(date2)) {
                            eDay = dayOfMonth
                            eMonth = monthOfYear
                            eYear = year

                            val cal = GregorianCalendar(year, monthOfYear, dayOfMonth)
                            val sdf = SimpleDateFormat("EEE, dd MMM yyyy")
                            val dateAsString = sdf.format(cal.time) //"08.01.2013"
                            txtStartDate!!.text = dateAsString

                            calendar.set(eYear,eMonth,eDay)
                            expiry=calendar.time
                        } else showToast("Event end date should be after start date")
                    }, nYear, nMonth, nDay)

            datePickerDialog.show()
        } else
            showToast("Please select event start date first")
    }


    fun onTimeSet(hours: Int, minute: Int, txt: TextView?) {

        if (hours == 12)
            txt!!.text = "${hours} : $minute PM"
        else if (hours == 0)
            txt!!.text = "12 : $minute AM"
        else if (hours > 12)
            txt!!.text = "${hours - 12} : $minute PM"
        else
            if (minute>9)
            txt!!.text = "$hours : $minute AM"
            else
                txt!!.text = "$hours : 0$minute AM"
    }
}