package com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings.profile.fragment

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.location.Location
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.manish.internetstatus.OttoBus
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.constants.Constants
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.isLocationEnabled
import com.sachtechsolution.neighbourhood.model.Post
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings.MapFragmentAdapter
import com.squareup.otto.Subscribe
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.fragment_real_estate.*

class FragmentRealEstate : BaseFragment(), OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, View.OnClickListener, MapFragmentAdapter.OpenDone {
    var mFusedLocationClient: FusedLocationProviderClient? = null
    private lateinit var mMap: GoogleMap
    private lateinit var mGoogleClient: GoogleApiClient
    lateinit var mLocation: Location
    var size = 1
    private var myMap: SupportMapFragment? = null
    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }
    private var markerView: View? = null

    @SuppressLint("MissingPermission")
    override fun onMapReady(p0: GoogleMap?) {
        mMap = p0!!
        mMap.uiSettings.isZoomControlsEnabled = true
        mMap.isMyLocationEnabled = true
    }


    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
    }


    @Subscribe
    fun getMessage(internetStatus: String) {

        if (internetStatus.equals("Not connected to Internet")) {
            InternetStatusDialogFragment.newInstance().show(fragmentManager, "option_fragment")
        } else {
            showToast("internet enabled ")
        }
    }


    @SuppressLint("MissingPermission")
    override fun onConnected(p0: Bundle?) {
        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleClient)

        if (mLocation != null) {
            getAllEstate()
            mMap?.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(mLocation.latitude, mLocation.longitude),14f))
        }
    }

    override fun onResume() {
        super.onResume()
        OttoBus.bus.register(this)
        getAllEstate()
    }

    var mList = HashMap<String, Post.RealEstate>()

    fun addMarker(estate: Post.RealEstate, size: Int) {

        var latlng = estate.latlng.split(",")
        var mLatLong = LatLng(latlng[0].toDouble(), latlng[1].toDouble())

        val addMarker = mMap.addMarker(MarkerOptions()
                .position(mLatLong)
                .icon(BitmapDescriptorFactory.fromBitmap(
                        createCustomMarker(activity!!, R.drawable.real_estate_icon))))
        //addMarker.title = estate.title

        mList[addMarker.id] = estate

        mMap.setOnMarkerClickListener {
        //}setOnInfoWindowClickListener {
            Constants.estate = mList[it.id]!!
            baseAcitivityListener.navigator.replaceFragment(FragmentViewEstate::class.java, true)
            true
        }
        if (this.size == size) {
            this.size = 1
            //var latLng=pref.getUserFromPref()!!.latlng.split(",")
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(pref.getUserFromPref()!!.lat, pref.getUserFromPref()!!.lng), 14f))
        }
    }

    @SuppressLint("MissingPermission")
    fun getAllEstate() {
        var size = 0
        fireStoreHelper.getEatate(pref.getUserFromPref()!!.zipCode).addOnCompleteListener {
            if (it.isSuccessful) {
                size = it.result.documents.size
                if (size > 0) {
                    it.result.documents.forEach {
                        val estate = it.toObject(Post.RealEstate::class.java)!!
                        addMarker(estate, size)
                        this.size = this.size + 1

                    }
                } else {

                    mFusedLocationClient = LocationServices.getFusedLocationProviderClient(context!!)
                    mFusedLocationClient!!.lastLocation.addOnSuccessListener {
                        if (it != null) {
                            val addMarker = mMap.addMarker(MarkerOptions()
                                    .position(LatLng(it.latitude,it.longitude)))
                            //var latLng=pref.getUserFromPref()!!.latlng.split(",")
                            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(it.latitude,it.longitude), 14f))

                        }
                        }
                    }

            } else {

                showToast("something went wrong")
            }
        }.addOnFailureListener {
            showToast("something went wrong")
        }
    }


    override fun onConnectionSuspended(p0: Int) {

    }

    override fun onClick(p0: View?) {

    }

    override fun OnEventDone() {

    }

    override fun viewToCreate(): Int {
        return R.layout.fragment_real_estate
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (activity!!.isLocationEnabled(activity!!)) {
            myMap = childFragmentManager.findFragmentById(R.id.map_fragment) as SupportMapFragment
            myMap?.getMapAsync(this)
            mGoogleClient = GoogleApiClient.Builder(activity!!)
                    .addConnectionCallbacks(this)
                    .addApi(LocationServices.API)
                    .build()
            mGoogleClient.connect()
            markerView = LayoutInflater.from(activity).inflate(R.layout.marker_layout, null)

        } else {
            showToast("enable location")
        }

        real_estate_back.setOnClickListener {
            activity!!.onBackPressed()
        }
        real_estate_add.setOnClickListener {
            baseAcitivityListener.navigator.replaceFragment(FragmentEditEstate::class.java, true)
        }
    }

    fun createCustomMarker(context: Context, image: Int): Bitmap {
        val marker = (context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater).inflate(R.layout.custom_marker_layout, null)
        val markerImage = marker.findViewById(R.id.user_dp) as CircleImageView
        markerImage.setImageResource(image)

        /* val txt_name = marker.findViewById(R.id.name) as TextView
         txt_name.text = _name*/

        val displayMetrics = DisplayMetrics()
        (context as Activity).windowManager.defaultDisplay.getMetrics(displayMetrics)
        marker.layoutParams = ViewGroup.LayoutParams(52, ViewGroup.LayoutParams.WRAP_CONTENT)
        marker.measure(displayMetrics.widthPixels, displayMetrics.heightPixels)
        marker.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels)
        marker.buildDrawingCache()
        val bitmap = Bitmap.createBitmap(marker.measuredWidth, marker.measuredHeight, Bitmap.Config.ARGB_8888)
        val canvas = Canvas(bitmap)
        marker.draw(canvas)

        return bitmap
    }
}