package com.sachtechsolution.neighbourhood.ui.home.activities

import android.annotation.SuppressLint
import android.app.ProgressDialog
import android.location.Geocoder
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.example.manish.internetstatus.OttoBus
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.places.ui.SupportPlaceAutocompleteFragment
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.LocationBFragment
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.model.AddBussiness
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings.MapFragmentAdapter
import com.sachtechsolution.neighbourhood.ui.login.SamplePresenter
import com.squareup.otto.Subscribe
import com.yayandroid.locationmanager.configuration.Configurations
import com.yayandroid.locationmanager.configuration.LocationConfiguration
import com.yayandroid.locationmanager.constants.FailType
import com.yayandroid.locationmanager.constants.ProcessType
import kotlinx.android.synthetic.main.activity_add_recommended.*
import java.util.concurrent.atomic.AtomicInteger

class AddRecommendedActivity : LocationBFragment(), View.OnClickListener, SamplePresenter.SampleView, OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, MapFragmentAdapter.OpenDone {

    private lateinit var mMap: GoogleMap
    private lateinit var mGoogleClient: GoogleApiClient
    lateinit var mLocation: Location
    var size = 1
    var selectedLatLng: LatLng? = null
    var selectedType: String? = null
    private var myMap: SupportMapFragment? = null
    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }
    private var progressDialog: ProgressDialog? = null
    var fragment: SupportPlaceAutocompleteFragment? = null
    private var samplePresenter: SamplePresenter? = null

    override fun getLocationConfiguration(): LocationConfiguration {
        return Configurations.defaultConfiguration("Give the permission!", "Would you mind to turn GPS on?")
    }

    override fun onLocationChanged(location: Location) {
        samplePresenter!!.onLocationChanged(location)
    }

    override fun onLocationFailed(@FailType failType: Int) {
        samplePresenter!!.onLocationFailed(failType)
    }

    override fun onProcessTypeChanged(@ProcessType processType: Int) {
        samplePresenter!!.onProcessTypeChanged(processType)
    }

    override fun onResume() {
        super.onResume()
        OttoBus.bus.register(this)

        if (locationManager.isWaitingForLocation && !locationManager.isAnyDialogShowing) {
            displayProgress()
        }
    }

    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
        dismissProgress()
        fragment = null
    }

    private fun displayProgress() {
        if (progressDialog == null) {
            progressDialog = ProgressDialog(context)
            progressDialog!!.window!!.addFlags(Window.FEATURE_NO_TITLE)
            progressDialog!!.setMessage("Getting location...")
        }

        if (!progressDialog!!.isShowing) {
            progressDialog!!.show()
        }
    }

    override fun getText(): String {
        return "good job"
    }

    override fun setText(text: String) {
        myMap = childFragmentManager.findFragmentById(R.id.map_fragment) as SupportMapFragment
        myMap?.getMapAsync(this)
        mGoogleClient = GoogleApiClient.Builder(context!!)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .build()
        mGoogleClient.connect()

    }

    override fun updateProgress(text: String) {
        if (progressDialog != null && progressDialog!!.isShowing) {
            progressDialog!!.setMessage(text)
        }
    }

    override fun dismissProgress() {
        if (progressDialog != null && progressDialog!!.isShowing) {
            progressDialog!!.dismiss()
        }
    }

    override fun viewToCreate(): Int {
        return R.layout.activity_add_recommended
    }


    @SuppressLint("MissingPermission")
    override fun onMapReady(p0: GoogleMap?) {
        mMap = p0!!
        mMap.uiSettings.isZoomControlsEnabled = true
        mMap.isMyLocationEnabled = true

    }

    @SuppressLint("MissingPermission")
    override fun onConnected(p0: Bundle?) {
        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleClient)

        if (mLocation != null) {
            //var latlong = pref.getUserFromPref()!!.latlng.split(",")
            addMarker(LatLng(pref.getUserFromPref()!!.lat, pref.getUserFromPref()!!.lng))
        }


    }

    var marker: Marker? = null;
    private fun addMarker(latLng: LatLng) {
        selectedLatLng=latLng
        if (marker == null)
            marker = mMap.addMarker(MarkerOptions()
                    .position(latLng)
                    .draggable(true))
        marker!!.title = "Pick Location"
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 11f))

        mMap.setOnMapClickListener { latLng ->
            selectedLatLng = latLng
            marker!!.position = latLng
            // context!!.showToast(latLng.toString())
        }

        mMap.setOnMarkerDragListener(object : GoogleMap.OnMarkerDragListener {
            override fun onMarkerDragStart(arg0: Marker) {}

            override fun onMarkerDragEnd(arg0: Marker) {
                Log.d("System out", "onMarkerDragEnd...")
                mMap.animateCamera(CameraUpdateFactory.newLatLng(arg0.position))
                //context!!.showToast(arg0.position.toString())
                selectedLatLng = latLng
            }

            override fun onMarkerDrag(arg0: Marker) {}
        })


    }

    private fun getAddress(latLng: LatLng): String {
        val locationList = Geocoder(context)
                .getFromLocation(latLng.latitude, latLng.longitude, 1)
        if (locationList.size > 0) {
            val location = locationList[0]
            return location.getAddressLine(0).toString()
        }
        return ""
    }

    override fun onConnectionSuspended(p0: Int) {

    }


    override fun OnEventDone() {

    }

    override fun onClick(view: View?) {

        var id = view!!.id
        when (id) {
            R.id.btnBack -> activity!!.onBackPressed()
            R.id.txtAdd -> {
                val name = etName.text.toString()
                if (name.isNullOrEmpty())
                    showToast("Enter Name")
                else if (selectedType.equals("Select Category"))
                    showToast("Select Category")
                else {
                    showProgress()
                    addBusiness()
                }
            }
        }
    }


    @Subscribe
    fun getMessage(internetStatus: String) {

        if (internetStatus.equals("Not connected to Internet")) {
            InternetStatusDialogFragment.newInstance().show(fragmentManager, "option_fragment")
        } else {
            showToast("internet enabled ")

        }

    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //listner
        btnBack.setOnClickListener(this)
        txtAdd.setOnClickListener(this)

        samplePresenter = SamplePresenter(this)
        getLocation()

        var categories = resources.getStringArray(R.array.place_type)
        // Creating adapter for spinner
        categories[0] = "Select Category"
        var dataAdapter = ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, categories)

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        // attaching data adapter to spinner
        typeSpinner.adapter = dataAdapter

        typeSpinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {
                showToast("nothing selected")
            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                selectedType = categories[position]
            }

        }
    }


    private fun addBusiness() {
        val counter = AtomicInteger(1254468955)
        var rid = Integer.toString(counter.incrementAndGet(), 45444454)
        fireStoreHelper.addBussiness(AddBussiness(rid = rid, name = etName.text.toString(),
                latlng = selectedLatLng!!.latitude.toString() + "," + selectedLatLng!!.longitude.toString(),
                type = selectedType!!, address = getAddress(latLng = selectedLatLng!!))
        ).addOnCompleteListener {

            if (it.isSuccessful) {
                hideProgress()
                showToast("Business Added")
                activity!!.onBackPressed()
            } else {
                hideProgress()
                showToast("error")
            }

        }.addOnFailureListener {
            showToast("failed")
            hideProgress()
        }.addOnCanceledListener {
            showToast("cancled")
            hideProgress()
        }
    }

}
