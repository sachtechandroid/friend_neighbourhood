package com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings.profile.fragment

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.example.manish.internetstatus.OttoBus
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.chat.ActivityChatDetails
import com.sachtechsolution.neighbourhood.chat.model.Friend
import com.sachtechsolution.neighbourhood.constants.Constants
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.gone
import com.sachtechsolution.neighbourhood.extension.visible
import com.sachtechsolution.neighbourhood.model.User
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.squareup.otto.Subscribe
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_view_estate.*

class FragmentViewEstate:BaseFragment() {

    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }
    var user: User?=null

    override fun viewToCreate(): Int {
        return R.layout.fragment_view_estate
    }


    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
    }


    @Subscribe
    fun getMessage(internetStatus:String) {

        if (internetStatus.equals("Not connected to Internet")){
            InternetStatusDialogFragment.newInstance().show(fragmentManager, "option_fragment")
        }else {
            showToast("internet enabled ")

        }

    }

    override fun onResume() {
        super.onResume()
        OttoBus.bus.register(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var estate=Constants.estate
        var images=estate.images.split("#")
            setImagePager(images)
        txtPrice.text=estate.title
        txtDetail.text=estate.message
        etAddress.text=estate.address

        getuser(estate.uid)
        btnBack.setOnClickListener { activity!!.onBackPressed() }
        makeChat.setOnClickListener {
            var friend=Friend(user!!.uid,user!!.firstName+" "+user!!.lastName,user!!.imageLink,user!!.society)
            ActivityChatDetails.navigate((activity as AppCompatActivity?)!!, ivProfile,friend)
        }

    }
    private fun setImagePager(images: List<String>) {
        if (images.size==1)
            viewPager.gone()
        else {
            viewPager.visible()
            var myCustomPagerAdapter = MyCustomPagerAdapter(context!!, images)
            viewPager!!.adapter = myCustomPagerAdapter
        }

    }
    fun getuser(uid: String) {
        fireStoreHelper.getUser(uid).addOnCompleteListener {
            if (it.isSuccessful)
            {
                if(it.result.exists()){

                    user = it.result.toObject(User::class.java)
                    txtUserName.text="Message to ${user!!.firstName+" "+user!!.lastName}"
                    if (user!!.uid.equals(pref.getUserFromPref()!!.uid))
                        makeChat.gone()
                    else
                        makeChat.visible()

                    if (user!!.imageLink.isNotEmpty()) {
                        progressBar.visible()
                        Picasso.with(context)
                                .load(user!!.imageLink)
                                .into(ivProfile, object : com.squareup.picasso.Callback {
                                    override fun onError() {
                                        progressBar.visible()
                                    }

                                    override fun onSuccess() {
                                        progressBar.gone()
                                    }
                                })
                    }
                }
            }
        }
    }
}