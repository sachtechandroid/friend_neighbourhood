package com.sachtechsolution.neighbourhood.ui.home.fab.adapter;

/* whoGuri 4/9/18 */

import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.sachtechsolution.neighbourhood.R;

import java.util.ArrayList;

public class AddChoiceAdapter extends RecyclerView.Adapter<AddChoiceAdapter.MyAdapter> {

    int i = 1;
    boolean status = true;
    public ArrayList<String> arrayList;


    public AddChoiceAdapter() {
        arrayList = new ArrayList<String>();
        //arrayList.add("");
    }

    @Override
    public MyAdapter onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.add_poll_item, parent, false);
        return new MyAdapter(view);
    }

    @Override
    public void onBindViewHolder(final MyAdapter holder, final int position) {

            holder.rec_img.setVisibility(View.GONE);
            holder.rec_edit.addTextChangedListener(new TextWatcher() {


            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (status && i <8) {
                    i++;
                    notifyItemInserted(i - 1);
                    status = false;
                }
                arrayList.set(position, s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    @Override
    public int getItemCount() {
        return i;
    }

    public class MyAdapter extends RecyclerView.ViewHolder {
        EditText rec_edit;
        ImageView rec_img;
        // LinearLayout rec_layout;

        public MyAdapter(View itemView) {
            super(itemView);

            rec_edit = itemView.findViewById(R.id.choice_edit);
            rec_img = itemView.findViewById(R.id.remove_choice);
            //            rec_layout = itemView.findViewById(R.id.rec_layout);
            rec_edit.setOnFocusChangeListener((v, hasFocus) -> {
                String text = ((EditText) v).getText().toString();
                if (text.length() >= 1) {
                    status = false;
                } else {
                    status = true;
                    arrayList.add("");
                }
            });
        }
    }
}