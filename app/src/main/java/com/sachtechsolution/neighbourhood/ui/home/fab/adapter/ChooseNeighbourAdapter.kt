package com.sachtechsolution.neighbourhood.ui.home.fab.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.extension.gone
import com.sachtechsolution.neighbourhood.model.User
import kotlinx.android.synthetic.main.view_choose_neighbour.view.*


class ChooseNeighbourAdapter(activity: Context,var listner:OnChooseNeighbour) : RecyclerView.Adapter<ChooseNeighbourAdapter.MyViewHolder>() {

    internal var context: Context = activity
    var userList=ArrayList<User>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val itemView: View = LayoutInflater.from(parent.context)
                .inflate(R.layout.view_choose_neighbour, parent, false)

        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

        var user=userList[position]
        if (position>userList.size)
            holder.itemView.view.gone()
        if (user.imageLink.isNotEmpty())
            Glide.with(context).load(user.imageLink).into(holder.itemView.chooseNeProfile)
        holder.itemView.chooseNeName.text=user.firstName+" "+user.lastName
        holder.itemView.chooseNeAddress.text=user.society


        holder.itemView.setOnClickListener {
            listner.OnChooseNeighbour(user,holder.itemView.chooseNeProfile)
        }
    }

    override fun getItemCount(): Int {
        return userList.size
    }

    fun add(user: User) {
       userList.add(user)
        notifyDataSetChanged()

    }

    fun update(list: ArrayList<User>) {
        userList.addAll(list)
        notifyDataSetChanged()

    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view)

    interface OnChooseNeighbour{
        fun OnChooseNeighbour(user: User, chooseNeProfile: ImageView)
    }
}
