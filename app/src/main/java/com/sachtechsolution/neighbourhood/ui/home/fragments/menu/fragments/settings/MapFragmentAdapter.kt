package com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.extension.gone
import kotlinx.android.synthetic.main.view_map.view.*


class MapFragmentAdapter(activity: Context, var listner: OpenDone) : RecyclerView.Adapter<MapFragmentAdapter.MyViewHolder>() {

    var index:Int?=null
    internal var context: Context = activity



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val itemView: View = LayoutInflater.from(parent.context)
                .inflate(R.layout.view_map, parent, false)

        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        if (position>18)
            holder.itemView.view.gone()

            holder.itemView.itemLayout.setOnClickListener{
                if (index==position)
                holder.itemView.txtSubHeading.setTextColor(context.resources.getColor(R.color.white))
                holder.itemView.txtHeading.setTextColor(context.resources.getColor(R.color.white))
                holder.itemView.itemLayout.setBackgroundColor(context.resources.getColor(R.color.colorPrimary))

            }

    }

    override fun getItemCount(): Int {
        return 20
    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view)

    interface OpenDone{
        fun OnEventDone()
    }
}
