package com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings.profile

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Build
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.support.v7.widget.GridLayoutManager
import android.view.View
import android.view.Window
import android.widget.TextView
import com.bumptech.glide.Glide
import com.example.manish.internetstatus.OttoBus
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.calculateNoOfColumns
import com.sachtechsolution.neighbourhood.model.AllFamily
import com.sachtechsolution.neighbourhood.model.Family
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings.adapters.AllFamilyAdapter
import com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings.profile.fragment.AddPetFragment
import com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings.profile.fragment.SelectFamilyOptionDialogFragment
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_family_member.*

class FamilyMemberFragment : BaseFragment(), AllFamilyAdapter.AddContact, SelectFamilyOptionDialogFragment.OnDelete {

    override fun deleteSuccess(position: Int) {
        adapter!!.Remove(position)
        adapter!!.notifyDataSetChanged()
    }

    override fun onContactClick(s: String, id: String, position: Int) {

        SelectFamilyOptionDialogFragment.newInstance(s,id,position,this,pref).show(fragmentManager, "option_fragment")
    }

    override fun viewToCreate(): Int {
        return R.layout.fragment_family_member
    }
    var adapter: AllFamilyAdapter?=null
    var familyList=ArrayList<AllFamily>()
    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }
    internal var textadd: TextView?=null
    internal var dialogtitle: TextView?=null
    internal var firstOpt: TextView?=null
    internal var secondOpt: TextView?=null
    internal var thirdOpt: TextView?=null
    internal var dialog: Dialog?=null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var user=pref.getUserFromPref()
        userName.text=user!!.firstName+" "+user!!.lastName
        if (user.imageLink.isNotEmpty()){
            Glide.with(context!!).load(user!!.imageLink).into(upDp)
            Glide.with(context!!).load(user!!.imageLink).into(dp)
        }
        familyList= ArrayList()
        textadd = view.findViewById(R.id.textadd)
        textadd!!.setOnClickListener { v -> createDialog() }
        txtDone!!.setOnClickListener { activity!!.onBackPressed() }

    }


    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
    }


    @Subscribe
    fun getMessage(internetStatus:String) {

        if (internetStatus.equals("Not connected to Internet")){
            InternetStatusDialogFragment.newInstance().show(fragmentManager, "option_fragment")
        }else {
            showToast("internet enabled ")

        }

    }



    override fun onResume() {
        super.onResume()
        OttoBus.bus.register(this)
        getChild()
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @SuppressLint("ResourceAsColor")
    private fun createDialog() {
        dialog = Dialog(activity)
        dialog!!.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog!!.setContentView(R.layout.dialog)
        dialog!!.show()
        dialogtitle = dialog!!.findViewById(R.id.titledialog)
        dialogtitle!!.text = "Add your family members"

        secondOpt = dialog!!.findViewById(R.id.dialog_second)
        secondOpt!!.text = "ADD A CHILD"
        secondOpt!!.setTextColor(R.color.colorPrimary)
        secondOpt!!.textAlignment = View.TEXT_ALIGNMENT_CENTER
        secondOpt!!.setOnClickListener { v ->
            baseAcitivityListener.navigator.replaceFragment(AddChildFragment::class.java,true)
            dialog!!.dismiss()
        }


        thirdOpt = dialog!!.findViewById(R.id.dialog_third)
        thirdOpt!!.text = "ADD A PET"
        thirdOpt!!.setTextColor(R.color.colorPrimary)
        thirdOpt!!.textAlignment = View.TEXT_ALIGNMENT_CENTER
        thirdOpt!!.setOnClickListener { v ->

            baseAcitivityListener.navigator.replaceFragment(AddPetFragment::class.java,true)
            dialog!!.dismiss()
        }

    }
    private fun getChild(){
        showProgress()
        fireStoreHelper.getChild(pref.getUserFromPref()!!.uid).addOnCompleteListener {
            if (it.isSuccessful) {
                it.result.documents.forEach {
                    var family=it.toObject(Family.Child()::class.java)
                    familyList!!.add(AllFamily(name = family!!.name,image = family!!.imageLink,age = family!!.age+" year old",id = it.id))
                }
                getPet()

            }
            else {
                hideProgress()
                showToast("something went wrong")
            }
        }
    }
    private fun getPet(){
        fireStoreHelper.getPet(pref.getUserFromPref()!!.uid).addOnCompleteListener {
            if (it.isSuccessful) {
                if(it.result.documents.isEmpty())
                {

                    setAdapter()

                }else {
                    it.result.documents.forEach {
                        var pet = it.toObject(Family.Pet()::class.java)
                        familyList!!.add(AllFamily(name = pet!!.name, image = pet!!.imageLink, age = pet.profile,category = pet.category,id = it.id))
                    }

                    setAdapter()
                }

            }
            else {
                hideProgress()
                showToast("something went wrong")
            }
        }
    }

    private fun setAdapter()
    {
        hideProgress()
        adapter = AllFamilyAdapter(context, familyList, this)
        famiyViews.layoutManager= GridLayoutManager(context,context!!.calculateNoOfColumns())
        famiyViews.adapter=adapter
    }

}
