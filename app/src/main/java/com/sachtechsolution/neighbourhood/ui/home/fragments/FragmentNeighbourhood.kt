package com.sachtechsolution.neighbourhood.ui.home.fragments

import android.os.Bundle
import android.view.View
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.ui.home.adapters.FoodDrinkAdapter
import kotlinx.android.synthetic.main.fragment_neighbourhood.*

class FragmentNeighbourhood:BaseFragment(){

    override fun viewToCreate(): Int {
        return R.layout.fragment_neighbourhood
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
       var mAdapter= FoodDrinkAdapter(this!!.activity!!)
        food_drink_rv.adapter=mAdapter
        personal_care_rv.adapter=mAdapter
        pet_care_rv.adapter=mAdapter
        retail_rv.adapter=mAdapter
        automotive_rv.adapter=mAdapter
        activities_rv.adapter=mAdapter
        home_services_rv.adapter=mAdapter

        neighbour_back.setOnClickListener {
    baseAcitivityListener.navigator.replaceFragment(FragmentRecommendations::class.java,true)
}
    }
}