package com.sachtechsolution.neighbourhood.ui.home.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.sachtechsolution.neighbourhood.R
import kotlinx.android.synthetic.main.item_all_categories.view.*
import java.util.ArrayList

class AllCategoriesAdapter(var context: Context, var images: ArrayList<Int>, var name: Array<String>, var placetype: Array<String>, var listner:onClickChoice) : RecyclerView.Adapter<AllCategoriesAdapter.MyAdapter>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyAdapter {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_all_categories, null, false)
        return MyAdapter(view)
    }

    override fun onBindViewHolder(holder: MyAdapter, position: Int) {
        Glide.with(context).load(images[position]).into(holder.itemView.item_image)
        holder.itemView.itemName.text = name[position]
        holder.itemView.setOnClickListener { v ->
            listner.onChoiceSelected(placetype[position],images[position], name[position])}
    }


    override fun getItemCount(): Int {
        return name.size
    }

    interface onClickChoice
    {
        fun onChoiceSelected(s: String, i: Int, get: String)
    }
    inner class MyAdapter(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }
}
