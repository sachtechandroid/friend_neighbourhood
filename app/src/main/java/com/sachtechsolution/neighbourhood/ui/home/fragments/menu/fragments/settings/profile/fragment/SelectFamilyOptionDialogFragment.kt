package com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings.profile.fragment

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.ProgressDialogFragment.hideProgress
import com.sachtechsolution.neighbourhood.basepackage.base.ProgressDialogFragment.showProgress
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.showToast
import com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings.profile.fragment.EmergencyContactDialogFragment.Companion.pref
import gurtek.mrgurtekbasejava.base.ModelPrefrence
import kotlinx.android.synthetic.main.view_family_edit.*

open class SelectFamilyOptionDialogFragment: DialogFragment() {

    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }


        private fun uploadData() {

            if (family.equals("child"))
                deleteChild(familyId)
            else if (family.equals("pet"))
                deletePet(familyId)

        }

    private fun deletePet(id: String) {
        showProgress(fragmentManager)
        fireStoreHelper.deletePet(id,pref!!.getUserFromPref()!!.uid).addOnCompleteListener {
            if (it.isSuccessful) {
                dismiss()
                context!!.showToast("Post deleted")
                hideProgress()
            } else {
                context!!.showToast("error")
                hideProgress()
            }
            hideProgress()
        }.addOnFailureListener {
            context!!.showToast("failed")
            hideProgress()
        }

    }

    private fun deleteChild(id: String) {
            showProgress(fragmentManager)
            fireStoreHelper.deleteChild(id,pref!!.getUserFromPref()!!.uid).addOnCompleteListener {
                if (it.isSuccessful) {
                    dismiss()
                    context!!.showToast("Post deleted")
                    hideProgress()
                    listner!!.deleteSuccess(position)
                } else {
                    context!!.showToast("error")
                    hideProgress()
                }
                hideProgress()
            }.addOnFailureListener {
                context!!.showToast("failed")
                hideProgress()
            }


    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        dialog.setCanceledOnTouchOutside(false)
        return inflater.inflate(R.layout.view_family_edit, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        //listner
        txtRemove.setOnClickListener {
            uploadData()
        }

    }

    companion object {
        var family=""
        var familyId=""
        var position=0
        var pref: ModelPrefrence?=null
        var listner:OnDelete?=null
        fun newInstance(family: String, id: String, position: Int, listner: OnDelete, pref: ModelPrefrence): SelectFamilyOptionDialogFragment {
            this.family=family
            familyId=id
            this.pref=pref
            this.listner=listner
            this.position=position
            return SelectFamilyOptionDialogFragment()

        }
    }
    interface OnDelete{
        fun deleteSuccess(position: Int)
    }
}