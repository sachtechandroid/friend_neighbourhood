package com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.extension.visible
import com.sachtechsolution.neighbourhood.model.AllFamily
import kotlinx.android.synthetic.main.view_family.view.*

class AllFamilyAdapter(var context: Context?, var familyList: ArrayList<AllFamily>?, var listner: AddContact) : RecyclerView.Adapter<AllFamilyAdapter.ViewHolder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val v: View = LayoutInflater.from(p0.context).inflate(R.layout.view_family,p0,false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return familyList!!.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var data=familyList!![position]
        holder.itemView.familyName.text=data.name
        holder.itemView.familyAge.text=data.age
        if (data.category.isNotEmpty()&&data.category.equals("More")) {
            holder.itemView.familyCategory.visible()
            holder.itemView.familyCategory.text = data.category
        }
        Glide.with(context!!).load(data.image).into(holder.itemView.familyProfilePic)

        holder.itemView.contactFull.setOnClickListener{
            if (data.category.isEmpty())
            listner.onContactClick("child",data.id,position)
            else
                listner.onContactClick("pet", data.id, position)

        }
    }

    fun Remove(position: Int) {
        familyList!!.removeAt(position)
        notifyItemRemoved(position)
    }

    fun clear() {

        familyList!!.clear()
        notifyDataSetChanged()
    }

    interface  AddContact
    {
        fun onContactClick(s: String, id: String, position: Int)
    }
    class ViewHolder(v: View): RecyclerView.ViewHolder(v) {

    }
}