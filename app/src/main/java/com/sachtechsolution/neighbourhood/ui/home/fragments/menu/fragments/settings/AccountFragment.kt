package com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.example.manish.internetstatus.OttoBus
import com.google.firebase.auth.FirebaseAuth
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.constants.Constants
import com.sachtechsolution.neighbourhood.model.Post
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.ProfileFragment
import com.sachtechsolution.neighbourhood.ui.login.LoginActivity
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_account.*

class AccountFragment : BaseFragment(), View.OnClickListener {

    override fun viewToCreate(): Int {
        return R.layout.fragment_account
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //listner
        btnBack.setOnClickListener(this)
        btnEdit.setOnClickListener(this)
        txtSignOut.setOnClickListener(this)
    }


    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
    }


    @Subscribe
    fun getMessage(internetStatus:String) {

        if (internetStatus.equals("Not connected to Internet")){
            InternetStatusDialogFragment.newInstance().show(fragmentManager, "option_fragment")
        }else {
            showToast("internet enabled ")

        }

    }



    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.btnBack -> activity!!.onBackPressed()

            R.id.btnEdit -> baseAcitivityListener.navigator.replaceFragment(EditProfileFragment::class.java, true)

            //R.id.txtName -> baseAcitivityListener.navigator.replaceFragment(EditProfileFragment::class.java, true)

           // R.id.txteditProfile -> baseAcitivityListener.navigator.replaceFragment(ProfileFragment::class.java, true)

           // R.id.txtAddress -> {}//context!!.openLink("http://www.google.com")

            R.id.txtSignOut -> signOut()
        }
    }

    private fun signOut() {
        //if(context!!.networkAvaileble()) {
        FirebaseAuth.getInstance().signOut()
        Constants.IMAGE_LIST.clear()
        Constants.LAT = 0.0
        Constants.LONG = 0.0
        Constants.email = ""
        Constants.IS_THANKED_LIST.clear()
        Constants.IS_POLLED_LIST.clear()
        Constants.POSITION = -1
        Constants.DELETED_POSITION = -1
        Constants.ADDRESS = ""
        //Constants.ZIP_CODE=""
        Constants.ADDRESSES.clear()
        Constants.estate = Post.RealEstate()
        Constants.MAIN_LOCALITY = ""
        Constants.NEXT = ""
        Constants.POST = null
        Constants.REDIUS = 0.00000901 * 2000

        pref.cleaUser()
        val intent = Intent(activity!!, LoginActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
        activity?.startActivity(intent)
        Constants.BACK=false
        activity!!.finishAffinity()
    }

    override fun onResume() {
        super.onResume()
        OttoBus.bus.register(this)
        var user = pref.getUserFromPref()
        txtName?.text = user?.firstName + " " + user?.lastName
        txtEmail?.text = user?.email
        etAddress?.text = user?.address
    }
}