package com.sachtechsolution.neighbourhood.ui.postDetails.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.extension.getDateDifference
import com.sachtechsolution.neighbourhood.extension.openActivity
import com.sachtechsolution.neighbourhood.model.Reply
import com.sachtechsolution.neighbourhood.model.User
import com.sachtechsolution.neighbourhood.ui.home.AnotherUserProfileActivity
import com.sachtechsolution.neighbourhood.ui.home.fragments.menu.activity.MenuBaseActivity
import gurtek.mrgurtekbasejava.base.ModelPrefrence
import kotlinx.android.synthetic.main.post_reply_item.view.*
import java.util.*

/* whoGuri 31/8/18 */
class PostReplyAdapter(val context: Context, val pref: ModelPrefrence) : RecyclerView.Adapter<PostReplyAdapter.MyViewHolder>() {
    var repliesList = ArrayList<Reply?>()
    var userList = ArrayList<User>()
    fun addItem(reply: Reply, user: User) {
        repliesList.add(reply)
        userList.add(user)
        notifyItemInserted(repliesList.size - 1)
    }

    fun clear() {
        repliesList.clear()
        userList.clear()
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(p0: MyViewHolder, p1: Int) {
        val user = userList[p1]
        val reply = repliesList[p1]
        p0.itemView.apply {
            post_reply_user_latter.setText(user.firstName.substring(0, 1))
            if (!user.imageLink.isNullOrEmpty())
                Glide.with(context).load(user.imageLink).into(post_reply_user_image)
            post_reply_text.text = reply!!.reply
            post_reply_info.text = getDateDifference(reply.date)
            post_reply_user_name.text = user.firstName + " " + user.lastName
        }
        p0.itemView.setOnClickListener { openCustomActivity(user.uid) }
    }

    fun openCustomActivity(uid: String) {
        if (uid.equals(pref.getUserFromPref()!!.uid))
            context.openActivity<MenuBaseActivity>(MenuBaseActivity.FRAGMENT, MenuBaseActivity.PROFILE)
        else
            context.openActivity<AnotherUserProfileActivity>("uid", uid)


    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): MyViewHolder {
        val itemView: View? = LayoutInflater.from(p0.context)
                .inflate(R.layout.post_reply_item, p0, false)
        return MyViewHolder(itemView!!)
    }

    override fun getItemCount(): Int {
        return repliesList.size
    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view)
    /*fun getDateDifference(startDate: Date, endDate: Date): String {
        var different = endDate.getTime() - startDate.getTime()

        val secondsInMilli: Long = 1000
        val minutesInMilli = secondsInMilli * 60
        val hoursInMilli = minutesInMilli * 60
        val daysInMilli = hoursInMilli * 24
        val monthInMilli = hoursInMilli * 24 * 30
        val yearInMilli = hoursInMilli * 24 * 30 * 12

        val years = different / yearInMilli
        different = different % yearInMilli

        val months = different / monthInMilli
        different = different % monthInMilli

        val days = different / daysInMilli
        different = different % daysInMilli

        val hours = different / hoursInMilli
        different = different % hoursInMilli

        val minutes = different / minutesInMilli

        if (years > 1)
            return "$years years ago"
        else if (years.equals(1))
            return "a year ago"
        else if (months > 1)
            return "$months months ago"
        else if (months.equals(1))
            return "a month ago"
        else if (days > 1)
            return "$days days ago"
        else if (days.equals(1))
            return "a day ago"
        else if (hours > 1)
            return "$hours hours ago"
        else if (hours.equals(1))
            return "a hour ago"
        else if (minutes > 1)
            return "$minutes minutes ago"
        else
            return "few seconds ago"
    }*/
}