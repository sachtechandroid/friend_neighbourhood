package com.sachtechsolution.neighbourhood.ui.login.fragments

import android.os.Bundle
import android.view.View
import android.widget.RadioButton
import com.example.manish.internetstatus.OttoBus
import com.facebook.AccessToken
import com.facebook.GraphRequest
import com.google.firebase.database.*
import com.sachtechsolution.neighbourhood.HomePage
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.chat.ActivitySelectFriend.USERS_CHILD
import com.sachtechsolution.neighbourhood.chat.data.SettingsAPI
import com.sachtechsolution.neighbourhood.constants.Constants
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.model.User
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_sign_up_info.*
import org.json.JSONException

class SignUpInfoFragment : BaseFragment() {

    var gender = ""
    var uid = ""
    var ref: DatabaseReference? = null
    var set: SettingsAPI? = null
    var token: AccessToken? = null
    var email = ""
    var image_url = ""

    override fun viewToCreate(): Int {
        return R.layout.fragment_sign_up_info
    }

    val firestore by lazy { FireStoreHelper.getInstance() }
    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }

    private var tUser: User? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        uid = fireStoreHelper.currentUserUid()
        set = SettingsAPI(activity)
        //listner

        if (arguments!==null) {
            token = arguments!!.get("token") as AccessToken
            getUserProfile(token!!)
        }
        sex.setOnCheckedChangeListener { radioGroup, i ->
            val selectedId = sex.checkedRadioButtonId
            var radioSexButton = view.findViewById(selectedId) as RadioButton
            gender = radioSexButton.text.toString()

        }
        signup_info_continue_tv.setOnClickListener {
            val firstName = signup_info_first_name_tiet.text.toString().capitalize()
            val lastName = signup_info_last_name_tiet.text.toString().capitalize()
            if (firstName.isNotEmpty()) {
                showProgress()

                if (token == null) {
                    fireStoreHelper.getUser(fireStoreHelper.currentUserUid()).addOnCompleteListener {
                        if (it.isSuccessful) {
                            if (it.result.exists()) {
                                tUser = it.result.toObject(User::class.java)!!

                                val user = User(uid = tUser!!.uid, firstName = firstName, lastName = lastName, street = tUser!!.street,
                                        lat = tUser!!.lat, lng = tUser!!.lng, email = tUser!!.email, gender = gender, groups = tUser!!.groups,
                                        mainLocality = tUser!!.mainLocality, society = tUser!!.society.trim(), address = tUser!!.address,
                                        zipCode = tUser!!.zipCode)

                                firestore.setUser(uid, user).addOnCompleteListener {
                                    if (it.isSuccessful) {
                                        pref.setUserToPref(user!!)
                                        setUserToDataBase("$firstName $lastName", user)
                                        //getUser()
                                    } else {
                                        showToast("error")
                                        hideProgress()
                                    }
                                }.addOnFailureListener {
                                    showToast("fail")
                                    hideProgress()
                                }
                            }
                        }
                    }
                } else {

                    val user = User(uid = fireStoreHelper.currentUserUid(), zipCode = Constants.ZIP_CODE, street = Constants.STREET,
                            lat = Constants.LAT, lng = Constants.LONG, email = email,
                            mainLocality = Constants.MAIN_LOCALITY, society = Constants.SOCIETY,
                            address = Constants.ADDRESS, firstName = firstName, lastName = lastName, gender = gender)

                    fireStoreHelper.setUser(uid, user).addOnCompleteListener {
                        if (it.isSuccessful) {
                            hideProgress()
                            baseAcitivityListener.navigator.replaceFragment(SignUpInfoFragment::class.java, true)
                            pref.setUserToPref(user!!)
                            tUser = user
                            setUserToDataBase("$firstName $lastName", user)
                        } else {
                            showToast("error")
                            hideProgress()
                        }
                    }.addOnFailureListener {
                        showToast("fail")
                        hideProgress()
                    }
                }
            } else
                showToast("enter name")

        }
    }

    private fun getUserProfile(currentAccessToken: AccessToken) {
        val request = GraphRequest.newMeRequest(
                currentAccessToken) { `object`, response ->
            try {
                val first_name = `object`.getString("first_name")
                val last_name = `object`.getString("last_name")

                email = `object`.getString("email")
                val id = `object`.getString("id")

                image_url = "https://graph.facebook.com/$id/picture?type=normal"

                //txtUsername.setText("First Name: $first_name\nLast Name: $last_name")
                //  txtEmail.setText(email)
                //    Picasso.with(this@MainActivity).load(image_url).into(imageView)

                signup_info_first_name_tiet.setText(first_name)
                signup_info_last_name_tiet.setText(last_name)
            } catch (e: JSONException) {
                e.printStackTrace()
            }
        }

        val parameters = Bundle()
        parameters.putString("fields", "first_name,last_name,email,id")
        request.parameters = parameters
        request.executeAsync()

    }

    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
    }


    @Subscribe
    fun getMessage(internetStatus: String) {

        if (internetStatus.equals("Not connected to Internet")) {
            InternetStatusDialogFragment.newInstance().show(fragmentManager, "option_fragment")
        } else {
            showToast("internet enabled ")

        }

    }

    override fun onResume() {
        super.onResume()
        OttoBus.bus.register(this)
    }


    private fun setUserToDataBase(name: String, user: User) {


        ref = FirebaseDatabase.getInstance().getReference(USERS_CHILD)
        ref!!.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                val usrNm = name
                val usrId = uid
                val usrDp = "http://www.gravatar.com/avatar/?d=mm"

                set!!.addUpdateSettings("myid", usrId)
                set!!.addUpdateSettings("myname", usrNm)
                set!!.addUpdateSettings("mydp", usrDp)
                set!!.addUpdateSettings("street", user.street)
                set!!.addUpdateSettings("zipCode", user.zipCode)

                if (!snapshot.hasChild(usrId!!)) {
                    ref!!.child(usrId!! + "/name").setValue(usrNm)
                    ref!!.child(usrId!! + "/photo").setValue(usrDp)
                    ref!!.child(usrId!! + "/id").setValue(usrId)
                    ref!!.child(usrId!! + "/street").setValue(user.street)
                    ref!!.child(usrId!! + "/zipCode").setValue(user.zipCode)
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {}
        })


        var list = ArrayList<String>()

        fireStoreHelper.getAddresses(tUser!!.zipCode +","+ tUser!!.mainLocality).addOnCompleteListener {
            if (it.isSuccessful) {
                Constants.ADDRESSES.clear()
                Constants.SOCIETIES.clear()
                it.result.documents.forEach {
                    if (it.id.equals(tUser!!.street))
                        list = it.data?.get("society") as ArrayList<String>
                }
                if (!list.contains(user!!.society))
                    list.add(tUser!!.society)
                val map = HashMap<String, ArrayList<String>>()
                map.put("society", list)
                firestore.addAddress(zip = tUser!!.zipCode +","+ tUser!!.mainLocality, street = tUser!!.street,
                        map = map).addOnCompleteListener {
                    if (it.isSuccessful) {
                        val map2 = HashMap<String, String>()
                        map2.put("soci","yo")
                        firestore.addAddressdummy(user!!.zipCode +","+ user!!.mainLocality,map2)
                        baseAcitivityListener.navigator.openActivity(HomePage::class.java)
                        activity?.finish()
                        Constants.ADDRESS = ""
                        Constants.SOCIETY = ""
                        Constants.STREET = ""
                        Constants.ZIP_CODE = ""
                        Constants.LAT = 0.0
                        Constants.LONG = 0.0
                        hideProgress()
                    } else
                        hideProgress()
                }.addOnFailureListener { hideProgress() }

            }

        }
    }
}
