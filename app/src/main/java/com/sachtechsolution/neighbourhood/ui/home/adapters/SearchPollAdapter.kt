package com.sachtechsolution.neighbourhood.ui.home.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.constants.Constants
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.showToast
import com.sachtechsolution.neighbourhood.intracter.Api_Hitter
import com.sachtechsolution.neighbourhood.model.Notification
import com.sachtechsolution.neighbourhood.model.PollResult
import com.sachtechsolution.neighbourhood.model.Post
import com.sachtechsolution.neighbourhood.model.User
import gurtek.mrgurtekbasejava.base.ModelPrefrence
import kotlinx.android.synthetic.main.poll_item.view.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/* whoGuri 4/9/18 */
class SearchPollAdapter(val context: Context, val post: Post?, val homePagePostAdapter: SearchAdapter, val position: Int, val post_poll_rv: RecyclerView?, val pref: ModelPrefrence) : RecyclerView.Adapter<SearchPollAdapter.ViewHolder>() {

    var clickAble = true
    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val itemView: View? = LayoutInflater.from(context)
                .inflate(R.layout.poll_item, p0, false)
        return ViewHolder(itemView!!)
    }


    override fun getItemCount(): Int {
        return post!!.poll!!.choices.size
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        var choices=post!!.poll!!.choices
        p0.itemView.choice_tv.text = choices[p1]
        val cuid=FireStoreHelper.getInstance().currentUserUid()

        p0.itemView.setOnClickListener {
            if (clickAble && ! Constants.IS_POLLED_LIST[position]) {
                clickAble = false
                //Toast.makeText(context, "clk", Toast.LENGTH_SHORT).show()

                fireStoreHelper.answerPoll(post.pid, PollResult(p1, fireStoreHelper.currentUserUid())).addOnCompleteListener {
                    if (it.isSuccessful) {

                        if (!post.uid.equals(cuid)) {

                            fireStoreHelper.getUser(post.uid).addOnCompleteListener {
                                if (it.isSuccessful) {
                                    if (it.result.exists()) {
                                        val pUser = it.result.toObject(User::class.java)

                                        fireStoreHelper.createNotification(uid = post.uid, pid = post.pid + "poll",
                                                notification = Notification(type = Notification.POLL,
                                                        suid = cuid, pid = post.pid, text = " polled your post"))

                                            val user=pref.getUserFromPref()

                                        val callback=Api_Hitter.ApiInterface("http://stsmentor.com/").sendNotification(user!!.firstName+" "+user.lastName+" "+"polled your post",pUser!!.token)
                                            callback.enqueue(object :Callback<ResponseBody> {
                                                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                                                context.showToast("got error")
                                                }

                                                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                                                    if (response.errorBody()!=null)
                                                    {
                                                        context.showToast("Notification Not sent")
                                                    }
                                                    else if (response.body()!=null)
                                                    {
                                                        context.showToast("Notification sent")
                                                    }
                                                }

                                            })

                                    }
                                }

                            }

                        }


                        Constants.IS_POLLED_LIST[position] = true

                        if (homePagePostAdapter != null) {
                            //homePagePostAdapter.isPolledList[position] = true
                            homePagePostAdapter.notifyItemChanged(position)
                        } else {

                            fireStoreHelper.getPollResult(post.pid).addOnCompleteListener {
                                if (it.isSuccessful){
                                val results = ArrayList<Int>()
                                for (i in 0..9) {
                                    results.add(0)
                                }
                                var totalPolls = 0
                                val documents = it.result.documents

                                    documents.forEach {
                                    val result = it.toObject(PollResult::class.java)
                                    results[result!!.choice] = results[result!!.choice] + 1
                                    totalPolls++
                                }
                                post_poll_rv!!.adapter = PollResultAdapter(context, choices, results, totalPolls)
                            }

                            }
                        }
                    }
                    else
                        clickAble=true
                }.addOnFailureListener { clickAble=true }
            }else
                homePagePostAdapter!!.notifyItemChanged(position)
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}