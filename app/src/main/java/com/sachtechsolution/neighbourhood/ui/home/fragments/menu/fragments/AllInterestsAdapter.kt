package com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments

import android.annotation.SuppressLint
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.constants.PostInterests
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.gone
import kotlinx.android.synthetic.main.item_follow_interests.view.*


class AllInterestsAdapter(val context: Context,val listner : onitemClick) : RecyclerView.Adapter<AllInterestsAdapter.ViewHolder>() {

    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }
    var list = ArrayList<PostInterests>()

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {

        val v = LayoutInflater.from(p0.context).inflate(R.layout.item_follow_interests, p0, false)
        var viewHolder = ViewHolder(v)

        return viewHolder
    }

    override fun getItemCount(): Int {
        return list.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var list = list[position]

        holder.itemView.txtName.text = list.type
        holder.itemView.txtFolow.gone()
        holder.itemView.setOnClickListener {
            listner.onInterestsClick(list.type)
        }


    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {}

    fun addItem(interests: PostInterests) {
        list.add(interests)
        notifyDataSetChanged()

    }
    interface onitemClick
    {
        fun onInterestsClick(type: String)
    }
}
