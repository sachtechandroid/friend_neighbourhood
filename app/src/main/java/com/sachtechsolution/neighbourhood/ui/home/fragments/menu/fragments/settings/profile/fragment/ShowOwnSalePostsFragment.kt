package com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings.profile.fragment

import android.os.Bundle
import android.view.View
import com.example.manish.internetstatus.OttoBus
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.constants.Constants
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.gone
import com.sachtechsolution.neighbourhood.model.Post
import com.sachtechsolution.neighbourhood.model.User
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.sachtechsolution.neighbourhood.ui.postDetails.SalePostDetailActivity
import com.sachtechsolution.neighbourhood.ui.sale.adapter.SalesAndFreeImageAdapter
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_show_own_sales.*
import kotlinx.android.synthetic.main.sale_toolbar.*

class ShowOwnSalePostsFragment : BaseFragment(), SalesAndFreeImageAdapter.OpenDetailActivity {

    override fun onOpenActivity(post: Post,user: User, position: Int) {
        baseAcitivityListener.navigator.openActivity(SalePostDetailActivity::class.java, Bundle().apply {
            putParcelable("post", post)
            putParcelable("user", user)
            putInt("position", position)

        })
    }


    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
    }


    @Subscribe
    fun getMessage(internetStatus:String) {

        if (internetStatus.equals("Not connected to Internet")){
            InternetStatusDialogFragment.newInstance().show(fragmentManager, "option_fragment")
        }else {
            showToast("internet enabled ")

        }

    }

    override fun onResume() {
        super.onResume()
        OttoBus.bus.register(this)
    }


    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }
    val uid by lazy { pref.getUserFromPref()!!.uid }
    private val salesAndFreeImageAdapter by lazy { SalesAndFreeImageAdapter(activity!!, this) }

    override fun viewToCreate(): Int {
        return R.layout.fragment_show_own_sales
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rvOwnSale.adapter = salesAndFreeImageAdapter
        getPosts()

        btnBack.setOnClickListener { activity!!.onBackPressed() }
        txtNext.gone()
    }

    private fun getPosts() {
        Constants.IS_THANKED_LIST.clear()
        Constants.IS_POLLED_LIST.clear()
        showProgress()
        fireStoreHelper.getOwnSalePosts().addOnCompleteListener {
            if (it.isSuccessful) {
                if (it.result.documents.size == 0)
                    ownSalePostMessage.text = "No item found"
                it.result.documents.forEach {
                    var post = it.toObject(Post::class.java)
                    post?.pid = it.id
                    salesAndFreeImageAdapter.addItem(post!!)
                    /*fireStoreHelper.getUser(post!!.uid).addOnCompleteListener {
                        if (it.isSuccessful && ownSalePostMessage != null) {
                            val user = it.result.toObject(User::class.java)
                            var salePost=SalePostBean(post=post,user = user)
                            ownSalePostMessage.gone()

                        }
                    }*/
                }
                hideProgress()
            }
        }.addOnFailureListener {
            hideProgress()
            showToast(it.message!!)
        }
    }
}