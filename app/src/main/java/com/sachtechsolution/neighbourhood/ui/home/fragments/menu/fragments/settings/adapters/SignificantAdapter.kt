package com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sachtechsolution.neighbourhood.R

class SignificantAdapter(var context: Context) : RecyclerView.Adapter<SignificantAdapter.MyViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val itemView: View = LayoutInflater.from(parent.context)
                .inflate(R.layout.view_significant, parent, false)

        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {

    }

    override fun getItemCount(): Int {
        return 2
    }



    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view)


}
