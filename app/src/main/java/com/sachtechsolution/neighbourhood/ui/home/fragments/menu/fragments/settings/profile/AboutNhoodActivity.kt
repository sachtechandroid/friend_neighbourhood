package com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings.profile

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import com.example.manish.internetstatus.OttoBus
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseActivity
import com.sachtechsolution.neighbourhood.basepackage.base.ProgressDialogFragment
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.showToast
import com.sachtechsolution.neighbourhood.model.Biography
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.squareup.otto.Subscribe
import fisk.chipcloud.ChipCloud
import fisk.chipcloud.ChipCloudConfig
import kotlinx.android.synthetic.main.activity_about_nhood.*

class AboutNhoodActivity : BaseActivity() {

    override fun fragmentContainer(): Int {
        return 0
    }

    var selectedList=""
    val fireStoreHelper:FireStoreHelper by lazy { FireStoreHelper.getInstance() }
    var list:ArrayList<String>?=null
    var chipCloud: ChipCloud?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about_nhood)
        list= ArrayList()

        getCharacter()
        getSettings()
        getActivity()
        getOther()

        txtDone.setOnClickListener {
            uploadData(filterData())
        }
    }


    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
    }


    @Subscribe
    fun getMessage(internetStatus:String) {

        if (internetStatus.equals("Not connected to Internet")){
            InternetStatusDialogFragment.newInstance().show(supportFragmentManager, "option_fragment")
        }else {
            showToast("internet enabled ")

        }

    }

    override fun onResume() {
        super.onResume()
        OttoBus.bus.register(this)
    }


    private fun filterData(): Biography.AboutLoveNei {
        for (i in 0 until list!!.size) {


            if (i==list!!.lastIndex)
                selectedList += list!![i]
            else
                selectedList += list!![i]+","
        }

        return Biography.AboutLoveNei(selectedList)
    }

    private fun uploadData(interest: Biography.AboutLoveNei) {
        /*intent.putStringArrayListExtra("recommendation",recommendation)
        setResult(2,intent)
       */
        showProgress()
        fireStoreHelper.updateLoveNeigh(interest,pref.getUserFromPref()!!.uid).addOnCompleteListener {
            if (it.isSuccessful) {
                showToast("Updated")
                hideProgress()
                var intent= Intent()
                intent.putStringArrayListExtra("recommendation",list)
                setResult(4,intent)
                finish()
                //finish()
            } else {
                showToast("error")
                ProgressDialogFragment.hideProgress()
            }
            ProgressDialogFragment.hideProgress()
        }.addOnFailureListener {
            showToast("error")
            ProgressDialogFragment.hideProgress()
        }
    }


    private fun getOther() {
        val config = ChipCloudConfig()
                .selectMode(ChipCloud.SelectMode.multi)
                .checkedChipColor(resources.getColor(R.color.optional))
                .checkedTextColor(Color.parseColor("#ffffff"))
                .uncheckedChipColor(Color.parseColor("#e0e0e0"))
                .uncheckedTextColor(resources.getColor(R.color.optional))


        chipCloud = ChipCloud(this, cvOther, config)
        chipCloud!!.addChip("Add your own...")

        chipCloud!!.setListener { index, checked, userClick ->
            if (userClick) {
                if (chipCloud!!.getLabel(index).equals("Add your own...")){
                    openActivity<MoreChipsActivty>(2)
                }else{

                    if (checked)
                        list!!.add(chipCloud!!.getLabel(index))
                    else {
                        for (i in 0..list!!.size) {
                            if (list!![i].equals(chipCloud!!.getLabel(index).toString())) {
                                list!!.removeAt(i)
                                break
                            }
                        }
                    }
                }

            }

        }
    }
    private inline fun <reified I> openActivity(requestCode: Int) {
        var intent=Intent(this, I::class.java)
        intent.putExtra("code",requestCode)
        startActivityForResult(intent,requestCode)
    }
    private fun getCharacter() {
        val config = ChipCloudConfig()
                .selectMode(ChipCloud.SelectMode.multi)
                .checkedChipColor(resources.getColor(R.color.optional))
                .checkedTextColor(Color.parseColor("#ffffff"))
                .uncheckedChipColor(Color.parseColor("#e0e0e0"))
                .uncheckedTextColor(resources.getColor(R.color.optional))

        val chipCloud = ChipCloud(this, cvCharacter, config)
        val demoArray = resources.getStringArray(R.array.array_character)
        chipCloud.addChips(demoArray)

        chipCloud.setListener { index, checked, userClick ->
            if (userClick) {
                if (checked)
                    list!!.add(chipCloud.getLabel(index))
                else {
                    for (i in 0..list!!.size) {
                        if (list!![i].equals(chipCloud.getLabel(index).toString())) {
                            list!!.removeAt(i)
                            break
                        }
                    }
                }

            }

        }
    }

    private fun getSettings() {
        val config = ChipCloudConfig()
                .selectMode(ChipCloud.SelectMode.multi)
                .checkedChipColor(resources.getColor(R.color.optional))
                .checkedTextColor(Color.parseColor("#ffffff"))
                .uncheckedChipColor(Color.parseColor("#e0e0e0"))
                .uncheckedTextColor(resources.getColor(R.color.optional))

        val chipCloud = ChipCloud(this, cvSettings, config)
        val demoArray = resources.getStringArray(R.array.array_settings)
        chipCloud.addChips(demoArray)

        chipCloud.setListener { index, checked, userClick ->
            if (userClick) {
                if (checked)
                    list!!.add(chipCloud.getLabel(index))
                else {
                    for (i in 0..list!!.size) {
                        if (list!![i].equals(chipCloud.getLabel(index).toString())) {
                            list!!.removeAt(i)
                            break
                        }
                    }
                }
            }

        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        // check if the request code is same as what is passed  here it is 2
        if (requestCode == 2&&data!=null) {

            var list = data!!.getStringExtra("data")

            chipCloud!!.addChip(list)
        }
    }

    private fun getActivity() {
        val config = ChipCloudConfig()
                .selectMode(ChipCloud.SelectMode.multi)
                .checkedChipColor(resources.getColor(R.color.optional))
                .checkedTextColor(Color.parseColor("#ffffff"))
                .uncheckedChipColor(Color.parseColor("#e0e0e0"))
                .uncheckedTextColor(resources.getColor(R.color.optional))

        val chipCloud = ChipCloud(this, cvActivity, config)
        val demoArray = resources.getStringArray(R.array.array_activities)
        chipCloud.addChips(demoArray)

        chipCloud.setListener { index, checked, userClick ->
            if (userClick) {
                if (checked)
                    list!!.add(chipCloud.getLabel(index))
                else {
                    for (i in 0..list!!.size) {
                        if (list!![i].equals(chipCloud.getLabel(index).toString())) {
                            list!!.removeAt(i)
                            break
                        }
                    }
                }

            }

        }
    }

}
