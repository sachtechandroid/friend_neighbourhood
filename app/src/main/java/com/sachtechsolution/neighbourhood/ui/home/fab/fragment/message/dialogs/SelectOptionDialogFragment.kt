package com.sachtechsolution.neighbourhood.ui.home.fab.fragment.message.dialogs

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.*
import com.sachtechsolution.neighbourhood.R
import kotlinx.android.synthetic.main.view_choose_option.*

class SelectOptionDialogFragment:DialogFragment(), View.OnClickListener {


    override fun onClick(p0: View?) {
        var id=p0!!.id

        when(id)
        {
            R.id.txtChooseGallery-> {
                dismiss()
                listner!!.forGallery()}
            R.id.txtChooseCamera-> {dismiss()
                listner!!.forCamera()}
        }
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dialog.window.setGravity(Gravity.CENTER)
        dialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        return inflater.inflate(R.layout.view_choose_option, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //listner
        txtChooseGallery.setOnClickListener(this)
        txtChooseCamera.setOnClickListener(this)
    }


    companion object {
        var listner: onOptionSelect?=null

        fun newInstance(listner: SelectOptionDialogFragment.onOptionSelect): SelectOptionDialogFragment {
            val fragment = SelectOptionDialogFragment()
            Companion.listner =listner
            return fragment

        }
    }

   interface onOptionSelect{
       fun forGallery()
       fun forCamera()
   }
}