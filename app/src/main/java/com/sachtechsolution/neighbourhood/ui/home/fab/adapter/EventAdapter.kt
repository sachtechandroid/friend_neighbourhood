package com.sachtechsolution.neighbourhood.ui.home.fab.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.extension.gone
import kotlinx.android.synthetic.main.view_event_neighbours.view.*


class EventAdapter(activity: Context, var listner: OpenDone) : RecyclerView.Adapter<EventAdapter.MyViewHolder>() {

    internal var context: Context = activity


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val itemView: View = LayoutInflater.from(parent.context)
                .inflate(R.layout.view_event_neighbours, parent, false)

        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        if (position>18)
            holder.itemView.view.gone()
    }

    override fun getItemCount(): Int {
        return 20
    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view)

    interface OpenDone{
        fun OnEventDone()
    }
}
