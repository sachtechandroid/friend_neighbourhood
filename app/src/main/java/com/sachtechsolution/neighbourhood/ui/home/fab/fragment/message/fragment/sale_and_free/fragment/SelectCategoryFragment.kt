package com.sachtechsolution.neighbourhood.ui.home.fab.fragment.message.fragment.sale_and_free.fragment

import android.os.Bundle
import android.view.View
import com.bususer.tiket.adapters.SelectCategoryAdapter
import com.example.manish.internetstatus.OttoBus
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.constants.classdata
import com.sachtechsolution.neighbourhood.extension.showToast
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_select_category.*
import kotlinx.android.synthetic.main.sale_toolbar.*

class SelectCategoryFragment:BaseFragment(), View.OnClickListener,SelectCategoryAdapter.SelectCategory {
    var seletedcategory=""
    val catogriesList= arrayListOf("Appliances","Baby & Kids",
            "Bicycles","Clothing & Accessories","Electronics","Furniture","Garage Sales"
            ,"Garden","Home Decor","Housing","Motoring","Other","Pet Supplies","Sports & Outdoors",
            "Tickets","Tools","Toys & Games")
    val saleListAdapter by lazy { SelectCategoryAdapter(this,activity!!,catogriesList) }




    override fun OnSelectCategory(s: String) {
        seletedcategory=s
    }



    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
    }


    @Subscribe
    fun getMessage(internetStatus:String) {

        if (internetStatus.equals("Not connected to Internet")){
            InternetStatusDialogFragment.newInstance().show(fragmentManager, "option_fragment")
        }else {
            showToast("internet enabled ")

        }

    }

    override fun onResume() {
        super.onResume()
        OttoBus.bus.register(this)
    }




    override fun onClick(p0: View?) {
        when(p0!!.id)
        {
            R.id.txtNext->next()
            R.id.btnBack->activity!!.onBackPressed()
        }
    }

    private fun next() {

        if (seletedcategory.isNotEmpty()){
            classdata.category=seletedcategory
            var bundle=Bundle()
            bundle!!.putString("image",activity!!.intent.getStringExtra("image"))
            //
            baseAcitivityListener.navigator.replaceFragment(SelectNeighbourFragment::class.java,true,bundle)
        }else
            activity!!.showToast("Select category")
    }


    override fun viewToCreate(): Int {
        return R.layout.fragment_select_category
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerview.adapter=saleListAdapter


        //listner
        txtNext.setOnClickListener(this)
        btnBack.setOnClickListener(this)
    }
}