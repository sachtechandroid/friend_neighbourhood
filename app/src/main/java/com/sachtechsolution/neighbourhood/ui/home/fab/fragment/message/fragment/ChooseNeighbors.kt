package com.sachtechsolution.neighbourhood.ui.home.fab.fragment.message.fragment

import android.os.Bundle
import android.view.View
import com.example.manish.internetstatus.OttoBus
import com.google.firebase.storage.StorageReference
import com.sachtechsolution.neighbourhood.HomePage
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.constants.Constants
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.gone
import com.sachtechsolution.neighbourhood.intracter.Api_Hitter
import com.sachtechsolution.neighbourhood.model.Post
import com.sachtechsolution.neighbourhood.model.User
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_lost_and_found.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList

/* whoGuri 25/8/18 */
class ChooseNeighbors : BaseFragment() {

    lateinit var mRef: StorageReference
    val zipCode by lazy { pref.getUserFromPref()!!.zipCode }
    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }
    val type by lazy { arguments?.getString("id") ?: "" }
    val subject by lazy { arguments?.getString("subject") ?: "" }
    val message by lazy { arguments?.getString("message") ?: "" }
    val address by lazy { arguments?.getString("address") ?: "" }
    val extra by lazy { arguments?.getString("extra") ?: "" }
    var imagesLinks = ""
    var pdfLinks = ""
    private var size = 0
    private var count = 0
    val user by lazy { pref.getUserFromPref()!! }
    //var adapter: ChooseNeighborsAdapter? = null

    override fun viewToCreate(): Int {
        return R.layout.fragment_lost_and_found
    }


    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
    }


    @Subscribe
    fun getMessage(internetStatus: String) {

        if (internetStatus.equals("Not connected to Internet")) {
            InternetStatusDialogFragment.newInstance().show(fragmentManager, "option_fragment")
        } else {
            showToast("internet enabled ")

        }

    }

    override fun onResume() {
        super.onResume()
        OttoBus.bus.register(this)
    }


    lateinit var list: ArrayList<String>

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        listeners()
        /*val subAddress = pref.getUserFromPref()!!.subLocality
        adapter = ChooseNeighborsAdapter(subAddress)
        addresses_list.adapter = adapter
        private_area.text = subAddress
*/

        list = ArrayList()
        list.add(user.society)

        sociteyRB.text = user.society
        subLocalityRB.text = user.street
        zipCodeRB.text = user.mainLocality + " + Nearby"

        if (user.society.equals(user.street)) {
            sociteyRB.gone()
            subLocalityRB.isChecked = true
        }

        sociteyRB.setOnCheckedChangeListener { buttonView, isChecked ->

            if (isChecked) {
                list.clear()
                list.add(user.society)
            }
        }

        subLocalityRB.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                list.clear()
                list = Constants.SOCIETIES
            }
        }
        zipCodeRB.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                list.clear()
                list = Constants.ADDRESSES
            }
        }


    }

    private fun listeners() {

        create_post_back.setOnClickListener {
            activity!!.onBackPressed()
        }

        create_post_send.setOnClickListener {
            showProgress()
            size = Constants.IMAGE_LIST?.size!!
            if (size > 0)
                uploadImage()
            else if (Constants.PDF_URI != null)
                uploadPdf()
            else
                createPost()
        }

/*        private_area.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                addresses_list.gone()
                adapter!!.addressList.clear()
                adapter!!.checkedList.clear()
            }
        }

        protected_area.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                addresses_list.visible()
            }
        }*/
    }


    private fun uploadImage() {
        var time = java.sql.Timestamp(Date().time)
        var timestamp = time.toString().replace(":", "").replace("-", "").replace(" ", "").replace(".", "")

        mRef = fireStoreHelper.getStorageRef().child("postImages").child(fireStoreHelper.currentUserUid() + timestamp)
        mRef.putFile(Constants.IMAGE_LIST!![count]!!).addOnCompleteListener {
            if (it.isSuccessful) {
                it.result

                mRef!!.downloadUrl.addOnSuccessListener { p0 ->
                    count += 1
                    imagesLinks = imagesLinks + p0.toString() + "#"
                    if (size == count)
                        createPost()
                    else if (Constants.PDF_URI != null)
                        uploadPdf()
                    else
                        uploadImage()
                }.addOnFailureListener {
                    hideProgress()
                }
            }
        }.addOnFailureListener {
            hideProgress()
        }
    }

    private fun uploadPdf() {
        var time = java.sql.Timestamp(Date().time)
        var timestamp = time.toString().replace(":", "").replace("-", "").replace(" ", "").replace(".", "")

        mRef = fireStoreHelper.getStorageRef().child("postPdf").child(fireStoreHelper.currentUserUid() + timestamp)
        mRef.putFile(Constants.PDF_URI!!).addOnCompleteListener {
            if (it.isSuccessful) {
                it.result

                mRef!!.downloadUrl.addOnSuccessListener { p0 ->
                    count += 1
                    pdfLinks = p0.toString()
                    createPost()
                    Constants.PDF_URI = null
                }.addOnFailureListener {
                    hideProgress()
                }
            }
        }.addOnFailureListener {
            hideProgress()
        }
    }

    private fun createPost() {
/*
        if (adapter!!.addressList.size == 0)
            adapter!!.addressList.add(user!!.subLocality)
*/
        val post = Post(subject = subject, message = message, extra = extra, society = pref.getUserFromPref()!!.society,
                uid = fireStoreHelper.currentUserUid(), privacy = list/*adapter!!.addressList*/, pdfLink = pdfLinks,
                type = type, imagesLinks = imagesLinks, zipCode = zipCode, location = address)

        fireStoreHelper.createPost(post).addOnCompleteListener {
            if (it.isSuccessful) {

                fireStoreHelper.getAllNeighbors(pref.getUserFromPref()!!.zipCode).addOnCompleteListener {
                    if (it.isSuccessful) {
                        var token = ""
                        it.result.documents.forEach {
                            if (!it.id.equals(pref.getUserFromPref()!!.uid) && list.contains(it.toObject(User::class.java)!!.society))
                                token = token + "," + it.toObject(User::class.java)!!.token
                        }

                        if (!token.isNullOrEmpty()) {
                            token = token.substring(1)
                            val title = "${pref.getUserFromPref()!!.firstName} ${pref.getUserFromPref()!!.lastName} just posted in neighborhood"

                            val callback = Api_Hitter.ApiInterface("http://stsmentor.com/").sendNotificationToAll(title, token, "zxc")
                            callback.enqueue(object : Callback<ResponseBody> {
                                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                                    next()
                                }

                                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                                    next()
                                }
                            })
                        } else {
                            next()
                        }
                    }
                }.addOnFailureListener { next() }
            } else {
                showToast("error")
            }

        }.addOnFailureListener {
            showToast("failed")
            hideProgress()

        }.addOnCanceledListener {
            showToast("cancled")
            hideProgress()
        }
    }

    fun next() {
        hideProgress()
        showToast("Posted")
        baseAcitivityListener.navigator.openActivity(HomePage::class.java)
        activity!!.finish()
    }

}