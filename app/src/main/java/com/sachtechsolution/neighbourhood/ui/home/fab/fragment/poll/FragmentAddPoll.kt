package com.sachtechsolution.neighbourhood.ui.home.fab.fragment.poll

import android.os.Bundle
import android.view.View
import com.example.manish.internetstatus.OttoBus
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.extension.showToast
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.custom_toolbar.*
import kotlinx.android.synthetic.main.fragment_addpoll.*

class FragmentAddPoll:BaseFragment(), View.OnClickListener {

    override fun viewToCreate(): Int {
        return R.layout.fragment_addpoll
    }

    val addressList by lazy { arguments!!.getStringArrayList("list") }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //setup listner
        nextLayout.setOnClickListener(this)
        cancelLayout.setOnClickListener(this)
    }


    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
    }



    @Subscribe
    fun getMessage(internetStatus:String) {

        if (internetStatus.equals("Not connected to Internet")){
            InternetStatusDialogFragment.newInstance().show(fragmentManager, "option_fragment")
        }else {
            showToast("internet enabled ")

        }

    }

    override fun onResume() {
        super.onResume()
        OttoBus.bus.register(this)
    }


    override fun onClick(view: View?) {
        var id=view!!.id

        when(id)
        {
                R.id.nextLayout->{

                val pollQuestion=poll_create_question.text.toString()
                if (pollQuestion.isNullOrEmpty()){context!!.showToast("Please provide Question for your poll")}

                else{
                            baseAcitivityListener.navigator.replaceFragment(AddPollChoicesFragment::class.java,
                            true,
                            Bundle().apply {
                                putString("question",pollQuestion)
                                putString("description",poll_create_description.text.toString())
                                putStringArrayList("list",addressList)
                            })
                   }
                  }

                R.id.cancelLayout->activity!!.onBackPressed()
        }
    }

}