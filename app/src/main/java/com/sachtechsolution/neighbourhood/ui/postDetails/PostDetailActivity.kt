package com.sachtechsolution.neighbourhood.ui.postDetails

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.support.v4.content.FileProvider
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions.bitmapTransform
import com.example.manish.internetstatus.OttoBus
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseActivity
import com.sachtechsolution.neighbourhood.constants.Constants
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.*
import com.sachtechsolution.neighbourhood.intracter.Api_Hitter
import com.sachtechsolution.neighbourhood.model.*
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.sachtechsolution.neighbourhood.ui.extra.ExtraActivity
import com.sachtechsolution.neighbourhood.ui.home.AnotherUserProfileActivity
import com.sachtechsolution.neighbourhood.ui.home.adapters.PollAdapter
import com.sachtechsolution.neighbourhood.ui.home.adapters.PollResultAdapter
import com.sachtechsolution.neighbourhood.ui.home.fragments.menu.activity.MenuBaseActivity
import com.sachtechsolution.neighbourhood.ui.postDetails.adapter.PostReplyAdapter
import com.squareup.otto.Subscribe
import jp.wasabeef.glide.transformations.BlurTransformation
import kotlinx.android.synthetic.main.activity_post_detail.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File


class PostDetailActivity : BaseActivity() {

    override fun fragmentContainer(): Int {
        return 0
    }

    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
    }

    @Subscribe
    fun getMessage(internetStatus: String) {

        if (internetStatus.equals("Not connected to Internet")) {
            InternetStatusDialogFragment.newInstance().show(supportFragmentManager, "option_fragment")
        } else {
            showToast("internet enabled ")
        }
    }

    override fun onResume() {
        super.onResume()
        OttoBus.bus.register(this)
    }

    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }
    lateinit var post: Post//by lazy {  }
    val isReplyOn by lazy { intent.getBooleanExtra("recommendation", false) }
    val position by lazy { intent.getIntExtra("position", -1) }
    //val isPolled by lazy { intent.getBooleanExtra("isPolled", false) }
    var text: String = ""
    val user by lazy { intent.getParcelableExtra<User>("user") }
    lateinit var adapter: PostReplyAdapter

    private var isThanked: Boolean = false
    lateinit var cUser:User

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_post_detail)

        cUser = pref.getUserFromPref()!!
        post = intent.getParcelableExtra("post")
        Constants.POSITION = position
        val images = post?.imagesLinks?.split("#")
        val size = images?.size!! - 1
        if (isReplyOn)
            post_detail_reply_et.requestFocus()
        if (post.thanksCount > 0) {
            post_detail_thanks_count.visible()
            post_detail_thanks_iv.visible()
            post_detail_thanks_count.text = post!!.thanksCount.toString()
        } else {
            post_detail_thanks_count.gone()
            post_detail_thanks_iv.gone()
        }
        if (post.repliesCount > 0) {
            post_detail_replies_count.visible()
            post_detail_replies_iv.visible()
            post_detail_replies_count.text = post!!.repliesCount.toString()
        } else {
            post_detail_replies_count.gone()
            post_detail_replies_iv.gone()
        }
        if (post.type.equals(Post.postType.POLL)) {
            post_detail_subject_tv.gone()
            post_detail_message_tv.gone()
            post_poll_layout.visible()
            share_post.visible()
            post_poll_question.text = "Poll : " + post.poll!!.question
            if (!Constants.IS_POLLED_LIST[position])
                post_poll_rv.adapter = PollAdapter(this, post, null, position, post_poll_rv, pref)
            else {
                showProgress()
                fireStoreHelper.getPollResult(post.pid).addOnCompleteListener {
                    if (it.isSuccessful) {
                        val results = ArrayList<Int>()
                        for (i in 0..9) {
                            results.add(0)
                        }
                        var totalPolls = 0
                        val documents = it.result.documents
                        documents.forEach {
                            val result = it.toObject(PollResult::class.java)
                            results[result!!.choice] = results[result!!.choice] + 1
                            totalPolls++
                        }
                        post_poll_rv!!.adapter = PollResultAdapter(this, post.poll!!.choices, results, totalPolls)
                        post_votes_count.visible()
                        post_votes_count.text = "total votes: $totalPolls "
                        hideProgress()
                    } else
                        hideProgress()
                }.addOnFailureListener { hideProgress() }
            }
        } else {
            post_detail_subject_tv.visible()
            post_detail_message_tv.visible()
            post_poll_layout.gone()
            share_post.visible()
        }
        post_detail_message_tv.text = post.message
        post_detail_subject_tv.text = post.subject
        post_detail_user_name.text = user.firstName + " " + user.lastName
        post_detail_user_name_latter.text = user.firstName.subSequence(0, 1)
        //post_detail_time.text = getDateDifference(post.date, Date())

        Glide.with(this).load(user.imageLink).into(post_detail_user_image)

        post_detail_user_image.setOnClickListener {
            openCustomActivity(post.uid)
        }
        post_detail_user_name_latter.setOnClickListener { openCustomActivity(post.uid) }

        if (size == 0) {
            post_detail_image_ll_1.gone()
            post_detail_image_rl_0.gone()
        } else if (size == 1) {
            post_detail_image_ll_1.gone()
            post_detail_image_rl_0.visible()
            Glide.with(this).load(images[0]).into(post_detail_image_0_front)
            Glide.with(this).load(images[0])
                    .apply(bitmapTransform(BlurTransformation(25, 3)))
                    .into(post_detail_image_0_back)

        } else if (size!! > 1) {
            post_detail_image_ll_1.visible()
            post_detail_image_rl_0.gone()
            Glide.with(this).load(images[0]).into(post_detail_image_1_front)
            Glide.with(this).load(images[0])
                    .apply(bitmapTransform(BlurTransformation(25, 3)))
                    .into(post_detail_image_1_back)
            Glide.with(this).load(images[1]).into(post_detail_image_2_front)
            Glide.with(this).load(images[1])
                    .apply(bitmapTransform(BlurTransformation(25, 3)))
                    .into(post_detail_image_2_back)
            if (size > 2) {
                post_detail_more_images.visible()
                post_detail_more_images.text = "+" + (size - 2)
            } else
                post_detail_more_images.gone()
        }

        setListener()

        if (post.uid.equals(pref.getUserFromPref()!!.uid)) {
            ivDeletePost.visible()
            ivReportPost.gone()
            ivAddBookmark.gone()
            ivRemoveBookmark.gone()
        } else {
            ivDeletePost.gone()
            ivReportPost.visible()

            fireStoreHelper.getBookmarkById(post.pid).addOnCompleteListener {
                if (it.isSuccessful) {
                    if (it.result.documents.size > 0) {
                        ivAddBookmark.gone()
                        ivRemoveBookmark.visible()
                    } else {
                        ivAddBookmark.visible()
                        ivRemoveBookmark.gone()
                    }
                }
            }
        }


        adapter = PostReplyAdapter(this, pref)
        post_detail_replies_rv.adapter = adapter
        getReplies(adapter)

        post_detail_thanks_ll2.isEnabled = false
        post_detail_thanks_ll.isEnabled = false

        if(position!=-1) {
            if (Constants.IS_THANKED_LIST[position]) {
                showThankedView()
            } else
                showThanksView()
        }else
            showThanksView()


        if (post.location.isNotEmpty()) {
            post_location_tv.text = post.location
            locationLayout.visible()
        } else
            locationLayout.gone()

        if (post.extra.isNotEmpty()) {
            post_extra_tv.text = post.extra
            extraLayout.visible()
        } else
            extraLayout.gone()

        if (post.pdfLink.isNotEmpty()) {
            postDocument.visible()
        } else
            postDocument.gone()

        postDocument.setOnClickListener {
            val path = "" + Environment.getExternalStorageDirectory() +
                    File.separator + "neighborhood"
            val path2 = "" + Environment.getExternalStorageDirectory() +
                    File.separator + "neighborhood" + File.separator + ".temp"
            val file = File(path, post.pid + ".pdf")
            val file2 = File(path2, post.pid + ".pdf")
            if (file.exists())
                Toast.makeText(this, "Pdf is already downloaded in neighborhood folder", Toast.LENGTH_SHORT).show()
            else if (file2.exists())
                Toast.makeText(this, "Wait Downloading..", Toast.LENGTH_SHORT).show()
            else {
                DownloadPdf(this, post.pdfLink, post.pid)
                Toast.makeText(this, "Downloading..", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun openCustomActivity(uid: String) {
        if (uid.equals(cUser.uid))
            openActivity<MenuBaseActivity>(MenuBaseActivity.FRAGMENT, MenuBaseActivity.PROFILE)
        else
            openActivity<AnotherUserProfileActivity>("uid", uid)


    }

    fun showThanksView() {
        isThanked = false
        if(position!=-1)
            Constants.IS_THANKED_LIST[position] = false
        post_detail_thanks_ll.setText("thank")
        post_detail_thanks_ll2.isEnabled = true
        post_detail_thanks_ll.isEnabled = true
    }

    fun showThankedView() {
        isThanked = true
        if(position!=-1)
            Constants.IS_THANKED_LIST[position] = true
        post_detail_thanks_ll.setText("thanked")
        post_detail_thanks_ll2.isEnabled = true
        post_detail_thanks_ll.isEnabled = true
        post_detail_thanks_count.visible()
        post_detail_thanks_iv.visible()
    }

    private fun setListener() {

        share_post.setOnClickListener {
            if (!post.imagesLinks.isNullOrEmpty()) {
                val path = "" + Environment.getExternalStorageDirectory() + File.separator + ".neighborhood"
                val path2 = "" + Environment.getExternalStorageDirectory() + File.separator + ".neighborhood" + File.separator + ".temp"
                var action = "download"
                val size = post.imagesLinks.split("#").size - 2
                for (i in 0..size) {
                    val file = File(path, post.pid + i + ".jpg")
                    if (file.exists())
                        action = "exist"
                    else
                        action = "download"
                }
                for (i in 0..0) {
                    val file2 = File(path2, post.pid + i + ".jpg")
                    if (file2.exists())
                        action = "temp"
                }
                if (action.equals("exist")) {
                    val uris = ArrayList<Uri>()
                    for (i in 0..size) {
                        val file = File(path, post.pid + i + ".jpg")
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                            val picUri = FileProvider.getUriForFile(
                                    this, getApplicationContext()
                                    .getPackageName() + ".provider", file)
                            uris.add(picUri)
                        } else {
                            uris.add(Uri.fromFile(file))
                        }
                    }
                    val intent = Intent()
                    intent.setAction(Intent.ACTION_SEND)
                    //intent.putExtra(Intent.EXTRA_TEXT,post.subject)
                    intent.setType("image/*")
                    intent.putExtra(Intent.EXTRA_STREAM, uris.get(0))
                    startActivity(Intent.createChooser(intent, "share"))
                } else if (action.equals("temp"))
                    Toast.makeText(this, "Wait Downloading..", Toast.LENGTH_SHORT).show()
                else
                //FileDownloader(context, data.get(position).getPostUri(), key + ext, itemHolder.sharebtn)
                    DownloadFile(this, post.imagesLinks, post.pid, post.subject, share_post)
            } else {
                val intent = Intent()
                intent.setAction(Intent.ACTION_SEND)
                intent.setType("text/*")
                intent.putExtra(Intent.EXTRA_TEXT, post.subject + ": " + post.message)
                startActivity(Intent.createChooser(intent, "share"))
            }
        }
        post_detail_image_ll_1.setOnClickListener {
            navigator.openActivity(ExtraActivity::class.java,
                    Bundle().apply { putString("images", post.imagesLinks) })
        }
        post_detail_image_rl_0.setOnClickListener {
            navigator.openActivity(ExtraActivity::class.java,
                    Bundle().apply { putString("images", post.imagesLinks) })
        }
        ivDeletePost.setOnClickListener { deletePost() }
        ivReportPost.setOnClickListener { reportPost() }
        ivAddBookmark.setOnClickListener { createBookmark() }
        ivRemoveBookmark.setOnClickListener {
            showProgress()
            onRemoveBookmark() }

        post_detail_reply_iv.setOnClickListener {
            hideKeyboard(this)
            val replyText = post_detail_reply_et.text.toString()
            if (!replyText.isNullOrEmpty()) {
                showProgress()
                fireStoreHelper.createReply(post.pid, Reply(reply = replyText,
                        uid = cUser.uid)).addOnCompleteListener {
                    if (it.isSuccessful) {
                        post_detail_reply_et.setText("")
                        post_detail_reply_et.clearFocus()
                        adapter.clear()
                        getReplies(adapter)
                        fireStoreHelper.getPostsById(post.pid).addOnFailureListener {

                        }.addOnCompleteListener {
                            if (it.isSuccessful) {
                                var updatedPost = it.result.toObject(Post::class.java)
                                updatedPost!!.repliesCount = updatedPost.repliesCount + 1
                                updatedPost!!.pid = post.pid
                                fireStoreHelper.updatePost(post = updatedPost, pid = post.pid).addOnCompleteListener {
                                    if (it.isSuccessful) {
                                        Constants.POST = updatedPost
                                        if (post.uid == cUser.uid) {
                                            post_detail_replies_count.visible()
                                            post_detail_replies_count.text = (updatedPost.repliesCount).toString()
                                            post_detail_replies_iv.visible()
                                            hideProgress()
                                        } else {
                                            if (updatedPost.repliesCount > 1)
                                                text = "and ${updatedPost.repliesCount - 1} replied on your "
                                            else
                                                text = " replied on your "
                                            if (post.type == Post.postType.POLL)
                                                text += "poll ${post.poll!!.question}"
                                            else
                                                text += "post ${post.subject}"

                                            fireStoreHelper.createNotification(uid = post.uid, pid = post.pid+"#",
                                                    notification = Notification(type = Notification.REPLY,
                                                            suid = cUser.uid, pid = post.pid, text = text))
                                                    .addOnSuccessListener {

                                                    }
                                            fireStoreHelper.getUser(post.uid).addOnCompleteListener {
                                                if (it.isSuccessful) {
                                                    if (it.result.exists()) {
                                                        val pUser = it.result.toObject(User::class.java)
                                                        val callback = Api_Hitter.ApiInterface("http://stsmentor.com/").sendNotification(cUser!!.firstName + " " + cUser!!.lastName + " replied on your", pUser!!.token)
                                                        callback.enqueue(object : Callback<ResponseBody> {
                                                            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                                                                //showToast("got error")
                                                            }

                                                            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                                                                if (response.errorBody() != null) {
                                                                    //showToast("Notification Not sent")
                                                                } else if (response.body() != null) {
                                                                    //showToast("Notification sent")
                                                                }
                                                            }

                                                        })
                                                    }
                                                }
                                                hideProgress()
                                            }.addOnFailureListener { hideProgress() }
                                        }
                                    } else
                                        showError("")
                                }.addOnFailureListener { showError("") }
                            } else showError("")
                        }

                    } else
                        showError("")
                }.addOnFailureListener {
                    showError("")
                }
            }
        }
        post_detail_reply_ll.setOnClickListener {
            post_detail_reply_et.requestFocus()
            openKeyboard(this, post_detail_reply_et)
        }
        post_detail_reply_ll2.setOnClickListener {
            post_detail_reply_et.requestFocus()
            openKeyboard(this, post_detail_reply_et)
        }
        post_detail_thanks_ll.setOnClickListener {
            doThanks()
        }
        post_detail_thanks_ll2.setOnClickListener {
            doThanks()
        }

    }

    private fun doThanks() {
        post_detail_thanks_ll2.isEnabled = false
        post_detail_thanks_ll.isEnabled = false
        if (isThanked)
            removeThanks()
        else
            postThanks()
    }

    private fun removeThanks() {
        fireStoreHelper.removeThanks(post.pid).addOnCompleteListener {
            if (it.isSuccessful) {
                fireStoreHelper.getThanksCount(post.pid).addOnFailureListener {
                }.addOnCompleteListener {
                    if (it.isSuccessful) {
                        val count = it.result.documents.size

/*                        var updatedPost = it.result.toObject(Post::class.java)
                        updatedPost!!.thanksCount = updatedPost.thanksCount - 1
                        updatedPost!!.pid = post.pid*/
                        post.thanksCount = count
                        fireStoreHelper.updatePost(post = post, pid = post.pid).addOnCompleteListener {
                            if (it.isSuccessful) {
                                showThanksView()
                                if (count == 0) {
                                    post_detail_thanks_count.gone()
                                    post_detail_thanks_iv.gone()
                                }
                                Constants.POST = post
                                post_detail_thanks_count.setText((count).toString())
                            } else {
                                //updatedPost!!.thanksCount = updatedPost.thanksCount + 1
                                Constants.POST = post

                                post_detail_thanks_ll2.isEnabled = true
                                post_detail_thanks_ll.isEnabled = true
                            }
                        }.addOnFailureListener {
                            post_detail_thanks_ll2.isEnabled = true
                            post_detail_thanks_ll.isEnabled = true
                        }
                    } else {
                        post_detail_thanks_ll2.isEnabled = true
                        post_detail_thanks_ll.isEnabled = true
                    }
                }
            }
        }
    }

    private fun postThanks() {
        fireStoreHelper.postThanks(post.pid).addOnCompleteListener {
            if (it.isSuccessful) {
                fireStoreHelper.getThanksCount(post.pid).addOnFailureListener {
                }.addOnCompleteListener {
                    if (it.isSuccessful) {
                        val count = it.result.documents.size
/*                        var updatedPost = it.result.toObject(Post::class.java)
                        updatedPost!!.thanksCount = updatedPost.thanksCount + 1
                        updatedPost!!.pid = post.pid*/
                        post.thanksCount = count
                        fireStoreHelper.updatePost(post = post, pid = post.pid).addOnCompleteListener {
                            if (it.isSuccessful) {
                                showThankedView()
                                Constants.POST = post
                                post_detail_thanks_count.text = (count).toString()

                                    if (post.uid == cUser.uid) {
                                        fireStoreHelper.createNotification(uid = post.uid, pid = post.pid,
                                            notification = Notification(type = Notification.THANKS,
                                                    suid = cUser.uid, pid = post.pid, text = " like your post"))
                                            .addOnSuccessListener {

                                            }
                                    fireStoreHelper.getUser(post.uid).addOnCompleteListener {
                                        if (it.isSuccessful) {
                                            if (it.result.exists()) {
                                                val pUser = it.result.toObject(User::class.java)
                                                val callback = Api_Hitter.ApiInterface("http://stsmentor.com/").sendNotification(cUser!!.firstName + " " + cUser!!.lastName + " like your post", pUser!!.token)
                                                callback.enqueue(object : Callback<ResponseBody> {
                                                    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                                                        //showToast("got error")
                                                    }

                                                    override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                                                        if (response.errorBody() != null) {
                                                            //showToast("Notification Not sent")
                                                        } else if (response.body() != null) {
                                                            //showToast("Notification sent")
                                                        }
                                                    }

                                                })
                                            }
                                        }
                                        hideProgress()
                                    }.addOnFailureListener { hideProgress() }

                                }

                            } else {
                                //post.thanksCount=updatedPost.thanksCount-1
                                Constants.POST = post
                                post_detail_thanks_ll2.isEnabled = true
                                post_detail_thanks_ll.isEnabled = true
                            }
                        }.addOnFailureListener {
                            post_detail_thanks_ll2.isEnabled = true
                            post_detail_thanks_ll.isEnabled = true
                        }
                    } else {
                        post_detail_thanks_ll2.isEnabled = true
                        post_detail_thanks_ll.isEnabled = true
                    }
                }
            }
        }
    }

    private fun getReplies(adapter: PostReplyAdapter) {
        fireStoreHelper.getReplies(post.pid).addOnCompleteListener {
            if (it.isSuccessful) {
                it.result.documents.forEach {
                    var reply = it.toObject(Reply::class.java)
                    fireStoreHelper.getUser(reply!!.uid).addOnCompleteListener {
                        if (it.isSuccessful) {
                            if (it.result.exists()) {
                                val user = it.result.toObject(User::class.java)
                                adapter.addItem(reply, user!!)
                            }
                        } else {
                            showError("")
                        }
                    }
                }
            } else
                showError("")
        }.addOnFailureListener { showError("") }
    }

    private fun showError(s: String) {
        hideProgress()
        if (s.isNullOrEmpty())
            showToast("Somthing went wrong")
        else
            showToast(s)
    }

    private fun createBookmark() {
        fireStoreHelper.createBookmark(Post.Bookmark(post_id = post.pid)).addOnCompleteListener {
            if (it.isSuccessful) {
                if (ivAddBookmark != null) {
                    ivAddBookmark.gone()
                    ivRemoveBookmark.visible()
                }
                showToast("Added to Bookmark")
            } else
                showToast("error to bookmark")
        }.addOnFailureListener {
            showToast("error to bookmark")
        }

    }

    fun reportPost() {
        fireStoreHelper.createReport(uid = post.uid, pid = post.pid, post = post).addOnCompleteListener {
            if (it.isSuccessful) {
                showToast("Reported Successfully")
            } else {
                showToast("Not Reported")
            }
        }.addOnCanceledListener { showToast("Not Reported") }
                .addOnCanceledListener { showToast("Not Reported") }
    }

    fun onRemoveBookmark() {
        fireStoreHelper.removeBookmark(post.pid).addOnCompleteListener {
            if (it.isSuccessful) {
                if (ivRemoveBookmark != null) {
                    ivAddBookmark.visible()
                    ivRemoveBookmark.gone()
                }
                hideProgress()
            } else {
                hideProgress()
            }
        }.addOnFailureListener {
            hideProgress()
        }.addOnCanceledListener {
            hideProgress()
        }
    }

    fun deletePost() {
        showProgress()
        fireStoreHelper.deletePost(post.pid).addOnCompleteListener {
            if (it.isSuccessful) {
                onRemoveBookmark()
                if (post.type.equals(Post.postType.REAL_ESTATE))
                {
                    fireStoreHelper.deleteEstate(post.pid).addOnCompleteListener {
                        if (it.isSuccessful)
                        {
                            super.onBackPressed()
                            Constants.DELETED_POSITION = position
                            hideProgress()
                        }
                        else
                        {
                            showToast("error")
                            hideProgress()
                        }
                    }.addOnFailureListener {
                        hideProgress()
                        showToast(it.message!!)
                    }
                }
                else{
                    super.onBackPressed()
                    Constants.DELETED_POSITION = position
                    hideProgress()
                }
                /*fireStoreHelper.deleteNotification(post.pid).addOnCompleteListener {

                }*/

            } else {
                showToast("error")
                hideProgress()
            }
        }.addOnFailureListener {
            showToast(it.message!!)
            hideProgress()
        }
    }

}

