package com.sachtechsolution.neighbourhood.ui.login.fragments

import android.os.Bundle
import android.util.Patterns
import android.view.View
import com.example.manish.internetstatus.OttoBus
import com.facebook.AccessToken
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.constants.Constants
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.isSecure
import com.sachtechsolution.neighbourhood.model.User
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_sign_up_credential.*


class SignUpCredentialFragment : BaseFragment() {

    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }
    var invitationCode=""

    override fun viewToCreate(): Int {
        return R.layout.fragment_sign_up_credential
    }


    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
    }


    @Subscribe
    fun getMessage(internetStatus:String) {

        if (internetStatus.equals("Not connected to Internet")){
            InternetStatusDialogFragment.newInstance().show(fragmentManager, "option_fragment")
        }else {
            showToast("internet enabled ")

        }

    }

    override fun onResume() {
        super.onResume()
        OttoBus.bus.register(this)
    }

    private lateinit var mAuth: FirebaseAuth

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (arguments!==null)
            invitationCode=arguments!!.getString("invitationCode")

        mAuth = FirebaseAuth.getInstance()

        signup_credential_continue_tv.setOnClickListener {
            val email = signup_credential_email_tiet.text.toString()
            val password = signup_credential_pasword_tiet.text.toString()
            if (!Patterns.EMAIL_ADDRESS.matcher(email).matches())
                showToast("Enter valid Email")
            else if (!password.isSecure())
                showToast("Password must be grater than 6 digit")
            else
                signUp(email, password)
        }

        signup_credential_back_iv.setOnClickListener {
            fragmentManager?.popBackStackImmediate()
        }

    }

    private fun signUp(email: String, password: String) {
        showProgress()
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(activity!!) { task ->
                    if (task.isSuccessful) {
                        if (invitationCode.isNotEmpty())
                        {
                            fireStoreHelper.isInvitationCodeDelete(invitationCode).addOnCompleteListener {
                                /*if (it.isSuccessful)
                                    showToast("invi code delete")
                                else
                                    showToast(" ini code not delete")*/
                            }
                        }
                        val uid =fireStoreHelper.currentUserUid()
                        Constants.email=email
                        val user = User(uid = uid,zipCode = Constants.ZIP_CODE,street = Constants.STREET,
                                lat = Constants.LAT, lng = Constants.LONG, email = email,
                                mainLocality = Constants.MAIN_LOCALITY,society= Constants.SOCIETY,
                                address = Constants.ADDRESS)

                        fireStoreHelper.setUser(uid, user).addOnCompleteListener {
                            if (it.isSuccessful) {
                                hideProgress()
                                baseAcitivityListener.navigator.replaceFragment(SignUpInfoFragment::class.java, true)
                               // setUserToDataBase("$firstName $lastName", Constants.SUB_LOCALITY)
                                //getUser()
                            } else {
                                showToast("error")
                                hideProgress()
                            }
                        }.addOnFailureListener {
                            showToast("fail")
                            hideProgress()
                        }
                    } else {
                        showToast(task.exception!!.message)
                        hideProgress()
                    }
                    // ...
                }.addOnFailureListener {  }

    }
}
