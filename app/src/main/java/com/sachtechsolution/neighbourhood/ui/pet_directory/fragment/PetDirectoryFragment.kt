package com.sachtechsolution.neighbourhood.ui.pet_directory.fragment

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.constants.PetDirectory
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.gone
import com.sachtechsolution.neighbourhood.extension.openActivity
import com.sachtechsolution.neighbourhood.extension.visible
import com.sachtechsolution.neighbourhood.model.User
import com.sachtechsolution.neighbourhood.ui.mail.data.SendInviteCodeFragment
import com.sachtechsolution.neighbourhood.ui.pet_directory.adapter.PetAdapter
import kotlinx.android.synthetic.main.fragment_pet_directory.*


class PetDirectoryFragment : BaseFragment(), PetAdapter.ItemClickListener {

    val petAdapter by lazy { PetAdapter(this.activity!!, this) }
    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }
    val user by lazy { pref.getUserFromPref() }
    var petList = ArrayList<PetDirectory>()

    override fun clickListner(pet: PetDirectory) {
        etSearch.setText("")
        var bundle = Bundle()
        bundle.putParcelable(PetDetailFragment.PET, pet)
        baseAcitivityListener.navigator.replaceFragment(PetDetailFragment::class.java, true, bundle)
    }


    override fun viewToCreate(): Int {
        return R.layout.fragment_pet_directory
    }


    private fun filterList(text: String, list: ArrayList<PetDirectory>): List<PetDirectory> {
        return list.filter { it?.petName.contains(text, true) }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView_pet.adapter = petAdapter

        fab.setOnClickListener {
            context!!.openActivity<AddPetActivity>()
        }
        petMsg.visible()
        invite.setOnClickListener { baseAcitivityListener.navigator.replaceFragment(SendInviteCodeFragment::class.java, true) }
        etSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {
                val filterlist = filterList(etSearch.text.toString(), petList)

                if (filterlist.isEmpty()) {
                    petAdapter.clearAll()
                    petAdapter.updateAll(petList)
                    petAdapter.notifyDataSetChanged()
                } else {
                    petAdapter.clearAll()
                    petAdapter.updateAll(filterlist as ArrayList<PetDirectory>)
                    petAdapter.notifyDataSetChanged()
                }

            }
        })
    }


    override fun onResume() {
        super.onResume()
        petAdapter.petlist.clear()
        getAllPet()
    }


    private fun getAllPet() {
        fireStoreHelper.getNeighborsByStreet(user!!.street).addOnCompleteListener {
            if (it.isSuccessful) {
                if (it.result.size() > 0) {
                    it.result.forEach {
                        val user = it.toObject(User::class.java)
                        fireStoreHelper.getPetDirectory(user!!.uid).addOnCompleteListener {
                            if (it.isSuccessful) {
                                petMsg.gone()
                                it.result.forEach {
                                    val pet = it.toObject(PetDirectory::class.java)
                                    petAdapter.update(pet)
                                    petList.add(pet)
                                    petAdapter.notifyDataSetChanged()
                                }
                            }
                        }
                    }
                }
            }
        }
    }

}