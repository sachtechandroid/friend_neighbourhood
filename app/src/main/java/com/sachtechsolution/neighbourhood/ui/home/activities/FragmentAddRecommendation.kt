package com.sachtechsolution.neighbourhood.ui.home.activities

import android.os.Bundle
import android.view.View
import com.bumptech.glide.Glide
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.model.places_bussiness.ResultsItem
import kotlinx.android.synthetic.main.fragment_addrecommendation.*

class FragmentAddRecommendation:BaseFragment() {

    override fun viewToCreate(): Int {
        return R.layout.fragment_addrecommendation
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        getRecommendation()
    }

    private fun getRecommendation() {
       val data= arguments!!.getParcelable<ResultsItem>(DATA)

        if (data!==null) {
            txtName.text = data.name
            etAddress.text=data.vicinity
            txtRating.text=data.rating?.toString()
            Glide.with(context!!).load(data.icon).into(icon)
        }
    }
    companion object {
        val DATA="data"

    }
}