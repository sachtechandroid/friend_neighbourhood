package com.sachtechsolution.neighbourhood.ui.sale.activity

import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.app.FragmentManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.RadioButton
import android.widget.RadioGroup
import com.example.manish.internetstatus.OttoBus
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.showToast
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.view_sale_showing.*


open class FragmentSaleShowingDialog : DialogFragment(), View.OnClickListener {

        val fireStoreHelper by lazy { FireStoreHelper.getInstance() }

        override fun onClick(p0: View?) {
            when (p0!!.id) {
                R.id.ivClose ->
                    dismiss()
                R.id.txt_confirm -> {
                    addListenerOnButton()
                }

            }
        }

        fun addListenerOnButton() {

            var radioGroup = dialog.findViewById(R.id.radioGroup) as RadioGroup
            // get selected radio button from radioGroup
            val selectedId = radioGroup.checkedRadioButtonId
            // find the radiobutton by returned id
            var radioButton = dialog.findViewById(selectedId) as RadioButton
            dismiss()
            listner!!.onShowing(radioButton.text.toString())

        }

        override fun onPause() {
            super.onPause()
            OttoBus.bus.unregister(this)
        }


        @Subscribe
        fun getMessage(internetStatus: String) {

            if (internetStatus.equals("Not connected to Internet")) {
                InternetStatusDialogFragment.newInstance().show(fragmentManager, "option_fragment")
            } else {
                context!!.showToast("internet enabled ")

            }

        }

        override fun onResume() {
            super.onResume()
            OttoBus.bus.register(this)
        }


        override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
            dialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
            dialog.setCancelable(false)
            dialog.setCanceledOnTouchOutside(false)
            return inflater.inflate(R.layout.view_sale_showing, container, false)
        }

        override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
            super.onViewCreated(view, savedInstanceState)

            //listner
            ivClose.setOnClickListener(this)
            txt_confirm.setOnClickListener(this)

        }

        companion object {
            var listner: Showing? = null
            var fragmentManager: FragmentManager? = null
            fun newInstance(listner: Showing): FragmentSaleShowingDialog {
                this.listner = listner
                return FragmentSaleShowingDialog()
            }
        }

        interface Showing {
            fun onShowing(text: String)

        }
    }