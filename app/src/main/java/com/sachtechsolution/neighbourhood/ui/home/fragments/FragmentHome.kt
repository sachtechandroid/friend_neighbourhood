package com.sachtechsolution.neighbourhood.ui.home.fragments

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.view.MenuItem
import android.view.View
import com.example.manish.internetstatus.OttoBus
import com.google.common.eventbus.Subscribe
import com.google.firebase.firestore.DocumentSnapshot
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.constants.Constants
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.gone
import com.sachtechsolution.neighbourhood.extension.isPostHasTime
import com.sachtechsolution.neighbourhood.extension.openActivity
import com.sachtechsolution.neighbourhood.extension.showToast
import com.sachtechsolution.neighbourhood.model.Post
import com.sachtechsolution.neighbourhood.model.User
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.sachtechsolution.neighbourhood.ui.extra.fragments.SearchFragment
import com.sachtechsolution.neighbourhood.ui.home.adapters.HomePagePostAdapter
import com.sachtechsolution.neighbourhood.ui.home.fab.activity.FabBaseActivity
import com.sachtechsolution.neighbourhood.ui.mail.data.SendInviteCodeFragment
import kotlinx.android.synthetic.main.fragment_homepage.*


class FragmentHome : BaseFragment(), HomePagePostAdapter.OpenBottomSheet {

    var postId = ""
    var postPosition = 0
    var adapter: HomePagePostAdapter? = null
    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }

    //lateinit var post: Post
    //val mainLocality by lazy { pref.getUserFromPref()?.mainLocality?: "" }
    val user by lazy { pref.getUserFromPref()!! }
    lateinit var postList: MutableList<DocumentSnapshot>
    var postCount = -1

    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
    }


    @Subscribe
    fun getMessage(internetStatus: String) {

        if (internetStatus.equals("Not connected to Internet")) {
            InternetStatusDialogFragment.newInstance().show(fragmentManager, "option_fragment")
        } else {
            getAllData()
            showToast("internet enabled")

        }

    }

    override fun OnOpenBottomSheet(pid: String, position: Int, type: String, post: Post, uid: String) {
        postId = pid
        postPosition = position
        if (type.equals(DELETE_POST)) {
            deletePost()
        }
        if (type.equals(ADD_BOOKMARK)) {
            createBookmark()
        }
        if (type.equals(REMOVE_BOOKMARK)) {
            onRemoveBookmark(postId)
        }
        if (type.equals(REPORT_POST)) {
            reportPost(uid = uid, pid = pid, post = post)
        }

    }

    override fun viewToCreate(): Int {
        return R.layout.fragment_homepage
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter = HomePagePostAdapter(context!!, this, baseAcitivityListener, pref)
        recyclerViewFragmentHome.adapter = adapter
        Constants.IS_THANKED_LIST.clear()
        Constants.IS_POLLED_LIST.clear()
        postList=ArrayList()
        etSearch!!.setOnClickListener { baseAcitivityListener.navigator.replaceFragment(SearchFragment::class.java, true) }

        getAllData()

        fab.addOnMenuItemClickListener { fab, label, itemId ->
            when (itemId) {
                R.id.menuPrivateMessage -> //context!!.openActivity<ActivitySelectFriend>()
                    activity!!.openActivity<FabBaseActivity>(FabBaseActivity.FRAGMENT_KEY, FabBaseActivity.PRIVATE_MESSAGE)
                R.id.menuAlert -> activity!!.openActivity<FabBaseActivity>(FabBaseActivity.FRAGMENT_KEY, FabBaseActivity.URGENT_ALERT)
                R.id.menuEvent -> activity!!.openActivity<FabBaseActivity>(FabBaseActivity.FRAGMENT_KEY, FabBaseActivity.EVENT)
                R.id.menuPoll -> activity!!.openActivity<FabBaseActivity>(FabBaseActivity.FRAGMENT_KEY, FabBaseActivity.POLL)
                R.id.menuMessage -> activity!!.openActivity<FabBaseActivity>(FabBaseActivity.FRAGMENT_KEY, FabBaseActivity.MESSAGE)
            }
        }

        home_invite.setOnClickListener {
            shareApp(context!!)

        }
        swipe_refresh_layout.setColorSchemeResources(R.color.colorAccent, R.color.colorPrimary, R.color.colorGray)
        swipe_refresh_layout.setOnRefreshListener {
            //handling swipe refresh
            Handler().postDelayed({
                postCount=-1
                adapter!!.clear()
                getAllData()
            }, 2000)
        }

    }

    private fun getAllData() {

        fireStoreHelper.getAllPosts(zipCode = user.zipCode, society = user.society).addOnCompleteListener {
            if (it.isSuccessful) {
                if (swipe_refresh_layout!=null)
                  swipe_refresh_layout.isRefreshing = false
                postList?.clear()
                postList = it.result.documents
                postCount=-1
                getPost()
            }
        }.addOnFailureListener {
            showToast(it.message!!)
        }

    }


    private fun getPost() {
        postCount++
        if (postList.size > postCount) {
            var post = postList[postCount].toObject(Post::class.java)
            post!!.pid = postList[postCount].id
            if (isPostHasTime(post.date) && home_welcome_text != null)
                getPostUser(post)
        }
    }



    private fun getPostUser(post: Post) {
        if (post.uid.isNullOrEmpty()) {
            adapter!!.addItem(post, User(), false, false)
            getPost()
        } else {
            fireStoreHelper.getUser(post!!.uid).addOnCompleteListener {
                if (it.isSuccessful) {
                    if (it.result.exists()) {
                        val user = it.result.toObject(User::class.java)
                        fireStoreHelper.getThanks(post.pid).addOnCompleteListener {
                            if (it.isSuccessful) {
                                var isThanked = false
                                if (it.result.exists())
                                    isThanked = true
                                if (post.type.equals(Post.postType.POLL)) {
                                    fireStoreHelper.getIsPollAnswered(post.pid).addOnCompleteListener {
                                        if (it.isSuccessful) {
                                            var isPolled = false
                                            if (it.result.documents.size > 0)
                                                isPolled = true
                                           adapter!!.addItem(post, user!!, isThanked, isPolled)
                                            getPost()

                                        } else {
                                            adapter!!.addItem(post, user!!, isThanked, false)
                                            getPost()

                                        }
                                    }.addOnFailureListener {
                                        getPost()
                                        showToast("")
                                    }.addOnCanceledListener {
                                        showToast("")
                                        getPost()
                                    }
                                } else {
                                    getPost()
                                    adapter!!.addItem(post, user!!, isThanked, false)
                                }
                                home_welcome_text?.gone()
                            } else {
                                getPost()
                            }
                        }
                    }
                }
            }.addOnFailureListener { getPost() }
        }
    }

    override fun onResume() {
        super.onResume()
        OttoBus.bus.register(this)
        //getAllPost()
        if (Constants.POSITION != -1) {
            if (Constants.POST != null)
                adapter?.postsList?.set(Constants.POSITION, Constants.POST)
            adapter!!.notifyItemChanged(Constants.POSITION)
            Constants.POSITION = -1
            Constants.POST = null
        }
        if (Constants.DELETED_POSITION != -1) {
            val position = Constants.DELETED_POSITION
            adapter!!.deletePost(position)
            Constants.DELETED_POSITION = -1
        }
        intracterListner!!.updateSelectedPos(0)
    }

    fun deletePost() {
        showProgress()
        fireStoreHelper.deletePost(postId).addOnCompleteListener {
            if (it.isSuccessful) {
                onRemoveBookmark(postId)
            } else {
                //  showToast("error")
                hideProgress()
            }

        }.addOnFailureListener {
            //  showToast("failed")
            hideProgress()
        }
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menuPrivateMessage -> {

                context!!.showToast("clicked")
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    fun shareApp(context: Context) {
        baseAcitivityListener.navigator.replaceFragment(SendInviteCodeFragment::class.java, true)
    }


    private fun createBookmark() {

        fireStoreHelper.createBookmark(Post.Bookmark(postId)).addOnCompleteListener {
            if (it.isSuccessful) {
                activity!!.showToast("Added to Bookmark")
            }
        }.addOnFailureListener {
            //activity!!.showToast("error to add bookmark")
        }

    }


    fun onRemoveBookmark(pid: String) {
        fireStoreHelper.removeBookmark(pid).addOnCompleteListener {
            if (it.isSuccessful) {
                showToast("Post deleted")
                adapter!!.deletePost(postPosition)
                adapter!!.notifyDataSetChanged()
                hideProgress()
            } else {
                hideProgress()
                context!!.showToast("unable to remove bookmark")
            }
        }.addOnFailureListener {
            hideProgress()
            //context!!.showToast("unable to remove bookmark")
        }.addOnCanceledListener {
            hideProgress()
            // context!!.showToast("unable to remove bookmark")
        }
    }

    companion object {
        val DELETE_POST = "deletePost"
        val ADD_BOOKMARK = "add_bookmark"
        val REMOVE_BOOKMARK = "remove_bookmark"
        val REPORT_POST = "report"
    }

    fun reportPost(uid: String, pid: String, post: Post) {
        fireStoreHelper.createReport(uid = uid, pid = pid, post = post).addOnCompleteListener {
            if (it.isSuccessful) {
                showToast("Reported Successfully")
            } else {
                showToast("Not Reported")
            }
        }.addOnCanceledListener { showToast("Not Reported") }
                .addOnCanceledListener { showToast("Not Reported") }
    }

}