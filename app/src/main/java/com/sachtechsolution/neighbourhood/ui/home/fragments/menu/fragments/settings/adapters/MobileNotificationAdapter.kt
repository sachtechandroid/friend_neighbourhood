package com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sachtechsolution.neighbourhood.R
import gurtek.mrgurtekbasejava.base.ModelPrefrence
import kotlinx.android.synthetic.main.item_mobile_notification.view.*

class MobileNotificationAdapter(val pref: ModelPrefrence,val listner:AlterSettings) :RecyclerView.Adapter<MobileNotificationAdapter.ViewHolder>() {
    val  options= arrayListOf("All Post",
            "Replies to my Posts","Vote on Post"
            ,"Message")
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val v:View=LayoutInflater.from(p0.context).inflate(R.layout.item_mobile_notification,p0,false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return options.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.options.text= options[position]
        holder.itemView.translater.isChecked=pref.getSettings(options[position])
        holder.itemView.translater.setOnCheckedChangeListener { p0, p1 ->
                listner.onAlter(options[position],p1)
        }

    }

    interface AlterSettings
    {
        fun onAlter(s: String, p1: Boolean)
    }
    class ViewHolder(v:View):RecyclerView.ViewHolder(v) {

    }
}