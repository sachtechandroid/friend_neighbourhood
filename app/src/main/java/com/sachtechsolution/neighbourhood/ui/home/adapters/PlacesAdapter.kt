package com.sachtechsolution.neighbourhood.ui.home.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import com.bumptech.glide.Glide
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.constants.BeanArray
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.gone
import com.sachtechsolution.neighbourhood.extension.invisible
import com.sachtechsolution.neighbourhood.extension.visible
import com.sachtechsolution.neighbourhood.model.places_bussiness.ResultsItem
import kotlinx.android.synthetic.main.item_places.view.*

class PlacesAdapter(val context: Context, val listner: onClickChoice,val progressBar: ProgressBar):RecyclerView.Adapter<PlacesAdapter.ViewHolder>() {

    var bean=ArrayList<BeanArray>()
    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.item_places,p0, false)
        var viewHolder = PlacesAdapter.ViewHolder(v)
        return viewHolder

    }

    override fun getItemCount(): Int {
        return bean.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        var data= bean[position]

        holder.itemView.place_name.text=data.name
        Glide.with(context).load(data.image).into(holder.itemView.places_icon)

        if (data.list.size>0&&data.list.size<2) {

            holder.itemView.ratingOneName.text=data.list[0]!!.name
            holder.itemView.ratingOne.text=data.list[0].rating.toString()
            holder.itemView.openOne.text=data.list[0].vicinity
            holder.itemView.place_one.visible()
            holder.itemView.place_two.invisible()
            holder.itemView.place_three.invisible()
            holder.itemView.see_more.invisible()
            progressBar.gone()

        } else if (data.list.size>0&&data.list.size<3  ){
            holder.itemView.ratingOneName.text=data.list[0]!!.name
            holder.itemView.ratingOne.text=data.list[0].rating.toString()
            holder.itemView.ratingTwoName.text=data.list[1]!!.name
            holder.itemView.ratingTwo.text=data.list[1].rating.toString()
            holder.itemView.openOne.text=data.list[0].vicinity
            holder.itemView.openTwo.text=data.list[1].vicinity
            holder.itemView.place_one.visible()
            holder.itemView.place_two.visible()
            holder.itemView.place_three.invisible()
            holder.itemView.see_more.invisible()
            progressBar.gone()
        }

        else if(data.list.size>0&&data.list.size>3) {
            holder.itemView.ratingOneName.text=data.list[0]!!.name
            holder.itemView.ratingOne.text=data.list[0].rating.toString()
            holder.itemView.ratingTwoName.text=data.list[1]!!.name
            holder.itemView.ratingTwo.text=data.list[1].rating.toString()
            holder.itemView.ratingThreeName.text=data.list[2]!!.name
            holder.itemView.ratingThree.text=data.list[2].rating.toString()
            holder.itemView.openOne.text=data.list[0].vicinity
            holder.itemView.openTwo.text=data.list[1].vicinity
            holder.itemView.openThree.text=data.list[2].vicinity
            holder.itemView.place_one.visible()
            holder.itemView.place_two.visible()
            holder.itemView.place_three.visible()
            holder.itemView.see_more.visible()
            progressBar.gone()
        }




        holder.itemView.see_more.setOnClickListener {
            listner.seeMore(data.name,data.image,data.type)
        }

        holder.itemView.place_one.setOnClickListener {

            listner.onChoiceSelected(data.name,data.image,data.type, data.list[0])
        }
        holder.itemView.place_two.setOnClickListener {
            listner.onChoiceSelected(data.name,data.image,data.type,data.list[0])
        }
        holder.itemView.place_three.setOnClickListener {
           listner.onChoiceSelected(data.name,data.image,data.type,data.list[0])
        }
    }

    fun add(response: BeanArray) {
        bean.add(response)
        notifyDataSetChanged()

    }

    class ViewHolder(itemView:View):RecyclerView.ViewHolder(itemView) {

    }
    interface onClickChoice
    {
        fun onChoiceSelected(name: String, image: Int, choice: String, item: ResultsItem)
        fun seeMore(name: String, image: Int, choice: String)
    }

}