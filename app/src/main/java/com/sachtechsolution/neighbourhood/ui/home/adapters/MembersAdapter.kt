package com.sachtechsolution.neighbourhood.ui.home.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sachtechsolution.neighbourhood.R

class MembersAdapter(context: Context):RecyclerView.Adapter<MembersAdapter.ViewHolder>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {

        val v =LayoutInflater.from(p0.context).inflate(R.layout.item_members,p0,false)
        var viewHolder=ViewHolder(v)
        return viewHolder

    }

    override fun getItemCount(): Int {
        return 4
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {

    }

    class ViewHolder(itemView:View):RecyclerView.ViewHolder(itemView) {

    }
}