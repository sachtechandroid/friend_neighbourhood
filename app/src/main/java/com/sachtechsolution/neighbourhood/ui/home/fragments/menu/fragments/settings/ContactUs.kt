package com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings

import android.os.Bundle
import android.view.View
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_contacts.*

class ContactUs:BaseFragment(), View.OnClickListener {


    override fun viewToCreate(): Int {
        return R.layout.fragment_contacts
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //listner
        btnBack.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        when(p0!!.id)
        {
            R.id.btnBack->activity!!.onBackPressed()
        }
    }
}