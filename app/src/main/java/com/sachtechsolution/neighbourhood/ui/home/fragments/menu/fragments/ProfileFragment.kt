package com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Color
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.v4.content.ContextCompat
import android.support.v4.content.FileProvider
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import com.bumptech.glide.Glide
import com.example.manish.internetstatus.OttoBus
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.StorageReference
import com.sachtechsolution.neighbourhood.BuildConfig
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.basepackage.base.ProgressDialogFragment
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.*
import com.sachtechsolution.neighbourhood.model.*
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.sachtechsolution.neighbourhood.ui.home.fab.fragment.message.dialogs.SelectOptionDialogFragment
import com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings.EditProfileFragment
import com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings.adapters.AllFamilyAdapter
import com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings.profile.*
import com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings.profile.fragment.EmergencyContactDialogFragment
import com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings.profile.fragment.OwnPostsFragment
import com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings.profile.fragment.ShowOwnSalePostsFragment
import com.squareup.otto.Subscribe
import fisk.chipcloud.ChipCloud
import fisk.chipcloud.ChipCloudConfig
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.view_profile_add_information.*
import kotlinx.android.synthetic.main.view_profile_add_map.*
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap


class ProfileFragment : BaseFragment(), OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, View.OnClickListener, GoogleMap.InfoWindowAdapter, AllFamilyAdapter.AddContact, SelectOptionDialogFragment.onOptionSelect {

    private var TAKE_PICTURE = 12
    private var GALLERY = 13
    var user: User? = null
    var photoURI: Uri? = null
    private lateinit var mMap: GoogleMap
    var familyList: ArrayList<AllFamily>? = null
    private lateinit var mGoogleClient: GoogleApiClient
    lateinit var mLocation: Location
    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }
    private var myMap: SupportMapFragment? = null
    private var markerView: View? = null



    override fun forGallery() {
        var intent = Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(intent, GALLERY)
    }


    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
    }


    @Subscribe
    fun getMessage(internetStatus: String) {

        if (internetStatus.equals("Not connected to Internet")) {
            InternetStatusDialogFragment.newInstance().show(fragmentManager, "option_fragment")
        } else {
            showToast("internet enabled ")

        }

    }


    override fun forCamera() {
        try {
            dispatchTakePictureIntent();
        } catch (e: IOException) {
        }
    }

    @Throws(IOException::class)
    private fun dispatchTakePictureIntent() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(context!!.packageManager) != null) {
            // Create the File where the photo should go
            var photoFile: File? = null
            try {
                photoFile = createImageFile()
            } catch (ex: IOException) {
                // Error occurred while creating the File
                return
            }

            // Continue only if the File was successfully created
            if (photoFile != null) {

                photoURI = FileProvider.getUriForFile(context!!,
                        BuildConfig.APPLICATION_ID + ".provider",
                        createImageFile())
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                startActivityForResult(takePictureIntent, TAKE_PICTURE)
            }
        }
    }

    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "JPEG_" + timeStamp + "_"
        val storageDir = File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM), "Camera")
        val image = File.createTempFile(
                imageFileName, /* prefix */
                ".jpg", /* suffix */
                storageDir      /* directory */
        )
        return image
    }



    @SuppressLint("MissingPermission")
    override fun onMapReady(p0: GoogleMap?) {
        mMap = p0!!
        mMap.uiSettings.isZoomControlsEnabled = true
        mMap.isMyLocationEnabled = true

    }

    fun getuser() {
        fireStoreHelper.getUser(FirebaseAuth.getInstance().uid).addOnCompleteListener {
            if (it.isSuccessful) {
                if (it.result.exists()) {
                    val user = it.result.toObject(User::class.java)
                    // val arrOfStr = user!!.latlng.split(",")
                    var latlang = LatLng(user!!.lat, user!!.lng)
                    upDateMarker(latlang)

                    fireStoreHelper.getPersonalProfileRef(user!!.uid).get().addOnCompleteListener {
                        if (it.isSuccessful) {

                            profileSeekBar?.max = 100
                            val size = it.result.documents.size
                            if (size in 6..7)
                                profileSeekBar.progress = 90
                            else if (size in 4..6)
                                profileSeekBar.progress = 70
                            else if (size in 2..4)
                                profileSeekBar.progress = 50
                            else if (size in 1..2)
                                profileSeekBar.progress = 30
                        }
                    }.addOnFailureListener {
                        showToast(it.message)
                    }
                }
            }
        }.addOnFailureListener {
            showToast(it.message!!)
        }
    }

    @SuppressLint("MissingPermission")
    override fun onConnected(p0: Bundle?) {
        if (checkLoc(activity!!))
            mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleClient)
        getuser()
    }

    override fun onConnectionSuspended(p0: Int) {
    }


    override fun viewToCreate(): Int {
        return R.layout.fragment_profile
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //set profile data

        user = pref.getUserFromPref()
        name.text = user!!.firstName + " " + user!!.lastName
        //etAddress.text = user!!.subLocality
        etAddress.text = user!!.society
        if (user!!.imageLink.isNotEmpty())
            Glide.with(context!!).load(user!!.imageLink).into(bigDp)

        familyList = ArrayList()

        //get all profiles
        getInterest()
        getSkills()
        getLoveForNei()
        if (checkLoc(activity!!) && activity!!.networkAvaileble()) {
            myMap = childFragmentManager.findFragmentById(R.id.map_fragment) as SupportMapFragment
            myMap?.getMapAsync(this)
            mGoogleClient = GoogleApiClient.Builder(activity!!)
                    .addConnectionCallbacks(this)
                    .addApi(LocationServices.API)
                    .build()
            mGoogleClient.connect()
            markerView = LayoutInflater.from(activity).inflate(R.layout.marker_layout, null)


        } else {
            showToast("Your location is disabled")
        }


        //listner
        profile_back_iv.setOnClickListener(this)
        txtBiography.setOnClickListener(this)
        txtInterests.setOnClickListener(this)
        txtSkills.setOnClickListener(this)
        txtAboutNhood.setOnClickListener(this)
        ivCamera.setOnClickListener(this)
        familyLayout.setOnClickListener(this)
        addContactInformation.setOnClickListener(this)
        txtEmergencyContact.setOnClickListener(this)
        allPostsLayout.setOnClickListener(this)
        ownSalePostLayout.setOnClickListener(this)
        profile_settings_iv.setOnClickListener(this)
        txtAddContacts.setOnClickListener(this)

    }


    override fun getInfoWindow(marker: Marker): View? {

        return prepareInfoView(marker)
    }

    override fun getInfoContents(marker: Marker): View {
        //return null;
        return prepareInfoView(marker)
    }

    @SuppressLint("SetTextI18n")
    private fun prepareInfoView(marker: Marker): View {
        //prepare InfoView programmatically
        return layoutInflater.inflate(R.layout.map_marker, null)
    }


    //click listner
    override fun onClick(p0: View?) {

        when (p0!!.id) {
            R.id.profile_back_iv -> activity!!.onBackPressed()
            R.id.txtBiography -> activity!!.openActivity<FillUpProfileActivity>()
            R.id.txtInterests -> openActivity<IntersetsActivity>(2)
            R.id.txtSkills -> openActivity<SkillsActivity>(3)
            R.id.txtAboutNhood -> openActivity<AboutNhoodActivity>(4)
            R.id.ivCamera -> openOptionsForImage()
            R.id.familyLayout -> activity!!.openActivity<AddFamilyBaseActivity>()
            R.id.addContactInformation -> baseAcitivityListener.navigator.replaceFragment(ContactnformationFragment::class.java, true)
            R.id.txtAddContacts -> baseAcitivityListener.navigator.replaceFragment(ContactnformationFragment::class.java, true)
            R.id.txtEmergencyContact -> EmergencyContactDialogFragment.newInstance(pref).show(fragmentManager, "option_fragment")
            R.id.allPostsLayout -> {
                baseAcitivityListener.navigator.replaceFragment(OwnPostsFragment::class.java, true)
            }
            R.id.ownSalePostLayout -> baseAcitivityListener.navigator.replaceFragment(ShowOwnSalePostsFragment::class.java, true)
            R.id.profile_settings_iv -> baseAcitivityListener.navigator.replaceFragment(EditProfileFragment::class.java, true)

        }
    }

    private fun upDateMarker(pafKietLocation: LatLng) {
        mMap.addMarker(MarkerOptions().position(pafKietLocation)
                .title("Your Location"))

        mMap.setMinZoomPreference(12.0f)
        mMap.setMaxZoomPreference(72.0f)
        mMap.moveCamera(CameraUpdateFactory.newLatLng(pafKietLocation))
        hideProgress()
    }

    private inline fun <reified I> openActivity(requestCode: Int) {
        var intent = Intent(activity, I::class.java)
        startActivityForResult(intent, requestCode)
    }

    override fun onResume() {
        super.onResume()
        OttoBus.bus.register(this)

        if (context!!.networkAvaileble()) {
            getChild()
            getContactInformation()
            getBiography()
            getUser()
        } else
            InternetStatusDialogFragment.newInstance().show(fragmentManager, "option_fragment")

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        // check if the request code is same as what is passed  here it is 2
        if (requestCode == TAKE_PICTURE) {
            uploadImageToStorage(photoURI!!)
        }
        if (data != null) {
            if (requestCode == 2) {

                var list = data!!.getCharSequenceArrayListExtra("recommendation")
                setChipInterest(list as List<String>)
            } else if (requestCode == 3) {

                var list = data!!.getCharSequenceArrayListExtra("recommendation")
                setChipLayoutForSkills(list as List<String>)

            } else if (requestCode == 4) {

                var list = data!!.getCharSequenceArrayListExtra("recommendation")
                setChipLayoutForAboutNhood(list as List<String>)
            } else if (requestCode == GALLERY)
                uploadImageToStorage(image = data!!.data)
        }


    }

    private fun uploadImageToFireStore(image: String) {
        fireStoreHelper.updateProfilePic(image).addOnCompleteListener {
            if (it.isSuccessful) {
                showToast("Updated")
                Glide.with(context!!).load(image).into(bigDp)
                getUser()
                UploadToRealTimeDatabase(image)
            } else {
                //showToast("error")
                ProgressDialogFragment.hideProgress()
            }
        }.addOnFailureListener {
            showToast(it.message!!)
            ProgressDialogFragment.hideProgress()
        }
    }

    private fun UploadToRealTimeDatabase(image: String) {

        var map = HashMap<String, String>()
        map.put("photo", image)
        var database = FirebaseDatabase.getInstance().reference.child("users")
        database.child(fireStoreHelper.currentUserUid()).updateChildren(map as Map<String, Any>)
        hideProgress()
    }

    private fun setChipLayoutForAboutNhood(list: List<String>) {

        cvAboutNhood.visible()
        val config = ChipCloudConfig()
                .selectMode(ChipCloud.SelectMode.multi)
                .checkedChipColor(ContextCompat.getColor(activity!!,R.color.optional))
                .checkedTextColor(Color.parseColor("#ffffff"))
                .uncheckedChipColor(ContextCompat.getColor(activity!!,R.color.optional))
                .uncheckedTextColor(Color.parseColor("#ffffff"))

        val chipCloud = ChipCloud(activity, cvAboutNhood, config)
        chipCloud.addChips(list)

        chipCloud.setListener { index, checked, userClick ->
            if (userClick) {
                list!!.drop(index)
            }
        }

    }

    private fun setChipLayoutForSkills(list: List<String>) {
        cvSkills.visible()
        val config = ChipCloudConfig()
                .selectMode(ChipCloud.SelectMode.multi)
                .checkedChipColor(ContextCompat.getColor(activity!!,R.color.optional))
                .checkedTextColor(Color.parseColor("#ffffff"))
                .uncheckedChipColor(ContextCompat.getColor(activity!!,R.color.optional))
                .uncheckedTextColor(Color.parseColor("#ffffff"))


        val chipCloud = ChipCloud(activity, cvSkills, config)
        chipCloud.addChips(list)

        chipCloud.setDeleteListener { index, label ->
            showToast("$label removed from profile")
        }

    }

    private fun setChipInterest(list: List<String>) {
        cvInterests.visible()
        val config = ChipCloudConfig()
                .selectMode(ChipCloud.SelectMode.multi)
                .checkedChipColor(ContextCompat.getColor(activity!!,R.color.optional))
                .checkedTextColor(Color.parseColor("#ffffff"))
                .uncheckedChipColor(ContextCompat.getColor(activity!!,R.color.optional))
                .uncheckedTextColor(Color.parseColor("#ffffff"))

        val chipCloud = ChipCloud(activity, cvInterests, config)
        chipCloud.addChips(list)

    }



    fun openOptionsForImage() {
        SelectOptionDialogFragment.newInstance(this)
                .show(fragmentManager, "option_fragment")
    }


    fun getBiography() {
        fireStoreHelper.getBiography(user!!.uid).addOnCompleteListener {
            if (it.isSuccessful && biographyLayout !== null) {
                val biography = it.result.toObject(Biography::class.java)
                if (biography != null && biography.aboutYou.isNotEmpty()) {
                    biographyEmptyLayout.gone()
                    biographyLayout.visible()
                    txtResident.text = "Resident since ${biography.movedYear}"
                    txtGrow.text = "Grow Up ${biography.growUp}"

                    if (biography!!.aboutYou.equals("I'am working"))
                        txtJOb.text = "${biography.aboutDo!!.jobTitle} at ${biography.aboutDo!!.company}(${biography.aboutDo!!.city})"
                    else
                        txtJOb.text = biography.aboutYou

                    txtBackground.text = biography.background
                } else {
                    biographyLayout.gone()
                    biographyEmptyLayout.visible()
                }
            }
        }.addOnFailureListener {
            hideProgress()
            showToast(it.message!!)
        }
    }



    fun getInterest() {
        fireStoreHelper.getInterest(user!!.uid).addOnCompleteListener {
            if (it.isSuccessful && cvInterests != null) {
                val interest = it.result.toObject(Biography.Interest::class.java)
                if (interest != null && interest.interest.isNotEmpty()) {
                    cvInterests.visible()
                    val separated = interest.interest.split(",")
                    setChipInterest(separated)
                }
            } else {
                // showToast("something went wrong")
            }
        }
    }




    fun getSkills() {
        fireStoreHelper.getSkills(user!!.uid).addOnCompleteListener {
            if (it.isSuccessful && cvSkills != null) {
                val skills = it.result.toObject(Biography.Skills::class.java)
                if (skills != null && skills.skills.isNotEmpty()) {
                    cvSkills?.visible()
                    val separated = skills.skills.split(",")
                    setChipLayoutForSkills(separated)
                }
            } else {
                //showToast("something went wrong")
            }
        }
    }



    lateinit var mRef: StorageReference

    private fun uploadImageToStorage(image: Uri) {
        showProgress()
        var time = java.sql.Timestamp(Date().time)
        var timestamp = time.toString().replace(":", "").replace("-", "").replace(" ", "").replace(".", "")
        mRef = fireStoreHelper.getStorageRef().child("profile_pics").child(fireStoreHelper.currentUserUid() + timestamp)
        mRef.putFile(image).addOnCompleteListener {
            if (it.isSuccessful) {
                it.result
                mRef!!.downloadUrl.addOnSuccessListener { p0 -> uploadImageToFireStore(p0.toString()) }.addOnFailureListener {
                    hideProgress()
                }
            }

        }.addOnFailureListener {
            showToast(it.message)
            hideProgress()
        }
    }




    private fun getLoveForNei() {
        fireStoreHelper.getLoveNeigh(user!!.uid).addOnCompleteListener {
            if (it.isSuccessful && cvAboutNhood != null) {
                val skills = it.result.toObject(Biography.AboutLoveNei::class.java)
                if (skills != null && skills.aboutLoveNei.isNotEmpty()) {
                    cvAboutNhood.visible()
                    val separated = skills.aboutLoveNei.split(",")
                    setChipLayoutForAboutNhood(separated)
                }
            } else {
                //showToast("something went wrong")
            }
        }
    }





    private fun getChild() {
        familyList!!.clear()
        fireStoreHelper.getChild(user!!.uid).addOnCompleteListener {
            if (it.isSuccessful && familyLayout != null) {
                it.result.documents.forEach {
                    var family = it.toObject(Family.Child()::class.java)
                    familyList!!.add(AllFamily(name = family!!.name, image = family!!.imageLink, age = family!!.age + " year old", id = it.id))
                    familyLayout.gone()
                }
                getPet()

            }
        }.addOnFailureListener {
            showToast(it.message!!)
        }
    }



    private fun getPet() {
        fireStoreHelper.getPet(user!!.uid).addOnCompleteListener {
            if (it.isSuccessful && familyLayout != null) {
                if (it.result.documents.isEmpty()) {
                    setAdapter()

                } else {
                    it.result.documents.forEach {
                        var pet = it.toObject(Family.Pet()::class.java)
                        familyList!!.add(AllFamily(name = pet!!.name, image = pet!!.imageLink, age = pet.profile, category = pet.category, id = it.id))
                    }
                    familyLayout.gone()
                    setAdapter()
                }

            } else {
                //showToast("something went wrong")
            }
        }.addOnFailureListener {
            showToast(it.message!!)
        }
    }

    private fun getContactInformation() {
        fireStoreHelper.getContactInformation(user!!.uid).addOnCompleteListener {
            if (it.isSuccessful && contactLayout != null) {
                val contacts = it.result.toObject(ContactInformation::class.java)
                if (contacts != null && contacts.email.isNotEmpty()) {
                    addContactInformation.gone()
                    contactLayout.visible()
                    familyEmail.text = contacts.email
                    familyHomePhone.text = contacts.homePhone
                    familyMobileNumber.text = contacts.mobilePhone

                }
            } else {
                // showToast("something went wrong")
            }
        }
    }

    private fun setAdapter() {
        if (familyRecycler != null) {
            var adapter = AllFamilyAdapter(context, familyList, this)
            familyRecycler.layoutManager = GridLayoutManager(context, context!!.calculateNoOfColumns()) as RecyclerView.LayoutManager?
            familyRecycler.adapter = adapter
        }
    }


    override fun onContactClick(s: String, id: String, position: Int) {
        activity!!.openActivity<AddFamilyBaseActivity>()
    }

    fun getUser() {
        fireStoreHelper.getUser(fireStoreHelper.currentUserUid()).addOnCompleteListener {
            if (it.isSuccessful) {
                if (it.result.exists()) {
                    val user = it.result.toObject(User::class.java)
                    pref.setUserToPref(user!!)
                }
            }
        }

    }
}
