package com.sachtechsolution.neighbourhood.ui.home.fab.fragment.message.fragment.sale_and_free.fragment

import android.app.Activity
import android.app.Activity.RESULT_OK
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import com.bususer.tiket.AddPostForSale
import com.example.manish.internetstatus.OttoBus
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.constants.classdata
import com.sachtechsolution.neighbourhood.constants.Constants
import com.sachtechsolution.neighbourhood.extension.showToast
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.squareup.otto.Subscribe
import com.zhihu.matisse.Matisse
import com.zhihu.matisse.MimeType
import com.zhihu.matisse.engine.impl.PicassoEngine
import kotlinx.android.synthetic.main.fragment_sale_andfree.*
import kotlinx.android.synthetic.main.sale_toolbar.*


class ForSaleAndFreeFragment : BaseFragment(), View.OnClickListener {

    var RequestCode = 12

    override fun viewToCreate(): Int {
        return R.layout.fragment_sale_andfree
    }

    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
    }

    @Subscribe
    fun getMessage(internetStatus: String) {

        if (internetStatus.equals("Not connected to Internet")) {
            InternetStatusDialogFragment.newInstance().show(fragmentManager, "option_fragment")
        } else {
            showToast("internet enabled ")

        }

    }

    override fun onResume() {
        super.onResume()
        OttoBus.bus.register(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //listner
        btnBack.setOnClickListener(this)
        txtNext.setOnClickListener(this)

        swtFree.setOnCheckedChangeListener { buttonView, isChecked ->

            if (swtFree.isChecked) {
                etPrice.setText("free")
                etPrice.isEnabled = false
                etDetail.requestFocus()
            } else {
                etPrice.setText("")
                etPrice.isEnabled = true
                etPrice.requestFocus()
            }
        }

    }

    override fun onClick(view: View?) {
        var id = view!!.id

        when (id) {
            R.id.btnBack -> activity!!.onBackPressed()
            R.id.txtNext -> toSelectImage()
        }

    }

    fun toSelectImage() {

        val title = etTitle.text.toString()
        val priceString = etPrice.text.toString()
        val originalPrice = etOriginalPrice.text.toString()
        val detail = etDetail.text.toString()

        if (title.isEmpty()) {
            showToast("Enter Title")
        } else if (detail.isEmpty()) {
            showToast("Enter Detail")
        } else {


            if (priceString.equals("free") || priceString.isEmpty())
                classdata.price = 0
            else
                classdata.price = priceString.toInt()
            if (originalPrice.isNotEmpty())
                classdata.originalPrice = originalPrice.toInt()
            classdata.detail = detail
            classdata.title = title
            getImage()
        }


    }

    private fun getImage() {

        Matisse.from(this)
                .choose(MimeType.ofImage())
                .theme(R.style.Matisse_Dracula)
                .countable(false)
                .maxSelectable(9)
                .originalEnable(true)
                .maxOriginalSize(10)
                .imageEngine(PicassoEngine())
                .forResult(RequestCode)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode == Activity.RESULT_OK && requestCode == RequestCode) {
            val mSelected: List<Uri>

            if (data !== null) {

                if (requestCode == RequestCode && resultCode == RESULT_OK) {
                    mSelected = Matisse.obtainResult(data)
                    Constants.IMAGE_LIST = mSelected as ArrayList<Uri?>
                    var intent = Intent(activity!!, AddPostForSale::class.java)
                    intent.putExtra("image", mSelected[0])
                    activity!!.startActivity(intent)
                    activity!!.finish()
                }

            } else
                activity!!.showToast("Select image again")
        }

    }

}