package com.sachtechsolution.neighbourhood.ui.home.fab.fragment.message.fragment.sale_and_free.fragment

import android.os.Bundle
import android.view.View
import com.example.manish.internetstatus.OttoBus
import com.google.firebase.storage.StorageReference
import com.sachtechsolution.neighbourhood.HomePage
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.constants.Constants
import com.sachtechsolution.neighbourhood.constants.classdata
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.gone
import com.sachtechsolution.neighbourhood.model.Post
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_select_neighbour.*
import kotlinx.android.synthetic.main.sale_toolbar.*
import java.util.*
import kotlin.collections.ArrayList

class SelectNeighbourFragment : BaseFragment(), View.OnClickListener {

    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }
    val user by lazy { pref.getUserFromPref() }
    var imagesLinks = ""
    private var size = 0
    private var count = 0
    //lateinit var adapter: ChooseNeighborsAdapter
    private var subAddress: String = ""

    override fun onClick(p0: View?) {
        when (p0!!.id) {
            R.id.btnBack -> activity!!.onBackPressed()
            R.id.txtNext -> uploadImage()
        }
    }


    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
    }


    @Subscribe
    fun getMessage(internetStatus: String) {

        if (internetStatus.equals("Not connected to Internet")) {
            InternetStatusDialogFragment.newInstance().show(fragmentManager, "option_fragment")
        } else {
            showToast("internet enabled ")

        }

    }

    override fun onResume() {
        super.onResume()
        OttoBus.bus.register(this)
    }


    lateinit var mRef: StorageReference

    private fun uploadImage() {

        showProgress()
        size = Constants.IMAGE_LIST.size
        var time = java.sql.Timestamp(Date().time)
        var timestamp = time.toString().replace(":", "").replace("-", "").replace(" ", "").replace(".", "")

        mRef = fireStoreHelper.getStorageRef().child("postImages").child(fireStoreHelper.currentUserUid() + timestamp)
        mRef.putFile(Constants.IMAGE_LIST[count]!!).addOnCompleteListener {
            if (it.isSuccessful) {
                it.result
                mRef.downloadUrl.addOnSuccessListener { p0 ->
                    count += 1
                    imagesLinks = imagesLinks + p0.toString() + "#"
                    if (count > size - 1)
                        createPost()
                    else
                        uploadImage()
                }.addOnFailureListener {
                    hideProgress()
                }
            } else {
                showToast("unable to post")
                hideProgress()
            }
        }.addOnFailureListener {
            showToast("unable to post")
            hideProgress()
        }
    }

    private fun createPost() {

        var discount = 0
        if (classdata.price !== 0 && classdata.originalPrice !== 0 && classdata.price < classdata.originalPrice) {
            discount = ((classdata.originalPrice-classdata.price)*100)/classdata.originalPrice
        }
        val post = Post(
                uid = fireStoreHelper.currentUserUid(), zipCode = user!!.zipCode, society = user!!.society,
                type = Post.postType.FOR_SALE, imagesLinks = imagesLinks, privacy = list/*adapter.addressList*/,
                sale = Post.Sale(title = classdata.title, discount = discount, street = user!!.street, price = classdata.price, originalPrice = classdata.originalPrice, detail = classdata.detail, type = classdata.category))

        fireStoreHelper.createPost(post).addOnCompleteListener {
            if (it.isSuccessful) {
                showToast("Sale Posted")
                baseAcitivityListener.navigator.openActivity(HomePage::class.java)
                activity!!.finish()

                /*fireStoreHelper.getAllNeighbors(user!!.zipCode).addOnCompleteListener {
                    if (it.isSuccessful) {
                        var token = ""
                        it.result.documents.forEach {
                            if (!it.id.equals(user!!.uid) && *//*adapter!!.addressList*//* list.contains(it.toObject(User::class.java)!!.society))
                                token = token + "," + it.toObject(User::class.java)!!.token
                        }

                        if (!token.isNullOrEmpty()) {
                            token = token.substring(1)
                            val title = "There is a new item for sale in your neighborhood"
                            val callback = Api_Hitter.ApiInterface("http://stsmentor.com/").sendNotificationToAll(title, token, "zxc")
                            callback.enqueue(object : Callback<ResponseBody> {
                                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                                hideProgress()
                                }

                                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                                 hideProgress()
                                }
                            })
                        } else {
                            hideProgress()
                        }
                    }
                }.addOnFailureListener {
                    showToast("unable to post")
                    hideProgress()
                }*/
            } else {
                hideProgress()
                showToast("unable to post")
            }
        }.addOnFailureListener {
            showToast("unable to post")
            hideProgress()
        }
    }

    override fun viewToCreate(): Int {
        return R.layout.fragment_select_neighbour
    }


    lateinit var list: ArrayList<String>

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnBack.setOnClickListener(this)
        txtNext.setOnClickListener(this)
        val user = user!!

        list = ArrayList()
        list.add(user.society)
        sociteyRB.text = user.society
        subLocalityRB.text = user.street
        zipCodeRB.text = user.mainLocality + " + Nearby"

        if (user.society.equals(user.street)) {
            sociteyRB.gone()
            subLocalityRB.isChecked = true
        }


        sociteyRB.setOnCheckedChangeListener { buttonView, isChecked ->

            if (isChecked) {
                list.clear()
                list.add(user.society)
            }
        }

        subLocalityRB.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                list.clear()
                list = Constants.SOCIETIES
            }
        }
        zipCodeRB.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                list.clear()
                list = Constants.ADDRESSES
            }
        }

    }
}