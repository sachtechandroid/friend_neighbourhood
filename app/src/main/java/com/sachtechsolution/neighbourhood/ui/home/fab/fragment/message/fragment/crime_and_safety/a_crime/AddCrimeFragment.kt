package com.sachtechsolution.neighbourhood.ui.home.fab.fragment.message.fragment.crime_and_safety.a_crime

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.v7.widget.GridLayoutManager
import android.view.View
import com.example.manish.internetstatus.OttoBus
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.constants.Constants
import com.sachtechsolution.neighbourhood.extension.showToast
import com.sachtechsolution.neighbourhood.model.Post
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.sachtechsolution.neighbourhood.ui.home.fab.fragment.message.adapter.SingleImageSelecterAapter
import com.sachtechsolution.neighbourhood.ui.home.fab.fragment.message.dialogs.ImageActionDialogFragment
import com.sachtechsolution.neighbourhood.ui.home.fab.fragment.message.dialogs.SelectOptionDialogFragment
import com.sachtechsolution.neighbourhood.ui.home.fab.fragment.message.dialogs.ViewImage
import com.sachtechsolution.neighbourhood.ui.home.fab.fragment.message.fragment.crime_and_safety.CreateCrimePostFragment
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_addcrime.*
import java.io.File


class AddCrimeFragment :BaseFragment(), SingleImageSelecterAapter.SelectImage, SelectOptionDialogFragment.onOptionSelect,
        ImageActionDialogFragment.onSelectAction, View.OnClickListener {

    private var TAKE_PICTURE = 12
    private var GALLERY = 13
    private var imageUri: Uri? = null
    var adapter: SingleImageSelecterAapter? = null



    override fun viewPhoto(uri: Uri?) {
        ViewImage.newInstance(uri)
                .show(fragmentManager, "option_fragment")
    }


    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
    }


    @Subscribe
    fun getMessage(internetStatus:String) {

        if (internetStatus.equals("Not connected to Internet")){
            InternetStatusDialogFragment.newInstance().show(fragmentManager, "option_fragment")
        }else {
            showToast("internet enabled ")

        }

    }

    override fun onResume() {
        super.onResume()
        OttoBus.bus.register(this)
    }


    override fun viewToCreate(): Int {
        return R.layout.fragment_addcrime
    }

    override fun removePhoto(position: Int) {
        adapter!!.removeImage(position)
        adapter!!.notifyItemRemoved(position)
    }


    override fun forGallery() {
        var intent = Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(intent, GALLERY)
    }

    override fun forCamera() {
        takePhoto()
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        adapter = SingleImageSelecterAapter(context!!, this)
        rvAddCrime.layoutManager = GridLayoutManager(context, 4)
        rvAddCrime.adapter = adapter

        //listner
        add_crime_back.setOnClickListener(this)
        add_crime_next.setOnClickListener(this)
    }


    override fun onClick(p0: View?) {
        when (p0!!.id) {

            R.id.add_crime_back -> activity!!.onBackPressed()
            R.id.add_crime_next -> {

                val subject = add_crime_subject_et.text.toString()
                val message = add_crime_message_et.text.toString()

                if (subject.isNullOrEmpty())
                    context!!.showToast("Enter Subject")

                else if (message.isNullOrEmpty())
                    context!!.showToast("Enter message")

                else {
                    Constants.IMAGE_LIST = adapter?.list!!
                    baseAcitivityListener.navigator.replaceFragment(CreateCrimePostFragment::class.java,
                            true, Bundle().apply {
                        putString("id", Post.postType.CRIME_SAFETY)
                        putString("subject", subject.capitalize())
                        putString("message", message.capitalize())
                    })
                }
            }
        }
    }

    override fun OnImageSelected(position: Int, uri: Uri?) {
        ImageActionDialogFragment.newInstance(this, position, uri)
                .show(fragmentManager, "option_fragment")
    }

    override fun OnImageChoose() {

        if (adapter!!.list.size == 9)
            showToast("you can't select more images")
        else
            SelectOptionDialogFragment.newInstance(this)
                    .show(fragmentManager, "option_fragment")
    }


    fun takePhoto() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        val photo = File(Environment.getExternalStorageDirectory(), ".jpg")
        intent.putExtra(MediaStore.EXTRA_OUTPUT,
                Uri.fromFile(photo))
        imageUri = Uri.fromFile(photo)
        startActivityForResult(intent, TAKE_PICTURE)
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {

            TAKE_PICTURE -> if (resultCode == Activity.RESULT_OK) {
                val selectedImage = imageUri
                adapter!!.addImage(selectedImage)
                adapter!!.notifyDataSetChanged()
            }

            GALLERY -> {
                if (data != null) {
                    val selectedImage = data!!.data
                    adapter!!.addImage(selectedImage)
                    adapter!!.notifyDataSetChanged()
                }

            }
        }
    }


}