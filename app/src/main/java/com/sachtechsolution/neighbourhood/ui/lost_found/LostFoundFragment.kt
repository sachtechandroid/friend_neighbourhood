package com.sachtechsolution.neighbourhood.ui.lost_found

import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.view.View
import com.example.manish.internetstatus.OttoBus
import com.google.firebase.firestore.DocumentSnapshot
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.constants.Constants
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.*
import com.sachtechsolution.neighbourhood.model.Post
import com.sachtechsolution.neighbourhood.model.User
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.sachtechsolution.neighbourhood.ui.home.adapters.HomePagePostAdapter
import com.sachtechsolution.neighbourhood.ui.home.fab.fragment.message.fragment.CreatePostFragment
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_lost_found.*

class LostFoundFragment : BaseFragment(), HomePagePostAdapter.OpenBottomSheet {

    override fun viewToCreate(): Int {
        return R.layout.fragment_lost_found
    }

    override fun OnOpenBottomSheet(pid: String, position: Int, type: String, post: Post, uid: String) {
    }


    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
    }


    @Subscribe
    fun getMessage(internetStatus: String) {

        if (internetStatus.equals("Not connected to Internet")) {
            InternetStatusDialogFragment.newInstance().show(fragmentManager, "option_fragment")
        } else {
            showToast("internet enabled ")

        }

    }

    val user by lazy { pref!!.getUserFromPref()!! }
    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnBack.setOnClickListener {
            activity!!.finish()
        }
        new_lost_found.setOnClickListener {
            if (context!!.isLocationEnabled(context!!)&&context!!.getLocationMode() == 3) {
                Constants.NEXT = "lost"
                baseAcitivityListener.navigator.replaceFragment(CreatePostFragment::class.java, true, Bundle().apply { putString("id", Post.postType.LOST_FOUND) })
            }
            else
            {
                context!!.showToast("Enable Location with high accuracy option")
                startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
            }
            }

    }

    private var postCount = -1
    lateinit var postLis: MutableList<DocumentSnapshot>
    lateinit var adapter: HomePagePostAdapter
    private fun getPost() {
        postCount++
        if (postCount<postLis.size) {
            var post = postLis[postCount].toObject(Post::class.java)
            post!!.pid = postLis[postCount].id
            if (isPostHasTime(post.date) && lost_found_text != null)
                getPostUser(post)
        }
    }

    private fun getPostUser(post: Post) {
        fireStoreHelper.getUser(post!!.uid).addOnCompleteListener {
            if (it.isSuccessful) {
                lost_found_text?.gone()
                if (it.result.exists() && lost_found_text != null) {
                    val user = it.result.toObject(User::class.java)
                    fireStoreHelper.getThanks(post.pid).addOnCompleteListener {
                        if (it.isSuccessful) {
                            var isThanked = false
                            if (it.result.exists())
                                isThanked = true
                            getPost()
                            lost_found_text?.gone()
                            adapter!!.addItem(post, user!!, isThanked, false)
                        } else {
                            getPost()
                            adapter!!.addItem(post, user!!, false, false)
                        }

                    }
                }
            }
        }.addOnFailureListener { getPost() }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        Constants.IS_THANKED_LIST.clear()
    }

    override fun onResume() {
        super.onResume()
        OttoBus.bus.register(this)
        //getAllPost()
        if (Constants.POSITION != -1) {
            if (Constants.POST != null) adapter?.postsList?.set(Constants.POSITION, Constants.POST)
            adapter!!.notifyItemChanged(Constants.POSITION)
            Constants.POSITION = -1
            Constants.POST = null
        }
        if (Constants.DELETED_POSITION != -1) {
            val position = Constants.DELETED_POSITION
            adapter!!.deletePost(position)
            Constants.DELETED_POSITION = -1
        }
        if (intracterListner!=null)
        intracterListner!!.updateSelectedPos(0)

       setupData()
    }

    private fun setupData() {
        postCount=-1
        adapter = HomePagePostAdapter(activity!!, this, baseAcitivityListener, pref)
        recyclerView_lost_found.adapter = adapter

        Constants.IS_THANKED_LIST.clear()
        Constants.IS_POLLED_LIST.clear()
        fireStoreHelper.getPostsByType(Post.postType.LOST_FOUND, zipCode = user.zipCode,
                society = user.society).addOnCompleteListener {
            if (it.isSuccessful) {
                postLis = it.result.documents
                if (postLis.size == 0)
                    lost_found_text.text = "No post yet"
                getPost()
            } else {
                lost_found_text.text = "No post yet"
                showToast(it.exception.toString())
            }
        }.addOnFailureListener {
            setupData()
            showToast("fail" + it.toString())
        }
    }

}
