package com.sachtechsolution.neighbourhood.ui.home.fab.fragment.message.dialogs

import android.net.Uri
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.*
import com.sachtechsolution.neighbourhood.R
import kotlinx.android.synthetic.main.view_select_option.*


open class ImageActionDialogFragment: DialogFragment(), View.OnClickListener {


    override fun onClick(p0: View?) {
        var id=p0!!.id
        when(id)
        {
            R.id.txtViewPhoto-> {
                dismiss()
                listner!!.viewPhoto(uri)}
            R.id.txtRemovePhoto-> {dismiss()
                listner!!.removePhoto(position)}
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dialog.window.setGravity(Gravity.CENTER)
        dialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        return inflater.inflate(R.layout.view_select_option, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //listner
        txtViewPhoto.setOnClickListener(this)
        txtRemovePhoto.setOnClickListener(this)
    }

    companion object {

        var listner: onSelectAction?=null
        var position=0
        var uri:Uri?=null
        fun newInstance(listner: ImageActionDialogFragment.onSelectAction, position: Int, uri: Uri?): ImageActionDialogFragment {
            val fragment = ImageActionDialogFragment()
            Companion.listner =listner
            Companion.uri =uri
            Companion.position =position
            return fragment

        }
    }

    interface onSelectAction{
        fun viewPhoto(uri: Uri?)
        fun removePhoto(position: Int)
    }
}