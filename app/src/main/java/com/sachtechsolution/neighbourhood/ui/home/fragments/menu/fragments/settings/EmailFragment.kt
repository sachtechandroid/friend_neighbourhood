package com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings

import android.os.Bundle
import android.view.View
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings.adapters.EmailAdapter
import kotlinx.android.synthetic.main.fragment_email.*

class EmailFragment:BaseFragment() {

    val emailAdapter by lazy { EmailAdapter() }
    override fun viewToCreate(): Int {
        return R.layout.fragment_email
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        email_recycler.adapter=emailAdapter
        other_email_recycler.adapter=emailAdapter
    }
}