package com.sachtechsolution.neighbourhood.ui.home.adapters

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Environment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions.bitmapTransform
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseAcitivityListener
import com.sachtechsolution.neighbourhood.basepackage.base.BaseActivity
import com.sachtechsolution.neighbourhood.basepackage.base.ProgressDialogFragment.hideProgress
import com.sachtechsolution.neighbourhood.chat.ActivityChatDetails
import com.sachtechsolution.neighbourhood.chat.model.Friend
import com.sachtechsolution.neighbourhood.constants.Constants
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.getDateDifference
import com.sachtechsolution.neighbourhood.extension.gone
import com.sachtechsolution.neighbourhood.extension.openActivity
import com.sachtechsolution.neighbourhood.extension.visible
import com.sachtechsolution.neighbourhood.intracter.Api_Hitter
import com.sachtechsolution.neighbourhood.model.Notification
import com.sachtechsolution.neighbourhood.model.PollResult
import com.sachtechsolution.neighbourhood.model.Post
import com.sachtechsolution.neighbourhood.model.User
import com.sachtechsolution.neighbourhood.ui.extra.ExtraActivity
import com.sachtechsolution.neighbourhood.ui.home.AnotherUserProfileActivity
import com.sachtechsolution.neighbourhood.ui.home.fragments.menu.activity.MenuBaseActivity
import com.sachtechsolution.neighbourhood.ui.postDetails.DownloadPdf
import com.sachtechsolution.neighbourhood.ui.postDetails.PostDetailActivity
import com.sachtechsolution.neighbourhood.ui.postDetails.SalePostDetailActivity
import gurtek.mrgurtekbasejava.base.ModelPrefrence
import jp.wasabeef.glide.transformations.BlurTransformation
import kotlinx.android.synthetic.main.ads_item.view.*
import kotlinx.android.synthetic.main.general_post_item.view.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.util.*

class HomePagePostAdapter(activity: Context, var listner: OpenBottomSheet?, val baseAcitivityListener: BaseAcitivityListener, val pref: ModelPrefrence) : RecyclerView.Adapter<HomePagePostAdapter.MyViewHolder>() {

    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }
    internal var context: Context = activity
    var postsList = ArrayList<Post?>()
    var userList = ArrayList<User>()
    val cUser by lazy { pref.getUserFromPref() }

    fun addItem(post: Post, user: User?, isThanked: Boolean, polled: Boolean) {
        postsList.add(post)
        userList.add(user!!)

        Constants.IS_THANKED_LIST.add(isThanked)
        Constants.IS_POLLED_LIST.add(polled)
        notifyItemInserted(postsList.size - 1)
    }

    fun clear() {
        userList.clear()
        Constants.IS_THANKED_LIST.clear()
        postsList.clear()
        Constants.IS_POLLED_LIST.clear()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {

        val itemView: View? = when (viewType) {
            1 -> {
                LayoutInflater.from(parent.context)
                        .inflate(R.layout.general_post_item, parent, false)
            }
            2 -> {
                LayoutInflater.from(parent.context)
                        .inflate(R.layout.message_item, parent, false)
            }
            else -> {
                LayoutInflater.from(parent.context)
                        .inflate(R.layout.ads_item, parent, false)
            }
        }
        return MyViewHolder(itemView!!)
    }


    override fun getItemViewType(position: Int): Int {
        if (postsList[position]!!.type.equals(Post.postType.ADS)) {
            return 0
        } else if (postsList[position]!!.type.equals(Post.postType.MESSAGE)) {
            return 2
        } else return 1
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val post = postsList[position]
        
        if (post!!.type.equals(Post.postType.ADS)) {
            holder.itemView.apply {
                ads_title.text = post.message
                Glide.with(context).load(post.imagesLinks).into(ads_image)
                Glide.with(context).load(post.imagesLinks)
                        .apply(bitmapTransform(BlurTransformation(25, 3)))
                        .into(ads_image_back)
            }
        } else if (post!!.type.equals(Post.postType.MESSAGE)) {
            val user = userList[position]
            holder.itemView.apply {
                post_subject_tv.text = post.message
                post_user_name.text = user.firstName + " " + user.lastName
                post_user_name_latter.text = user.firstName.subSequence(0, 1)
                post_time.text = getDateDifference(post.date)
                Glide.with(context).load(user.imageLink).into(post_user_image)
            }
            holder.itemView.post_user_name.setOnClickListener { openCustomActivity(post.uid) }
            holder.itemView.post_user_image.setOnClickListener { openCustomActivity(post.uid) }

        } else {
            val user = userList[position]
            val images = post?.imagesLinks?.split("#")
            var size = images?.size!! - 1
            holder.itemView.apply {
                post_subject_tv.text = post.subject
                post_message_tv.text = post.message

                if (post.type.equals(Post.postType.REAL_ESTATE)) {

                    if (post.uid.equals(pref.getUserFromPref()!!.uid))
                        estate_message_bt.gone()
                    else
                        estate_message_bt.visible()

                }



                if (post.type.equals(Post.postType.FOR_SALE)) {
                    event_layout.gone()
                    post_poll_layout.gone()
                    val sale = post.sale
                    sale_card.visible()
                    sale_details_tv.text = sale?.title
                    Glide.with(context).load(images[0]).into(sale_item_image)
                    size = 0
                    var prize: String

                    if (sale!!.price == 0)
                        prize = "Free  "
                    else
                        prize = "$ ${sale.price}  "
                    val title = SpannableString(prize + sale.title)
                    title.setSpan(ForegroundColorSpan(0xFF4CAF50.toInt()), 0, prize.length, 0)
                    post_subject_tv.text = title
                    post_message_tv.text = sale?.detail
                    post_item_bottom_layout.gone()

                    if (post.uid.equals(pref.getUserFromPref()!!.uid))
                        sale_message_bt.gone()
                    else
                        sale_message_bt.visible()
                } else {
                    event_layout.gone()
                    sale_card.gone()
                    post_item_bottom_layout.visible()
                    if (Constants.IS_THANKED_LIST.size > position) {
                        if (Constants.IS_THANKED_LIST[position]) {
                            post_thanks_ll2.text = "thanked"
                        } else {
                            post_thanks_ll2.text = "thank"
                        }
                    }
                }


                if (post.type.equals(Post.postType.POLL)) {
                    post_subject_tv.gone()
                    //this.gone()
                    post_votes_count.gone()
                    post_message_tv.gone()
                    post_poll_layout.visible()
                    post_poll_question.text = "Poll : " + post.poll!!.question

                    if (Constants.IS_POLLED_LIST[position]) {
                        fireStoreHelper.getPollResult(post.pid).addOnCompleteListener {

                            if (it.isSuccessful) {
                                val results = ArrayList<Int>()
                                for (i in 0..9) {
                                    results.add(0)
                                }

                                var totalPolls = 0
                                val documents = it.result.documents
                                documents.forEach {
                                    val result = it.toObject(PollResult::class.java)
                                    results[result!!.choice] = results[result!!.choice] + 1
                                    totalPolls++
                                }
                                post_votes_count.visible()
                                post_votes_count.text = "total votes: $totalPolls"
                                post_poll_rv.adapter = PollResultAdapter(context, post.poll.choices, results, totalPolls)
                                //this.visible()
                            }
                        }
                    } else {
                        post_poll_rv.adapter = PollAdapter(context, post, this@HomePagePostAdapter, position, null, pref)
                        //this.visible()
                    }
                } else if (post.type.equals(Post.postType.EVENT) && post.event!!.expiry.after(Date())) {
                    event_layout.visible()
                    event_from.text = post.event!!.startDate
                    event_to.text = post.event!!.endDate
                    event_location.text = post.event!!.location
                    post_poll_layout.gone()
                    post_subject_tv.visible()
                    post_message_tv.visible()

                } else {
                    post_subject_tv.visible()
                    post_message_tv.visible()
                    post_poll_layout.gone()
                    event_layout.gone()
                }
                if (post.thanksCount > 0) {
                    post_thanks_count.visible()
                    post_thanks_iv.visible()
                    post_thanks_count.text = post!!.thanksCount.toString()

                } else {
                    post_thanks_count.gone()
                    post_thanks_iv.gone()
                }
                if (post.repliesCount > 0) {
                    post_replies_count.visible()
                    post_replies_iv.visible()
                    post_replies_count.text = post!!.repliesCount.toString()

                } else {
                    post_replies_count.gone()
                    post_replies_iv.gone()
                }

                if (post.location.isNotEmpty()) {
                    post_location_tv.text = post.location
                    locationLayout.visible()

                } else
                    locationLayout.gone()

                if (post.extra.isNotEmpty()) {
                    post_extra_tv.text = post.extra
                    extraLayout.visible()

                } else
                    extraLayout.gone()
                post_user_name.text = user.firstName + " " + user.lastName
                post_user_name_latter.text = user.firstName.subSequence(0, 1)
                post_time.text = getDateDifference(post.date)
                Glide.with(context).load(user.imageLink).into(post_user_image)

                if (size == 0) {
                    post_image_ll_1.gone()
                    post_image_rl_0.gone()


                } else if (size == 1) {
                    post_image_ll_1.gone()
                    post_image_rl_0.visible()
                    Glide.with(context).load(images[0]).into(post_image_0_front)
                    Glide.with(context).load(images[0])
                            .apply(bitmapTransform(BlurTransformation(25, 3)))
                            .into(post_image_0_back)

                } else if (size!! > 1) {
                    post_image_ll_1.visible()
                    post_image_rl_0.gone()
                    Glide.with(context).load(images[0]).into(post_image_1_front)
                    Glide.with(context).load(images[0])
                            .apply(bitmapTransform(BlurTransformation(25, 3)))
                            .into(post_image_1_back)
                    Glide.with(context).load(images[1]).into(post_image_2_front)
                    Glide.with(context).load(images[1])
                            .apply(bitmapTransform(BlurTransformation(25, 3)))
                            .into(post_image_2_back)

                    if (size > 2) {
                        post_more_images.visible()
                        post_more_images.text = "+" + (size - 2)
                    } else
                        post_more_images.gone()

                }

                sale_message_bt.setOnClickListener {
                    var friend = Friend(user!!.uid, user!!.firstName + " " + user!!.lastName, user!!.imageLink, user!!.society)
                    ActivityChatDetails.navigate((context as AppCompatActivity?)!!, sale_message_bt, friend)
                }
                estate_message_bt.setOnClickListener {
                    var friend = Friend(user!!.uid, user!!.firstName + " " + user!!.lastName, user!!.imageLink, user!!.society)
                    ActivityChatDetails.navigate((context as AppCompatActivity?)!!, sale_message_bt, friend)
                }

                post_image_ll_1.setOnClickListener {
                    baseAcitivityListener.navigator.openActivity(ExtraActivity::class.java,
                            Bundle().apply { putString("images", post.imagesLinks) })
                }
                post_image_rl_0.setOnClickListener {
                    baseAcitivityListener.navigator.openActivity(ExtraActivity::class.java,
                            Bundle().apply { putString("images", post.imagesLinks) })
                }
                post_reply_ll.setOnClickListener {
                    baseAcitivityListener.navigator.openActivity(PostDetailActivity::class.java, Bundle().apply {
                        putParcelable("post", post)
                        putParcelable("user", user)
                        putInt("position", position)
                        putBoolean("recommendation", true)
                    })
                }

                if (post.pdfLink.isNotEmpty()) {
                    postDocument.visible()
                } else
                    postDocument.gone()

                postDocument.setOnClickListener {
                    val path = "" + Environment.getExternalStorageDirectory() +
                            File.separator + "neighborhood"
                    val path2 = "" + Environment.getExternalStorageDirectory() +
                            File.separator + "neighborhood" + File.separator + ".temp"
                    val file = File(path, post.pid + ".pdf")
                    val file2 = File(path2, post.pid + ".pdf")
                    if (file.exists())
                        Toast.makeText(context, "Pdf is already downloaded in neighborhood folder", Toast.LENGTH_SHORT).show()
                    else if (file2.exists())
                        Toast.makeText(context, "Wait Downloading..", Toast.LENGTH_SHORT).show()
                    else {
                        DownloadPdf(context, post.pdfLink, post.pid)
                        Toast.makeText(context, "Downloading..", Toast.LENGTH_SHORT).show()
                    }
                }
            }

            holder.itemView.post_thanks_ll.setOnClickListener { thank(holder) }
            holder.itemView.post_thanks_ll2.setOnClickListener { thank(holder) }
            holder.itemView.post_user_name.setOnClickListener { openCustomActivity(post.uid) }
            holder.itemView.post_user_image.setOnClickListener { openCustomActivity(post.uid) }
            holder.itemView.post_user_name_latter.setOnClickListener { openCustomActivity(post.uid) }

            holder.itemView.setOnClickListener {
                if (post.type.equals(Post.postType.FOR_SALE)) {
                    baseAcitivityListener.navigator.openActivity(SalePostDetailActivity::class.java, Bundle().apply {
                        putParcelable("post", post)
                        putParcelable("user", user)
                        putInt("position", position)
                    })
                } else {
                    val intent = Intent(context, PostDetailActivity::class.java).putExtras(Bundle().apply {
                        putParcelable("post", post)
                        putParcelable("user", user)
                        putInt("position", position)
                    })

                    (context as BaseActivity).startActivityForResult(intent, 900)
                }
            }
        }
    }

    fun openCustomActivity(uid: String) {
        if (uid.equals(pref.getUserFromPref()!!.uid))
            context.openActivity<MenuBaseActivity>(MenuBaseActivity.FRAGMENT, MenuBaseActivity.PROFILE)
        else
            context.openActivity<AnotherUserProfileActivity>("uid", uid)
    }

    private fun thank(holder: HomePagePostAdapter.MyViewHolder) {
        holder.itemView.post_thanks_ll2.isEnabled = false
        holder.itemView.post_thanks_ll.isEnabled = false
        val position = holder.adapterPosition
        if (Constants.IS_THANKED_LIST[position]) {
            removeThanks(holder)
        } else {
            postThanks(holder)
        }
    }

    private fun removeThanks(holder: HomePagePostAdapter.MyViewHolder) {
        val position = holder.adapterPosition
        fireStoreHelper.removeThanks(postsList[position]!!.pid).addOnCompleteListener {
            if (it.isSuccessful) {

                fireStoreHelper.getThanksCount(postsList[position]!!.pid).addOnFailureListener {
                }.addOnCompleteListener {

                    if (it.isSuccessful) {
                        val count = it.result.documents.size
                        postsList[position]!!.thanksCount = count

                        fireStoreHelper.updatePost(post = postsList[position]!!, pid = postsList[position]!!.pid).addOnCompleteListener {
                            if (it.isSuccessful) {
                                Constants.IS_THANKED_LIST.set(position, false)
                                postsList.set(position, postsList[position]!!)
                                notifyItemChanged(position)
                                enableThank(holder)
                            } else {
                                enableThank(holder)
                            }
                        }.addOnFailureListener {
                            enableThank(holder)
                        }
                    } else {
                        enableThank(holder)
                    }
                }
            }
        }
    }

    private fun postThanks(holder: HomePagePostAdapter.MyViewHolder) {
        val position = holder.adapterPosition
        fireStoreHelper.postThanks(postsList[position]!!.pid).addOnCompleteListener {
            if (it.isSuccessful) {
                if (!fireStoreHelper.currentUserUid().equals(postsList[position]!!.uid)) {
                    fireStoreHelper.createNotification(uid = postsList.get(position)!!.uid, pid = postsList.get(position)!!.pid,
                            notification = Notification(type = Notification.THANKS,
                                    suid = cUser!!.uid, pid = postsList.get(position)!!.pid, text = " like your post"))
                            .addOnSuccessListener {

                            }
                    fireStoreHelper.getUser(postsList.get(position)!!.uid).addOnCompleteListener {
                        if (it.isSuccessful) {
                            if (it.result.exists()) {
                                val pUser = it.result.toObject(User::class.java)
                                val callback = Api_Hitter.ApiInterface("http://stsmentor.com/").sendNotification(cUser!!.firstName + " " + cUser!!.lastName + " like your post", pUser!!.token)
                                callback.enqueue(object : Callback<ResponseBody> {
                                    override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                                        //context.showToast("got error")
                                    }

                                    override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                                        if (response.errorBody() != null) {
                                            //context.showToast("Notification Not sent")
                                        } else if (response.body() != null) {
                                            //context.showToast("Notification sent")
                                        }
                                    }

                                })
                            }
                        }
                        hideProgress()
                    }.addOnFailureListener { hideProgress() }

                }


                fireStoreHelper.getThanksCount(postsList[position]!!.pid).addOnFailureListener {
                }.addOnCompleteListener {
                    if (it.isSuccessful) {
                        val count = it.result.documents.size
                        postsList[position]!!.thanksCount = count
                        fireStoreHelper.updatePost(post = postsList[position]!!, pid = postsList[position]!!.pid).addOnCompleteListener {
                            if (it.isSuccessful) {
                                Constants.IS_THANKED_LIST.set(position, true)
                                postsList.set(position, postsList[position]!!)
                                notifyItemChanged(position)
                                enableThank(holder)
                            } else {
                                enableThank(holder)
                            }
                        }.addOnFailureListener {
                            enableThank(holder)
                        }
                    } else {
                        enableThank(holder)
                    }
                }
            }
        }
    }

    private fun enableThank(holder: MyViewHolder) {
        holder.itemView.post_thanks_ll2.isEnabled = true
        holder.itemView.post_thanks_ll.isEnabled = true
    }


    override fun getItemCount(): Int {
        return postsList.size
    }

    fun deletePost(postPosition: Int) {
        postsList.removeAt(postPosition)
        userList.removeAt(postPosition)
        Constants.IS_POLLED_LIST.removeAt(postPosition)
        Constants.IS_THANKED_LIST.removeAt(postPosition)
        notifyItemRemoved(postPosition)
    }

    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view)

    interface OpenBottomSheet {
        fun OnOpenBottomSheet(pid: String, position: Int, type: String, post: Post, uid: String)
    }
}
