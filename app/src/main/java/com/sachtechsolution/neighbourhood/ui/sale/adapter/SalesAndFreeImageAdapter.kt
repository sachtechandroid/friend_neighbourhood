package com.sachtechsolution.neighbourhood.ui.sale.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.ProgressDialogFragment
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.gone
import com.sachtechsolution.neighbourhood.extension.visible
import com.sachtechsolution.neighbourhood.model.Post
import com.sachtechsolution.neighbourhood.model.User
import kotlinx.android.synthetic.main.sales_images_item.view.*


class SalesAndFreeImageAdapter(val context: Context, var listner: OpenDetailActivity) : RecyclerView.Adapter<SalesAndFreeImageAdapter.ViewHolder>() {

    var list = ArrayList<Post>()
    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {


        var post = list!![position]
        holder.itemView.menu_title.text = post!!.sale!!.title

        val parts = post!!.imagesLinks.split("#") // String array, each element is text between dots
        Glide.with(context).load(parts[0]).into(holder.itemView.menu_image)

        if (post.sale!!.discount!=0)
        {

            holder.itemView.menu_price.text = "Price." + post!!.sale!!.price
            holder.itemView.discountView.visible()
            holder.itemView.discountView.text = "Price. "+post.sale!!.discount.toString()+" %off"
            holder.itemView.menu_original_price.visible()
            holder.itemView.menu_original_price.text ="Price. "+post.sale!!.originalPrice.toString()
        }

        else {
            if (post.sale!!.price!=0)
            {
                holder.itemView.menu_price.text = "Price." + post!!.sale!!.price
            }else
            {
                holder.itemView.menu_price.text = "free"
            }
            holder.itemView.menu_original_price.gone()
            holder.itemView.discountView.gone()
        }

        holder.itemView.viewFull.setOnClickListener {
            val manager = (context as AppCompatActivity).supportFragmentManager
            ProgressDialogFragment.showProgress(manager)
            fireStoreHelper.getUser(post!!.uid).addOnCompleteListener {
                if (it.isSuccessful) {
                    if (it.result.exists()) {
                        ProgressDialogFragment.hideProgress()
                        val user = it.result.toObject(User::class.java)
                        listner.onOpenActivity(post, user!!, position)
                    }
                } else
                    ProgressDialogFragment.hideProgress()
            }.addOnFailureListener {
                ProgressDialogFragment.hideProgress()
            }

        }
    }


    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val v: View = LayoutInflater.from(p0.context).inflate(R.layout.sales_images_item, p0, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return list.size
    }


    fun clearAll() {
        list.clear()
        notifyDataSetChanged()
    }

    fun addItem(post: Post) {
        list.add(post)
        notifyItemInserted(list.size - 1)
    }

    fun removeItem(position: Int) {
        list.removeAt(position)
        notifyItemRemoved(position)
    }

    fun update(list: ArrayList<Post>) {
        this.list.addAll(list)
        notifyDataSetChanged()
    }


    interface OpenDetailActivity {
        fun onOpenActivity(post: Post, user: User, position: Int)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}