package com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings.profile

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import com.bumptech.glide.Glide
import com.example.manish.internetstatus.OttoBus
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseActivity
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.showToast
import com.sachtechsolution.neighbourhood.extension.visible
import com.sachtechsolution.neighbourhood.model.Biography
import com.sachtechsolution.neighbourhood.model.User
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.squareup.otto.Subscribe
import fisk.chipcloud.ChipCloud
import fisk.chipcloud.ChipCloudConfig
import kotlinx.android.synthetic.main.activity_intersets.*
import kotlinx.android.synthetic.main.fragment_profile.*

class IntersetsActivity : BaseActivity() {

    override fun fragmentContainer(): Int {
        return 0
    }


    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
    }


    @Subscribe
    fun getMessage(internetStatus:String) {

        if (internetStatus.equals("Not connected to Internet")){
            InternetStatusDialogFragment.newInstance().show(supportFragmentManager, "option_fragment")
        }else {
            showToast("internet enabled ")

        }

    }

    override fun onResume() {
        super.onResume()
        OttoBus.bus.register(this)
    }

    val fireStoreHelper:FireStoreHelper by lazy { FireStoreHelper.getInstance() }
    var list:ArrayList<String>?=null
    var cHobbies: ChipCloud?=null
    var cSport: ChipCloud?=null
    var cFamily: ChipCloud?=null
    var cHome: ChipCloud?=null
    var user:User?= null
    var cGames: ChipCloud?=null
    var cNeig: ChipCloud?=null
    var cOther: ChipCloud?=null
    var selectedList=""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_intersets)
        user=pref.getUserFromPref()
        list=ArrayList()
        getSelected()
        getHobbies()
        getSports()
        getFamily()
        getHome()
        getGames()
        getNeig()
        getOther()

        if(user!!.imageLink.isNotEmpty())
            Glide.with(this).load(user!!.imageLink).into(pic)

        txtDone.setOnClickListener {
            uploadData(filterData(),user!!.uid)
        }

    }

    private fun filterData(): Biography.Interest {


        for (i in 0 until list!!.size) {


            if (i==list!!.lastIndex)
                selectedList += list!![i]
            else
                selectedList += list!![i]+","
        }
        return Biography.Interest(interest = selectedList)
    }


    private fun uploadData(interest: Biography.Interest, uid: String) {
        /*intent.putStringArrayListExtra("recommendation",recommendation)
        setResult(2,intent)
       */
        showProgress()
        fireStoreHelper.updateInterest(interest,uid).addOnCompleteListener {
            if (it.isSuccessful) {
                showToast("Updated")
                hideProgress()
                intent.putStringArrayListExtra("recommendation",list)
                setResult(2,intent)
                finish()
            } else {
                showToast("error")
                hideProgress()
            }
            hideProgress()
        }.addOnFailureListener {
            showToast("error")
            hideProgress()
        }
    }


    private fun getOther() {
        val config = ChipCloudConfig()
                .selectMode(ChipCloud.SelectMode.multi)
                .checkedChipColor(resources.getColor(R.color.optional))
                .checkedTextColor(Color.parseColor("#ffffff"))
                .uncheckedChipColor(Color.parseColor("#e0e0e0"))
                .uncheckedTextColor(resources.getColor(R.color.optional))


        cOther = ChipCloud(this, cvOther, config)
        cOther!!.addChip("More...")

        cOther!!.setListener { index, checked, userClick ->
            if (userClick) {
                if (cOther!!.getLabel(index).equals("More...")){
                    openActivity<MoreChipsActivty>(10)
                }else{

                    if (checked)
                        list!!.add(cOther!!.getLabel(index))
                    else {
                        for (i in 0..list!!.size) {
                            if (list!![i].equals(cOther!!.getLabel(index).toString())) {
                                list!!.removeAt(i)
                                break
                            }

                        }
                    }
                }
            }

        }
    }

    private fun getNeig() {
        val config = ChipCloudConfig()
                .selectMode(ChipCloud.SelectMode.multi)
                .checkedChipColor(resources.getColor(R.color.optional))
                .checkedTextColor(Color.parseColor("#ffffff"))
                .uncheckedChipColor(Color.parseColor("#e0e0e0"))
                .uncheckedTextColor(resources.getColor(R.color.optional))


        cNeig = ChipCloud(this, cvTheNeigh, config)
        cNeig!!.addChip("More...")

        cNeig!!.setListener { index, checked, userClick ->
            if (userClick) {
                if (cNeig!!.getLabel(index).equals("More...")){
                    openActivity<MoreChipsActivty>(9)
                }else{

                    if (checked)
                        list!!.add(cNeig!!.getLabel(index))
                    else {
                        for (i in 0..list!!.size) {
                            if (list!![i].equals(cNeig!!.getLabel(index).toString())) {
                                list!!.removeAt(i)
                                break
                            }

                        }
                    }
                }


            }

        }
    }

    private fun getGames() {
        val config = ChipCloudConfig()
                .selectMode(ChipCloud.SelectMode.multi)
                .checkedChipColor(resources.getColor(R.color.optional))
                .checkedTextColor(Color.parseColor("#ffffff"))
                .uncheckedChipColor(Color.parseColor("#e0e0e0"))
                .uncheckedTextColor(resources.getColor(R.color.optional))


        cGames = ChipCloud(this, cvGames, config)
        val demoArray = resources.getStringArray(R.array.array_games)
        cGames!!.addChips(demoArray)

        cGames!!.setListener { index, checked, userClick ->
            if (userClick) {
                if (cGames!!.getLabel(index).equals("More...")) {
                    openActivity<MoreChipsActivty>(8)
                } else {

                    if (checked)
                        list!!.add(cGames!!.getLabel(index))
                    else {
                        for (i in 0..list!!.size) {
                            if (list!![i].equals(cGames!!.getLabel(index).toString())) {
                                list!!.removeAt(i)
                                break
                            }

                        }
                    }
                }
            }

        }
    }

    private fun getHome() {
        val config = ChipCloudConfig()
                .selectMode(ChipCloud.SelectMode.multi)
                .checkedChipColor(resources.getColor(R.color.optional))
                .checkedTextColor(Color.parseColor("#ffffff"))
                .uncheckedChipColor(Color.parseColor("#e0e0e0"))
                .uncheckedTextColor(resources.getColor(R.color.optional))


        cHome = ChipCloud(this, cvHome, config)
        val demoArray = resources.getStringArray(R.array.array_homes)
        cHome!!.addChips(demoArray)

        cHome!!.setListener { index, checked, userClick ->
            if (userClick) {
                if (cHome!!.getLabel(index).equals("More...")){
                    openActivity<MoreChipsActivty>(7)
                }else{

                    if (checked)
                        list!!.add(cHome!!.getLabel(index))
                    else {
                        for (i in 0..list!!.size) {
                            if (list!![i].equals(cHome!!.getLabel(index).toString())) {
                                list!!.removeAt(i)
                                break
                            }

                    }
                }
                }

            }

        }
    }

    private fun getFamily() {

        val config = ChipCloudConfig()
                .selectMode(ChipCloud.SelectMode.multi)
                .checkedChipColor(resources.getColor(R.color.optional))
                .checkedTextColor(Color.parseColor("#ffffff"))
                .uncheckedChipColor(Color.parseColor("#e0e0e0"))
                .uncheckedTextColor(resources.getColor(R.color.optional))


        cFamily = ChipCloud(this, cvFamily, config)
        val demoArray = resources.getStringArray(R.array.array_family)
        cFamily!!.addChips(demoArray)

        cFamily!!.setListener { index, checked, userClick ->
            if (userClick) {
                if (cFamily!!.getLabel(index).equals("More...")){
                    openActivity<MoreChipsActivty>(6)
                }else{

                    if (checked)
                        list!!.add(cFamily!!.getLabel(index))
                    else {
                        for (i in 0..list!!.size) {
                            if (list!![i].equals(cFamily!!.getLabel(index).toString())) {
                                list!!.removeAt(i)
                                break
                            }
                        }
                    }
                }
            }

        }
    }

    private fun getSports() {
        val config = ChipCloudConfig()
                .selectMode(ChipCloud.SelectMode.multi)
                .checkedChipColor(resources.getColor(R.color.optional))
                .checkedTextColor(Color.parseColor("#ffffff"))
                .uncheckedChipColor(Color.parseColor("#e0e0e0"))
                .uncheckedTextColor(resources.getColor(R.color.optional))


        cSport = ChipCloud(this, cvSports, config)
        val demoArray = resources.getStringArray(R.array.array_sport)
        cSport!!.addChips(demoArray)

        cSport!!.setListener { index, checked, userClick ->
            if (userClick) {
                if (cSport!!.getLabel(index).equals("More...")){
                    openActivity<MoreChipsActivty>(5)
                }else{

                    if (checked)
                        list!!.add(cSport!!.getLabel(index))
                    else {
                        for (i in 0..list!!.size) {
                            if (list!![i].equals(cSport!!.getLabel(index).toString())) {
                                list!!.removeAt(i)
                                break
                            }
                        }
                    }
                }
            }
        }

    }

    private inline fun <reified I> openActivity(requestCode: Int) {
        var intent=Intent(this, I::class.java)
        intent.putExtra("code",requestCode)
        startActivityForResult(intent,requestCode)
    }

    private fun getHobbies() {
        val config = ChipCloudConfig()
                .selectMode(ChipCloud.SelectMode.multi)
                .checkedChipColor(resources.getColor(R.color.optional))
                .checkedTextColor(Color.parseColor("#ffffff"))
                .uncheckedChipColor(Color.parseColor("#e0e0e0"))
                .uncheckedTextColor(resources.getColor(R.color.optional))


        cHobbies = ChipCloud(this, cvHobbies, config)
        val demoArray = resources.getStringArray(R.array.array_hobbies)
        cHobbies!!.addChips(demoArray)

        cHobbies!!.setListener { index, checked, userClick ->
            if (userClick) {
                if (cHobbies!!.getLabel(index).equals("More...")){
                    openActivity<MoreChipsActivty>(4)
                }else{

                    if (checked)
                        list!!.add(cHobbies!!.getLabel(index))
                    else {
                        for (i in 0..list!!.size) {
                            if (list!![i].equals(cHobbies!!.getLabel(index).toString())) {
                                list!!.removeAt(i)
                                break
                            }
                        }
                    }
                }

            }

        }
    }

    private fun getSelected() {
        val config = ChipCloudConfig()
                .selectMode(ChipCloud.SelectMode.multi)
                .checkedChipColor(resources.getColor(R.color.colorGrayDark))
                .checkedTextColor(Color.parseColor("#ffffff"))
                .uncheckedChipColor(resources.getColor(R.color.colorGrayDark))
                .uncheckedTextColor(Color.parseColor("#ffffff"))

        val chipCloud = ChipCloud(this, cvSelected, config)
        val demoArray = resources.getStringArray(R.array.selected_array)
        chipCloud.addChips(demoArray)
        chipCloud.setListener { index, checked, userClick ->
            if (userClick) {

            }
        }
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        // check if the request code is same as what is passed  here it is 2
        if (data!=null){
            if (requestCode == 4) {

                var list = data!!.getStringExtra("data")

                cHobbies!!.addChip(list)
            }
            else if (requestCode == 5) {

                var list = data!!.getStringExtra("data")

                cSport!!.addChip(list)

            }else if (requestCode == 6) {

                var list = data!!.getStringExtra("data")

                cFamily!!.addChip(list)
            }else if (requestCode == 7) {

                var list = data!!.getStringExtra("data")

                cHome!!.addChip(list)
            }else if (requestCode == 8) {

                var list = data!!.getStringExtra("data")

                cGames!!.addChip(list)
            }else if (requestCode == 9) {

                var list = data!!.getStringExtra("data")

                cNeig!!.addChip(list)
            }else if (requestCode == 10) {

                var list = data!!.getStringExtra("data")

                cOther!!.addChip(list)
            }
        }
    }

}
