package com.sachtechsolution.neighbourhood.ui.home.fab.fragment.message.fragment

import android.annotation.SuppressLint
import android.app.Activity
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Intent
import android.location.Geocoder
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.v4.content.FileProvider
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import com.example.manish.internetstatus.OttoBus
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.storage.StorageReference
import com.sachtechsolution.neighbourhood.BuildConfig
import com.sachtechsolution.neighbourhood.HomePage
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.constants.Constants
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.gone
import com.sachtechsolution.neighbourhood.extension.isLocationEnabled
import com.sachtechsolution.neighbourhood.extension.isNetConnected
import com.sachtechsolution.neighbourhood.extension.visible
import com.sachtechsolution.neighbourhood.intracter.Api_Hitter
import com.sachtechsolution.neighbourhood.model.Post
import com.sachtechsolution.neighbourhood.model.User
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.sachtechsolution.neighbourhood.ui.home.fab.fragment.message.adapter.SingleImageSelecterAapter
import com.sachtechsolution.neighbourhood.ui.home.fab.fragment.message.dialogs.ImageActionDialogFragment
import com.sachtechsolution.neighbourhood.ui.home.fab.fragment.message.dialogs.SelectOptionDialogFragment
import com.sachtechsolution.neighbourhood.ui.home.fab.fragment.message.dialogs.ViewImage
import com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings.MapFragmentAdapter
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.custom_toolbar.*
import kotlinx.android.synthetic.main.fragment_general.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


class CreatePostFragment : BaseFragment(), SingleImageSelecterAapter.SelectImage, SelectOptionDialogFragment.onOptionSelect, ImageActionDialogFragment.onSelectAction, View.OnClickListener, OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, MapFragmentAdapter.OpenDone {

    var photoURI: Uri? = null
    private var TAKE_PICTURE = 12
    private var GALLERY = 13
    var adapter: SingleImageSelecterAapter? = null
    private lateinit var mMap: GoogleMap
    private lateinit var mGoogleClient: GoogleApiClient
    lateinit var mLocation: Location
    private var mFirebaseDatabase: FirebaseDatabase? = null
    //var size = 1
    var selectedAddress: String? = ""
    private var myMap: SupportMapFragment? = null
    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }
    private var markerView: View? = null
    private var mYear: Int = 0
    private var mMonth: Int = 0
    private var mDay: Int = 0
    val type by lazy { arguments?.getString("id") ?: "" }
    val gid by lazy { arguments?.getString("gid") ?: "" }

    @SuppressLint("MissingPermission")
    override fun onMapReady(p0: GoogleMap?) {
        mMap = p0!!
        mMap.uiSettings.isZoomControlsEnabled = true
        mMap.isMyLocationEnabled = true

    }


    @SuppressLint("MissingPermission")
    override fun onConnected(p0: Bundle?) {
        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleClient)

        if (mLocation != null) {
            //var latlong= pref.getUserFromPref()!!.latlng.split(",")
            addMarker(LatLng(pref.getUserFromPref()!!.lat, pref.getUserFromPref()!!.lng))
        }
    }
    var addMarker: Marker?=null
    private fun addMarker(latLng: LatLng) {

        if (addMarker==null)

        addMarker = mMap.addMarker(MarkerOptions()
                .position(latLng)
                .draggable(true))
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 11f))
        mMap.setOnMapClickListener { latLng ->
            if (isNetConnected()) {
                showProgress()
                getAddress(latLng)
                addMarker!!.position = latLng
            }else
            {
                showToast("enable internet")
            }
        }

        mMap.setOnMarkerDragListener(object : GoogleMap.OnMarkerDragListener {
            override fun onMarkerDragStart(arg0: Marker) {}

            override fun onMarkerDragEnd(arg0: Marker) {
                if (isNetConnected())
                {
                    showProgress()
                    mMap.animateCamera(CameraUpdateFactory.newLatLng(arg0.position))
                    //context!!.showToast(arg0.position.toString())
                    getAddress(arg0.position)
                }else
                {
                  showToast("enable internet")
                }
            }

            override fun onMarkerDrag(arg0: Marker) {}
        })


    }

    private fun getAddress(latLng: LatLng) {

        val locationList = Geocoder(activity)
                .getFromLocation(latLng.latitude, latLng.longitude, 1)
        if (locationList.size > 0) {
            val location = locationList[0]
            selectedAddress = location.getAddressLine(0).toString()
            general_address.setText(selectedAddress)
            hideProgress()
        }
    }


    override fun onConnectionSuspended(p0: Int) {}

    override fun OnEventDone() {}

    override fun viewPhoto(uri: Uri?) {
        ViewImage.newInstance(uri)
                .show(fragmentManager, "option_fragment")
    }

    override fun removePhoto(position: Int) {
        adapter!!.removeImage(position)
        adapter!!.notifyItemRemoved(position)
    }


    override fun forGallery() {
        var intent = Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(intent, GALLERY)
    }


    override fun forCamera() {
        try {
            dispatchTakePictureIntent();
        } catch (e: IOException) {
        }
    }

    @Throws(IOException::class)
    private fun dispatchTakePictureIntent() {

        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        // Ensure that there's a camera activity to handle the intent

        if (takePictureIntent.resolveActivity(context!!.packageManager) != null) {
            // Create the File where the photo should go
            var photoFile: File? = null
            try {
                photoFile = createImageFile()
            } catch (ex: IOException) {
                // Error occurred while creating the File
                return
            }

            // Continue only if the File was successfully created
            if (photoFile != null) {

                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                    // only for gingerbread and newer versions
                    photoURI = FileProvider.getUriForFile(context!!,
                            BuildConfig.APPLICATION_ID + ".provider",
                            createImageFile())
                }else
                {
                   photoURI = Uri.fromFile(createImageFile())
                }

                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                startActivityForResult(takePictureIntent, TAKE_PICTURE)
            }
        }
    }

    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name

        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "JPEG_" + timeStamp + "_"
        val storageDir = File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM), "Camera")

        val image = File.createTempFile(
                imageFileName, /* prefix */
                ".jpg", /* suffix */
                storageDir      /* directory */)
        return image
    }


    override fun viewToCreate(): Int {
        return R.layout.fragment_general
        return R.layout.fragment_general
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val c = Calendar.getInstance()
        mFirebaseDatabase = FirebaseDatabase.getInstance()
        mDatabaseReference = mFirebaseDatabase!!.reference
        mYear = c.get(Calendar.YEAR)
        mMonth = c.get(Calendar.MONTH)
        mDay = c.get(Calendar.DAY_OF_MONTH)
        Constants.PDF_URI = null
        general_time_et.gone()
        if (type.equals(Post.postType.GENERAL))
            create_post_heading.text = "POSTING TO GENERAL"
        else if (type.equals(Post.postType.LOST_FOUND))
            create_post_heading.text = "POSTING TO LOST AND FOUND"
        else if (type.equals(Post.postType.LOST_FOUND))
            create_post_heading.text = "POSTING TO RECOMMENDATIONS"
        else if (type.equals(Post.postType.GROUP))
            create_post_heading.text = "POSTING TO GROUP"
        else if (type.equals(Post.postType.CAMPAIGN)) {
            create_post_heading.text = "POSTING TO CAMPAIGN"
            general_time_et.visible()
            general_address.visible()
        }
        general_time_et.keyListener = null
        general_time_et.setOnClickListener {
            pickUpDate()
        }
        adapter = SingleImageSelecterAapter(context!!, this)
        rvGeneralPost.layoutManager = GridLayoutManager(context, 6) as RecyclerView.LayoutManager?
        rvGeneralPost.adapter = adapter

        //listner
        cancelLayout.setOnClickListener(this)
        nextLayout.setOnClickListener(this)
        attachFile.setOnClickListener(this)
        if (activity!!.isLocationEnabled(activity!!)) {
            myMap = childFragmentManager.findFragmentById(R.id.map_fragment) as SupportMapFragment
            myMap?.getMapAsync(this)
            mGoogleClient = GoogleApiClient.Builder(activity!!)
                    .addConnectionCallbacks(this)
                    .addApi(LocationServices.API)
                    .build()
            mGoogleClient.connect()
            markerView = LayoutInflater.from(activity).inflate(R.layout.marker_layout, null)

        } else {
            showToast("enable location")
        }
    }

    override fun OnImageSelected(position: Int, uri: Uri?) {
        ImageActionDialogFragment.newInstance(this, position, uri)
                .show(fragmentManager, "option_fragment")
    }

    override fun OnImageChoose() {
        if (adapter!!.list.size == 9)
            showToast("you can't select more images")
        else
            SelectOptionDialogFragment.newInstance(this)
                    .show(fragmentManager, "option_fragment")
    }



    //private var pdfUri: Uri?=null

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {

            TAKE_PICTURE -> {

                if (resultCode == Activity.RESULT_OK) {
                    adapter!!.addImage(photoURI)
                    adapter!!.notifyDataSetChanged()
                }
            }

            GALLERY -> {

                if (data != null) {
                    val selectedImage = data!!.data
                    adapter!!.addImage(selectedImage)
                    adapter!!.notifyDataSetChanged()
                }

            }
            ANDROID_FILE_PICKER -> {
                Constants.PDF_URI = data?.data
                showToast("File Selected")
            }
        }
    }

    private val ANDROID_FILE_PICKER: Int = 12340
    var subject=""
    var message=""
    override fun onClick(view: View?) {
        var id = view!!.id

        when (id) {
            R.id.attachFile -> {
                val openIntent = Intent(Intent.ACTION_GET_CONTENT)
                openIntent.addCategory(Intent.CATEGORY_OPENABLE)
                openIntent.setType("application/pdf")
                startActivityForResult(openIntent, ANDROID_FILE_PICKER)
            }
            R.id.cancelLayout -> activity!!.onBackPressed()
            R.id.nextLayout -> {
                subject = general_subject_et.text.toString()
                message = general_message_et.text.toString()

                if (subject.isNullOrEmpty())
                    showToast("enter subject")
                else if (message.isNullOrEmpty())
                    showToast("enter message")
                else if (type.equals(Post.postType.CAMPAIGN) && campDate.isNullOrEmpty())
                    showToast("select date and time")
                else if (type.equals(Post.postType.CAMPAIGN) && selectedAddress.isNullOrEmpty())
                    showToast("select a campaign location from map")
                else {
                    if (type.equals(Post.postType.GROUP)) {
                        showProgress()
                        Constants.IMAGE_LIST=adapter?.list!!
                        size = adapter?.list!!.size
                        if (size > 0)
                            uploadImage()
                        else if (Constants.PDF_URI != null)
                            uploadPdf()
                        else
                            createPost()
                    } else {
                        Constants.IMAGE_LIST = adapter?.list!!
                        baseAcitivityListener.navigator.replaceFragment(ChooseNeighbors::class.java,
                                true, Bundle().apply {
                            putString("id", type)
                            putString("subject", subject.capitalize())
                            putString("message", message.capitalize())
                            putString("address", general_address.text.toString())
                            putString("extra", campDate)
                        })
                    }
                }
            }
        }
    }

    private var campDate: String = ""

    private fun pickUpDate() {
        general_time_et.setText("")

        val datePickerDialog = DatePickerDialog(context,
                DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

                    val calendar = Calendar.getInstance()
                    calendar.set(year, monthOfYear, dayOfMonth)
                    val date = calendar.time

                    if (date.after(Date())) {
                        var month=monthOfYear+1
                        campDate = dayOfMonth.toString()+"/"+month+"/"+year
                        //campDate = "$dayOfMonth / $monthOfYear+1 / $year"
                        general_time_et.setText(campDate)
                        pickUpEndTime()

                    } else
                        showToast("Selected date should be after current date")
                }, mYear, mMonth, mDay)

        datePickerDialog.show()
    }

    private fun pickUpEndTime() {
        val timePickerDialog = TimePickerDialog(context,
                TimePickerDialog.OnTimeSetListener { view, hourOfDay, minute ->
                    campDate = "$campDate $hourOfDay : $minute"
                    general_time_et.setText(campDate)
                }, 0, 0, true)
        timePickerDialog.show()
    }

    lateinit var mRef: StorageReference
    var imagesLinks = ""
    var pdfLinks = ""
    private var size = 0
    private var count = 0

    private fun uploadImage() {
        var time = java.sql.Timestamp(Date().time)
        var timestamp = time.toString().replace(":", "").replace("-", "").replace(" ", "").replace(".", "")

        mRef = fireStoreHelper.getStorageRef().child("postImages").child(fireStoreHelper.currentUserUid() + timestamp)
        mRef.putFile(Constants.IMAGE_LIST!![count]!!).addOnCompleteListener {
            if (it.isSuccessful) {
                it.result

                mRef!!.downloadUrl.addOnSuccessListener { p0 ->
                    count += 1
                    imagesLinks = imagesLinks + p0.toString() + "#"
                    if (size == count)
                        createPost()
                    else if (Constants.PDF_URI != null)
                        uploadPdf()
                    else
                        uploadImage()
                }.addOnFailureListener {
                    hideProgress()
                }
            }
        }.addOnFailureListener {
            hideProgress()
        }
    }

    private fun uploadPdf() {
        var time = java.sql.Timestamp(Date().time)
        var timestamp = time.toString().replace(":", "").replace("-", "").replace(" ", "").replace(".", "")

        mRef = fireStoreHelper.getStorageRef().child("postPdf").child(fireStoreHelper.currentUserUid() + timestamp)
        mRef.putFile(Constants.PDF_URI!!).addOnCompleteListener {
            if (it.isSuccessful) {
                it.result

                mRef!!.downloadUrl.addOnSuccessListener { p0 ->
                    count += 1
                    pdfLinks = p0.toString()
                    createPost()
                    Constants.PDF_URI=null
                }.addOnFailureListener {
                    hideProgress()
                }
            }
        }.addOnFailureListener {
            hideProgress()
        }
    }
    private var mDatabaseReference: DatabaseReference? = null
    private fun createPost() {
        val post = Post(subject = subject, message = message,  uid = pref.getUserFromPref()!!.uid, pdfLink = pdfLinks,
                type = type, imagesLinks = imagesLinks,location = general_address.text.toString()!!)

        mDatabaseReference!!.child("group").child(gid)
                .push()
                .setValue(post)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        activity!!.finish()
                        hideProgress()
                    } else {
                        hideProgress()
                        showToast("message not send")
                    }
                }
    }
}