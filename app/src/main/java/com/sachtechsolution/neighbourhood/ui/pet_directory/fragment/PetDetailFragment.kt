package com.sachtechsolution.neighbourhood.ui.pet_directory.fragment

import android.os.Bundle
import android.view.View
import com.bumptech.glide.Glide
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.constants.PetDirectory
import com.sachtechsolution.neighbourhood.extension.gone
import com.sachtechsolution.neighbourhood.extension.visible
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_pet_detail.*

class PetDetailFragment : BaseFragment() {

    override fun viewToCreate(): Int {
        return R.layout.fragment_pet_detail
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val pet = arguments!!.get(PET) as PetDirectory
        if (pet != null) {
            pet_nme.text = pet.petName
            pet_sublocation.text = pet.street
            breed_value.text = pet.petbreed
            color_value.text = pet.petColor
            size_value.text = pet.petSize
            userName.text = pet.userName
            Picasso.with(context)
                    .load(pet.petImage)
                    .into(pet_img, object : com.squareup.picasso.Callback {
                        override fun onError() {
                            pBar.visible()
                        }

                        override fun onSuccess() {
                            if(pBar!==null)
                                pBar.gone()
                        }

                    })
            if (pet.userPic.isNotEmpty())
                Glide.with(context!!).load(pet.userPic).into(userDp)
        }

        ivBack.setOnClickListener { activity!!.onBackPressed() }
    }

    companion object {

        val PET = "petDetail"
    }

}