package sachtech.com.blablademo.firebase

import android.content.Intent
import android.support.v4.app.Fragment
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import sachtech.com.blablademo.firebase.listeners.LoginWithFacebookListener
import java.util.*



/**
 * Created by SachTech on 24-04-2018.
 */
class FacebookHelper(fragment: Fragment, listener: LoginWithFacebookListener) {

    var activity:Fragment=fragment
    var callbackManager:CallbackManager = CallbackManager.Factory.create()
    var listener=listener

    fun signin(){
        LoginManager.getInstance().logInWithReadPermissions(activity, Arrays.asList("email", "public_profile"))
        LoginManager.getInstance().registerCallback(callbackManager, object : FacebookCallback<LoginResult> {
            override fun onSuccess(loginResult: LoginResult) {

                listener.onLoginSuccess(loginResult.accessToken)
            }

            override fun onCancel() {
                listener.onLoginFailure("User Canceled")

            }

            override fun onError(error: FacebookException) {
                listener.onLoginFailure(error .localizedMessage)

            }
        })
    }

    fun setupCallback( requestCode:Int,  resultCode:Int,  data:Intent?){
        callbackManager.onActivityResult(requestCode, resultCode, data)
    }


}