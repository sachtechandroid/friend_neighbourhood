package com.sachtechsolution.neighbourhood.ui.bookmark.adapter

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseAcitivityListener
import com.sachtechsolution.neighbourhood.basepackage.base.BaseActivity
import com.sachtechsolution.neighbourhood.constants.Constants
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.getDateDifference
import com.sachtechsolution.neighbourhood.extension.gone
import com.sachtechsolution.neighbourhood.extension.visible
import com.sachtechsolution.neighbourhood.model.PollResult
import com.sachtechsolution.neighbourhood.model.Post
import com.sachtechsolution.neighbourhood.model.User
import com.sachtechsolution.neighbourhood.ui.extra.ExtraActivity
import com.sachtechsolution.neighbourhood.ui.home.adapters.PollResultAdapter
import com.sachtechsolution.neighbourhood.ui.postDetails.PostDetailActivity
import com.sachtechsolution.neighbourhood.ui.postDetails.SalePostDetailActivity
import jp.wasabeef.glide.transformations.BlurTransformation
import kotlinx.android.synthetic.main.general_post_item.view.*

class BookmarkAdapter(activity: Context, val baseAcitivityListener: BaseAcitivityListener, var listner: removeBookmark) : RecyclerView.Adapter<BookmarkAdapter.MyViewHolder>() {

    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }
    internal var context: Context = activity
    var postsList = ArrayList<Post?>()
    var userList = ArrayList<User>()


    fun addItem(post: Post, user: User, isThanked: Boolean, polled: Boolean) {
        postsList.add(post)
        userList.add(user)
        Constants.IS_THANKED_LIST.add(isThanked)
        Constants.IS_POLLED_LIST.add(polled)
        notifyItemInserted(postsList.size - 1)
    }

    fun clear() {
        userList.clear()
        Constants.IS_THANKED_LIST.clear()
        Constants.IS_POLLED_LIST.clear()
        userList.clear()
        notifyDataSetChanged()
    }

    fun deletePost(position: Int) {
        userList.removeAt(position)
        userList.removeAt(position)
        Constants.IS_THANKED_LIST.removeAt(position)
        Constants.IS_POLLED_LIST.removeAt(position)
        notifyItemRemoved(position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView: View? = LayoutInflater.from(parent.context)
                .inflate(R.layout.general_post_item, parent, false)
        return MyViewHolder(itemView!!)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val post = postsList[position]
        val user = userList[position]
        val images = post?.imagesLinks?.split("#")
        var size = images?.size!! - 1
        holder.itemView.apply {
            post_subject_tv.text = post.subject
            post_message_tv.text = post.message
            if (post.type == Post.postType.FOR_SALE) {
                val sale = post.sale
                sale_card.visible()
                sale_details_tv.text = sale?.title
                Glide.with(context).load(images[0]).into(sale_item_image)
                size = 0
                val prize: String
                prize = if (sale!!.price == 0)
                    "Free "
                else
                    "$ ${sale.price}"
                val title = SpannableString(prize + sale.title)
                title.setSpan(ForegroundColorSpan(0xFF4CAF50.toInt()), 0, prize.length, 0)
                post_subject_tv.text = title
                post_message_tv.text = sale.detail
                post_item_bottom_layout.gone()
            } else {
                sale_card.gone()
                post_item_bottom_layout.visible()
                if (Constants.IS_THANKED_LIST[position]) {
                    post_thanks_ll2.text = "thanked"
                } else {
                    post_thanks_ll2.text = "thank"
                }
            }
            if (post.type == Post.postType.POLL) {
                post_subject_tv.gone()
                //this.gone()
                post_votes_count.gone()
                post_message_tv.gone()
                post_poll_layout.visible()
                post_poll_question.text = "Poll : " + post.poll!!.question
                if (Constants.IS_POLLED_LIST[position]) {
                    fireStoreHelper.getPollResult(post.pid).addOnCompleteListener {
                        if (it.isSuccessful) {
                            val results = ArrayList<Int>()
                            for (i in 0..9) {
                                results.add(0)
                            }
                            var totalPolls = 0
                            val documents = it.result.documents
                            documents.forEach {
                                val result = it.toObject(PollResult::class.java)
                                results[result!!.choice] = results[result!!.choice] + 1
                                totalPolls++
                            }
                            post_votes_count.visible()
                            post_votes_count.text = "total votes: $totalPolls"
                            post_poll_rv.adapter = PollResultAdapter(context, post.poll.choices, results, totalPolls)
                            //this.visible()
                        }
                    }
                }
            }
            else {
                post_subject_tv.visible()
                post_message_tv.visible()
                post_poll_layout.gone()
            }
            if (post.thanksCount > 0) {
                post_thanks_count.visible()
                post_thanks_iv.visible()
                post_thanks_count.text = post!!.thanksCount.toString()
            }
            else {
                post_thanks_count.gone()
                post_thanks_iv.gone()
            }
            if (post.repliesCount > 0) {
                post_replies_count.visible()
                post_replies_iv.visible()
                post_replies_count.text = post!!.repliesCount.toString()
            }
            else {
                post_replies_count.gone()
                post_replies_iv.gone()
            }


            if (post.location.isNotEmpty()) {
                post_location_tv.text = post.location
                locationLayout.visible()
            }
            else
                locationLayout.gone()

            if (post.extra.isNotEmpty()) {
                post_extra_tv.text = post.extra
                extraLayout.visible()
            }
            else
                extraLayout.gone()


            post_user_name.text = user.firstName + " " + user.lastName
            post_user_name_latter.text = user.firstName.subSequence(0, 1)
            post_time.text = getDateDifference(post.date)
            Glide.with(context).load(user.imageLink).into(post_user_image)

            if (size == 0) {
                post_image_ll_1.gone()
                post_image_rl_0.gone()
            }
            else if (size == 1) {

                post_image_ll_1.gone()
                post_image_rl_0.visible()
                Glide.with(context).load(images[0]).into(post_image_0_front)
                Glide.with(context).load(images[0])
                        .apply(RequestOptions.bitmapTransform(BlurTransformation(25, 3)))
                        .into(post_image_0_back)

            }
            else if (size!! > 1) {
                post_image_ll_1.visible()
                post_image_rl_0.gone()
                Glide.with(context).load(images[0]).into(post_image_1_front)
                Glide.with(context).load(images[0])
                        .apply(RequestOptions.bitmapTransform(BlurTransformation(25, 3)))
                        .into(post_image_1_back)
                Glide.with(context).load(images[1]).into(post_image_2_front)
                Glide.with(context).load(images[1])
                        .apply(RequestOptions.bitmapTransform(BlurTransformation(25, 3)))
                        .into(post_image_2_back)

                if (size > 2) {
                    post_more_images.visible()
                    post_more_images.text = "+" + (size - 2)
                }
                else
                    post_more_images.gone()
            }

            post_image_ll_1.setOnClickListener {
                baseAcitivityListener.navigator.openActivity(ExtraActivity::class.java,
                        Bundle().apply { putString("images", post.imagesLinks) })
            }
            post_image_rl_0.setOnClickListener {
                baseAcitivityListener.navigator.openActivity(ExtraActivity::class.java,
                        Bundle().apply { putString("images", post.imagesLinks) })
            }
            //ivAddBookmark.setOnClickListener { listner.onRemoveBookmark(post.pid,position) }

            post_reply_ll.setOnClickListener {

                baseAcitivityListener.navigator.openActivity(PostDetailActivity::class.java, Bundle().apply {
                    putParcelable("post", post)
                    putParcelable("user", user)
                    putInt("position", position)
                    putBoolean("recommendation", true)
                })
            }


        }
        holder.itemView.post_thanks_ll.setOnClickListener { thank(holder) }
        holder.itemView.post_thanks_ll2.setOnClickListener { thank(holder) }
        holder.itemView.setOnClickListener {
            if (post.type.equals(Post.postType.FOR_SALE)) {
                baseAcitivityListener.navigator.openActivity(SalePostDetailActivity::class.java, Bundle().apply {
                    putParcelable("post", post)
                    putParcelable("user", user)
                    putInt("position", position)

                })
            } else {
                val intent = Intent(context, PostDetailActivity::class.java).putExtras(Bundle().apply {
                    putParcelable("post", post)
                    putParcelable("user", user)
                    putInt("position", position)

                })
                (context as BaseActivity).startActivityForResult(intent, 900)
            }
        }
    }

    private fun thank(holder: BookmarkAdapter.MyViewHolder) {
        holder.itemView.post_thanks_ll2.isEnabled = false
        holder.itemView.post_thanks_ll.isEnabled = false
        val position = holder.adapterPosition
        if (Constants.IS_THANKED_LIST[position]) {
            removeThanks(holder)
        } else {
            postThanks(holder)

        }
    }

    private fun removeThanks(holder: BookmarkAdapter.MyViewHolder) {
        val position = holder.adapterPosition

        fireStoreHelper.removeThanks(postsList[position]!!.pid).addOnCompleteListener {
            if (it.isSuccessful) {

                fireStoreHelper.getThanksCount(postsList[position]!!.pid).addOnFailureListener {
                }.addOnCompleteListener {

                    if (it.isSuccessful) {

                        val count = it.result.documents.size
                        postsList[position]!!.thanksCount = count

                        fireStoreHelper.updatePost(post = postsList[position]!!, pid = postsList[position]!!.pid).addOnCompleteListener {

                            if (it.isSuccessful) {

                                Constants.IS_THANKED_LIST.set(position, false)
                                postsList.set(position, postsList[position]!!)
                                notifyItemChanged(position)
                                enableThank(holder)
                            }
                            else {
                                enableThank(holder)
                            }
                        }.addOnFailureListener {
                            enableThank(holder)
                        }
                    }
                    else {
                        enableThank(holder)
                    }
                }
            }
        }
    }



    private fun postThanks(holder: BookmarkAdapter.MyViewHolder) {
        val position = holder.adapterPosition
        fireStoreHelper.postThanks(postsList[position]!!.pid).addOnCompleteListener {
            if (it.isSuccessful) {

                fireStoreHelper.getThanksCount(postsList[position]!!.pid).addOnFailureListener {
                }.addOnCompleteListener {
                    if (it.isSuccessful) {

                        val count = it.result.documents.size
                        postsList[position]!!.thanksCount = count

                        fireStoreHelper.updatePost(post = postsList[position]!!, pid = postsList[position]!!.pid).addOnCompleteListener {
                            if (it.isSuccessful) {

                                //showThankedView(holder.itemView, position)
                                //holder.itemView.post_thanks_count.text = (updatedPost.thanksCount).toString()
                                Constants.IS_THANKED_LIST.set(position, true)
                                postsList.set(position, postsList[position]!!)
                                notifyItemChanged(position)
                                enableThank(holder)
                            }
                            else {
                                enableThank(holder)
                            }
                        }.addOnFailureListener {
                            enableThank(holder)
                        }
                    }
                    else {
                        enableThank(holder)
                    }
                }
            }
        }
    }


    private fun enableThank(holder: BookmarkAdapter.MyViewHolder) {
        holder.itemView.post_thanks_ll2.isEnabled = true
        holder.itemView.post_thanks_ll.isEnabled = true
    }


    override fun getItemCount(): Int {
        return postsList.size
    }

    fun removePost(position: Int) {
        postsList.removeAt(position)
        userList.removeAt(position)
        notifyItemRemoved(position)
    }


    inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view)

    interface removeBookmark {
        fun onRemoveBookmark(pid: String, position: Int)

    }
}
