package com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings.profile.fragment

import android.os.Bundle
import android.view.View
import com.example.manish.internetstatus.OttoBus
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.constants.ObjBiography
import com.sachtechsolution.neighbourhood.extension.showToast
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings.profile.FillUpProfileActivity
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_about_yourself.*

class AboutYourselfFragment : BaseFragment() {

    override fun viewToCreate(): Int {
        return R.layout.fragment_about_yourself
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnNext.setOnClickListener {
            val year = etYear.text.toString().toInt()
            if (etYear.text.isEmpty() || year > 2018 || year < 1930)
                activity!!.showToast("enter valid year")
            else if (etGrow.text.isEmpty())
                activity!!.showToast("enter a place")
            else
                UpdateData()
        }
    }

    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
    }


    @Subscribe
    fun getMessage(internetStatus: String) {

        if (internetStatus.equals("Not connected to Internet")) {
            InternetStatusDialogFragment.newInstance().show(fragmentManager, "option_fragment")
        } else {
            showToast("internet enabled ")

        }

    }


    private fun UpdateData() {
        ObjBiography.moveYear = etYear.text.toString()
        ObjBiography.growUp = etGrow.text.toString()
        baseAcitivityListener.navigator.replaceFragment(AboutWhatDoFragment::class.java, true)
    }

    override fun onResume() {
        super.onResume()
        OttoBus.bus.register(this)
        FillUpProfileActivity.mcount!!.text = "1 of 4"
    }
}