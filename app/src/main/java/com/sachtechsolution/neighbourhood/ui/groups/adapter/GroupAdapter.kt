package com.sachtechsolution.neighbourhood.ui.groups.adapter

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseAcitivityListener
import com.sachtechsolution.neighbourhood.constants.Constants
import com.sachtechsolution.neighbourhood.extension.openActivity
import com.sachtechsolution.neighbourhood.model.Group
import com.sachtechsolution.neighbourhood.ui.MainActivity
import com.sachtechsolution.neighbourhood.ui.groups.GroupDetailFragment
import kotlinx.android.synthetic.main.item_group.view.*

class GroupAdapter(val context: Context) : RecyclerView.Adapter<GroupAdapter.ViewHolder>() {

    val groupsList: ArrayList<Group> = ArrayList()
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val v: View = LayoutInflater.from(p0.context).inflate(R.layout.item_group, p0, false)
        return ViewHolder(v)
    }

    override fun getItemCount(): Int {
        return groupsList.size
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        val group = groupsList[p1]
        p0.itemView.created_by.text = "Created on "+group.createdOn
        p0.itemView.apply {
            groupName.text = group.name
        }
        p0.itemView.setOnClickListener {
            var intent=Intent(context,MainActivity::class.java)
            intent.putExtra("gName",group)
            context!!.startActivity(intent)
           /* //Constants.POSITION=-2
            baseAcitivityListener.navigator.replaceFragment(GroupDetailFragment::class.java, true,
                    Bundle().apply { putParcelable("group",group) })*/
        }
    }

    fun addGroup(group: Group) {
        if (!groupsList.contains(group)) {
            groupsList.add(group)
            notifyItemChanged(groupsList.size - 1)
        }
    }

    class ViewHolder(v: View) : RecyclerView.ViewHolder(v)
}