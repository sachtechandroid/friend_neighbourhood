package com.sachtechsolution.neighbourhood.ui.home.fragments

import android.os.Bundle
import android.view.View
import com.example.manish.internetstatus.OttoBus
import com.google.firebase.firestore.DocumentSnapshot
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.gone
import com.sachtechsolution.neighbourhood.extension.visible
import com.sachtechsolution.neighbourhood.model.User
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.sachtechsolution.neighbourhood.ui.home.adapters.MemberListAdapter
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_list.*

class ListFRagment : BaseFragment() {
    var mAdapter: MemberListAdapter? = null
    var list = ArrayList<User>()
    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }
    val currentUser by lazy { pref.getUserFromPref() }
    val currentUserId by lazy { fireStoreHelper.currentUserUid() }
    override fun viewToCreate(): Int {
        return R.layout.fragment_list
    }

    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
    }

    @Subscribe
    fun getMessage(internetStatus: String) {

        if (internetStatus.equals("Not connected to Internet")) {
            InternetStatusDialogFragment.newInstance().show(fragmentManager, "option_fragment")
        } else {
            showToast("internet enabled ")

        }
    }

    override fun onResume() {
        super.onResume()
        OttoBus.bus.register(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mAdapter = MemberListAdapter(this!!.activity!!)
        members_list.adapter = mAdapter
    }

    fun setList(list: MutableList<DocumentSnapshot>) {
        if (list?.size ?: 1 == 1) {
            neighbors__message.text = "No neighbor found"
            neighbors__message.visible()
        } else {
            neighbors__message.gone()
            mAdapter?.add(list)
        }
        hideProgress()

    }
}