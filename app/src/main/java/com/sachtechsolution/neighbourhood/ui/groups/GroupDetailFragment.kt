package com.sachtechsolution.neighbourhood.ui.groups

import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.View
import com.example.manish.internetstatus.OttoBus
import com.google.firebase.firestore.DocumentSnapshot
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.constants.Constants
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.*
import com.sachtechsolution.neighbourhood.model.Group
import com.sachtechsolution.neighbourhood.model.Post
import com.sachtechsolution.neighbourhood.model.User
import com.sachtechsolution.neighbourhood.ui.home.adapters.HomePagePostAdapter
import com.sachtechsolution.neighbourhood.ui.home.fab.fragment.message.fragment.CreatePostFragment
import kotlinx.android.synthetic.main.fragment_group_detail.*

//import kotlinx.android.synthetic.main.general_post_item.*

class GroupDetailFragment : BaseFragment() {
    override fun viewToCreate(): Int {
        return R.layout.fragment_group_detail
    }

    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }
    val group by lazy { activity!!.intent.getParcelableExtra("group") as Group }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListeners()
        msg_text.gone()
        getPosts()
        hideKeyboard(activity!!)
    }

    lateinit private var postLis: MutableList<DocumentSnapshot>

    private fun getPosts() {
        showProgress()
        msg_text.visible()
        msg_text.text = "Loading posts"
        fireStoreHelper.getGroupPost(gid = group.gid).addOnCompleteListener {
            if (it.isSuccessful) {
                hideProgress()
                hideProgress()
                postLis = it.result.documents
                if (postLis.isEmpty())
                    msg_text.setText("No post yet. To create post click on +.")
                postCount = -1
                getPost()
            } else {
                //showToast("something went wrong")
            }
        }.addOnFailureListener {
            Log.e("", "")
        }
    }

    private var postCount = -1

    private fun getPost() {
        postCount++
        if (postLis.size > postCount) {
            var post = postLis[postCount].toObject(Post::class.java)
            post!!.pid = postLis[postCount].id
            /*if (isPostHasTime(post.date.time) && home_welcome_text != null)*/
            getPostUser(post)
        }
    }


    private fun getPostUser(post: Post) {
        fireStoreHelper.getUser(post!!.uid).addOnCompleteListener {
            if (it.isSuccessful) {
                if (it.result.exists()) {
                    val user = it.result.toObject(User::class.java)
                    fireStoreHelper.getThanks(post.pid).addOnCompleteListener {
                        if (it.isSuccessful) {
                            msg_text?.gone()
                            var isThanked = false
                            if (it.result.exists())
                                isThanked = true
                            if (post.type.equals(Post.postType.POLL)) {
                                fireStoreHelper.getIsPollAnswered(post.pid).addOnCompleteListener {
                                    if (it.isSuccessful) {
                                        var isPolled = false
                                        if (it.result.documents.size > 0)
                                            isPolled = true
                                        adapter!!.addItem(post, user!!, isThanked, isPolled)
                                        getPost()
                                    } else {
                                        adapter!!.addItem(post, user!!, isThanked, false)
                                        getPost()
                                    }
                                }.addOnFailureListener {
                                    getPost()
                                    showToast("")
                                }.addOnCanceledListener {
                                    showToast("")
                                    getPost()
                                }
                            } else {
                                getPost()
                                adapter!!.addItem(post, user!!, isThanked, false)
                            }
                            msg_text?.gone()
                        } else {
                            getPost()
                        }
                    }
                }
            }
        }.addOnFailureListener { getPost() }
    }

    override fun onResume() {
        super.onResume()
        OttoBus.bus.register(this)

        /*if (Constants.POSITION == -2) {
            adapter.clear()
            getPosts()
            Constants.POSITION=-1
        }*/
/*
        if (Constants.POSITION != -1 && Constants.POSITION != -2) {
            if (Constants.POST != null)
                adapter?.postsList?.set(Constants.POSITION, Constants.POST)
            adapter!!.notifyItemChanged(Constants.POSITION)
            Constants.POSITION = -1
            Constants.POST = null
        }
        if (Constants.DELETED_POSITION != -1) {
            val position = Constants.DELETED_POSITION
            adapter!!.deletePost(position)
            Constants.DELETED_POSITION = -1
        }
*/
        //intracterListner!!.updateSelectedPos(0)
    }

    lateinit private var adapter: HomePagePostAdapter

    private fun setListeners() {
        createGroupMessage.setOnClickListener {
            val message=groupMessageEt.text.toString().trim()
            if (message.length>0){
                showProgress()
                val post = Post( message = message,  uid = fireStoreHelper.currentUserUid(),
                        type = Post.postType.MESSAGE)
                fireStoreHelper.createGroupPost(group.gid,post).addOnCompleteListener {
                    if (it.isSuccessful) {
                        hideProgress()
                        getPosts()
                        //activity!!.onBackPressed()
                    } else {
                        hideProgress()
                        showToast("try again")
                    }

                }.addOnFailureListener {
                    showToast("try again")
                    hideProgress()
                }
            }else
                showToast("Enter message")
        }
        groupDetail_btnBack.setOnClickListener {
            activity!!.onBackPressed()
        }
        groupName.text = group.name
        groupName.setOnClickListener { getGroupInfo() }
        groupInfo.setOnClickListener {  getGroupInfo() }
        createGroupPost.setOnClickListener {

            if (context!!.isLocationEnabled(context!!)&&context!!.getLocationMode() == 3) {
                baseAcitivityListener.navigator.replaceFragment(CreatePostFragment::class.java, true,
                        Bundle().apply {
                            putString("id", Post.postType.GROUP)
                            putString("gid", group.gid)
                        })}
            else
            {
                context!!.showToast("Enable Location with high accuracy option")
                startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
            }

        }
        //if (Constants.POSITION==-2) {
        adapter = HomePagePostAdapter(activity!!, null, baseAcitivityListener, pref)
        groupPosts.adapter = adapter
        //}
    }

    private fun getGroupInfo() {
        baseAcitivityListener.navigator.replaceFragment(GroupInfoFragment::class.java, true,
                Bundle().apply {
                    putParcelable("group", group)
                })
    }
}
