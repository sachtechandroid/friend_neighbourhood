package com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings.profile

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.v4.content.FileProvider
import android.view.View
import android.widget.SeekBar
import com.bumptech.glide.Glide
import com.example.manish.internetstatus.OttoBus
import com.google.firebase.storage.StorageReference
import com.sachtechsolution.neighbourhood.BuildConfig
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.showToast
import com.sachtechsolution.neighbourhood.model.Family
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.sachtechsolution.neighbourhood.ui.home.fab.fragment.message.dialogs.SelectOptionDialogFragment
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_add_child.*
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class AddChildFragment : BaseFragment(), View.OnClickListener,SelectOptionDialogFragment.onOptionSelect {

    private var TAKE_PICTURE = 12
    private var GALLERY = 13
    var selectedName = ""
    var selectedAge = "2"
    private var selectedImage: Uri? = null
    lateinit var mRef: StorageReference
    val fireStoreHelper: FireStoreHelper by lazy { FireStoreHelper.getInstance() }

    override fun forGallery() {
        var intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(intent, GALLERY)
    }


    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
    }


    @Subscribe
    fun getMessage(internetStatus:String) {

        if (internetStatus.equals("Not connected to Internet")){
            InternetStatusDialogFragment.newInstance().show(fragmentManager, "option_fragment")
        }else {
            showToast("internet enabled ")

        }

    }

    override fun onResume() {
        super.onResume()
        OttoBus.bus.register(this)
    }
    var photoURI: Uri?=null
    override fun forCamera() {
        try {
            dispatchTakePictureIntent();
        } catch ( e: IOException) {
        }
    }

    @Throws(IOException::class)
    private fun dispatchTakePictureIntent() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(context!!.packageManager) != null) {
            // Create the File where the photo should go
            var photoFile: File? = null
            try {
                photoFile = createImageFile()
            } catch (ex: IOException) {
                // Error occurred while creating the File
                return
            }

            // Continue only if the File was successfully created
            if (photoFile != null) {

                photoURI = FileProvider.getUriForFile(context!!,
                        BuildConfig.APPLICATION_ID + ".provider",
                        createImageFile())
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                startActivityForResult(takePictureIntent,TAKE_PICTURE)
            }
        }
    }
    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "JPEG_" + timeStamp + "_"
        val storageDir = File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM), "Camera")
        val image = File.createTempFile(
                imageFileName, /* prefix */
                ".jpg", /* suffix */
                storageDir      /* directory */
        )
        return image
    }



    override fun onClick(p0: View?) {
        when(p0!!.id)
        {
            R.id.txtCancel -> activity!!.onBackPressed()
            R.id.txtAdd -> uploadImage(selectedImage)
            R.id.ivCamera -> SelectOptionDialogFragment.newInstance(this)
                    .show(fragmentManager, "option_fragment")

        }
    }

    override fun viewToCreate(): Int {
        return R.layout.fragment_add_child
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        //listner
        txtCancel.setOnClickListener(this)
        txtAdd.setOnClickListener(this)
        ivCamera.setOnClickListener(this)
        seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                yearold.visibility = View.VISIBLE
                selectedAge=progress.toString()
                yearold.text = progress.toString() + " years old"
                if (progress < 5) {
                    ivIcon.setImageResource(R.drawable.seek_one)
                    icon1.setColorFilter(R.color.optional)
                    icon2.clearColorFilter()
                    icon3.clearColorFilter()
                    icon4.clearColorFilter()

                } else if (progress in 5..8) {
                    ivIcon.setImageResource(R.drawable.seek_two)
                    icon2.setColorFilter(R.color.optional)
                    icon1.clearColorFilter()
                    icon3.clearColorFilter()
                    icon4.clearColorFilter()

                } else if (progress in 9..12) {
                    ivIcon.setImageResource(R.drawable.seek_three)
                    icon3.setColorFilter(R.color.optional)
                    icon2.clearColorFilter()
                    icon1.clearColorFilter()
                    icon4.clearColorFilter()

                } else if (progress in 13..18) {
                    ivIcon.setImageResource(R.drawable.seek_four)
                    icon4.setColorFilter(R.color.optional)
                    icon2.clearColorFilter()
                    icon3.clearColorFilter()
                    icon1.clearColorFilter()

                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar) {
                txtselect.visibility = View.INVISIBLE
            }

            override fun onStopTrackingTouch(seekBar: SeekBar) {

            }
        })
    }

    private fun uploadData(image: String) {

        var data = Family.Child(name = selectedName, imageLink = image, age = selectedAge)
        fireStoreHelper.updateChild(data, "child",pref.getUserFromPref()!!.uid).addOnCompleteListener {

            if (it.isSuccessful) {
                showToast("Updated")
                hideProgress()
                activity!!.onBackPressed()

            } else {
                showToast("error")
                hideProgress()
            }


        }.addOnFailureListener {
            showToast("error")
            hideProgress()
        }


    }



    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            TAKE_PICTURE -> if (resultCode == Activity.RESULT_OK) {
                setImage(photoURI)
            }

            GALLERY -> {
                if (data != null) {
                    val selectedImage = data!!.data
                    this.selectedImage = selectedImage
                    setImage(selectedImage)
                }

            }
        }
    }

    fun setImage(uri: Uri?) {
        Glide.with(activity!!).load(uri).into(ivIcon)
    }

    private fun uploadImage(selectedImage: Uri?) {
        selectedName=etName.text.toString().trim()
        if (selectedName.isEmpty())
            activity!!.showToast("enter a name")
        else if(selectedImage==null)
            activity!!.showToast("select a image")
        else {
            showProgress()
            var time = java.sql.Timestamp(Date().time)
            var timestamp = time.toString().replace(":", "").replace("-", "").replace(" ", "").replace(".", "")
            mRef = fireStoreHelper.getStorageRef().child("Family").child(fireStoreHelper.currentUserUid() + timestamp)
            mRef.putFile(selectedImage!!).addOnCompleteListener {
                if (it.isSuccessful) {
                    it.result
                    mRef.downloadUrl.addOnSuccessListener { p0 -> uploadData(p0.toString()) }.addOnFailureListener {
                    }
                }
            }.addOnFailureListener {
                hideProgress()
            }
        }
    }
}
