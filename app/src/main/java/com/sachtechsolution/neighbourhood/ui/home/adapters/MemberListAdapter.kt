package com.sachtechsolution.neighbourhood.ui.home.adapters

import android.content.Context
import android.content.Intent
import android.provider.Settings
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.firebase.firestore.DocumentSnapshot
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.*
import com.sachtechsolution.neighbourhood.model.User
import com.sachtechsolution.neighbourhood.ui.home.AnotherUserProfileActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_member_descirption.view.*

class MemberListAdapter(val context: Context, val boolean: Boolean = false, val boolean2: Boolean = false) : RecyclerView.Adapter<MemberListAdapter.ViewHolder>() {
    //var userList:
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.item_member_descirption, p0, false)
        return ViewHolder(v)
    }

    val cuid by lazy { FireStoreHelper.getInstance().currentUserUid() }
    override fun getItemCount(): Int {
        return userList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val user = userList[position]
        holder.itemView.userName.text = user.firstName + " " + user.lastName
        holder.itemView.userAddress.text = user.society
        if (user.imageLink.isNotEmpty()) {
            holder.itemView.progressBar.visible()
            Picasso.with(context)
                    .load(user.imageLink)
                    .into(holder.itemView.profilePic, object : com.squareup.picasso.Callback {
                        override fun onError() {
                            holder.itemView.progressBar.visible()
                        }

                        override fun onSuccess() {
                            holder.itemView.progressBar.gone()
                        }

                    })
        }
        holder.itemView.setOnClickListener {
                if (!user.uid.equals(cuid)&&context!!.getLocationMode() != 3) {
                    context.showToast("please select high accuracy option")
                    context.startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
                } // do stuff
                else
                context.openActivity<AnotherUserProfileActivity>("uid", user.uid)
        }
        if (boolean) {
            holder.itemView.groupAction.visible()
            holder.itemView.groupAction.setOnClickListener {
                if (selectedList.contains(user.uid)) {
                    selectedList.remove(user.uid)
                    holder.itemView.groupAction.text = "add"
                } else {
                    selectedList.add(user.uid)
                    holder.itemView.groupAction.text = "remove"
                }
                notifyItemChanged(position)
            }
            if (selectedList.contains(user.uid)) {
                holder.itemView.groupAction.text = "remove"
            } else {
                holder.itemView.groupAction.text = "add"
            }
        } else if (boolean2) {
            holder.itemView.groupAction.visible()
            holder.itemView.groupAction.text = "remove"
            holder.itemView.groupAction.setOnClickListener {
                userList.removeAt(position)
                userIdList.removeAt(position)
                notifyItemRemoved(position)
                //selectedList.add(user.uid)
            }
        } else
            holder.itemView.groupAction.gone()
        if (user.uid.equals(cuid)) {
            holder.itemView.groupAction.gone()
        }
        // Glide.with(context).load(user.imageLink).into(holder.itemView.profilePic)
    }

    var userList: ArrayList<User> = ArrayList()
    var userIdList: ArrayList<String> = ArrayList()
    var selectedList = ArrayList<String>()

    fun add(list: MutableList<DocumentSnapshot>?) {
        userList.clear()
        list?.forEach {

                val user = it.toObject(User::class.java)!!
                if (!user.uid.equals(cuid))
                {
                    userList.add(user)
                    userIdList.add(user.uid)
                }

        }
        notifyDataSetChanged()
        //notifyItemInserted(userList.size-1)
    }

    fun add2(list: MutableList<DocumentSnapshot>?, members: ArrayList<String>) {
        userList.clear()
        list?.forEach {
            val user = it.toObject(User::class.java)!!
            if (!members.contains(user.uid)) {
                userList.add(user)
                userIdList.add(user.uid)
                notifyDataSetChanged()
            }
        }

        //notifyItemInserted(userList.size-1)
    }

    fun addUser(user: User) {
        if (!cuid.equals(user.uid)) {
            userList.add(user)
            userIdList.add(user.uid)
            notifyItemInserted(userList.size - 1)
        }
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }
}