package com.bususer.tiket.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.Toast
import com.sachtechsolution.neighbourhood.R




class SelectCategoryAdapter(var listner :SelectCategory, var context: Context, var offersList: ArrayList<String>) : RecyclerView.Adapter<SelectCategoryAdapter.ViewHolder>() {

    private var lastSelectedPosition = -1

    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): SelectCategoryAdapter.ViewHolder {

        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_sale_list, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: SelectCategoryAdapter.ViewHolder,
                                  position: Int) {
        holder.selectionState.text=offersList[position]
        //since only one radio button is allowed to be selected,
        // this condition un-checks previous selections
        holder.selectionState.isChecked = lastSelectedPosition == position
    }

    override fun getItemCount(): Int {
        return offersList.size
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {


        var selectionState: RadioButton = view.findViewById<View>(R.id.sale_name) as RadioButton

        init {

            selectionState.setOnClickListener {
                lastSelectedPosition = adapterPosition
                notifyDataSetChanged()

                listner.OnSelectCategory(selectionState.text.toString())

            }
        }
    }

    interface SelectCategory {

        fun OnSelectCategory(string: String)
    }
}
