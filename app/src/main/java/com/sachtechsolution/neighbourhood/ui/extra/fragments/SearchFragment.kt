package com.sachtechsolution.neighbourhood.ui.extra.fragments

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import com.example.manish.internetstatus.OttoBus
import com.google.firebase.firestore.DocumentSnapshot
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.constants.Constants
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.gone
import com.sachtechsolution.neighbourhood.extension.hideKeyboard
import com.sachtechsolution.neighbourhood.extension.isPostHasTime
import com.sachtechsolution.neighbourhood.model.Ads
import com.sachtechsolution.neighbourhood.model.Post
import com.sachtechsolution.neighbourhood.model.User
import com.sachtechsolution.neighbourhood.model.places_bussiness.ResultsItem
import com.sachtechsolution.neighbourhood.ui.home.adapters.SearchAdapter
import com.sachtechsolution.neighbourhood.ui.home.fragments.SearchBean
import kotlinx.android.synthetic.main.fragment_search.*

class SearchFragment : BaseFragment() {

    val adapter by lazy { SearchAdapter(activity!!, baseAcitivityListener, pref) }
    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }
    var postList=ArrayList<SearchBean>()
    //lateinit var post: Post
    //val mainLocality by lazy { pref.getUserFromPref()?.mainLocality?: "" }
    val user by lazy { pref.getUserFromPref()!! }

    lateinit var postLis: MutableList<DocumentSnapshot>
    var postCount = -1
    private var adsList = ArrayList<Ads>()
    private var adsTotal: Int = 0
    private var adsCount: Int = 0

    override fun viewToCreate(): Int {
        return R.layout.fragment_search
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerView.adapter = adapter
        Constants.IS_THANKED_LIST.clear()
        Constants.IS_POLLED_LIST.clear()

        search_back_iv.setOnClickListener {
            hideKeyboard(activity!!)
            activity?.onBackPressed()
        }
        getAllData()

        etSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {
                val filterlist=filterList(etSearch.text.toString(),postList)
                adapter.postList.clear()
                if (filterlist.isEmpty()) {
                    adapter.update(postList)
                    adapter.notifyDataSetChanged()
                }
                else {
                    adapter.update(filterlist as ArrayList<SearchBean>)
                    adapter.notifyDataSetChanged()
                }

            }
        })
    }

    private fun filterList(text: String, list: ArrayList<SearchBean>): List<SearchBean> {

        return list.filter {
            if (it?.post!!.subject.isNotEmpty()) {
                it?.post!!.subject?.contains(text, true)
            }

           else
                it?.post!!.poll!!.question?.contains(text,true)!!
        }/*.filter {
            if (it?.post!!.sale!=null)
                it?.post!!.sale!!.title.contains(text,true)

            else
                it?.post!!.message.contains(text,true)
        }
*/
    }
    var previousSize: Int = 0

    fun getAllData() {

        fireStoreHelper.getAds().addOnCompleteListener {
            if (it.isSuccessful) {
                it.result.forEach {
                    val ads = it.toObject(Ads::class.java)
                    adsList!!.add(ads)
                }
                adsList.shuffle()
                adsCount = 0
                adsTotal = adsList.size
                previousSize = 0
                postAds()
            }
        }

        fireStoreHelper.getAllPosts(zipCode = user.zipCode, society = user.society).addOnCompleteListener {
            if (it.isSuccessful) {
                postLis = it.result.documents
                getPost()
            } else {
                //showToast("something went wrong")
            }
        }.addOnFailureListener {
            Log.e("", "")
        }

    }


    fun postAds() {
        if (adsTotal > adsCount && adapter!!.postList.size > previousSize + (3..6).shuffled().first()) {
            adapter!!.addItem(SearchBean(Post(type = Post.postType.ADS, message = adsList[adsCount].title, imagesLinks = adsList[adsCount].imageLink)))
            previousSize = adapter!!.postList.size
            adsCount++
        }
    }

    fun getPost() {
        postCount++
        if (postLis.size > postCount) {
            var post = postLis[postCount].toObject(Post::class.java)
            post!!.pid = postLis[postCount].id
            if (isPostHasTime(post.date) && home_welcome_text != null)
                getPostUser(post)
        }
    }

    fun getPostUser(post: Post) {
        if (post.uid.isNullOrEmpty())
            getPost()
        else {
            fireStoreHelper.getUser(post?.uid).addOnCompleteListener {
                if (it.isSuccessful) {
                    if (it.result.exists()) {
                        val user = it.result.toObject(User::class.java)
                        fireStoreHelper.getThanks(post.pid).addOnCompleteListener {
                            if (it.isSuccessful) {
                                var isThanked = false
                                if (it.result.exists())
                                    isThanked = true
                                if (post.type.equals(Post.postType.POLL)) {
                                    fireStoreHelper.getIsPollAnswered(post.pid).addOnCompleteListener {
                                        if (it.isSuccessful) {
                                            var isPolled = false
                                            if (it.result.documents.size > 0)
                                                isPolled = true
                                            var bean = SearchBean(post = post, user = user, thanked = isThanked, polled = isPolled)
                                            adapter.addItem(bean)
                                            adapter.notifyDataSetChanged()
                                            postList.add(bean)
                                            getPost()
                                        } else {
                                            var bean = SearchBean(post = post, user = user, thanked = isThanked, polled = false)
                                            postList.add(bean)
                                            adapter!!.addItem(bean)
                                            adapter.notifyDataSetChanged()
                                            getPost()
                                        }
                                    }.addOnFailureListener {
                                        getPost()
                                        showToast("")
                                    }.addOnCanceledListener {
                                        showToast("")
                                        getPost()
                                    }
                                } else {
                                    getPost()
                                    if (adsTotal > 0)
                                        postAds()
                                    var bean = SearchBean(post = post, user = user, thanked = isThanked, polled = false)
                                    postList.add(bean)
                                    adapter.addItem(bean)
                                    adapter.notifyDataSetChanged()
                                }
                                home_welcome_text?.gone()
                            } else {
                                getPost()
                            }
                        }
                    }
                }
            }.addOnFailureListener { getPost() }
        }
    }

    override fun onResume() {
        super.onResume()
        OttoBus.bus.register(this)
        //getAllPost()
        if (Constants.POSITION != -1) {
            if (Constants.POST != null)
                adapter?.postList?.set(Constants.POSITION, SearchBean(Constants.POST))
            adapter!!.notifyItemChanged(Constants.POSITION)
            Constants.POSITION = -1
            Constants.POST = null
        }

        if (Constants.DELETED_POSITION != -1) {
            val position = Constants.DELETED_POSITION
            adapter!!.deletePost(position)
            Constants.DELETED_POSITION = -1
        }
        intracterListner!!.updateSelectedPos(0)
    }


}
