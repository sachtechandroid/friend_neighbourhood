package com.sachtechsolution.neighbourhood.ui.home.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseAcitivityListener
import com.sachtechsolution.neighbourhood.ui.home.fragments.ActivityCommentDescription

class CommentsAdapter(context: Context, val baseAcitivityListener: BaseAcitivityListener):RecyclerView.Adapter<CommentsAdapter.ViewHolder>() {


    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {

        val v=LayoutInflater.from(p0.context).inflate(R.layout.item_comments,p0,false)
        var viewHolder=ViewHolder(v)

        return viewHolder
    }

    override fun getItemCount(): Int {
        return 10
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
        p0.itemView.setOnClickListener { baseAcitivityListener.navigator.openActivity(ActivityCommentDescription::class.java)}
        }

    class ViewHolder(itemView: View):RecyclerView.ViewHolder(itemView) {

    }
}