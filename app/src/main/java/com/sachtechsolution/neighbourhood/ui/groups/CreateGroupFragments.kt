package com.sachtechsolution.neighbourhood.ui.groups

import android.os.Bundle
import android.os.Handler
import android.view.View
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.model.Group
import com.sachtechsolution.neighbourhood.model.User
import com.sachtechsolution.neighbourhood.ui.home.adapters.MemberListAdapter
import kotlinx.android.synthetic.main.fragment_create_group.*
import java.sql.Timestamp
import java.text.SimpleDateFormat
import java.util.*

/* whoGuri 10/10/18 */

class CreateGroupFragments : BaseFragment() {

    override fun viewToCreate(): Int {
        return R.layout.fragment_create_group
    }
    val fireBaseHelper by lazy { FireStoreHelper.getInstance() }

    private var mAdapter: MemberListAdapter? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setListener()
        mAdapter = MemberListAdapter(this!!.activity!!, true,false)
        neighbors_list.adapter = mAdapter
        getNeighborsByZip()
    }

    val uid by lazy { fireBaseHelper.currentUserUid() }
    private fun setListener() {
        createGroupBack.setOnClickListener { activity!!.onBackPressed() }
        create_group.setOnClickListener {
            val name = groupName.text.toString()
            val desc = groupDescription.text.toString()
            //val list = ArrayList<String>()
            //list.add(fireBaseHelper.currentUserUid())
            if (name.isNullOrEmpty())
                showToast("Enter Group name")
            else if (desc.isNullOrEmpty())
                showToast("Enter Group Descrption")
            else if (mAdapter!!.selectedList.size==0)
                showToast("Select neighbors to add in group")
            else {
                showProgress()
                mAdapter!!.selectedList.add(uid)
                val pattern = "dd-MM-yyyy"
                val dateInString = SimpleDateFormat(pattern).format(Date())

                fireBaseHelper.checkGroup(name).addOnCompleteListener {
                    if (it.isSuccessful)
                    {
                       if (it.result.exists())
                       {
                           hideProgress()
                           showToast("Group already exist")
                       }
                        else{

                           fireBaseHelper.createGroup(Group(name = name, desc = desc, members = mAdapter!!.selectedList,
                                   membersCount = mAdapter!!.selectedList.size,createdOn= dateInString,
                                   zipCode = pref.getUserFromPref()!!.zipCode,createdBy = fireBaseHelper.currentUserUid())).addOnCompleteListener {
                               if (it.isSuccessful) {
                                   val gid = it.result.id
                                   mAdapter!!.selectedList.forEach {
                                       val id = it
                                       fireBaseHelper.getUser(id).addOnCompleteListener {
                                           if (it.isSuccessful) {
                                               val user = it.result.toObject(User::class.java)
                                               if (user!=null)
                                               {
                                                   user?.groups?.add(gid)
                                                   fireBaseHelper.setUser(id, user!!)
                                               }
                                           }
                                       }
                                   }
                                   Handler().postDelayed({
                                       val user = pref.getUserFromPref()
                                       user!!.groups.add(gid)
                                       pref.setUserToPref(user)
                                       hideProgress()
                                       activity!!.onBackPressed()
                                   }, 2000)
                               } else {
                                   hideProgress()
                                   showError()
                               }
                           }.addOnFailureListener {
                               hideProgress()
                               showError() }
                       }
                    }
                }.addOnFailureListener {
                    showError()
                    hideProgress() }

            }
        }
    }

    private fun showError() {
        hideProgress()
        showToast("Try again")
    }

    private fun getNeighborsByZip() {
        showProgress()
        fireBaseHelper.getAllNeighbors(pref.getUserFromPref()!!.zipCode).addOnCompleteListener {
            if (it.isSuccessful) {
                mAdapter?.add(it.result.documents)
                mAdapter!!.notifyDataSetChanged()
                hideProgress()
            } else {
                hideProgress()
                showToast("something went wrong")
            }
        }.addOnFailureListener {
            hideProgress()
            showToast("something went wrong")
        }
    }

}