package com.sachtechsolution.neighbourhood.ui.home.adapters

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseAcitivityListener

import com.sachtechsolution.neighbourhood.ui.home.fragments.FragmentCategory
import kotlinx.android.synthetic.main.item_categories.view.*

class CategoriesAdapter(context: Context,val baseAcitivityListener: BaseAcitivityListener): RecyclerView.Adapter<CategoriesAdapter.ViewHolder>() {


    var list= arrayListOf(R.drawable.dentists_icon,R.drawable.restraunt_icon,R.drawable.gym,
           R.drawable.mall,R.drawable.hair,R.drawable.atm,
           R.drawable.plumbers_icon,R.drawable.hospital,R.drawable.real_estate_icon)

    var nameList= arrayListOf("Dentists","Restraunts","Gym",
            "Shopping mall","Hair care","ATM",
            "Plumbers","Hospitals","Real estate agents")


    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.item_categories,p0, false)
        var viewHolder = ViewHolder(v)
        return viewHolder

    }

    override fun getItemCount(): Int {
        return 9
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.itemView.categories_image.setImageResource(list[position])
        holder.itemView.categories_name.text=nameList[position]
        holder.itemView.setOnClickListener {
            var bundle=Bundle()
            bundle.putString(FragmentCategory.CATEGORY,nameList[position])
           baseAcitivityListener.navigator.replaceFragment(FragmentCategory::class.java,true,bundle)
        }
    }

    class ViewHolder(itemView: View):RecyclerView.ViewHolder(itemView) {

    }
}