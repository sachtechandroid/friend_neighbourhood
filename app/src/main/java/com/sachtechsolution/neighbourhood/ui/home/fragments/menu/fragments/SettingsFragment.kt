package com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments

import android.os.Bundle
import android.view.View
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.extension.openLink
import com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings.AccountFragment
import com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings.EmailFragment
import com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings.MobileNotification
import com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings.ContactUs
import kotlinx.android.synthetic.main.fragment_settings.*

class SettingsFragment:BaseFragment(), View.OnClickListener {


    override fun viewToCreate(): Int {
        return R.layout.fragment_settings
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        //listner
        btnBack.setOnClickListener(this)
        accountLayout.setOnClickListener(this)
        mobile_options.setOnClickListener(this)
        email_notification.setOnClickListener(this)
        contact_us.setOnClickListener(this)
        help.setOnClickListener(this)


    }

    override fun onClick(p0: View?) {
        when(p0!!.id)
        {
            R.id.btnBack->activity!!.onBackPressed()

            R.id.accountLayout-> baseAcitivityListener.navigator.replaceFragment(AccountFragment::class.java,true)

           // R.id.nearbyNeighbourLayout-> baseAcitivityListener.navigator.replaceFragment(NearByNeighbourhoods::class.java,true)

            R.id.contact_us-> baseAcitivityListener.navigator.replaceFragment(ContactUs::class.java,true)

            R.id.mobile_options -> baseAcitivityListener.navigator.replaceFragment(MobileNotification::class.java,true)

            R.id.email_notification -> baseAcitivityListener.navigator.replaceFragment(EmailFragment::class.java,true)

            R.id.help ->  context!!.openLink("http://www.google.com")


        }
    }
}