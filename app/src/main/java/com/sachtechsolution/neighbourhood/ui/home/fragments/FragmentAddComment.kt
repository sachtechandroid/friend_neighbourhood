package com.sachtechsolution.neighbourhood.ui.home.fragments

import android.os.Bundle
import android.view.View
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_add_comment.*

class FragmentAddComment:BaseFragment() {
    override fun viewToCreate(): Int {
        return R.layout.fragment_add_comment
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        clear_comment.setOnClickListener {
            baseAcitivityListener.navigator.replaceFragment(FragmentBusinessDetails::class.java,true)
        }
    }
}