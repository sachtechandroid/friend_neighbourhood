package com.sachtechsolution.neighbourhood.ui.home.fab.fragment.urgent_alert

import android.os.Bundle
import android.view.View
import com.example.manish.internetstatus.OttoBus
import com.sachtechsolution.neighbourhood.HomePage
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.constants.Constants
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.showToast
import com.sachtechsolution.neighbourhood.intracter.Api_Hitter
import com.sachtechsolution.neighbourhood.model.Post
import com.sachtechsolution.neighbourhood.model.User
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_sendmessage.*
import kotlinx.android.synthetic.main.send_toolbar.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class FragmentUrgentAlert : BaseFragment() {

    override fun viewToCreate(): Int {
        return R.layout.fragment_urgent_alert
    }


    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
    }


    @Subscribe
    fun getMessage(internetStatus:String) {

        if (internetStatus.equals("Not connected to Internet")){
            InternetStatusDialogFragment.newInstance().show(fragmentManager, "option_fragment")
        }else {
            showToast("internet enabled ")

        }

    }

    override fun onResume() {
        super.onResume()
        OttoBus.bus.register(this)
    }

    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val list = ArrayList<String>()
        list.add(pref.getUserFromPref()!!.society)
        backLayout.setOnClickListener { activity!!.onBackPressed() }
        sendLayout.setOnClickListener {

            val message = etMessage.text.toString()
            if (message.isEmpty())
                activity!!.showToast("Please provide a subject")
            else {
                showProgress()
                val post = Post(subject = "Urgent Alert", message = message,
                        uid = fireStoreHelper.currentUserUid(), zipCode= pref.getUserFromPref()!!.zipCode,
                        type = Post.postType.ALERT, privacy = Constants.SOCIETIES,society = pref.getUserFromPref()!!.society)

                fireStoreHelper.createPost(post).addOnCompleteListener {
                    if (it.isSuccessful) {

                        fireStoreHelper.getAllNeighbors(pref.getUserFromPref()!!.zipCode).addOnCompleteListener {
                            if (it.isSuccessful) {
                                var token = ""
                                it.result.documents.forEach {
                                    if (!it.id.equals(pref.getUserFromPref()!!.uid)&& list.contains(it.toObject(User::class.java)!!.society))
                                        token = token + "," + it.toObject(User::class.java)!!.token
                                }
                                if (!token.isNullOrEmpty()) {
                                    token = token.substring(1)
                                    val title = "${pref.getUserFromPref()!!.firstName} ${pref.getUserFromPref()!!.lastName} post an urgent message in neighborhood"
                                    val callback = Api_Hitter.ApiInterface("http://stsmentor.com/").sendNotificationToAll(title, token, "zxc")
                                    callback.enqueue(object : Callback<ResponseBody> {
                                        override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                                            next()
                                        }

                                        override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                                            next()
                                        }
                                    })
                                } else {
                                    next()
                                }
                            }
                        }.addOnFailureListener {
                            next()
                        }

                    } else {
                        showToast("Try again")
                    }
                }.addOnFailureListener {
                    showToast("Try again")
                    hideProgress()
                }.addOnCanceledListener {
                    showToast("Try again")
                    hideProgress()
                }
            }
        }
    }

    private fun next() {
        hideProgress()
        showToast("Posted")
        baseAcitivityListener.navigator.openActivity(HomePage::class.java)
        activity!!.finish()
    }
}