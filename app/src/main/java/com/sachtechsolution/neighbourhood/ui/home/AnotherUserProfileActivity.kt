package com.sachtechsolution.neighbourhood.ui.home

import android.annotation.SuppressLint
import android.graphics.Color
import android.location.Location
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import com.bumptech.glide.Glide
import com.example.manish.internetstatus.OttoBus
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseActivity
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.*
import com.sachtechsolution.neighbourhood.model.*
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings.adapters.AllFamilyAdapter
import com.squareup.otto.Subscribe
import fisk.chipcloud.ChipCloud
import fisk.chipcloud.ChipCloudConfig
import kotlinx.android.synthetic.main.activity_another_user_profile.*
import java.util.*

class AnotherUserProfileActivity : BaseActivity(), OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, View.OnClickListener, GoogleMap.InfoWindowAdapter, AllFamilyAdapter.AddContact {

    var user: User? = null
    private lateinit var mMap: GoogleMap
    var familyList: ArrayList<AllFamily>? = null
    private lateinit var mGoogleClient: GoogleApiClient
    lateinit var mLocation: Location
    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }
    private var myMap: SupportMapFragment? = null
    private var markerView: View? = null

    override fun onContactClick(s: String, id: String, position: Int) {

    }

    override fun fragmentContainer(): Int {
        return 0
    }


    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
    }


    @Subscribe
    fun getMessage(internetStatus: String) {

        if (internetStatus.equals("Not connected to Internet")) {
            InternetStatusDialogFragment.newInstance().show(supportFragmentManager, "option_fragment")
        } else {
            showToast("internet enabled ")

        }

    }


    @SuppressLint("MissingPermission")
    override fun onMapReady(p0: GoogleMap?) {
        mMap = p0!!
        mMap.uiSettings.isZoomControlsEnabled = true
        mMap.isMyLocationEnabled = true

    }


    @SuppressLint("MissingPermission")
    override fun onConnected(p0: Bundle?) {
        if (checkLoc(context = this))
            mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleClient)

    }

    override fun onConnectionSuspended(p0: Int) {

    }

    override fun getInfoWindow(marker: Marker): View? {

        return prepareInfoView(marker)
    }

    override fun getInfoContents(marker: Marker): View {
        //return null;
        return prepareInfoView(marker)
    }

    @SuppressLint("SetTextI18n")
    private fun prepareInfoView(marker: Marker): View {
        //prepare InfoView programmatically
        return layoutInflater.inflate(R.layout.map_marker, null)
    }


    //click listner
    override fun onClick(p0: View?) {

        when (p0!!.id) {
            R.id.profile_back_iv -> onBackPressed()

        }
    }

    private fun upDateMarker(pafKietLocation: LatLng) {
        mMap.addMarker(MarkerOptions().position(pafKietLocation)
                .title("Your Location"))

        mMap.setMinZoomPreference(12.0f)
        mMap.setMaxZoomPreference(72.0f)
        mMap.moveCamera(CameraUpdateFactory.newLatLng(pafKietLocation))
        hideProgress()
    }

    override fun onResume() {
        super.onResume()

        OttoBus.bus.register(this)
        if (checkLoc(this!!) && this!!.networkAvaileble()) {
            myMap = supportFragmentManager.findFragmentById(R.id.map_fragment) as SupportMapFragment
            myMap?.getMapAsync(this)
            mGoogleClient = GoogleApiClient.Builder(this!!)
                    .addConnectionCallbacks(this)
                    .addApi(LocationServices.API)
                    .build()
            mGoogleClient.connect()
            markerView = LayoutInflater.from(this).inflate(R.layout.marker_layout, null)

            showProgress()
            fireStoreHelper.getUser(intent.getStringExtra("uid")).addOnCompleteListener {
                if (it.isSuccessful) {
                    if (it.result.exists()) {

                        user = it.result.toObject(User::class.java)
                        //val arrOfStr = user!!.latlng.split(",")
                        var latlang = LatLng(user!!.lat, user!!.lng)

                        name.text = user!!.firstName + " " + user!!.lastName
                        etAddress.text = user!!.society
                        if (user!!.imageLink.isNotEmpty())
                            Glide.with(this!!).load(user!!.imageLink).into(bigDp)

                        getInterest()
                        upDateMarker(latlang)
                        getSkills()
                        getLoveForNei()
                        getChild()
                        getContactInformation()
                        getBiography()
                        getUser()
                    }
                }
            }
        } else {
            showToast("Your location is disabled")
        }

    }


    private fun setChipLayoutForAboutNhood(list: List<String>) {

        cvAboutNhood.visible()
        layoutLoveNei.gone()
        val config = ChipCloudConfig()
                .selectMode(ChipCloud.SelectMode.multi)
                .checkedChipColor(resources.getColor(R.color.optional))
                .checkedTextColor(Color.parseColor("#ffffff"))
                .uncheckedChipColor(resources.getColor(R.color.optional))
                .uncheckedTextColor(Color.parseColor("#ffffff"))

        val chipCloud = ChipCloud(this, cvAboutNhood, config)
        chipCloud.addChips(list)

        chipCloud.setListener { index, checked, userClick ->
            if (userClick) {
                list!!.drop(index)
            }
        }

    }

    private fun setChipLayoutForSkills(list: List<String>) {
        cvSkills.visible()
        layoutSkill.gone()
        val config = ChipCloudConfig()
                .selectMode(ChipCloud.SelectMode.multi)
                .checkedChipColor(resources.getColor(R.color.optional))
                .checkedTextColor(Color.parseColor("#ffffff"))
                .uncheckedChipColor(resources.getColor(R.color.optional))
                .uncheckedTextColor(Color.parseColor("#ffffff"))


        val chipCloud = ChipCloud(this, cvSkills, config)
        chipCloud.addChips(list)

        chipCloud.setDeleteListener { index, label ->
            showToast("$label removed from profile")
        }

    }

    private fun setChipInterest(list: List<String>) {
        cvInterests.visible()
        layoutInterest.gone()
        val config = ChipCloudConfig()
                .selectMode(ChipCloud.SelectMode.multi)
                .checkedChipColor(resources.getColor(R.color.optional))
                .checkedTextColor(Color.parseColor("#ffffff"))
                .uncheckedChipColor(resources.getColor(R.color.optional))
                .uncheckedTextColor(Color.parseColor("#ffffff"))

        val chipCloud = ChipCloud(this, cvInterests, config)
        chipCloud.addChips(list)

    }


    fun getBiography() {
        fireStoreHelper.getBiography(user!!.uid).addOnCompleteListener {
            if (it.isSuccessful) {
                val biography = it.result.toObject(Biography::class.java)
                if (biography != null && biography.aboutYou.isNotEmpty()) {
                    biographyEmptyLayout.gone()
                    biographyLayout.visible()
                    txtResident.text = "Resident since ${biography.movedYear}"
                    txtGrow.text = "Grow Up ${biography.growUp}"

                    if (biography!!.aboutYou.equals("I'am working"))
                        txtJOb.text = "${biography.aboutDo!!.jobTitle} at ${biography.aboutDo!!.company}(${biography.aboutDo!!.city})"
                    else
                        txtJOb.text = biography.aboutYou

                    txtBackground.text = biography.background
                } else {
                    biographyLayout.gone()
                    biographyEmptyLayout.visible()
                }
            } else {
                showToast("something went wrong")
            }
        }
    }

    fun getInterest() {
        fireStoreHelper.getInterest(user!!.uid).addOnCompleteListener {
            if (it.isSuccessful) {
                val interest = it.result.toObject(Biography.Interest::class.java)
                if (interest != null && interest.interest.isNotEmpty()) {
                    cvInterests.visible()
                    val separated = interest.interest.split(",")
                    setChipInterest(separated)
                }
            } else {
                showToast("something went wrong")
            }
        }
    }

    fun getSkills() {
        fireStoreHelper.getSkills(user!!.uid).addOnCompleteListener {
            if (it.isSuccessful) {
                val skills = it.result.toObject(Biography.Skills::class.java)
                if (skills != null && skills.skills.isNotEmpty()) {
                    cvSkills.visible()
                    val separated = skills.skills.split(",")
                    setChipLayoutForSkills(separated)
                }
            } else {
                showToast("something went wrong")
            }
        }
    }

    private fun getLoveForNei() {
        fireStoreHelper.getLoveNeigh(user!!.uid).addOnCompleteListener {
            if (it.isSuccessful) {
                val skills = it.result.toObject(Biography.AboutLoveNei::class.java)
                if (skills != null && skills.aboutLoveNei.isNotEmpty()) {
                    cvAboutNhood.visible()
                    val separated = skills.aboutLoveNei.split(",")
                    setChipLayoutForAboutNhood(separated)
                }
            } else {
                showToast("something went wrong")
            }
        }
    }


    private fun getChild() {
        familyList!!.clear()
        fireStoreHelper.getChild(user!!.uid).addOnCompleteListener {
            if (it.isSuccessful) {
                it.result.documents.forEach {
                    var family = it.toObject(Family.Child()::class.java)
                    familyList!!.add(AllFamily(name = family!!.name, image = family!!.imageLink, age = family!!.age + " year old", id = it.id))
                    layoutFamily.gone()
                }
                getPet()

            } else {
                showToast("something went wrong")
            }
        }
    }

    private fun getPet() {
        fireStoreHelper.getPet(user!!.uid).addOnCompleteListener {
            if (it.isSuccessful) {
                if (it.result.documents.isEmpty()) {
                    setAdapter()

                } else {
                    it.result.documents.forEach {
                        var pet = it.toObject(Family.Pet()::class.java)
                        familyList!!.add(AllFamily(name = pet!!.name, image = pet!!.imageLink, age = pet.profile, category = pet.category, id = it.id))
                    }
                    layoutFamily.gone()
                    setAdapter()
                }

            } else {
                showToast("something went wrong")
            }
        }
    }

    private fun getContactInformation() {
        fireStoreHelper.getContactInformation(user!!.uid).addOnCompleteListener {
            if (it.isSuccessful) {
                val contacts = it.result.toObject(ContactInformation::class.java)
                if (contacts != null && contacts.email.isNotEmpty()) {
                    layoutContactInfo.gone()
                    contactLayout.visible()
                    familyEmail.text = contacts.email
                    familyHomePhone.text = contacts.homePhone
                    familyMobileNumber.text = contacts.mobilePhone

                }
            } else {
                showToast("something went wrong")
            }
        }
    }

    private fun setAdapter() {
        if (familyRecycler != null) {
            var adapter = AllFamilyAdapter(this, familyList, this)
            familyRecycler.layoutManager = GridLayoutManager(this, this!!.calculateNoOfColumns())
            familyRecycler.adapter = adapter
        }
    }


    fun getUser() {
        fireStoreHelper.getUser(fireStoreHelper.currentUserUid()).addOnCompleteListener {
            if (it.isSuccessful) {
                if (it.result.exists()) {
                    val user = it.result.toObject(User::class.java)
                    pref.setUserToPref(user!!)
                }
            }
        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_another_user_profile)
        //set profile data
        familyList = ArrayList()


        //listner
        profile_back_iv.setOnClickListener(this)
    }
}
