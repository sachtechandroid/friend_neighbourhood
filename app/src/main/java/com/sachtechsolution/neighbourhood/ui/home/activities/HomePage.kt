package com.sachtechsolution.neighbourhood

import android.os.Bundle
import android.os.Environment
import com.eightbitlab.bottomnavigationbar.BottomBarItem
import com.eightbitlab.bottomnavigationbar.BottomNavigationBar
import com.sachtechsolution.neighbourhood.basepackage.base.BaseActivity
import com.sachtechsolution.neighbourhood.chat.fragment.ChatsFragment
import com.sachtechsolution.neighbourhood.constants.Constants
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.isPermissionsGranted
import com.sachtechsolution.neighbourhood.intracter.HomeActivityIntracter
import com.sachtechsolution.neighbourhood.model.User
import com.sachtechsolution.neighbourhood.ui.RiverstoneFragmenthome.fragments.menu.fragments.GeneralFragment
import com.sachtechsolution.neighbourhood.ui.home.fragments.FragmentHome
import com.sachtechsolution.neighbourhood.ui.home.fragments.FragmentMenu
import com.sachtechsolution.neighbourhood.ui.home.fragments.FragmentNotification
import com.sachtechsolution.neighbourhood.ui.home.fragments.FragmentRecommendations
import com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.CrimeSafetyFragment
import com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.FragmentEvents
import com.sachtechsolution.neighbourhood.ui.lost_found.LostFoundFragment
import com.sachtechsolution.neighbourhood.ui.sale.activity.SalesAndFreeFragment
import java.io.File

class HomePage : BaseActivity(), HomeActivityIntracter {

    var bottomNavigationBar: BottomNavigationBar? = null
    val cUser by lazy { pref.getUserFromPref() }
    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }


    override fun updateSelectedPos(pos: Int) {
        if (bottomNavigationBar != null)
            bottomNavigationBar!!.selectTab(pos, false)
    }


    override fun fragmentContainer(): Int {
        return R.id.homePageContainer
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home_page)
        bottomNavigationBar = findViewById(R.id.bottom_bar)

        setBottomBar()
        getAddresses()
        updateUser()

        if (Constants.NEXT.equals("event"))
            navigator.replaceFragment(FragmentEvents::class.java, true)
        else if (Constants.NEXT.equals("crime"))
            navigator.replaceFragment(CrimeSafetyFragment::class.java, true)
        else if (Constants.NEXT.equals("lost"))
            navigator.replaceFragment(LostFoundFragment::class.java, true)
        else if (Constants.NEXT.equals("sale"))
            navigator.replaceFragment(SalesAndFreeFragment::class.java, true)
        else if (Constants.NEXT.equals("general"))
            navigator.replaceFragment(GeneralFragment::class.java, true)
        else {
            navigator.replaceFragment(FragmentHome::class.java, true)
        }

        Constants.NEXT = "home"

        if (isPermissionsGranted(this, arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE, android.Manifest.permission.WRITE_EXTERNAL_STORAGE))) {
            val file = File("" + Environment.getExternalStorageDirectory() + File.separator + ".neighborhood" + File.separator + ".temp")
            if (file.exists())
                file.delete()
        }
    }

    private fun updateUser() {
        fireStoreHelper.getUser(cUser?.uid).addOnCompleteListener {
            if (it.isSuccessful) {
                val user = it.result.toObject(User::class.java) ?: cUser!!
                pref.setUserToPref(user)
            }
        }
    }


    private fun getAddresses() {
        fireStoreHelper.getAddresses(cUser?.zipCode + "," + cUser?.mainLocality).addOnCompleteListener {
            if (it.isSuccessful) {
                Constants.ADDRESSES.clear()
                Constants.SOCIETIES.clear()

                it.result.documents.forEach {
                    val list = it.data?.get("society") as ArrayList<String>
                    if (it.id.equals(cUser?.street))
                        Constants.SOCIETIES = list
                    list.forEach {
                        Constants.ADDRESSES.add(it)
                    }
                }
            }
        }
    }

    private fun setBottomBar() {

        val itemOne = BottomBarItem(R.drawable.icsplash)
        val itemTwo = BottomBarItem(R.drawable.ic_heartbox)
        val itemThree = BottomBarItem(R.drawable.ic_notification)
        val itemFour = BottomBarItem(R.drawable.ic_message)
        val itemfive = BottomBarItem(R.drawable.ic_menu)

        bottomNavigationBar!!.addTab(itemOne)
        bottomNavigationBar!!.addTab(itemTwo)
        bottomNavigationBar!!.addTab(itemThree)
        bottomNavigationBar!!.addTab(itemFour)
        bottomNavigationBar!!.addTab(itemfive)
        bottomNavigationBar!!.setOnSelectListener { position -> doStuff(position) }
    }


    private fun doStuff(position: Int) {
        when (position) {
            0 -> navigator.replaceFragment(FragmentHome::class.java, true)
            1 -> navigator.replaceFragment(FragmentRecommendations::class.java, true)
            2 -> navigator.replaceFragment(FragmentNotification::class.java, true)
            3 -> navigator.replaceFragment(ChatsFragment::class.java, true)
            4 -> navigator.replaceFragment(FragmentMenu::class.java, true)
        }
    }

}
