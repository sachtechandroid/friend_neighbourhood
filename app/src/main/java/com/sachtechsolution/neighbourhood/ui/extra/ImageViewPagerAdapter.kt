package com.sachtechsolution.neighbourhood.ui.extra

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.sachtechsolution.neighbourhood.ui.extra.fragments.ImageViewPagerItem

/* whoGuri 28/8/18 */
class ImageViewPagerAdapter(val supportFragmentManager: FragmentManager, val images: String) : FragmentPagerAdapter(supportFragmentManager){

    private var imagesLink: List<String> = images.split("#")

    override fun getItem(p0: Int): Fragment? {
        return ImageViewPagerItem(imagesLink[p0])//.apply { Bundle().putString("link",imagesLink[p0]) }
    }

    override fun getCount(): Int {
        return imagesLink.size-1
    }
}