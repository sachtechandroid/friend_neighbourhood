package com.sachtechsolution.neighbourhood.ui.home.fab.fragment.private_message

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.ImageView
import com.example.manish.internetstatus.OttoBus
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.chat.ActivityChatDetails
import com.sachtechsolution.neighbourhood.chat.model.Friend
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.gone
import com.sachtechsolution.neighbourhood.extension.showToast
import com.sachtechsolution.neighbourhood.model.User
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.sachtechsolution.neighbourhood.ui.home.fab.adapter.ChooseNeighbourAdapter
import com.sachtechsolution.neighbourhood.ui.mail.data.SendInviteCodeFragment
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.custom_toolbar.*
import kotlinx.android.synthetic.main.fragment_private_message.*

class FragmentPrivateMessage : BaseFragment(), ChooseNeighbourAdapter.OnChooseNeighbour {

    val adapter by lazy { ChooseNeighbourAdapter(activity!!, this) }
    val currentUser by lazy { pref.getUserFromPref() }
    var userList = ArrayList<User>()
    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }

    override fun OnChooseNeighbour(user: User, chooseNeProfile: ImageView) {
        var friend = Friend(user!!.uid, user!!.firstName + " " + user!!.lastName, user!!.imageLink, user!!.society)
        ActivityChatDetails.navigate((context as AppCompatActivity?)!!, chooseNeProfile, friend)
    }

    override fun viewToCreate(): Int {
        return R.layout.fragment_private_message
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        chooseNeRecyclerView.adapter = adapter
        cancelLayout.setOnClickListener { activity!!.onBackPressed() }
        nextLayout.setOnClickListener { context!!.showToast("Please choose a Neighbour") }
        home_invite.setOnClickListener { baseAcitivityListener.navigator.replaceFragment(SendInviteCodeFragment::class.java, true) }

        etSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {
                val filterlist = filterList(etSearch.text.toString(), userList)
                adapter.userList.clear()
                if (filterlist.isEmpty()) {
                    adapter.update(userList)
                    adapter.notifyDataSetChanged()
                } else {
                    adapter.update(filterlist as ArrayList<User>)
                    adapter.notifyDataSetChanged()
                }

            }
        })
    }


    private fun filterList(text: String, list: ArrayList<User>): List<User> {
        return list.filter { it?.firstName.contains(text, true) }
    }

    fun getAllUsers() {
        fireStoreHelper.getNeighborsByStreet(currentUser!!.street).addOnCompleteListener {
            if (it.isSuccessful) {

                if (it.result.documents.size<1)
                    neighbors__message.text = "No neighbor found"
                else{
                    it.result.documents.forEach {

                        val user = it.toObject(User::class.java)!!
                        if (!user.uid.equals(currentUser!!.uid)) {
                            neighbors__message.gone()
                            user.uid = it.id
                            adapter!!.add(user)
                            userList.add(user)
                        }
                    }
                }

            } else {
                showToast("something went wrong")
            }
        }.addOnFailureListener {
            hideProgress()
            showToast("something went wrong")
        }
    }


    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
    }


    @Subscribe
    fun getMessage(internetStatus: String) {

        if (internetStatus.equals("Not connected to Internet")) {
            InternetStatusDialogFragment.newInstance().show(fragmentManager, "option_fragment")
        } else {
            showToast("internet enabled ")
        }
    }


    override fun onResume() {
        super.onResume()
        if (adapter.userList.size>0) {
            userList.clear()
            adapter.userList.clear()
            adapter.notifyDataSetChanged()
        }
        OttoBus.bus.register(this)
        getAllUsers()
    }

}