package com.sachtechsolution.neighbourhood.ui.login.fragments

import android.Manifest
import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.util.Patterns
import android.view.View
import com.example.manish.internetstatus.OttoBus
import com.facebook.AccessToken
import com.facebook.GraphRequest
import com.google.firebase.auth.FacebookAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.iid.FirebaseInstanceId
import com.sachtechsolution.neighbourhood.HomePage
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.*
import com.sachtechsolution.neighbourhood.model.User
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_login.*
import org.json.JSONException
import sachtech.com.blablademo.firebase.FacebookHelper
import sachtech.com.blablademo.firebase.listeners.LoginWithFacebookListener


class LoginFragment : BaseFragment(), LoginWithFacebookListener {

    lateinit var facebookHelper: FacebookHelper

    override fun onLoginFailure(error: String) {
        activity?.showToast(error)
    }

    override fun onLoginSuccess(token: AccessToken) {
        loginWithFacebookAccessToken(token)
    }


    override fun viewToCreate(): Int {
        return R.layout.fragment_login
    }

    private lateinit var mAuth: FirebaseAuth
    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }

    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        facebookHelper.setupCallback(requestCode, resultCode, data)
    }
    

    private fun loginWithFacebookAccessToken(token: AccessToken) {
        showProgress()
        val credential = FacebookAuthProvider.getCredential(token.token)
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(activity!!) { task ->
                    if (task.isSuccessful) {
                        handleLoginProcess(token)
                    }

                }.addOnFailureListener {
                    hideProgress()
                    it.message
                    showToast("Authentication error")
                }
    }

    private fun handleLoginProcess(token: AccessToken) {

        fireStoreHelper.getUser(FirebaseAuth.getInstance().uid).addOnCompleteListener {
            if (it.isSuccessful) {
                if (it.result.exists()) {
                    hideProgress()
                    val user=it.result.toObject(User::class.java)
                    pref.setUserToPref(user!!)
                    context!!.openActivity<HomePage>()
                }
                else {
                    if(context!!.getLocationMode() != 3)
                    {
                        showToast("please select high accuracy option")
                        startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
                        // do stuff
                    }else {
                        var bundle = Bundle()
                        bundle.putParcelable("token", token)
                        hideProgress()
                        baseAcitivityListener.navigator.replaceFragment(SignUpAddressFragment::class.java, true, bundle)
                    }
                }
            }
        }

    }


    @Subscribe
    fun getMessage(internetStatus: String) {

        if (internetStatus.equals("Not connected to Internet")) {
            InternetStatusDialogFragment.newInstance().show(fragmentManager, "option_fragment")
        } else {
            showToast("internet enabled ")

        }

    }

    override fun onResume() {
        super.onResume()
        OttoBus.bus.register(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mAuth = FirebaseAuth.getInstance()
        /*FacebookSdk.sdkInitialize(activity)
        AppEventsLogger.activateApp(context)*/
        facebookHelper = FacebookHelper(this, this)

        login_sign_in_tv.setOnClickListener {

            val email = login_email_tiet.text.toString()
            val password = login_password_tiet.text.toString()

            if (!Patterns.EMAIL_ADDRESS.matcher(email).matches())
                showToast("Enter valid Email")
            else if (!password.isSecure())
                showToast("Password must be grater than 6 digit")
            else
                logIn(email, password)
        }
        login_sign_up_tv.setOnClickListener {
            if (isPermissionsGranted(context!!, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION)))
                if(context!!.getLocationMode() != 3)
                {
                    showToast("please select high accuracy option")
                    startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
                    // do stuff
                }else
                baseAcitivityListener.navigator.replaceFragment(SignUpAddressFragment::class.java, true)
            else
                askForPermissions(activity!!, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION))
        }
        login_back_iv.setOnClickListener {
            fragmentManager?.popBackStackImmediate()
        }
        login_forget_pass_tv.setOnClickListener {
            baseAcitivityListener.navigator.replaceFragment(ForgetPasswordFragment::class.java, true)
        }

        login_fb_ll.setOnClickListener { facebookHelper.signin() }
        sigunOut.setOnClickListener { FirebaseAuth.getInstance().signOut() }
    }

    private fun logIn(email: String, password: String) {
        showProgress()
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(activity!!) { task ->
                    if (task.isSuccessful) {
                        getUser()
                    } else {
                        hideProgress()
                        showToast(task.exception!!.message)
                    }

                }.addOnFailureListener {
                    hideProgress() }
    }

    fun getUser() {
        val map = HashMap<String, String>()
        map["token"] = FirebaseInstanceId.getInstance().token!!

        fireStoreHelper.updateToken(map).addOnCompleteListener {
            if (it.isSuccessful) {
                fireStoreHelper.getUser(fireStoreHelper.currentUserUid()).addOnCompleteListener {
                    if (it.isSuccessful) {
                        val user = it.result.toObject(User::class.java)
                        if (it.result.exists()) {
                            pref.setUserToPref(user!!)

                            if (user.firstName.isNotEmpty() && user.lastName.isNotEmpty()) {
                                hideProgress()
                                baseAcitivityListener.navigator.openActivity(HomePage::class.java)
                                activity?.finish()
                            } else {
                                hideProgress()
                                baseAcitivityListener.navigator.replaceFragment(SignUpInfoFragment::class.java, true)
                            }
                        }
                    }
                }
            } else
                hideProgress()
        }.addOnFailureListener { hideProgress() }
    }
}
