package com.sachtechsolution.neighbourhood.ui.home

import java.util.*

class PhoneAlbum {
    private var id: Int = 0
    private var name: String? = null
    private var coverUri: String? = null
    private var albumPhotos: Vector<PhonePhoto>? = null

    fun getId(): Int {
        return id
    }

    fun setId(id: Int) {
        this.id = id
    }

    fun getName(): String? {
        return name
    }

    fun setName(name: String) {
        this.name = name
    }

    fun getCoverUri(): String? {
        return coverUri
    }

    fun setCoverUri(albumCoverUri: String) {
        this.coverUri = albumCoverUri
    }

    fun getAlbumPhotos(): Vector<PhonePhoto> {
        if (albumPhotos == null) {
            albumPhotos = Vector()
        }
        return this.albumPhotos!!
    }

    fun setAlbumPhotos(albumPhotos: Vector<PhonePhoto>) {
        this.albumPhotos = albumPhotos
    }
}