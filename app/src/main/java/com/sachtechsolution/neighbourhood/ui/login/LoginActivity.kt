package com.sachtechsolution.neighbourhood.ui.login

import android.os.Bundle
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseActivity
import com.sachtechsolution.neighbourhood.constants.Constants
import com.sachtechsolution.neighbourhood.ui.login.fragments.LoginMainFragment

/* whoGuri garden/appliances/home_decor */
class LoginActivity:BaseActivity(){

    override fun fragmentContainer(): Int {
        return R.id.login_container
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_activity)

        getNavigator().addFragment(LoginMainFragment::class.java,true)
    }

    override fun onBackPressed() {
       // if (Constants.BACK)
            super.onBackPressed()
    }
}