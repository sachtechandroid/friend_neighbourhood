package com.sachtechsolution.neighbourhood.ui.home.fab.fragment.message.dialogs

import android.net.Uri
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.*
import com.bumptech.glide.Glide
import com.sachtechsolution.neighbourhood.R
import kotlinx.android.synthetic.main.view_viewimage.*

open class ViewImage: DialogFragment(), View.OnClickListener {


    override fun onClick(p0: View?) {
        var id=p0!!.id
        when(id)
        {
            R.id.closeViewImage->
                dismiss()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        dialog.window.setGravity(Gravity.CENTER)
        dialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        return inflater.inflate(R.layout.view_viewimage, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewImage
        Glide.with(context!!).load(uri).into(viewImage)
        //listner
        closeViewImage.setOnClickListener(this)
    }

    companion object {
        var uri:Uri?=null
        fun newInstance(uri: Uri?): ViewImage {
            val fragment = ViewImage()
            Companion.uri =uri
            return fragment

        }
    }


}