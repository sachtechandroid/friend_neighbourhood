package com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.v4.content.FileProvider
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.google.firebase.storage.StorageReference
import com.sachtechsolution.neighbourhood.BuildConfig
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.constants.Constants
import com.sachtechsolution.neighbourhood.constants.PostInterests
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.showToast
import com.sachtechsolution.neighbourhood.intracter.Api_Hitter
import com.sachtechsolution.neighbourhood.model.Post
import com.sachtechsolution.neighbourhood.model.User
import com.sachtechsolution.neighbourhood.ui.home.fab.fragment.message.adapter.SingleImageSelecterAapter
import com.sachtechsolution.neighbourhood.ui.home.fab.fragment.message.dialogs.ImageActionDialogFragment
import com.sachtechsolution.neighbourhood.ui.home.fab.fragment.message.dialogs.SelectOptionDialogFragment
import com.sachtechsolution.neighbourhood.ui.home.fab.fragment.message.dialogs.ViewImage
import kotlinx.android.synthetic.main.custom_toolbar.*
import kotlinx.android.synthetic.main.fragment_add_interests_post.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class FragmentAaddInterestsPost : BaseFragment(), View.OnClickListener, SingleImageSelecterAapter.SelectImage, ImageActionDialogFragment.onSelectAction, SelectOptionDialogFragment.onOptionSelect {


    lateinit var mRef: StorageReference
    var imagesLinks = ""
    var type = ""
    val user by lazy { pref.getUserFromPref() }
    private var size = 0
    private var count = 0
    var photoURI: Uri? = null
    private var TAKE_PICTURE = 12
    private var GALLERY = 13
    var adapter: SingleImageSelecterAapter? = null
    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }

    override fun viewToCreate(): Int {
        return R.layout.fragment_add_interests_post
    }


    override fun viewPhoto(uri: Uri?) {
        ViewImage.newInstance(uri)
                .show(fragmentManager, "option_fragment")
    }

    override fun removePhoto(position: Int) {
        adapter!!.removeImage(position)
        adapter!!.notifyItemRemoved(position)
    }


    override fun forGallery() {
        var intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(intent, GALLERY)
    }


    override fun forCamera() {
        //openCamera()
        try {
            dispatchTakePictureIntent();
        } catch (e: IOException) {
        }
    }

    @Throws(IOException::class)
    private fun dispatchTakePictureIntent() {

        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        // Ensure that there's a camera activity to handle the intent

        if (takePictureIntent.resolveActivity(context!!.packageManager) != null) {
            // Create the File where the photo should go
            var photoFile: File? = null
            try {
                photoFile = createImageFile()
            } catch (ex: IOException) {
                // Error occurred while creating the File
                return
            }

            // Continue only if the File was successfully created
            if (photoFile != null) {

                photoURI = FileProvider.getUriForFile(context!!,
                        BuildConfig.APPLICATION_ID + ".provider",
                        createImageFile())
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                startActivityForResult(takePictureIntent, TAKE_PICTURE)
            }
        }
    }

    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name

        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "JPEG_" + timeStamp + "_"
        val storageDir = File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM), "Camera")

        val image = File.createTempFile(
                imageFileName, /* prefix */
                ".jpg", /* suffix */
                storageDir      /* directory */)
        return image
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        type = arguments!!.getString("type")
        adapter = SingleImageSelecterAapter(context!!, this)
        recyclerView.layoutManager = GridLayoutManager(context, 6) as RecyclerView.LayoutManager?
        recyclerView.adapter = adapter

        //listner
        cancelLayout.setOnClickListener(this)
        nextLayout.setOnClickListener(this)
    }

    override fun OnImageSelected(position: Int, uri: Uri?) {
        ImageActionDialogFragment.newInstance(this, position, uri)
                .show(fragmentManager, "option_fragment")
    }

    override fun OnImageChoose() {
        if (adapter!!.list.size == 9)
            showToast("you can't select more images")
        else
            SelectOptionDialogFragment.newInstance(this)
                    .show(fragmentManager, "option_fragment")
    }


    //private var pdfUri: Uri?=null

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {

            TAKE_PICTURE -> {

                if (resultCode == Activity.RESULT_OK) {
                    adapter!!.addImage(photoURI)
                    adapter!!.notifyDataSetChanged()
                }
            }

            GALLERY -> {

                if (data != null) {
                    val selectedImage = data!!.data
                    adapter!!.addImage(selectedImage)
                    adapter!!.notifyDataSetChanged()
                }

            }
        }
    }

    var subject = ""
    var message = ""
    override fun onClick(view: View?) {
        var id = view!!.id

        when (id) {
            R.id.cancelLayout -> activity!!.onBackPressed()
            R.id.nextLayout -> {
                subject = general_subject_et.text.toString()
                message = general_message_et.text.toString()

                if (subject.isNullOrEmpty())
                    showToast("enter subject")
                else if (message.isNullOrEmpty())
                    showToast("enter message")
                else {
                    showProgress()
                    val size = adapter?.list!!.size
                    if (size > 0)
                        uploadImage()
                    else
                        createPost()
                }
            }
        }
    }


    private fun uploadImage() {
        size = adapter!!.list.size
        var time = java.sql.Timestamp(Date().time)
        var timestamp = time.toString().replace(":", "").replace("-", "").replace(" ", "").replace(".", "")

        mRef = fireStoreHelper.getStorageRef().child("postImages").child(fireStoreHelper.currentUserUid() + timestamp)
        mRef.putFile(adapter!!.list[count]!!).addOnCompleteListener {
            if (it.isSuccessful) {
                it.result

                mRef!!.downloadUrl.addOnSuccessListener { p0 ->
                    count += 1
                    imagesLinks = imagesLinks + p0.toString() + "#"
                    if (size == count)
                        createPost()
                    else
                        uploadImage()
                }.addOnFailureListener {
                    hideProgress()
                }
            }
        }.addOnFailureListener {
            hideProgress()
        }
    }


    private fun createPost() {
        val post = Post(subject = subject, message = message,  uid = user!!.uid,
                type = Post.postType.INTERESTS, imagesLinks = imagesLinks,subType = type,zipCode = user!!.zipCode)

        fireStoreHelper.createIntrestsPosts(post = post).addOnCompleteListener {
            if (it.isSuccessful) {
                sentNotification()
            } else {
                hideProgress()
                showToast("error")
            }

        }.addOnFailureListener {
            showToast("failed")
            hideProgress()

        }.addOnCanceledListener {
            showToast("cancled")
            hideProgress()
        }
    }

    private fun sentNotification() {
        fireStoreHelper.getfollowersUser(type).addOnCompleteListener {
            if (it.isSuccessful)
            {
                it.result.forEach {
                    val interest=it.toObject(PostInterests::class.java)
                   if(!interest.uid.equals(user!!.uid))
                   {
                       fireStoreHelper.getUser(interest.uid).addOnCompleteListener {
                           if (it.isSuccessful)
                           {
                               val user=it.result.toObject(User::class.java)
                               val callback= Api_Hitter.ApiInterface("http://stsmentor.com/").sendNotification("in $type someone add a new post ",user!!.token)
                               callback.enqueue(object : Callback<ResponseBody> {
                                   override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                                       context!!.showToast(t.message!!)
                                   }

                                   override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                                   }

                               })
                           }
                       }.addOnFailureListener {
                           hideProgress()
                           showToast(it.message!!)
                       }
                   }
                }
                hideProgress()
                activity!!.onBackPressed()
            }
        }.addOnFailureListener {
            hideProgress()
            showToast(it.message!!)
        }
    }
}
