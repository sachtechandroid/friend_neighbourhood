package com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments

import android.os.Bundle
import android.view.View
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.constants.PostInterests
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.gone
import kotlinx.android.synthetic.main.fragment_all_interests.*

class FragmentAllInterests : BaseFragment(), View.OnClickListener, AllInterestsAdapter.onitemClick {


    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }
    val adapter by lazy { AllInterestsAdapter(context!!,this) }

    override fun viewToCreate(): Int {
        return R.layout.fragment_all_interests
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerView.adapter=adapter

        //set listner
        icBack.setOnClickListener(this)
        add_interests.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        when(view!!.id)
        {
            R.id.icBack->activity!!.onBackPressed()
            R.id.add_interests-> baseAcitivityListener.navigator.replaceFragment(FragmentFollowInterests::class.java,true)
        }
    }

    override fun onResume() {
        super.onResume()
        if (adapter.list.size>0)
           adapter.list.clear()
        allInterests()
    }

    fun allInterests()
    {
        fireStoreHelper.getfollowersInterest(uid = pref.getUserFromPref()!!.uid).addOnCompleteListener {
            if (it.isSuccessful)
            {
                if(it.result.size()<1)
                {
                    txtEmpty.text = "no any interest found"

                }else {

                    it.result.forEach {
                        if (txtEmpty!=null)
                            txtEmpty.gone()

                        var interests = it.toObject(PostInterests::class.java)
                        pref.setFollow(interests.type, it.id)
                        adapter!!.addItem(interests = interests)
                        adapter!!.notifyDataSetChanged()
                    }

                }
            }else
            {
                hideProgress()
                showToast("got error")
            }
        }.addOnFailureListener {
            hideProgress()
        }
    }

    override fun onInterestsClick(type: String) {

        var bundle=Bundle()
        bundle.putString("type",type)
        baseAcitivityListener.navigator.replaceFragment(FragmentViewInterests::class.java,true,bundle)
    }

}