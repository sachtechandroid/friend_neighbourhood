package com.sachtechsolution.neighbourhood.ui.groups

import android.os.Bundle
import android.os.Handler
import android.view.View
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.gone
import com.sachtechsolution.neighbourhood.extension.visible
import com.sachtechsolution.neighbourhood.model.Group
import com.sachtechsolution.neighbourhood.model.User
import com.sachtechsolution.neighbourhood.ui.home.adapters.MemberListAdapter
import kotlinx.android.synthetic.main.fragment_group_info.*

/* whoGuri 10/10/18 */
class GroupInfoFragment : BaseFragment() {

    override fun viewToCreate(): Int {
        return R.layout.fragment_group_info
    }

    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }
    val group by lazy { activity!!.intent.getParcelableExtra("group") as Group }
    lateinit private var mAdapter2: MemberListAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (group.createdBy.equals(fireStoreHelper.currentUserUid())) {
            mAdapter2 = MemberListAdapter(activity!!, false, true)
            groupExit.setImageResource(R.drawable.ic_save)
            groupExit.setOnClickListener {
                saveGroup()
            }
        } else {
            mAdapter2 = MemberListAdapter(activity!!)
            groupExit.setImageResource(R.drawable.ic_exit)
            groupExit.setOnClickListener {
                exitGroup()
            }
        }
        membersList.adapter = mAdapter2
        groupDetail_btnBack.setOnClickListener {
            activity!!.onBackPressed()
        }

        name.text = group.name
        info.text = group.desc
        extra.text = "This group is created on ${group.createdOn}"
        if (fireStoreHelper.currentUserUid().equals(group.createdBy))
            addMembers.visible()
        else
            addMembers.gone()
        addMembers.setOnClickListener {
            baseAcitivityListener.navigator.replaceFragment(AddNewMembers::class.java, true,
                    Bundle().apply { putParcelable("group", group) })
        }
        getUser()
        getAllMembers()
    }

    private fun saveGroup() {
        group.members = mAdapter2.userIdList
        if (!group.members.contains(fireStoreHelper.currentUserUid()))
            group.members.add(fireStoreHelper.currentUserUid())
        showProgress()
        fireStoreHelper.updateGroup(group = group, gid = group.gid).addOnCompleteListener {
            if (it.isSuccessful) {
                mAdapter2.userIdList.forEach {
                    fireStoreHelper.getUser(it).addOnCompleteListener {
                        val user = it.result.toObject(User::class.java)!!
                        if (user.groups.contains(group.gid)) {
                            user!!.groups.remove(group.gid)
                            fireStoreHelper.setUser(user.uid, user)
                        }
                    }
                }
                Handler().postDelayed({
                    hideProgress()
                    showToast("Group updated")
                    activity!!.finish()
                    baseAcitivityListener.navigator.openActivity(GroupActivity::class.java)
                }, 2000)
            } else {
                onError()
            }
        }.addOnFailureListener { onError() }
    }

    private fun exitGroup() {
        showProgress()
        val user = pref.getUserFromPref()
        user!!.groups.remove(group.gid)
        if (group.members.contains(user.uid))
            group.members.remove(user.uid)
        fireStoreHelper.setUser(fireStoreHelper.currentUserUid(), user).addOnCompleteListener {
            if (it.isSuccessful) {
                group.members.remove(fireStoreHelper.currentUserUid())
                fireStoreHelper.updateGroup(group = group, gid = group.gid)
                        .addOnCompleteListener {
                            pref.setUserToPref(user)
                            activity!!.finish()
                            hideProgress()
                        }
            } else {
                onError()
            }
        }.addOnFailureListener {
            onError()
        }
    }

    private fun onError() {
        hideProgress()
        showToast("try again")
    }

    private fun getAllMembers() {
        group.members.forEach {
            fireStoreHelper.getUser(it).addOnCompleteListener {
                if (it.isSuccessful) {
                    val user = it.result.toObject(User::class.java)
                    user!!.uid = it.result.id
                    mAdapter2.addUser(user = user!!)
                }
            }
        }
    }

    private fun getUser() {
        showProgress()
        fireStoreHelper.getUser(group.createdBy).addOnCompleteListener {
            if (it.isSuccessful) {
                val user = it.result.toObject(User::class.java)
                extra.text = "This group is created on ${group.createdOn} by ${user!!.firstName} ${user!!.lastName}"
                hideProgress()
            } else
                hideProgress()
        }.addOnFailureListener { hideProgress() }
    }
}