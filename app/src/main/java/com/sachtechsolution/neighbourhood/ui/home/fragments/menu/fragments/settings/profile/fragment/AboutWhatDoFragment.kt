package com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings.profile.fragment

import android.os.Bundle
import android.view.View
import android.widget.RadioButton
import com.example.manish.internetstatus.OttoBus
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings.profile.FillUpProfileActivity
import kotlinx.android.synthetic.main.fragment_what_do.*
import com.sachtechsolution.neighbourhood.constants.ObjBiography
import com.sachtechsolution.neighbourhood.extension.showToast
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.squareup.otto.Subscribe


class AboutWhatDoFragment:BaseFragment() {
    var selectedText=""

    override fun viewToCreate(): Int {
        return R.layout.fragment_what_do
    }
    override fun onResume() {
        super.onResume()
        OttoBus.bus.register(this)
        FillUpProfileActivity.mcount!!.text="2 of 4"
    }

    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
    }


    @Subscribe
    fun getMessage(internetStatus:String) {

        if (internetStatus.equals("Not connected to Internet")){
            InternetStatusDialogFragment.newInstance().show(fragmentManager, "option_fragment")
        }else {
            showToast("internet enabled ")

        }

    }




    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        radioGroup.setOnCheckedChangeListener { group, checkedId ->
            val radioButton = group.findViewById(checkedId) as RadioButton
            selectedText = radioButton.text.toString()

        }
        btnNext.setOnClickListener {
            if (selectedText.isEmpty())
                context!!.showToast("select Option")
            else {
                ObjBiography.aboutYou=selectedText
                if (selectedText.equals("I'am working"))
                baseAcitivityListener.navigator.replaceFragment(AboutDoForWorkFragment::class.java, true)
                else
                    baseAcitivityListener.navigator.replaceFragment(AboutBackgroundFragment::class.java,true)
            }

        }
    }
}