package com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings.profile

import android.os.Bundle
import android.view.View
import com.example.manish.internetstatus.OttoBus
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.extension.showToast
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings.adapters.SignificantAdapter
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_add_significant_other.*

class AddSignificantOtherFragment : BaseFragment(), View.OnClickListener {


    override fun onClick(p0: View?) {
        when(p0!!.id)
        {
            R.id.txtCancel->activity!!.onBackPressed()
            R.id.txtAdd->activity!!.showToast("add Clicked")
        }
    }


    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
    }


    @Subscribe
    fun getMessage(internetStatus:String) {

        if (internetStatus.equals("Not connected to Internet")){
            InternetStatusDialogFragment.newInstance().show(fragmentManager, "option_fragment")
        }else {
            showToast("internet enabled ")

        }

    }

    override fun onResume() {
        super.onResume()
        OttoBus.bus.register(this)
    }

    override fun viewToCreate(): Int {
        return R.layout.fragment_add_significant_other
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        //listner
        txtCancel.setOnClickListener(this)
        txtAdd.setOnClickListener(this)

        var adapter=SignificantAdapter(activity!!)
        recyclerView.adapter=adapter
    }
}
