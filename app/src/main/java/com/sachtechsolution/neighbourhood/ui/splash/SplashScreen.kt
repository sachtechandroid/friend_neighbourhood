package com.sachtechsolution.neighbourhood

import android.os.Bundle
import android.os.Handler
import com.example.manish.internetstatus.OttoBus
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessaging
import com.google.gson.Gson
import com.pedro.library.AutoPermissions
import com.pedro.library.AutoPermissionsListener
import com.sachtechsolution.neighbourhood.basepackage.base.BaseActivity
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.openActivity
import com.sachtechsolution.neighbourhood.extension.showToast
import com.sachtechsolution.neighbourhood.model.User
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.sachtechsolution.neighbourhood.ui.login.LoginActivity
import com.squareup.otto.Subscribe
import android.widget.Toast
import android.content.pm.PackageManager
import android.content.pm.PackageInfo
import android.util.Base64
import android.util.Log
import java.security.MessageDigest
import java.security.NoSuchAlgorithmException
import com.facebook.FacebookSdk.getApplicationContext
import com.google.android.gms.common.util.ClientLibraryUtils.getPackageInfo
import android.app.Activity




class SplashScreen : BaseActivity(), AutoPermissionsListener {

    override fun onDenied(requestCode: Int, permissions: Array<String>) {
    }


    override fun fragmentContainer(): Int {
        return R.id.container
    }


    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
    }


    @Subscribe
    fun getMessage(internetStatus:String) {

        if (internetStatus.equals("Not connected to Internet")){
            InternetStatusDialogFragment.newInstance().show(supportFragmentManager, "option_fragment")
        }else {
            showToast("internet enabled ")

        }

    }

    override fun onResume() {
        super.onResume()
        OttoBus.bus.register(this)
    }

    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }

    override fun onGranted(requestCode: Int, permissions: Array<String>) {

        Handler().postDelayed({
            if (pref.getUserFromPref()!=null && !fireStoreHelper.currentUserUid().isNullOrEmpty()) {
                openActivity<HomePage>()
            } else {
                openActivity<LoginActivity>()
            }
            finish()
        }, 2000)

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

      //  printKeyHash(this)
        AutoPermissions.loadAllPermissions(this, 1)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        AutoPermissions.parsePermissions(this, requestCode, permissions, this)
    }


    fun printKeyHash(context: Activity): String? {
        val packageInfo: PackageInfo
        var key: String? = null
        try {
            //getting application package name, as defined in manifest
            val packageName = context.applicationContext.packageName

            //Retriving package info
            packageInfo = context.packageManager.getPackageInfo(packageName,
                    PackageManager.GET_SIGNATURES)

            Log.e("Package Name=", context.applicationContext.packageName)

            for (signature in packageInfo.signatures) {
                val md = MessageDigest.getInstance("SHA")
                md.update(signature.toByteArray())
                key = String(Base64.encode(md.digest(), 0))

                // String key = new String(Base64.encodeBytes(md.digest()));
                Log.e("Key Hash=", key)
            }
        } catch (e1: PackageManager.NameNotFoundException) {
            Log.e("Name not found", e1.toString())
        } catch (e: NoSuchAlgorithmException) {
            Log.e("No such an algorithm", e.toString())
        } catch (e: Exception) {
            Log.e("Exception", e.toString())
        }

        return key
    }

    
}
