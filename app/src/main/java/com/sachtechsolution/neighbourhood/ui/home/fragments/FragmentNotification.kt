package com.sachtechsolution.neighbourhood.ui.home.fragments

import android.os.Bundle
import android.view.View
import com.example.manish.internetstatus.OttoBus
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.constants.Constants
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.gone
import com.sachtechsolution.neighbourhood.extension.visible
import com.sachtechsolution.neighbourhood.model.Notification
import com.sachtechsolution.neighbourhood.model.Post
import com.sachtechsolution.neighbourhood.model.User
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.sachtechsolution.neighbourhood.ui.home.adapters.NotificationAdapter
import com.sachtechsolution.neighbourhood.ui.mail.data.SendInviteCodeFragment
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_notification.*

class FragmentNotification : BaseFragment() {

    override fun viewToCreate(): Int {
        return R.layout.fragment_notification
    }

    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
    }


    @Subscribe
    fun getMessage(internetStatus:String) {

        if (internetStatus.equals("Not connected to Internet")){
            InternetStatusDialogFragment.newInstance().show(fragmentManager, "option_fragment")
        }else {
            showToast("internet enabled ")
        }
    }



    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }
    private lateinit var adapter: NotificationAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        txtInvite.setOnClickListener {baseAcitivityListener.navigator.replaceFragment(SendInviteCodeFragment::class.java,true)  }
        adapter = NotificationAdapter(activity, baseAcitivityListener)
        notification_rv.adapter = adapter
        notification_text.visible()
        fireStoreHelper.getNotification().addOnFailureListener { }.addOnCompleteListener {
            if (it.isSuccessful) {
                val noties=it.result.documents
                noties.forEach {
                    val notification = it.toObject(Notification::class.java)
                    fireStoreHelper.getUser(notification!!.suid).addOnCompleteListener {
                        if (it.isSuccessful && it.result.exists()) {
                            val user = it.result.toObject(User::class.java)
                            fireStoreHelper.getPostsById(notification.pid).addOnCompleteListener {
                                if (it.isSuccessful && it.result.exists()) {
                                    var post = it.result.toObject(Post::class.java)
                                    if (notification.pid.contains("#"))
                                        post!!.pid=notification.pid.replace(notification.pid.substring(notification.pid.length-1), "")

                                    else if(notification.pid.contains("poll"))
                                        post!!.pid=notification.pid.replace(notification.pid.substring(notification.pid.length-1), "")

                                    else{
                                    post!!.pid=notification.pid}
                                    adapter.addNotification(notification!!, user, post!!)
                                }
                                notification_text?.gone()
                            }
                        }
                    }
                }
                if (noties.size==0)
                    notification_text?.text="No Notification"
            }
        }
    }

    override fun onResume() {
        super.onResume()
        OttoBus.bus.register(this)
        if (Constants.DELETED_POSITION!=-1)
            adapter.removeItem(Constants.DELETED_POSITION)
        intracterListner!!.updateSelectedPos(2)
    }
}