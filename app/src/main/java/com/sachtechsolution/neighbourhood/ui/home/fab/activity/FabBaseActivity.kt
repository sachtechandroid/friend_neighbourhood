package com.sachtechsolution.neighbourhood.ui.home.fab.activity

import android.os.Bundle
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseActivity
import com.sachtechsolution.neighbourhood.ui.home.fab.fragment.event.FragmentEvent
import com.sachtechsolution.neighbourhood.ui.home.fab.fragment.message.fragment.FragmentFabMessage
import com.sachtechsolution.neighbourhood.ui.home.fab.fragment.poll.FragmentPoll
import com.sachtechsolution.neighbourhood.ui.home.fab.fragment.private_message.FragmentPrivateMessage
import com.sachtechsolution.neighbourhood.ui.home.fab.fragment.urgent_alert.FragmentUrgentAlert

class FabBaseActivity : BaseActivity() {

    override fun fragmentContainer(): Int {
        return R.id.container
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_fab_base)

        val fragment: String = intent.getStringExtra(FRAGMENT_KEY)

        if (fragment.equals(PRIVATE_MESSAGE))
            navigator.replaceFragment(FragmentPrivateMessage::class.java, true)
        else if (fragment.equals(URGENT_ALERT))
            navigator.replaceFragment(FragmentUrgentAlert::class.java, true)
        else if (fragment.equals(EVENT))
            navigator.replaceFragment(FragmentEvent::class.java, true)
        else if (fragment.equals(POLL))
            navigator.replaceFragment(FragmentPoll::class.java, true)
        else if (fragment.equals(MESSAGE))
            navigator.replaceFragment(FragmentFabMessage::class.java, true)
    }


    companion object {
        var FRAGMENT_KEY = "fragment"
        var PRIVATE_MESSAGE = "privateMessaeg"
        var URGENT_ALERT = "argentAlert"
        var EVENT = "event"
        var POLL = "poll"
        var MESSAGE = "messge"
    }
}
