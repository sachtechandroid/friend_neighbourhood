package com.sachtechsolution.neighbourhood.ui.home.fragments

import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.constants.NearestBussiness
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.gone
import com.sachtechsolution.neighbourhood.extension.showToast
import com.sachtechsolution.neighbourhood.intracter.Api_Hitter
import com.sachtechsolution.neighbourhood.model.places_bussiness.ResultsItem
import com.sachtechsolution.neighbourhood.model.places_bussiness.SearchPlaced
import kotlinx.android.synthetic.main.fragment_top_of_year.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList

class TopOfYearRecommendations :BaseFragment(), TopYearAdapter.OnClick {

    override fun onBussinessClick(type: String, resultsItem: NearestBussiness) {

    }

    var mplace: Array<out String>? = null
    var position = 0
    var images = ArrayList<Int>()
    var places: Array<out String>? = null
    val radius="8000"
    val list = java.util.ArrayList<NearestBussiness>()
    var bussiness: Call<SearchPlaced>?=null
    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }
    var topYearAdapter :TopYearAdapter?=null



    override fun viewToCreate(): Int {
      return R.layout.fragment_top_of_year
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        txtyear.text = "Announcing the ${Calendar.getInstance()[Calendar.YEAR]}"
        topYearAdapter =TopYearAdapter(activity!!, this)
        recyclerView.adapter=topYearAdapter
        getPlaces()
    }


    private fun getPlaces() {

        mplace = resources.getStringArray(R.array.place_type)
        if (position < mplace!!.size)
            getDataForPlaces(mplace!![position], ""+pref.getUserFromPref()!!.lat+","+pref.getUserFromPref()!!.lng,radius)
    /*else{

        }*/
    }

    private fun getDataForPlaces(type: String, location: String, radius: String) {

        if (bussiness!=null)
            bussiness=null
        bussiness = Api_Hitter.ApiInterface("https://maps.googleapis.com/maps/api/place/nearbysearch/").getBussiness(type, location,radius)
        bussiness!!.enqueue(object : Callback<SearchPlaced> {
            override fun onResponse(call: Call<SearchPlaced>, response: Response<SearchPlaced>) {

                if (response.body()!!.results!!.isNotEmpty()) {
                    val data = response.body()!!.results
                    if (data != null) {
                        if (pBar!=null)
                        {
                            pBar.gone()
                        }
                        topYearAdapter!!.update(filterList(data as List<ResultsItem>)[0])
                        position += 1
                        getPlaces()
                        }
                }else {
                    getDataForPlaces(type,location,radius)
                }
            }

            override fun onFailure(call: Call<SearchPlaced>, t: Throwable) {
                Toast.makeText(context, t.message, Toast.LENGTH_SHORT).show()
            }
        })


    }



    private fun filterList(list: List<ResultsItem>): ArrayList<ResultsItem> {
        val filterList = arrayListOf<ResultsItem>()
        filterList.addAll(list)
        return filterList.apply { sortByDescending { nearestBussiness -> nearestBussiness.rating } }
    }

}
