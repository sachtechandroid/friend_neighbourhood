package com.sachtechsolution.neighbourhood.ui.local_agencies

import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.intracter.Api_Hitter
import com.sachtechsolution.neighbourhood.model.places_bussiness.ResultsItem
import com.sachtechsolution.neighbourhood.model.places_bussiness.SearchPlaced
import com.sachtechsolution.neighbourhood.ui.mail.data.SendInviteCodeFragment
import kotlinx.android.synthetic.main.fragment_local_agencies.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class LocalAgenciesFragment : BaseFragment() {

    val localAgencyAdapter by lazy { LocalAgencyAdapter(context!!, pBar) }
    var count = 0
    val user by lazy { pref.getUserFromPref() }
    var bussiness: Call<SearchPlaced>? = null
    val list = ArrayList<ResultsItem?>()


    override fun onResume() {
        super.onResume()
        getData()
    }

    override fun viewToCreate(): Int {
        return R.layout.fragment_local_agencies
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView_local_agencies.adapter = localAgencyAdapter

        invite.setOnClickListener { baseAcitivityListener.navigator.replaceFragment(SendInviteCodeFragment::class.java, true) }
        etSearch.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
            }

            override fun afterTextChanged(p0: Editable?) {
                val filterlist=filterList(etSearch.text.toString(),list)
                localAgencyAdapter.list.clear()
                if (filterlist.isEmpty()) {
                    localAgencyAdapter.update(list)
                    localAgencyAdapter.notifyDataSetChanged()
                }
                else {
                    localAgencyAdapter.update(filterlist)
                    localAgencyAdapter.notifyDataSetChanged()
                }

            }
        })
    }

    private fun getData() {
        val agency = resources.getStringArray(R.array.agency_array)
        if (count < agency.size - 1)
            getAgency(agency[count], user!!.lat.toString() + "," + user!!.lng.toString(), "12000",agency.size)
    }


    private fun getAgency(type: String, location: String, radius: String, size: Int) {

        if (bussiness != null)
            bussiness = null

        bussiness = Api_Hitter.ApiInterface("https://maps.googleapis.com/maps/api/place/nearbysearch/").getBussiness(type, location, radius)
        bussiness!!.enqueue(object : Callback<SearchPlaced> {
            override fun onResponse(call: Call<SearchPlaced>, response: Response<SearchPlaced>) {

                if (response.body()!!.results!!.isNotEmpty()) {
                    val data = response.body()
                    if (data != null) {

                        localAgencyAdapter.update(data.results)
                        localAgencyAdapter.notifyDataSetChanged()
                        list.addAll(data.results!!)
                        getData()
                    }
                } else {
                    getAgency(type, location, radius, size)
                }
            }

            override fun onFailure(call: Call<SearchPlaced>, t: Throwable) {
                    hideProgress()
            }
        })

    }


    private fun filterList(text: String, list: ArrayList<ResultsItem?>): List<ResultsItem?> {
        return list.filter {
            it?.name?.contains(text,true) == true }
    }

    override fun onPause() {
        super.onPause()
        hideProgress()
        bussiness!!.cancel()
    }
}