package com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings.profile

import android.os.Bundle
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseActivity

class AddFamilyBaseActivity : BaseActivity() {

    override fun fragmentContainer(): Int {
        return R.id.container
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_family_base)
        navigator.replaceFragment(FamilyMemberFragment::class.java, true)
    }
}
