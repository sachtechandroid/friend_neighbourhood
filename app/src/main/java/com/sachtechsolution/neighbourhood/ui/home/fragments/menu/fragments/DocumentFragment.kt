package com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments

import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.MenuItem
import android.view.View
import com.example.manish.internetstatus.OttoBus
import com.google.common.eventbus.Subscribe
import com.google.firebase.firestore.DocumentSnapshot
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.constants.Constants
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.*
import com.sachtechsolution.neighbourhood.model.Ads
import com.sachtechsolution.neighbourhood.model.Post
import com.sachtechsolution.neighbourhood.model.User
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.sachtechsolution.neighbourhood.ui.home.adapters.HomePagePostAdapter
import com.sachtechsolution.neighbourhood.ui.home.fab.fragment.message.fragment.CreatePostFragment
import kotlinx.android.synthetic.main.fragment_document.*

class DocumentFragment : BaseFragment(), HomePagePostAdapter.OpenBottomSheet {

    override fun viewToCreate(): Int {
        return R.layout.fragment_document
    }

    var postId = ""
    var postPosition = 0
    var adapter: HomePagePostAdapter? = null


    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
    }


    @Subscribe
    fun getMessage(internetStatus: String) {

        if (internetStatus.equals("Not connected to Internet")) {
            InternetStatusDialogFragment.newInstance().show(fragmentManager, "option_fragment")
        } else {
            getAllData()
            showToast("internet enabled ")

        }

    }

    override fun OnOpenBottomSheet(pid: String, position: Int, type: String, post: Post, uid: String) {
        postId = pid
        postPosition = position
        if (type.equals(DELETE_POST)) {
            deletePost()
        }
        if (type.equals(ADD_BOOKMARK)) {
            createBookmark()
        }
        if (type.equals(REMOVE_BOOKMARK)) {
            onRemoveBookmark(postId)
        }
        if (type.equals(REPORT_POST)) {
            reportPost(uid = uid, pid = pid, post = post)
        }

    }


    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }

    //lateinit var post: Post
    //val mainLocality by lazy { pref.getUserFromPref()?.mainLocality?: "" }
    val user by lazy { pref.getUserFromPref()!! }

    lateinit var postLis: MutableList<DocumentSnapshot>
    var postCount = -1
    private var adsList = ArrayList<Ads>()
    private var adsTotal: Int = 0
    private var adsCount: Int = 0


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter = HomePagePostAdapter(context!!, this, baseAcitivityListener, pref)
        recyclerView_document.adapter = adapter
        Constants.IS_THANKED_LIST.clear()
        Constants.IS_POLLED_LIST.clear()
        documentsBack.setOnClickListener {
            activity!!.onBackPressed()
        }
        home_welcome_text.visible()
        postLis = ArrayList()
        getAllData()
        new_document.setOnClickListener {
            if (context!!.isLocationEnabled(context!!)&&context!!.getLocationMode() == 3) {
                Constants.NEXT = "general"
                baseAcitivityListener.navigator.replaceFragment(CreatePostFragment::class.java, true, Bundle().apply { putString("id", Post.postType.GENERAL) })
            }
            else
            {
                context!!.showToast("Enable Location with high accuracy option")
                startActivity(Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS))
            }
        }
    }

    private fun getAllData() {
        fireStoreHelper.getAllPosts(zipCode = user.zipCode, society = user.society).addOnCompleteListener {
            if (it.isSuccessful) {
                val list = it.result.documents
                list.forEach {
                    if (!it.toObject(Post::class.java)?.pdfLink.isNullOrEmpty())
                        postLis.add(it)
                }
                if (postLis.size == 0)
                    home_welcome_text.text = "No Document Found"
                getPost()
            } else {
                //showToast("something went wrong")
            }
        }.addOnFailureListener {
            Log.e("", "")
        }

    }

    private var previousSize: Int = 0

    private fun postAds() {
        if (adsTotal > adsCount && adapter!!.postsList.size > previousSize + (3..6).shuffled().first()) {
            adapter!!.addItem(Post(type = Post.postType.ADS, message = adsList[adsCount].title, imagesLinks = adsList[adsCount].imageLink), null, false, false)
            previousSize = adapter!!.postsList.size
            adsCount++
        }
    }

    private fun getPost() {
        postCount++
        if (postLis.size > postCount) {
            var post = postLis[postCount].toObject(Post::class.java)
            post!!.pid = postLis[postCount].id
            if (isPostHasTime(post.date) && home_welcome_text != null)
                getPostUser(post)
        }
    }

    private fun getPostUser(post: Post) {
        fireStoreHelper.getUser(post!!.uid).addOnCompleteListener {
            if (it.isSuccessful) {
                if (it.result.exists()) {
                    val user = it.result.toObject(User::class.java)
                    fireStoreHelper.getThanks(post.pid).addOnCompleteListener {
                        if (it.isSuccessful) {
                            var isThanked = false
                            if (it.result.exists())
                                isThanked = true
                            if (post.type.equals(Post.postType.POLL)) {
                                fireStoreHelper.getIsPollAnswered(post.pid).addOnCompleteListener {
                                    if (it.isSuccessful) {
                                        var isPolled = false
                                        if (it.result.documents.size > 0)
                                            isPolled = true
                                        adapter!!.addItem(post, user!!, isThanked, isPolled)
                                        getPost()
                                        if (adsTotal > 0)
                                            postAds()
                                    } else {
                                        adapter!!.addItem(post, user!!, isThanked, false)
                                        getPost()
                                        if (adsTotal > 0)
                                            postAds()
                                    }
                                }.addOnFailureListener {
                                    getPost()
                                    showToast("")
                                }.addOnCanceledListener {
                                    showToast("")
                                    getPost()
                                }
                            } else {
                                getPost()
                                if (adsTotal > 0)
                                    postAds()
                                adapter!!.addItem(post, user!!, isThanked, false)
                            }
                            home_welcome_text?.gone()
                        } else {
                            getPost()
                        }
                    }
                }
            }
        }.addOnFailureListener { getPost() }
    }

    override fun onResume() {
        super.onResume()
        OttoBus.bus.register(this)
        //getAllPost()
        if (Constants.POSITION != -1) {
            if (Constants.POST != null)
                adapter?.postsList?.set(Constants.POSITION, Constants.POST)
            adapter!!.notifyItemChanged(Constants.POSITION)
            Constants.POSITION = -1
            Constants.POST = null
        }
        if (Constants.DELETED_POSITION != -1) {
            val position = Constants.DELETED_POSITION
            adapter!!.deletePost(position)
            Constants.DELETED_POSITION = -1
        }
        //intracterListner!!.updateSelectedPos(0)
    }

    fun deletePost() {
        showProgress()
        fireStoreHelper.deletePost(postId).addOnCompleteListener {
            if (it.isSuccessful) {
                onRemoveBookmark(postId)
            } else {
                //  showToast("error")
                hideProgress()
            }

        }.addOnFailureListener {
            //  showToast("failed")
            hideProgress()
        }
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.menuPrivateMessage -> {
                context!!.showToast("clicked")
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun createBookmark() {

        fireStoreHelper.createBookmark(Post.Bookmark(postId)).addOnCompleteListener {
            if (it.isSuccessful) {
                activity!!.showToast("Added to Bookmark")
            }
        }.addOnFailureListener {
            //activity!!.showToast("error to add bookmark")
        }
    }

    fun onRemoveBookmark(pid: String) {
        fireStoreHelper.removeBookmark(pid).addOnCompleteListener {
            if (it.isSuccessful) {
                showToast("Post deleted")
                adapter!!.deletePost(postPosition)
                adapter!!.notifyDataSetChanged()
                hideProgress()
            } else {
                hideProgress()
                context!!.showToast("unable to remove bookmark")
            }
        }.addOnFailureListener {
            hideProgress()
            //context!!.showToast("unable to remove bookmark")
        }.addOnCanceledListener {
            hideProgress()
            // context!!.showToast("unable to remove bookmark")
        }
    }

    companion object {
        val DELETE_POST = "deletePost"
        val ADD_BOOKMARK = "add_bookmark"
        val REMOVE_BOOKMARK = "remove_bookmark"
        val REPORT_POST = "report"
    }

    fun reportPost(uid: String, pid: String, post: Post) {
        fireStoreHelper.createReport(uid = uid, pid = pid, post = post).addOnCompleteListener {
            if (it.isSuccessful) {
                showToast("Reported Successfully")
            } else {
                showToast("Not Reported")
            }
        }.addOnCanceledListener { showToast("Not Reported") }
                .addOnCanceledListener { showToast("Not Reported") }
    }
}
