package com.bususer.tiket

import android.os.Bundle
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseActivity
import com.sachtechsolution.neighbourhood.ui.home.fab.fragment.message.fragment.sale_and_free.fragment.SelectCategoryFragment

class AddPostForSale : BaseActivity(){

    override fun fragmentContainer(): Int {
        return R.id.container
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_addpost_forsale)
        navigator.replaceFragment(SelectCategoryFragment::class.java,true)
    }


}
