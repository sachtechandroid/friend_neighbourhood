package com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings.profile.fragment

import android.os.Bundle
import android.view.View
import com.example.manish.internetstatus.OttoBus
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.constants.ObjBiography
import com.sachtechsolution.neighbourhood.extension.showToast
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings.profile.FillUpProfileActivity
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_do_for_work.*

class AboutDoForWorkFragment:BaseFragment() {


    override fun viewToCreate(): Int {
        return R.layout.fragment_do_for_work
    }
    override fun onResume() {
        super.onResume()
        OttoBus.bus.register(this)
        FillUpProfileActivity.mcount!!.text="3 of 4"
    }


    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
    }


    @Subscribe
    fun getMessage(internetStatus:String) {

        if (internetStatus.equals("Not connected to Internet")){
            InternetStatusDialogFragment.newInstance().show(fragmentManager, "option_fragment")
        }else {
            showToast("internet enabled ")

        }

    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        btnNext.setOnClickListener {

            if(etJobTitle.text.isEmpty()||etCompany.text.isEmpty()||etCity.text.isEmpty())
                context!!.showToast("fill all field")

            else
            {
                ObjBiography.jobTitle=etJobTitle.text.toString()
                ObjBiography.city=etCity.text.toString()
                ObjBiography.company=etCompany.text.toString()
                baseAcitivityListener.navigator.replaceFragment(AboutBackgroundFragment::class.java,true)
            }

        }
    }
}