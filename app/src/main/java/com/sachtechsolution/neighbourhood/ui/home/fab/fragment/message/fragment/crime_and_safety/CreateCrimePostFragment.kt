package com.sachtechsolution.neighbourhood.ui.home.fab.fragment.message.fragment.crime_and_safety

import android.net.Uri
import android.os.Bundle
import android.view.View
import com.example.manish.internetstatus.OttoBus
import com.google.android.gms.tasks.OnSuccessListener
import com.google.firebase.storage.StorageReference
import com.sachtechsolution.neighbourhood.HomePage
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseFragment
import com.sachtechsolution.neighbourhood.constants.Constants
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.gone
import com.sachtechsolution.neighbourhood.model.Post
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.squareup.otto.Subscribe
import kotlinx.android.synthetic.main.fragment_create_crime_post.*
import java.util.*
import kotlin.collections.ArrayList

class CreateCrimePostFragment : BaseFragment() {

    val fireStoreHelper by lazy { FireStoreHelper.getInstance() }
    val subject by lazy { arguments?.getString("subject") ?: "" }
    val message by lazy { arguments?.getString("message") ?: "" }
    var imagesLinks = ""
    private var size = 0
    private var count = 0
    lateinit var mRef: StorageReference


    override fun viewToCreate(): Int {
        return R.layout.fragment_create_crime_post
    }


    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
    }


    @Subscribe
    fun getMessage(internetStatus: String) {

        if (internetStatus.equals("Not connected to Internet")) {
            InternetStatusDialogFragment.newInstance().show(fragmentManager, "option_fragment")
        } else {
            showToast("internet enabled ")

        }

    }

    override fun onResume() {
        super.onResume()
        OttoBus.bus.register(this)
    }


    lateinit var list: ArrayList<String>

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val user = pref.getUserFromPref()!!

        list = ArrayList()
        list.add(user.society)
        sociteyRB.text = user.society
        subLocalityRB.text = user.street
        zipCodeRB.text = user.mainLocality + " + Nearby"

        if (user.society.equals(user.street)) {
            sociteyRB.gone()
            subLocalityRB.isChecked = true
        }

        sociteyRB.setOnCheckedChangeListener { buttonView, isChecked ->

            if (isChecked) {
                list.clear()
                list.add(user.society)
            }
        }

        subLocalityRB.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                list.clear()
                list = Constants.SOCIETIES
            }
        }
        zipCodeRB.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                list.clear()
                list = Constants.ADDRESSES
            }
        }

        crime_create_post_back.setOnClickListener { activity!!.onBackPressed() }
        crime_create_post_tv.setOnClickListener {
            showProgress()
            size = Constants.IMAGE_LIST.size
            if (size == 0)
                createPost()
            else
                uploadImage()
        }
    }


    private fun uploadImage() {

        var time = java.sql.Timestamp(Date().time)
        var timestamp = time.toString().replace(":", "").replace("-", "").replace(" ", "").replace(".", "")
        mRef = fireStoreHelper.getStorageRef().child("postImages").child(fireStoreHelper.currentUserUid() + timestamp)
        mRef.putFile(Constants.IMAGE_LIST[count]!!).addOnCompleteListener {

            if (it.isSuccessful) {
                it.result
                mRef.downloadUrl.addOnSuccessListener(object : OnSuccessListener<Uri> {
                    override fun onSuccess(p0: Uri?) {
                        count = count + 1
                        imagesLinks = imagesLinks + p0.toString() + "#"
                        if (size == count)
                            createPost()
                        else
                            uploadImage()
                    }
                }).addOnFailureListener {
                    hideProgress()
                }
            }
        }.addOnFailureListener {
            hideProgress()
        }
    }

    private fun createPost() {

        val post = Post(subject = subject, message = message,
                uid = fireStoreHelper.currentUserUid(), zipCode = pref.getUserFromPref()!!.zipCode, society = pref.getUserFromPref()!!.society,
                type = Post.postType.CRIME_SAFETY, imagesLinks = imagesLinks, privacy = list/*adapter.addressList*/)

        fireStoreHelper.createPost(post).addOnCompleteListener {
            if (it.isSuccessful) {
                next()
                /*fireStoreHelper.getAllNeighbors(pref.getUserFromPref()!!.zipCode).addOnCompleteListener {
                    if (it.isSuccessful) {
                        var token = ""
                        it.result.documents.forEach {
                            if (!it.id.equals(pref.getUserFromPref()!!.uid) && list.contains(it.toObject(User::class.java)!!.society))
                                token = token + "," + it.toObject(User::class.java)!!.token
                        }

                        if (!token.isNullOrEmpty()) {
                            token = token.substring(1)
                            val title = "${pref.getUserFromPref()!!.firstName} ${pref.getUserFromPref()!!.lastName} just posted in neighborhood"

                            val callback = Api_Hitter.ApiInterface("http://stsmentor.com/").sendNotificationToAll(title, token, "zxc")
                            callback.enqueue(object : Callback<ResponseBody> {
                                override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                                    next()
                                }

                                override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {
                                    next()
                                }
                            })
                        }
                    } else
                        next()
                }.addOnFailureListener {
                    next()*/
                //}

            } else {
                hideProgress()
                showToast("unable to post")
            }
        }.addOnFailureListener {
            showToast("unable to post")
            hideProgress()
        }
    }

    fun next() {
        hideProgress()
        showToast("Posted")
        baseAcitivityListener.navigator.openActivity(HomePage::class.java)
        activity!!.finish()
    }
}
