package com.sachtechsolution.neighbourhood.ui.extra

import android.os.Bundle
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseActivity
import com.sachtechsolution.neighbourhood.ui.extra.fragments.ImageViewFragment
import com.sachtechsolution.neighbourhood.ui.extra.fragments.SearchFragment

class ExtraActivity : BaseActivity() {

    override fun fragmentContainer(): Int {
        return R.id.extra_container
    }

    val toString by lazy { intent.getStringExtra("to")?:"" }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_extra)

        if (toString.equals("search"))
            getNavigator().replaceFragment(SearchFragment::class.java, false)

        else {
            val images=intent.getStringExtra("images")
            getNavigator().replaceFragment(ImageViewFragment::class.java, true,
                    Bundle().apply { putString("images",images)  })
        }
    }

    
}
