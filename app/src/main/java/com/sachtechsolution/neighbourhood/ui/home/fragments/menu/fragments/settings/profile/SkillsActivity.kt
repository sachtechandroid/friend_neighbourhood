package com.sachtechsolution.neighbourhood.ui.home.fragments.menu.fragments.settings.profile

import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import com.example.manish.internetstatus.OttoBus
import com.sachtechsolution.neighbourhood.R
import com.sachtechsolution.neighbourhood.basepackage.base.BaseActivity
import com.sachtechsolution.neighbourhood.basepackage.base.ProgressDialogFragment
import com.sachtechsolution.neighbourhood.database.FireStoreHelper
import com.sachtechsolution.neighbourhood.extension.showToast
import com.sachtechsolution.neighbourhood.model.Biography
import com.sachtechsolution.neighbourhood.otto_bus.InternetStatusDialogFragment
import com.squareup.otto.Subscribe
import fisk.chipcloud.ChipCloud
import fisk.chipcloud.ChipCloudConfig
import kotlinx.android.synthetic.main.activity_skills.*

class SkillsActivity : BaseActivity() {

    override fun fragmentContainer(): Int {
        return 0
    }

    var list:ArrayList<String>?=null
    var selectedList=""
    val fireStoreHelper:FireStoreHelper by lazy { FireStoreHelper.getInstance() }
    var chipCloud: ChipCloud?=null



    override fun onPause() {
        super.onPause()
        OttoBus.bus.unregister(this)
    }


    @Subscribe
    fun getMessage(internetStatus:String) {

        if (internetStatus.equals("Not connected to Internet")){
            InternetStatusDialogFragment.newInstance().show(supportFragmentManager, "option_fragment")
        }else {
            showToast("internet enabled ")

        }

    }

    override fun onResume() {
        super.onResume()
        OttoBus.bus.register(this)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_skills)
        list= ArrayList()

        getServices()
        getLanguages()
        getEmergency()
        getOther()

        txtDone.setOnClickListener {
            uploadData(filterData())
        }
    }
    private fun filterData(): Biography.Skills {

        for (i in 0 until list!!.size) {


            if (i==list!!.lastIndex)
                selectedList += list!![i]
            else
                selectedList += list!![i]+","
        }

        return Biography.Skills(skills = selectedList)
    }

    private fun uploadData(interest: Biography.Skills) {
        /*intent.putStringArrayListExtra("recommendation",recommendation)
        setResult(2,intent)
       */
        showProgress()
        fireStoreHelper.updateSkills(interest,pref.getUserFromPref()!!.uid).addOnCompleteListener {
            if (it.isSuccessful) {
                showToast("Updated")
                hideProgress()
                var intent= Intent()
                intent.putStringArrayListExtra("recommendation",list)
                setResult(3,intent)
                finish()

            } else {
                showToast("error")
                ProgressDialogFragment.hideProgress()
            }
            ProgressDialogFragment.hideProgress()
        }.addOnFailureListener {
            showToast("error")
            ProgressDialogFragment.hideProgress()
        }
    }


    private fun getOther() {
        val config = ChipCloudConfig()
                .selectMode(ChipCloud.SelectMode.multi)
                .checkedChipColor(resources.getColor(R.color.optional))
                .checkedTextColor(Color.parseColor("#ffffff"))
                .uncheckedChipColor(Color.parseColor("#e0e0e0"))
                .uncheckedTextColor(resources.getColor(R.color.optional))

        chipCloud = ChipCloud(this, cvOther, config)
        chipCloud!!.addChip("Add your own...")

        chipCloud!!.setListener { index, checked, userClick ->
            if (userClick) {
                if (chipCloud!!.getLabel(index).equals("Add your own...")){
                openActivity<MoreChipsActivty>(2)
            }else{

                if (checked)
                    list!!.add(chipCloud!!.getLabel(index))
                else {
                    for (i in 0..list!!.size) {
                        if (list!![i].equals(chipCloud!!.getLabel(index).toString())) {
                            list!!.removeAt(i)
                            break
                        }
                    }
                }
            }


            }

        }
    }

    private fun getServices() {
        val config = ChipCloudConfig()
                .selectMode(ChipCloud.SelectMode.multi)
                .checkedChipColor(resources.getColor(R.color.optional))
                .checkedTextColor(Color.parseColor("#ffffff"))
                .uncheckedChipColor(Color.parseColor("#e0e0e0"))
                .uncheckedTextColor(resources.getColor(R.color.optional))

        val chipCloud = ChipCloud(this, cvServices, config)
        val demoArray = resources.getStringArray(R.array.array_services)
        chipCloud.addChips(demoArray)

        chipCloud.setListener { index, checked, userClick ->
            if (userClick) {
                if (checked)
                    list!!.add(chipCloud.getLabel(index))
                else {
                    for (i in 0..list!!.size) {
                        if (list!![i].equals(chipCloud.getLabel(index).toString())) {
                            list!!.removeAt(i)
                            break
                        }
                    }
                }

            }

        }
    }

    private fun getLanguages() {
        val config = ChipCloudConfig()
                .selectMode(ChipCloud.SelectMode.multi)
                .checkedChipColor(resources.getColor(R.color.optional))
                .checkedTextColor(Color.parseColor("#ffffff"))
                .uncheckedChipColor(Color.parseColor("#e0e0e0"))
                .uncheckedTextColor(resources.getColor(R.color.optional))


        var chipCloud = ChipCloud(this, cvLanguages, config)
        val demoArray = resources.getStringArray(R.array.array_languages)
        chipCloud.addChips(demoArray)

        chipCloud.setListener { index, checked, userClick ->
            if (userClick) {
                if (chipCloud.getLabel(index).equals("Add your own...")){
                    openActivity<MoreChipsActivty>(2)
                }else{

                    if (checked)
                        list!!.add(chipCloud.getLabel(index))
                    else {
                        for (i in 0..list!!.size) {
                            if (list!![i].equals(chipCloud.getLabel(index).toString())) {
                                list!!.removeAt(i)
                                break
                            }
                        }
                    }
                }
            }

        }
    }

    private inline fun <reified I> openActivity(requestCode: Int) {
        var intent=Intent(this, I::class.java)
        intent.putExtra("code",requestCode)
        startActivityForResult(intent,requestCode)
    }


    private fun getEmergency() {
        val config = ChipCloudConfig()
                .selectMode(ChipCloud.SelectMode.multi)
                .checkedChipColor(resources.getColor(R.color.optional))
                .checkedTextColor(Color.parseColor("#ffffff"))
                .uncheckedChipColor(Color.parseColor("#e0e0e0"))
                .uncheckedTextColor(resources.getColor(R.color.optional))

        val chipCloud = ChipCloud(this, cvEmergencySkills, config)
        val demoArray = resources.getStringArray(R.array.array_emergency_skills)
        chipCloud.addChips(demoArray)

        chipCloud.setListener { index, checked, userClick ->
            if (userClick) {
                if (checked)
                    list!!.add(chipCloud.getLabel(index))
                else {
                    for (i in 0..list!!.size) {
                        if (list!![i].equals(chipCloud.getLabel(index).toString())) {
                            list!!.removeAt(i)
                            break
                        }
                    }
                }

            }

        }
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        // check if the request code is same as what is passed  here it is 2
        if (requestCode == 2&&data!=null) {

            var list = data!!.getStringExtra("data")

            chipCloud!!.addChip(list)
        }
    }

}
