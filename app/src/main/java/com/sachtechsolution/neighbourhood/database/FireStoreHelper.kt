package com.sachtechsolution.neighbourhood.database

import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.FirebaseFirestoreSettings
import com.google.firebase.firestore.Query
import com.google.firebase.storage.FirebaseStorage
import com.sachtechsolution.neighbourhood.constants.InvitationCode
import com.sachtechsolution.neighbourhood.constants.PetDirectory
import com.sachtechsolution.neighbourhood.constants.PostInterests
import com.sachtechsolution.neighbourhood.model.*

class FireStoreHelper {

    private val firestoreRef by lazy {
        FirebaseFirestore.getInstance().apply {
            firestoreSettings = FirebaseFirestoreSettings.Builder()
                    .setPersistenceEnabled(true)
                    .build()
        }
    }

    val fStorageRef by lazy { FirebaseStorage.getInstance().reference }

    companion object {

        private val fireStoreHelper by lazy { FireStoreHelper() }
        fun getInstance(): FireStoreHelper {
            return fireStoreHelper
        }
    }

    fun getStoreRef() = firestoreRef

    fun getStorageRef() = fStorageRef

    fun getInvitationCodeeRef() = getStoreRef().collection("invitation_code")

    fun getUserRef() = getStoreRef().collection("users")

    fun getInterestPostRef() = getStoreRef().collection("interests_post")

    fun createReport(uid: String, pid: String, post: Post) = getStoreRef().collection("report").document(uid).collection("posts").document(pid).set(post)

    fun getBookmarkRef() = getStoreRef().collection("bookmark").document(currentUserUid()).collection("bookmark")

    fun getPostRef() = getStoreRef().collection("posts")


    fun getEstateRef() = getStoreRef().collection("real_estate")

    fun getReplyRef() = getStoreRef().collection("replies")

    fun getRecommRef() = getStoreRef().collection("recommendations")

    fun getThanksRef() = getStoreRef().collection("thanks")

    fun getPollRef() = getStoreRef().collection("polls")

    fun getAddressRef() = getStoreRef().collection("addresses")

    fun getAdsRef() = getStoreRef().collection("ads")

    fun getGroupRef() = getStoreRef().collection("group")

    fun getGroupPostsRef() = getStoreRef().collection("groupPost")

    fun setUser(uid: String?, user: User) = getUserRef().document(uid!!).set(user)

    fun getUser(uid: String?) = getUserRef().document(uid!!).get()

    fun getUserByEmail(email: String?) = getUserRef().whereEqualTo("email", email).get()

    fun getAllNeighbors(zipCode: String) = getUserRef().whereEqualTo("zipCode", zipCode).get()

    fun getNeighborsBySociety(society: String) = getUserRef().whereEqualTo("society", society).get()

    fun getNeighborsByStreet(street: String) = getUserRef().whereEqualTo("street", street).get()

    fun currentUserUid() = FirebaseAuth.getInstance().currentUser?.uid ?: ""

    // manage post methods
    fun createPost(post: Post) = getPostRef().add(post)

    fun updatePost(pid: String, post: Post) = getPostRef().document(pid).set(post)

    fun deletePost(id: String) = getPostRef().document(id).delete()

    fun getAllPosts(society: String, zipCode: String) = getPostRef()
            .whereEqualTo("zipCode", zipCode)
            .whereArrayContains("privacy", society)
            .orderBy("date", Query.Direction.DESCENDING).get()

    fun getPostsByType(type: String, society: String, zipCode: String) = getPostRef()
            .whereEqualTo("zipCode", zipCode)
            .whereEqualTo("type", type)
            .whereArrayContains("privacy", society)
            .orderBy("date", Query.Direction.DESCENDING)
            .get()

    fun getPostsBySociety(society: String, zipCode: String) = getPostRef()
            .whereEqualTo("zipCode", zipCode)
            .whereEqualTo("society", society)
            .whereArrayContains("privacy", society)
            .orderBy("date", Query.Direction.DESCENDING)
            .get()

    fun getSalePosts(zipCode: String) = getPostRef()
            .whereEqualTo("zipCode", zipCode)
            .whereEqualTo("type", Post.postType.FOR_SALE)
            .orderBy("date", Query.Direction.DESCENDING)
            .get()

    fun getOwnSalePosts() = getPostRef()
            .whereEqualTo("uid", currentUserUid())
            .whereEqualTo("type", Post.postType.FOR_SALE)
            .orderBy("date", Query.Direction.DESCENDING)
            .get()

    fun getPostsById(pid: String) = getPostRef().document(pid).get()

    fun getOwnPosts(uid: String) = getPostRef().whereEqualTo("uid", uid)
            .orderBy("date", Query.Direction.DESCENDING)
            .get()

    fun createReply(pid: String, reply: Reply) = getReplyRef().document(pid).collection("replies").add(reply)

    fun getReplies(pid: String) = getReplyRef().document(pid).collection("replies").orderBy("date", Query.Direction.DESCENDING).get()

    fun getThanks(pid: String) = getThanksRef().document(pid).collection("thanks").document(currentUserUid()).get()

    fun getThanksCount(pid: String) = getThanksRef().document(pid).collection("thanks").get()

    fun postThanks(pid: String) = getThanksRef().document(pid).collection("thanks").document(currentUserUid()).set(Thanks())


    fun removeThanks(pid: String) = getThanksRef().document(pid).collection("thanks").document(currentUserUid()).delete()
    // update profile methods
    fun getPersonalProfileRef(id: String) = getUserRef().document(id).collection("PersonalProfile")

    fun updateBiography(biography: Biography, uid: String) = getPersonalProfileRef(uid).document("biography").set(biography)

    fun updateInterest(interest: Biography.Interest, uid: String) = getPersonalProfileRef(uid).document("interest").set(interest)

    fun updateSkills(skills: Biography.Skills, uid: String) = getPersonalProfileRef(uid).document("skills").set(skills)

    //fun updateSignificant(family: Family.Significant,category:String)=getPersonalProfileRef().document("family").collection(category).document().set(family)

    fun updateChild(family: Family.Child, category: String, uid: String) = getPersonalProfileRef(uid).document("family").collection(category).document().set(family)

    fun updatePet(pet: Family.Pet, category: String, uid: String) = getPersonalProfileRef(uid).document("family").collection(category).document().set(pet)

    fun updatePetDirectory(pet: PetDirectory, uid: String) = getStoreRef().collection("pets").document(uid).collection("family").document().set(pet)

    fun updateLoveNeigh(love: Biography.AboutLoveNei, uid: String) = getPersonalProfileRef(uid).document("loveAboutNeighbours").set(love)

    fun updateEmergencyContacts(EmergencyContacts: EmergencyContacts, uid: String) = getPersonalProfileRef(uid).document("emergencyContacts").set(EmergencyContacts)

    fun updateContactsInformation(contacts: ContactInformation, uid: String) = getPersonalProfileRef(uid).document("contacts").set(contacts)

    fun updateProfilePic(image: String) = getUserRef().document(currentUserUid()).update("imageLink", image)


    //fetch Profile Methods

    fun getBiography(uid: String) = getPersonalProfileRef(uid).document("biography").get()

    fun getInterest(uid: String) = getPersonalProfileRef(uid).document("interest").get()

    fun getSkills(uid: String) = getPersonalProfileRef(uid).document("skills").get()

    fun getLoveNeigh(uid: String) = getPersonalProfileRef(uid).document("loveAboutNeighbours").get()

    fun getFamilyRef(uid: String) = getPersonalProfileRef(uid).document("family")

    fun getChild(uid: String) = getFamilyRef(uid).collection("child").get()

    fun getContactInformation(uid: String) = getPersonalProfileRef(uid).document("contacts").get()

    fun getPet(uid: String) = getFamilyRef(uid).collection("pet").get()

    fun getPetDirectory(uid: String) = getStoreRef().collection("pets").document(uid).collection("family").get()

    fun deletePet(id: String, uid: String) = getFamilyRef(uid).collection("pet").document(id).delete()

    fun deleteChild(id: String, uid: String) = getFamilyRef(uid).collection("child").document(id).delete()

    fun getPollResult(pid: String) = getPollRef().document(pid).collection("pollResult").get()

    fun getIsPollAnswered(pid: String) = getPollRef().document(pid).collection("pollResult").whereEqualTo("uid", currentUserUid()).get()

    fun answerPoll(pid: String, pollResult: PollResult) = getPollRef().document(pid).collection("pollResult").add(pollResult)


    //notification
    fun createNotification(uid: String, pid: String, notification: Notification) = getUserRef().document(uid).collection("notification").document(pid).set(notification)

    fun getNotification() = getUserRef().document(currentUserUid()).collection("notification").orderBy("date", Query.Direction.DESCENDING).get()

    fun deleteNotification(pid: String) = getUserRef().document(currentUserUid()).collection("notification").document(pid).delete()

    fun addAddress(zip: String, street: String, map: Map<String, ArrayList<String>>) = getAddressRef().document(zip)
            .collection("street").document(street).set(map)//collection("society").add(society)

    fun addAddressdummy(zip: String, map: Map<String, String>) = getAddressRef().document(zip)
            .set(map)//collection("society").add(society)

    fun getAddresses(zip: String) = getAddressRef().document(zip).collection("street").get()
/*
            .whereGreaterThan("lat",lat-Constants.REDIUS)
            .whereLessThan("lat",lat+Constants.REDIUS)
*/
    //.get()

    fun getRecommendation(id: String) = getRecommRef().document(id).collection("recommendations").orderBy("date", Query.Direction.DESCENDING).get()

    fun createRecommendaton(id: String, comment: RecoComment) = getRecommRef().document(id).collection("recommendations").document().set(comment)

    //bookmark
    fun createBookmark(bookmark: Post.Bookmark) = getBookmarkRef().document(bookmark.post_id).set(bookmark)

    fun getBookmark() = getBookmarkRef().get()

    fun getBookmarkById(id: String) = getBookmarkRef().whereEqualTo("post_id", id).get()

    fun removeBookmark(id: String) = getBookmarkRef().document(id).delete()

    //realEstate
    fun createEstate(estate: Post.RealEstate, pid: String) = getEstateRef().document(pid).set(estate)

    fun deleteEstate(pid: String) = getEstateRef().document(pid).delete()


    fun updateToken(map: HashMap<String, String>) = getUserRef().document(currentUserUid()).update(map as Map<String, Any>)

    fun getEatate(zipCode: String) = getEstateRef().whereEqualTo("zipCode", zipCode).get()

    fun getAds() = getAdsRef().get()

    fun addBussiness(bussiness: AddBussiness) = getStoreRef().collection("bussiness").document().set(bussiness)

    fun getBussiness(type: String) = getStoreRef().collection("bussiness").whereEqualTo("type", type).get()


    //InvitationCode


    fun isInvitationCodePresent(code: String) = getInvitationCodeeRef().document(code).get()

    fun isInvitationCodeDelete(code: String) = getInvitationCodeeRef().document(code).delete()

    fun storeInvitationCode(code: InvitationCode, invitationCode: String) = getInvitationCodeeRef().document(invitationCode).set(code)

    fun createGroup(group: Group) = getGroupRef().add(group)

    fun checkGroup(name: String) = getGroupRef().document(name).get()

    fun createGroupPost(gid: String, post: Post) = getGroupPostsRef().document(gid).collection("gPosts").add(post)

    fun getGroup(gid: String) = getGroupRef().document(gid).get()

    fun updateGroup(gid: String, group: Group) = getGroupRef().document(gid).set(group)

    fun getGroupPost(gid: String) = getGroupPostsRef().document(gid).collection("gPosts")
            .orderBy("date", Query.Direction.DESCENDING).get()

    fun followInterests(interests: PostInterests) = getStoreRef().collection("followers").add(interests)

    fun unfollowInterests(id: String) = getStoreRef().collection("followers").document(id).delete()

    fun checkfollow(type: String,uid: String)=getStoreRef().collection("followers").whereEqualTo("type",type).whereEqualTo("uid",uid).get()

    fun getfollowersInterest(uid: String)=getStoreRef().collection("followers").whereEqualTo("uid",uid).get()

    fun getfollowersUser(type: String)=getStoreRef().collection("followers").whereEqualTo("type",type).get()

    fun getInterestsPosts(type: String,zipCode: String)=getInterestPostRef().whereEqualTo("subType",type).whereEqualTo("zipCode",zipCode).get()

    fun createIntrestsPosts(post: Post)=getInterestPostRef().document().set(post)
}